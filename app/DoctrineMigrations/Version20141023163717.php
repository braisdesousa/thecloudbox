<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141023163717 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var Container
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('ALTER TABLE Task DROP FOREIGN KEY FK_F24C741B6C8B7086');
        $this->addSql('ALTER TABLE taskgroup_followers DROP FOREIGN KEY FK_84EB8A6695FD12D0');
        $this->addSql('ALTER TABLE taskgroup_member_groups DROP FOREIGN KEY FK_3EA7ECDA95FD12D0');
        $this->addSql('ALTER TABLE taskgroup_member_users DROP FOREIGN KEY FK_BFB2909E95FD12D0');
        $this->addSql('CREATE TABLE task_comment (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, task_id INT DEFAULT NULL, creation_date DATETIME NOT NULL, update_date DATETIME NOT NULL, deleted_date DATETIME DEFAULT NULL, comment LONGTEXT NOT NULL, INDEX IDX_8B957886A76ED395 (user_id), INDEX IDX_8B9578868DB60186 (task_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task_group (id INT AUTO_INCREMENT NOT NULL, workGroup_id INT DEFAULT NULL, creation_date DATETIME NOT NULL, update_date DATETIME NOT NULL, deleted_date DATETIME DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_AA645FE5166D1F9C (workGroup_id), UNIQUE INDEX taskgroup_idx (name, workGroup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taskgroup_users (taskgroup_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_3BF7355995FD12D0 (taskgroup_id), INDEX IDX_3BF73559A76ED395 (user_id), PRIMARY KEY(taskgroup_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taskgroup_groups (taskgroup_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_3B23FE6C95FD12D0 (taskgroup_id), INDEX IDX_3B23FE6CFE54D947 (group_id), PRIMARY KEY(taskgroup_id, group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taskTiming (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, task_id INT DEFAULT NULL, creation_date DATETIME NOT NULL, update_date DATETIME NOT NULL, deleted_date DATETIME DEFAULT NULL, description LONGTEXT DEFAULT NULL, date_start DATETIME NOT NULL, date_end DATETIME DEFAULT NULL, seconds BIGINT NOT NULL, INDEX IDX_59816461A76ED395 (user_id), INDEX IDX_598164618DB60186 (task_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task_activity (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, task_id INT DEFAULT NULL, creation_date DATETIME NOT NULL, update_date DATETIME NOT NULL, deleted_date DATETIME DEFAULT NULL, description LONGTEXT NOT NULL, action LONGTEXT NOT NULL, INDEX IDX_ECB4E316A76ED395 (user_id), INDEX IDX_ECB4E3168DB60186 (task_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE enterprise_users (enterprise_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_F62164A97D1AC3 (enterprise_id), INDEX IDX_F62164A76ED395 (user_id), PRIMARY KEY(enterprise_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE enterprise_admin_users (enterprise_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_152F1445A97D1AC3 (enterprise_id), INDEX IDX_152F1445A76ED395 (user_id), PRIMARY KEY(enterprise_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE task_comment ADD CONSTRAINT FK_8B957886A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE task_comment ADD CONSTRAINT FK_8B9578868DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE task_group ADD CONSTRAINT FK_AA645FE5166D1F9C FOREIGN KEY (workGroup_id) REFERENCES workGroup (id)');
        $this->addSql('ALTER TABLE taskgroup_users ADD CONSTRAINT FK_3BF7355995FD12D0 FOREIGN KEY (taskgroup_id) REFERENCES task_group (id)');
        $this->addSql('ALTER TABLE taskgroup_users ADD CONSTRAINT FK_3BF73559A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE taskgroup_groups ADD CONSTRAINT FK_3B23FE6C95FD12D0 FOREIGN KEY (taskgroup_id) REFERENCES task_group (id)');
        $this->addSql('ALTER TABLE taskgroup_groups ADD CONSTRAINT FK_3B23FE6CFE54D947 FOREIGN KEY (group_id) REFERENCES userGroup (id)');
        $this->addSql('ALTER TABLE taskTiming ADD CONSTRAINT FK_59816461A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE taskTiming ADD CONSTRAINT FK_598164618DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE task_activity ADD CONSTRAINT FK_ECB4E316A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE task_activity ADD CONSTRAINT FK_ECB4E3168DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE enterprise_users ADD CONSTRAINT FK_F62164A97D1AC3 FOREIGN KEY (enterprise_id) REFERENCES enterprise (id)');
        $this->addSql('ALTER TABLE enterprise_users ADD CONSTRAINT FK_F62164A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE enterprise_admin_users ADD CONSTRAINT FK_152F1445A97D1AC3 FOREIGN KEY (enterprise_id) REFERENCES enterprise (id)');
        $this->addSql('ALTER TABLE enterprise_admin_users ADD CONSTRAINT FK_152F1445A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');

        $this->addSql('DROP TABLE TaskActivity');
        $this->addSql('DROP TABLE TaskComments');
        $this->addSql('DROP TABLE TaskGroup');
        $this->addSql('DROP TABLE workGroup_member_groups');
        $this->addSql('DROP TABLE workGroup_member_users');
        $this->addSql('DROP TABLE subTasks_users');
        $this->addSql('DROP TABLE taskgroup_followers');
        $this->addSql('DROP TABLE taskgroup_member_groups');
        $this->addSql('DROP TABLE taskgroup_member_users');
        $this->addSql('ALTER TABLE Task ADD CONSTRAINT FK_527EDB256C8B7086 FOREIGN KEY (taskGroup_id) REFERENCES task_group (id)');
        $this->addSql('ALTER TABLE SubTask DROP FOREIGN KEY FK_E4443F0CFFB7D5FF');
        $this->addSql('DROP INDEX IDX_E4443F0CFFB7D5FF ON SubTask');
        $this->addSql('ALTER TABLE SubTask DROP description, DROP estimationTime, DROP creationUser_id');
        $this->addSql('ALTER TABLE WorkGroup DROP FOREIGN KEY FK_E00EE972CC283C73');
        $this->addSql('DROP INDEX IDX_E00EE972CC283C73 ON WorkGroup');
        $this->addSql('ALTER TABLE WorkGroup DROP adminUser_id');
        $this->addSql('ALTER TABLE User DROP FOREIGN KEY FK_2DA17977A97D1AC3');
        $this->addSql('DROP INDEX IDX_2DA17977A97D1AC3 ON User');
        $this->addSql('ALTER TABLE User DROP enterprise_id');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql('ALTER TABLE taskgroup_users DROP FOREIGN KEY FK_3BF7355995FD12D0');
        $this->addSql('ALTER TABLE taskgroup_groups DROP FOREIGN KEY FK_3B23FE6C95FD12D0');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB256C8B7086');
        $this->addSql('CREATE TABLE TaskActivity (id INT AUTO_INCREMENT NOT NULL, task_id INT DEFAULT NULL, user_id INT DEFAULT NULL, creation_date DATETIME NOT NULL, update_date DATETIME NOT NULL, deleted_date DATETIME DEFAULT NULL, description LONGTEXT NOT NULL, action LONGTEXT NOT NULL, INDEX IDX_269A7B63A76ED395 (user_id), INDEX IDX_269A7B638DB60186 (task_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE TaskComments (id INT AUTO_INCREMENT NOT NULL, task_id INT DEFAULT NULL, user_id INT DEFAULT NULL, creation_date DATETIME NOT NULL, update_date DATETIME NOT NULL, deleted_date DATETIME DEFAULT NULL, comment LONGTEXT NOT NULL, INDEX IDX_D570E413A76ED395 (user_id), INDEX IDX_D570E4138DB60186 (task_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE TaskGroup (id INT AUTO_INCREMENT NOT NULL, workGroup_id INT DEFAULT NULL, creation_date DATETIME NOT NULL, update_date DATETIME NOT NULL, deleted_date DATETIME DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, viewBy VARCHAR(255) NOT NULL, UNIQUE INDEX taskgroup_idx (name, workGroup_id), INDEX IDX_660D02166D1F9C (workGroup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE workGroup_member_groups (workGroup_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_2A2C45166D1F9C (workGroup_id), INDEX IDX_2A2C45FE54D947 (group_id), PRIMARY KEY(workGroup_id, group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE workGroup_member_users (workGroup_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_5C05D479166D1F9C (workGroup_id), INDEX IDX_5C05D479A76ED395 (user_id), PRIMARY KEY(workGroup_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subTasks_users (subtask_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_6413722CC6D4A949 (subtask_id), INDEX IDX_6413722CA76ED395 (user_id), PRIMARY KEY(subtask_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taskgroup_followers (taskgroup_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_84EB8A6695FD12D0 (taskgroup_id), INDEX IDX_84EB8A66A76ED395 (user_id), PRIMARY KEY(taskgroup_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taskgroup_member_groups (taskgroup_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_3EA7ECDA95FD12D0 (taskgroup_id), INDEX IDX_3EA7ECDAFE54D947 (group_id), PRIMARY KEY(taskgroup_id, group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taskgroup_member_users (taskgroup_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_BFB2909E95FD12D0 (taskgroup_id), INDEX IDX_BFB2909EA76ED395 (user_id), PRIMARY KEY(taskgroup_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE TaskActivity ADD CONSTRAINT FK_269A7B638DB60186 FOREIGN KEY (task_id) REFERENCES Task (id)');
        $this->addSql('ALTER TABLE TaskActivity ADD CONSTRAINT FK_269A7B63A76ED395 FOREIGN KEY (user_id) REFERENCES User (id)');
        $this->addSql('ALTER TABLE TaskComments ADD CONSTRAINT FK_D570E4138DB60186 FOREIGN KEY (task_id) REFERENCES Task (id)');
        $this->addSql('ALTER TABLE TaskComments ADD CONSTRAINT FK_D570E413A76ED395 FOREIGN KEY (user_id) REFERENCES User (id)');
        $this->addSql('ALTER TABLE TaskGroup ADD CONSTRAINT FK_660D02166D1F9C FOREIGN KEY (workGroup_id) REFERENCES WorkGroup (id)');
        $this->addSql('ALTER TABLE workGroup_member_groups ADD CONSTRAINT FK_2A2C45166D1F9C FOREIGN KEY (workGroup_id) REFERENCES WorkGroup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE workGroup_member_groups ADD CONSTRAINT FK_2A2C45FE54D947 FOREIGN KEY (group_id) REFERENCES userGroup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE workGroup_member_users ADD CONSTRAINT FK_5C05D479166D1F9C FOREIGN KEY (workGroup_id) REFERENCES WorkGroup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE workGroup_member_users ADD CONSTRAINT FK_5C05D479A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subTasks_users ADD CONSTRAINT FK_6413722CA76ED395 FOREIGN KEY (user_id) REFERENCES User (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subTasks_users ADD CONSTRAINT FK_6413722CC6D4A949 FOREIGN KEY (subtask_id) REFERENCES SubTask (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE taskgroup_followers ADD CONSTRAINT FK_84EB8A6695FD12D0 FOREIGN KEY (taskgroup_id) REFERENCES TaskGroup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE taskgroup_followers ADD CONSTRAINT FK_84EB8A66A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE taskgroup_member_groups ADD CONSTRAINT FK_3EA7ECDA95FD12D0 FOREIGN KEY (taskgroup_id) REFERENCES TaskGroup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE taskgroup_member_groups ADD CONSTRAINT FK_3EA7ECDAFE54D947 FOREIGN KEY (group_id) REFERENCES userGroup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE taskgroup_member_users ADD CONSTRAINT FK_BFB2909E95FD12D0 FOREIGN KEY (taskgroup_id) REFERENCES TaskGroup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE taskgroup_member_users ADD CONSTRAINT FK_BFB2909EA76ED395 FOREIGN KEY (user_id) REFERENCES User (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE task_comment');
        $this->addSql('DROP TABLE task_group');
        $this->addSql('DROP TABLE taskgroup_users');
        $this->addSql('DROP TABLE taskgroup_groups');
        $this->addSql('DROP TABLE taskTiming');
        $this->addSql('DROP TABLE task_activity');
        $this->addSql('DROP TABLE enterprise_users');
        $this->addSql('DROP TABLE enterprise_admin_users');
        $this->addSql('ALTER TABLE workGroup ADD adminUser_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workGroup ADD CONSTRAINT FK_E00EE972CC283C73 FOREIGN KEY (adminUser_id) REFERENCES User (id)');
        $this->addSql('CREATE INDEX IDX_E00EE972CC283C73 ON workGroup (adminUser_id)');
        $this->addSql('ALTER TABLE subtask ADD description LONGTEXT NOT NULL, ADD estimationTime INT NOT NULL, ADD creationUser_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subtask ADD CONSTRAINT FK_E4443F0CFFB7D5FF FOREIGN KEY (creationUser_id) REFERENCES User (id)');
        $this->addSql('CREATE INDEX IDX_E4443F0CFFB7D5FF ON subtask (creationUser_id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_F24C741B6C8B7086 FOREIGN KEY (taskGroup_id) REFERENCES TaskGroup (id)');
        $this->addSql('ALTER TABLE user ADD enterprise_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_2DA17977A97D1AC3 FOREIGN KEY (enterprise_id) REFERENCES Enterprise (id)');
        $this->addSql('CREATE INDEX IDX_2DA17977A97D1AC3 ON user (enterprise_id)');
    }

}
