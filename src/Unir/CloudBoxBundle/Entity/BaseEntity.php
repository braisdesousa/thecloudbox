<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/18/14
 * Time: 1:46 PM
 */

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\MappedSuperclass
 *
 */
class BaseEntity
{
    /**
     * @var \DateTime $created
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="create")
     * @Groups({"list","dashboard_list","wod_preview","workGroup_preview","userAll","enterprise_preview"})
     * @ORM\Column(name="creation_date", type="datetime")
     *
     */
    private $created;
    /**
     * @var \DateTime $updated
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="update")
     * @Groups({"list","wod_preview"})
     * @ORM\Column(name="update_date", type="datetime", nullable=false)
     */
    private $updated;
    /**
     * @var \DateTime $removed
     * @Assert\DateTime()
     * @Groups({"list","enterprise_preview","workGroup_preview"})
     * @ORM\Column(name="deleted_date", type="datetime", nullable=true)
     */
    private $deleted;


    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return $this
     */
    public function setCreated($date=null)
    {
        $this->created = new \DateTime($date?$date:"now");
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @return $this
     */
    public function setDeleted($date=null)
    {
        if (!$this->deleted) {
            $this->deleted=new \DateTime($date?$date:"now");
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @return $this
     */
    public function setUpdated($date=null)
    {
        $this->updated = new \DateTime($date?$date:"now");
        return $this;
    }
}
