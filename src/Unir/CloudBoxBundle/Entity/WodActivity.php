<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * WodActivity
 *
 * @ORM\Table(name="wod_activity")
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\WodActivityRepository")
 */
class WodActivity extends BaseEntity
{

    const UPDATE = "update";
    const OPENED = "open";
    const CLOSED = "closed";
    const RESOLVED = "resolved";
    const INPROGRESS = "in progress";
    const CRITICAL = "critical";
    const HIGH = "high";
    const LOW = "low";
    const MINOR = "minor";
    const NORMAL = "normal";





    /**
     * @var integer
     * @Groups({"list","dashboard_list","wod_preview"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     * @Groups({"list","userAll","dashboard_list","wod_preview"})
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $description;
    /**
     * @var
     * @Groups({"dashboard_list","userAll","wod_preview"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\User", inversedBy="wodActivities")
     * @Assert\NotNull()
     */
    private $user;
    /**
     * @var
     * @Groups({"dashboard_list","userAll"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\Wod", inversedBy="wodActivities")
     * @Assert\NotNull()
     */
    private $wod;
    /**
     * @var string
     * @Groups({"list","userAll","dashboard_list","wod_preview"})
     * @ORM\Column(name="action", type="text")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $action;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set description
     *
     * @param string $description
     * @return WodActivity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $wod
     */
    public function setWod($wod)
    {
        $this->wod = $wod;
    }

    /**
     * @return Wod
     */
    public function getWod()
    {
        return $this->wod;
    }
    public function getStatuses()
    {
        return [
            self::CLOSED,
            self::OPENED,
            self::INPROGRESS,
            self::RESOLVED,
        ];
    }
    public function getDifficulties()
    {
        return [
            self::HIGH,
            self::LOW,
            self::MINOR,
            self::NORMAL,
        ];
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }


}
