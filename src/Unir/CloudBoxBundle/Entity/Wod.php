<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;
use Unir\CloudBoxBundle\Entity\WodGroup;

/**
 * Wod
 *
 * @ORM\Table(name="wod")
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\WodRepository")
 */
class Wod extends BaseEntity
{
 
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * COSNTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    const  VIEW_BY_ASSIGNED = "assigned";

    /**
     *
     */
    const VIEW_BY_ASSIGNED_AND_FOLLOWERS = "assigned_and_followers";

    /**
     *
     */
    const VIEW_BY_ALL = "all";

    /**
     *
     */
    const STATUS_OPEN = "open";

    /**
     *
     */
    const STATUS_CLOSED = "closed";

    /**
     *
     */
    const STATUS_RESOLVED = "resolved";

    /**
     *
     */
    const STATUS_INPROGRESS = "in progress";


    /**
     *
     */
    const DIFFICULTY_CRITICAL = "critical";

    /**
     *
     */
    const DIFFICULTY_HIGH = "high";

    /**
     *
     */
    const DIFFICULTY_LOW = "low";

    /**
     *
     */
    const DIFFICULTY_MINOR = "minor";

    /**
     *
     */
    const DIFFICULTY_NORMAL = "normal";


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * @var integer
     * @Groups({"list","short","wod","dashboard_list","userAll","wodTiming_list","wod_preview","workGroup_preview","base_data","user_show"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Groups({"list","short","userAll","wod","dashboard_list","wod_preview"})
     * @ORM\Column(name="status", type="string", length=255)
     * @Assert\Choice(callback = "getStatuses", message = "Choose a valid status.")
     */
    private $status;

    /**
     * @var string
     * @Groups({"list","short","wod","wod_preview","user_show"})
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;

    /**
     * @var string
     * @Groups({"list","short","userAll","wod","dashboard_list","wodTiming_list","wod_preview","workGroup_preview","base_data","user_show"})
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(max=255,maxMessage="The title cannot be longer than {{ limit }} characters length")
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @Groups({"list","short","userAll","wod","dashboard_list","wodTiming_list","wod_preview","base_data"})
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     * @Groups({"list","short","userAll","wod","dashboard_list","wod_preview"})
     * @ORM\Column(name="difficulty", type="string", length=255)
     * @Assert\Choice(callback="getDifficulties", message = "Choose a valid difficulty.")
     */
    private $difficulty;

    /**
     * @var integer
     * @Groups({"list","short","userAll","wod","wod_preview"})
     * @ORM\Column(name="estimationTime", type="integer", nullable=true)
     * @Assert\Range(
     *      min=0,
     *      max=1000,
     *      minMessage="Estimation time should be {{ limit }} or more",
     *      maxMessage="Estimation time should be {{ limit }} or less"
     * )
     */
    private $estimationTime;

    /**
     * @var
     * @Groups({"list","short","dashboard_list","wod_preview"})
     *
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\WodGroup",inversedBy="wods")
     * @Assert\NotNull()
     */
    private $wodGroup;

    /**
     * @var
     * @Groups({"list","wod_preview"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\User", inversedBy="createdWods")
     */
    private $creationUser;

    /**
     * @var
     * @Groups({"list","dashboard_list","wod_preview"})
     *
     * @ORM\ManyToMany(targetEntity="Unir\CloudBoxBundle\Entity\User", inversedBy="assignedWods")
     * @ORM\JoinTable(name="wods_users")
     * @ORM\OrderBy({"username"="ASC"})
     *
     */
    private $usersAssigned;

    /**
     * @var
     * @Groups({"list","wod_preview"})
     *
     * @ORM\ManyToMany(targetEntity="Unir\CloudBoxBundle\Entity\User", inversedBy="followedWods")
     * @ORM\JoinTable(name="wods_followers")
     * @ORM\OrderBy({"username"= "ASC"})
     *
     */
    private $usersFollower;

    /**
     * @var
     * @Groups({"list","wod_preview"})
     *@ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\WodActivity", mappedBy="wod")
     */
    private $wodActivities;
    /**
     * @var
     * @Groups({"list","wod","wod_preview"})
     *@ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\SubWod", mappedBy="wod")
     */
    private $subWods;

    /**
     * @var
     * @Groups({"list","userAll","wod_preview"})
     * @ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\WodComments", mappedBy="wod", cascade="remove")
     */
    private $wodComments;

    /**
     * @Groups({"list", "single","short","wod","wod_preview"})
     * @ORM\Column(name="viewBy", type="string", length=255)
     */
    private $viewBy;



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * COSNTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     *
     */
    public function __construct()
    {
        $this->usersAssigned = new ArrayCollection();
        $this->usersFollower = new ArrayCollection();
        $this->subwods = new ArrayCollection();
        $this->subWods = new ArrayCollection();
        $this->viewBy = self::VIEW_BY_ALL;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATION
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }




    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Wod
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }



    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Wod
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }




    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Wod
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }




    /**
     * Get difficulty
     *
     * @return string
     */
    public function getDifficulty()
    {
        return $this->difficulty;
    }

    /**
     * Set difficulty
     *
     * @param string $difficulty
     * @return Wod
     */
    public function setDifficulty($difficulty)
    {
        $this->difficulty = $difficulty;
        return $this;
    }





    /**
     * Get estimationTime
     *
     * @return integer
     */
    public function getEstimationTime()
    {
        return $this->estimationTime;
    }

    /**
     * Set estimationTime
     *
     * @param integer $estimationTime
     * @return Wod
     */
    public function setEstimationTime($estimationTime)
    {
        $this->estimationTime = $estimationTime;
        return $this;
    }






    /**
     * @return WodGroup
     */
    public function getWodGroup()
    {
        return $this->wodGroup;
    }

    /**
     * @param $wodGroup
     * @return Wod
     */
    public function setWodGroup($wodGroup)
    {
        $this->wodGroup = $wodGroup;
        return $this;
    }





    /**
     * @return ArrayCollection
     */
    public function getUsersAssigned()
    {
        return $this->usersAssigned;
    }

    /**
     * @param $user
     * @return Wod
     */
    public function addUsersAssigned($user)
    {
        if (!$this->usersAssigned->contains($user)) {
            $this->usersAssigned->add($user);
        }
        return $this;
    }

    /**
     * @param User $user
     * @return Wod
     */
    public function removeUsersAssigned(User $user)
    {
        if ($this->usersAssigned->contains($user)) {
            $this->usersAssigned->removeElement($user);
        }
        return $this;
    }

    /**
     * @return Wod
     */
    public function clearUsersAssigned()
    {
        $this->usersAssigned->clear();
        return $this;
    }

    /**
     * @param mixed $usersAssigned
     * @return Wod
     */
    public function setUsersAssigned($usersAssigned)
    {
        $this->usersAssigned = $usersAssigned;
        return $this;
    }





    /**
     * @return ArrayCollection
     */
    public function getUsersFollower()
    {
        return $this->usersFollower;
    }

    /**
     * @param $user
     * @return Wod
     */
    public function addUsersFollower($user)
    {
        if (!$this->usersFollower->contains($user)) {
            $this->usersFollower->add($user);
        }
        return $this;
    }

    /**
     * @param User $user
     * @return Wod
     */
    public function removeUsersFollower(User $user)
    {
        if ($this->usersFollower->contains($user)) {
            $this->usersFollower->removeElement($user);
        }
        return $this;
    }

    /**
     * @return Wod
     */
    public function clearUsersFollower()
    {
        $this->usersFollower->clear();
        return $this;
    }

    /**
     * @param mixed $usersFollower
     * @return Wod
     */
    public function setUsersFollower($usersFollower)
    {
        $this->usersFollower = $usersFollower;
        return $this;
    }





    /**
     * @return User
     */
    public function getCreationUser()
    {
        return $this->creationUser;
    }

    /**
     * @param $creationUser
     * @return $this
     */
    public function setCreationUser($creationUser)
    {
        $this->creationUser = $creationUser;
        return $this;
    }




    /**
     * @return mixed
     */
    public function getWodActivities()
    {
        return $this->wodActivities;
    }

    /**
     * @param mixed $wodActivities
     * @return Wod
     */
    public function setWodActivities($wodActivities)
    {
        $this->wodActivities = $wodActivities;
        return $this;
    }




    /**
     * @return mixed
     */
    public function getWodComments()
    {
        return $this->wodComments;
    }

    /**
     * @param mixed $wodComments
     * @return Wod
     */
    public function setWodComments($wodComments)
    {
        $this->wodComments = $wodComments;
        return $this;
    }




    /**
     * @return array
     */
    public static function getViewOptions()
    {
        return [
            self::VIEW_BY_ALL,
            self::VIEW_BY_ASSIGNED,
            self::VIEW_BY_ASSIGNED_AND_FOLLOWERS
        ];
    }

    /**
     * @return string
     */
    public function getAvailableViewers()
    {
        return $this->viewBy
            ? $this->viewBy
            : self::VIEW_BY_ALL;
    }
    public function getViewBy()
    {
        $this->getAvailableViewers();
    }

    /**
     * @param $aviableViewers
     * @return Wod
     */
    public function setAvailableViewers($aviableViewers)
    {
        if (in_array($aviableViewers, self::getViewOptions())) {
            $this->viewBy = $aviableViewers;
        }
        return $this;
    }
    public function setViewBy($aviableViewers)
    {
        $this->setAvailableViewers($aviableViewers);
    }

    /**
     * @return mixed
     */
    public function getSubWods()
    {
        return $this->subWods;
    }

    /**
     * @param mixed $subWods
     */
    public function setSubWods($subWods)
    {
        $this->subWods = $subWods;
    }

    public function addSubWod(SubWod $wod){

        if (!$this->subWods->contains($wod)){
            $this->subWods->add($wod);
        }
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * METHODS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param User $user
     * @return bool
     */
    public function isUserAssigned(User $user)
    {
        if (($user == $this->creationUser) || $this->usersAssigned->contains($user)) {
            return true;
        }
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * STATIC PROPERTIES
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * @return array
     */
    public static function getDifficulties()
    {
        return [
            strtolower(self::DIFFICULTY_LOW),
            strtolower(self::DIFFICULTY_MINOR),
            strtolower(self::DIFFICULTY_NORMAL),
            strtolower(self::DIFFICULTY_HIGH),
            strtolower(self::DIFFICULTY_CRITICAL),
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            strtolower(self::STATUS_OPEN),
            strtolower(self::STATUS_INPROGRESS),
            strtolower(self::STATUS_RESOLVED),
            strtolower(self::STATUS_CLOSED),
        ];
    }
}
