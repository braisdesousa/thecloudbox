<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * WodComments
 * @ORM\Table(name="wod_comment")
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\WodCommentsRepository")
 */
class WodComments extends BaseEntity
{

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var integer
     * @Groups({"list","dashboard_list","userAll","wod_preview"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Groups({"list","dashboard_list","userAll","wod_preview"})
     *
     * @ORM\Column(name="comment", type="text")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $comment;

    /**
     * @var
     * @Groups({"dashboard_list","wod_preview"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\User",inversedBy="wodComments")
     * @Assert\NotNull()
     */
    private $user;

    /**
     * @var
     * @Groups({"dashboard_list","userAll"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\Wod",inversedBy="wodComments")
     * @Assert\NotNull()
     */
    private $wod;
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * COSNTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {

    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PROPERTIES
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }





    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return WodComments
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }




    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getWod()
    {
        return $this->wod;
    }

    /**
     * Set wod
     *
     * @param $wod
     * @return $this
     */
    public function setWod($wod)
    {
        $this->wod = $wod;
        return $this;
    }
}
