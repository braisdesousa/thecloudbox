<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;
use Unir\CloudBoxBundle\Entity\WodGroup;

/**
 * Wod
 *
 * @ORM\Table(name="subwod")
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\WodRepository")
 */
class SubWod extends BaseEntity
{
 
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * COSNTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */


    const STATUS_OPEN = "open";

    const STATUS_CLOSED = "closed";

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * @var integer
     * @Groups({"list","short","userAll","wod","wod_preview","dashboard_list","wodTiming_list"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Groups({"list","short","userAll","wod","dashboard_list","wod_preview"})
     * @ORM\Column(name="status", type="string", length=255)
     * @Assert\Choice(callback = "getStatuses", message = "Choose a valid status.")
     */
    private $status;


    /**
     * @var string
     * @Groups({"list","short","userAll","wod","dashboard_list","wodTiming_list","wod_preview"})
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(max=255,maxMessage="The title cannot be longer than {{ limit }} characters length")
     */
    private $title;

    /**
     * @var
     * @Groups({"list","short","dashboard_list"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\Wod",inversedBy="subWods")
     * @Assert\NotNull()
     */
    private $wod;

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * COSNTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */
    public function __construct()
    {

        $this->status = self::STATUS_OPEN;
    }
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getWod()
    {
        return $this->wod;
    }

    /**
     * @param mixed $wod
     */
    public function setWod($wod)
    {
        $this->wod = $wod;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


}
