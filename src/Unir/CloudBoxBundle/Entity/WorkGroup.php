<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation\Groups;
use Unir\CloudBoxBundle\Entity\Enterprise;

/**
 * WorkGroup
 *
 * @ORM\Table(name="workGroup")
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\WorkGroupRepository")
 */
class WorkGroup extends BaseEntity
{

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var integer
     * @Groups({"userAll", "list","short","userAll","findWorkGroupById", "idname","base_data","enterprise_preview","workGroup_preview","wodgroup_preview","workGroup_user_info"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Groups({"userAll", "list","short","userAll","findWorkGroupById", "idname","base_data","enterprise_preview","workGroup_preview","wodgroup_preview","workGroup_user_info"})
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(max="50",maxMessage = "The name cannot be longer than {{ limit }} characters length, try to simplify")
     */
    private $name;

    /**
     * @var string
     * @Groups({"list","short","findWorkGroupById","workGroup_preview"})
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\Length(max="255",maxMessage = "The description cannot be longer than {{ limit }} characters length")
     */
    private $description;

    /**
     * @var
     * @Groups({"list","short","workGroup_preview"})
     *
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\Enterprise", inversedBy="workGroups")
     * @Assert\NotNull()
     */
    private $enterprise;

    /**
     * @Groups({"list","workGroup_preview"})
     * @var
     * @ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\WodGroup", mappedBy="workGroup", cascade="remove")
     */
    private $wodGroups;

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * COSNTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {
        $this->wodGroups = new ArrayCollection();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PROPERTIES
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }



    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return WorkGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }




    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return WorkGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
    /**
     * @return Enterprise
     */
    public function getEnterprise()
    {
        return $this->enterprise;
    }

    /**
     * @param mixed $enterprise
     */
    public function setEnterprise($enterprise)
    {
        $this->enterprise = $enterprise;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWodGroups()
    {
        return $this->wodGroups;
    }

    /**
     * @param $wodGroups
     * @return $this
     */
    public function setWodGroups($wodGroups)
    {
        $this->wodGroups = $wodGroups;
        return $this;
    }
    public function addWodGroup(WodGroup $wodGroup)
    {
        if(!$this->wodGroups->contains($wodGroup))
            $this->wodGroups->add($wodGroup);
    }

}