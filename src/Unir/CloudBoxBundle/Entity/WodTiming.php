<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * WodActivity
 *
 * @ORM\Table(name="wodTiming")
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\WodTimingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class WodTiming extends BaseEntity
{
    /**
     * @var integer
     * @Groups({"wodTiming_list","base_data","userAll"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Groups({"wodTiming_list","base_data","userAll"})
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $comment;
    /**
     * @var
     * @Groups({"wodTiming_list","base_data"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\User")
     * @Assert\NotNull()
     */
    private $user;
    /**
     * @var
     * @Groups({"wodTiming_list","base_data","userAll"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\Wod")
     * @Assert\NotNull()
     */
    private $wod;
    /**
     * @var \DateTime $created
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="create")
     * @Groups({"wodTiming_list","base_data","userAll"})
     * @ORM\Column(name="date_start", type="datetime")
     *
     */
    private $date_start;
    /**
     * @var \DateTime $created
     * @Assert\DateTime()
     * @Groups({"wodTiming_list","base_data","userAll"})
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     *
     */
    private $date_end;
    /**
     * @ORM\Column(name="seconds", type="bigint")
     * @Groups({"wodTiming_list","base_data","userAll"})
     */
    private $seconds;
    /**
     * Get id
     *
     * @return integer 
     */
    public function __construct(){
        $this->seconds=0;
    }
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getWod()
    {
        return $this->wod;
    }

    /**
     * @param mixed $wod
     */
    public function setWod($wod)
    {
        $this->wod = $wod;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * @param \DateTime $date_end
     */
    public function forceClose()
    {
        if (!$this->date_end) {
            $this->date_end= new \DateTime();
        }
        $this->setSeconds();
    }

    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;
        $this->setSeconds();
    }

    /**
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @param \DateTime $date_start
     */
    public function setDateStart($date_start)
    {
        $this->date_start = $date_start;
    }

    /**
     * @return mixed
     */
    public function getSeconds()
    {
        return $this->seconds;
    }

    /**
     * @param mixed $seconds
     * @ORM\PrePersist
     */
    public function setSeconds()
    {
        if ($this->date_start && $this->date_end){
            $this->seconds = $this->date_end->getTimestamp() - $this->date_start->getTimestamp();
        }
    }



}
