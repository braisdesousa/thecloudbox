<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Unir\CloudBoxBundle\Entity\WorkGroup;

/**
 * WodGroup
 *
 * @ORM\Table(name="wod_group",uniqueConstraints={@ORM\UniqueConstraint(name="wodgroup_idx", columns={"name", "workGroup_id"})})
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\WodGroupRepository")
 * @UniqueEntity(
 *      fields={"name","workGroup"},
 *      errorPath="name",
 *      message="Wodgroup already exists with given name in this workGroup"
 * )
 */
class WodGroup extends BaseEntity
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var integer
     * @Groups({"userAll","short", "list","findWodgroup","wodgroup_preview","dashboard_list","wod_preview","workGroup_preview","base_data"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Groups({"userAll", "list","short","findWodgroup","wodgroup_preview","dashboard_list","wod_preview","workGroup_preview","base_data"})
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(max="255",maxMessage = "The name cannot be longer than {{ limit }} characters length")
     */
    private $name;

    /**
     * @var string
     * @Groups({"list","short","base_data","wodgroup_preview"})
     * @ORM\Column(name="description", type="string", length=255, nullable=true)     *
     * @Assert\Length(max="255",maxMessage = "The description cannot be longer than {{ limit }} characters length")
     */
    private $description;

    /**
     *@Groups({"list","wodgroup_preview"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\WorkGroup",inversedBy="wodGroups")
     * @Assert\NotNull()
     */
    private $workGroup;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\Wod", mappedBy="wodGroup", cascade="remove")
     * @Groups({"list","workGroup_preview"})
     */
    private $wods;

    /**
     * @Groups({"list","short","findWodgroup","wodgroup_preview"})
     * @ORM\ManyToMany(targetEntity="Unir\CloudBoxBundle\Entity\User")
     * @ORM\JoinTable(name="wodgroup_users",
     *      joinColumns={@ORM\JoinColumn(name="wodgroup_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     **/
    private $users;

    /**
     * @Groups({"list","short","findWodgroup","wodgroup_preview"})
     * @ORM\ManyToMany(targetEntity="Unir\CloudBoxBundle\Entity\Group")
     * @ORM\JoinTable(name="wodgroup_groups",
     *      joinColumns={@ORM\JoinColumn(name="wodgroup_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     **/
    private $groupMembers;

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * 
     */
    public function __construct()
    {
        $this->wods = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->groupMembers = new ArrayCollection();
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PROPERTIES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Sets the id
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return WodGroup
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return WodGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    /**
     * @return WorkGroup
     */
    public function getWorkGroup()
    {
        return $this->workGroup;
    }

    /**
     * @param $workGroup
     * @return $this
     */
    public function setWorkGroup($workGroup)
    {
        $this->workGroup = $workGroup;
        return $this;
    }
    /**
     * @return ArrayCollection
     */
    public function getWods()
    {
        return $this->wods;
    }

    /**
     * @param mixed $wods
     * @return WodGroup
     */
    public function setWods($wods)
    {
        $this->wods = $wods;
        return $this;
    }
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getGroupMembers()
    {
        return $this->groupMembers;
    }

    /**
     * @param Group $group
     * @return WodGroup
     */
    public function addGroupMember(Group $group)
    {
        if (!$this->groupMembers->contains($group)) {
            $this->groupMembers->add($group);
        }
        return $this;
    }

    /**
     * @param Group $group
     * @return WodGroup
     */
    public function removeGroupMember(Group $group)
    {
        if ($this->groupMembers->contains($group)) {
            $this->groupMembers->removeElement($group);
        }
        return $this;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $groupMembers
     * @return WodGroup
     */
    public function setGroupMembers($groupMembers)
    {
        $this->groupMembers = $groupMembers;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User $user
     * @return WodGroup
     */
    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }
    }

    /**
     * @param User $user
     * @return WodGroup
     */
    public function removeUser(User $user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $users
     * @return WodGroup
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }
    public function hasUser(User $user){
        return ($this->users->contains($user)||$this->isUserFromGroup($user));
    }

    public function isUserFromGroup(User $user)
    {
        foreach($this->groupMembers as $groups) {
            if($groups->getUsers()->contains($user)){
                return true;
            }
        }
        return false;
    }
}
