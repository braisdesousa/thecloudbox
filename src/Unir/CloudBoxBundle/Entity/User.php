<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use FOS\UserBundle\Model\GroupInterface;
use Unir\CloudBoxBundle\Model\User as BaseUser;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\Enterprise;

/**
 *
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="Ya existe un usario registrado con este email",groups={"default","owner"})
 * @UniqueEntity(
 *      fields={"username"},
 *      message="Ya existe un usario registrado con este nombre",
 *      groups={"default","owner"}
 * )
 */
class User extends BaseUser
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var integer
     * @Groups({"userAll", "list","short","findWodgroup","idnamemail","findWorkGroupById", "idname","dashboard_list","base_data","wodTiming_list","wod_preview","enterprise_preview","workGroup_preview","wodgroup_preview","workGroup_user_info","enterprise_user_info"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * * @Groups({"userAll", "list","short","findWodgroup","idnamemail","findWorkGroupById", "idname","dashboard_list","base_data","wodTiming_list","wod_preview","enterprise_preview","workGroup_preview","wodgroup_preview","workGroup_user_info"})
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;
    /**
     * @Groups({"userAll"})
     * @var
     * @ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\Wod", mappedBy="creationUser")
     */
    protected $createdWods;

    /**
     * @var
     * @Groups({"userAll"})
     *
     * @ORM\ManyToMany(targetEntity="Unir\CloudBoxBundle\Entity\Wod", mappedBy="usersAssigned")
     */
    protected $assignedWods;

    /**
     * @Groups({"userAll"})
     * @ORM\ManyToMany(targetEntity="Unir\CloudBoxBundle\Entity\Wod", mappedBy="usersFollower")
     */
    protected $followedWods;

    /**
     * @ORM\Column(name="username", type="string", length=255)
     * @Groups({"userAll", "list","short","findWodgroup","idnamemail","findWorkGroupById", "idname","dashboard_list","base_data","wodTiming_list","wod_preview","enterprise_preview","workGroup_preview","wodgroup_preview","workGroup_user_info","enterprise_user_info"})
     */
    protected $username;

    /**
     * @Groups({"userAll","idnamemail"})
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\NotBlank(groups={"default","owner",})
     * @Assert\Email(groups={"default","owner"})
     */
    protected $email;

    /**
     * @var
     * @Groups({"userAll"})
     *
     * @ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\WodActivity", mappedBy="user")
     */
    protected $wodActivities;

    /**
     * @var
     * @Groups({"userAll"})
     *
     * @ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\WodComments", mappedBy="user")
     */
    protected $wodComments;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * * @Groups({"base_data"})
     * @ORM\JoinTable(name="users_roles")
     **/
    protected $roles;

    /**
     * @Groups({"userAll"})
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="users_groups")
     **/
    protected $groups;

    /**
     * @ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\LoginLog", mappedBy="user")
     */
    protected $logins;
    /**
     * @Groups({"userAll"})
     */
    protected $wodTiming;
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->assignedWods = new ArrayCollection();
        $this->followedWods = new ArrayCollection();
        $this->wodComments = new ArrayCollection();
        $this->createdWods = new ArrayCollection();
        $this->wodActivities = new ArrayCollection();
        $this->logins = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->wodTiming= new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getAssignedWods()
    {
        return $this->assignedWods;
    }

    /**
     * @param $assignedWods
     * @return User
     */
    public function setAssignedWods($assignedWods)
    {
        $this->assignedWods = $assignedWods;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getFollowedWods()
    {
        return $this->followedWods;
    }

    /**
     * @param $followedWods
     * @return $this
     */
    public function setFollowedWods($followedWods)
    {
        $this->followedWods = $followedWods;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getCreatedWods()
    {
        return $this->createdWods;
    }

    /**
     * @param $createdWods
     * @return $this
     */
    public function setCreatedWods($createdWods)
    {
        $this->createdWods = $createdWods;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getWodActivities()
    {
        return $this->wodActivities;
    }

    /**
     * @param $wodActivities
     * @return $this
     */
    public function setWodActivities($wodActivities)
    {
        $this->wodActivities = $wodActivities;
        return $this;
    }




    /**
     * @return mixed
     */
    public function getWodComments()
    {
        return $this->wodComments;
    }

    /**
     * @param $wodComments
     * @return $this
     */
    public function setWodComments($wodComments)
    {
        $this->wodComments = $wodComments;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getLogins()
    {
        return $this->logins;
    }

    /**
     * @param $logins
     * @return $this
     */
    public function setLogins($logins)
    {
        $this->logins = $logins;
        return $this;
    }



    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        if ($this->roles->isEmpty()) {
            return [self::ROLE_DEFAULT];
        } else {
            $roleNames = [];
            foreach ($this->roles as $role) {
                $roleNames[] = $role->getRole();
            }
            return $roleNames;
        }
    }

    /**
     * Never use this to check if this user has access to anything!
     *
     * Use the SecurityContext, or an implementation of AccessDecisionManager
     * instead, e.g.
     *
     *         $securityContext->isGranted('ROLE_USER');
     *
     * @param string $role
     *
     * @return boolean
     */
    public function hasRole($role)
    {
        foreach ($this->roles as $role_persisted) {
            if ($role_persisted->getRole() == $role) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a role to the user.
     *
     * @param Role $role
     *
     * @return User
     */
    public function addRole($role)
    {
        if (!($role instanceof Role)) {
            return;
        }

        if (!$this->hasRole($role->getRole()) && $role->getRole() != self::ROLE_DEFAULT) {
            $this->roles->add($role);
        }
        return $this;
    }

    /**
     * Removes a role to the user.
     *
     * @param string $role
     *
     * @return User
     */
    public function removeRole($role)
    {
        foreach ($this->roles as $role_persisted) {
            if ($role_persisted->getRole()==$role) {
                $this->roles->removeElement($role_persisted);
            }
        }
        return $this;
    }

    /**
     * Tells if the the given user has the super admin role.
     *
     * @return boolean
     */
    public function isSuperAdmin()
    {
        foreach ($this->roles as $role) {
            if ($role->getRole() == self::ROLE_SUPER_ADMIN) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sets the super admin status
     *
     * @param boolean $boolean
     *
     * @return self
     */
    public function setSuperAdmin($boolean)
    {
        throw new \Exception("Method setSuperAdmin is no Callable");
    }

    /**
     * Sets the roles of the user.
     *
     * This overwrites any previous roles.
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles(array $roles)
    {
        $this->roles = new ArrayCollection($roles);
        return $this;
    }

    /**
     *
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * adds the user to a group
     * @param Group $group
     * @return $this
     */
    public function addToGroup(Group $group)
    {
        if (!$this->groups->contains($group)) {
            $this->groups->add($group);
        }

        return $this;
    }

    /**
     * Removes a user from a group
     * @param Group $group
     * @return $this
     */
    public function removeFromGroup(Group $group)
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
        }

        return $this;
    }

    /**
     * @param $groups
     * @return User
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWodTiming()
    {
        return $this->wodTiming;
    }

    /**
     * @param mixed $wodTiming
     */
    public function setWodTiming($wodTiming)
    {
        $this->wodTiming = $wodTiming;
    }



    public function addGroup(GroupInterface $group)
    {
        if(!$this->groups->contains($group)) {
            $this->groups->add($group);
        }
        return $this;
    }
    public function removeGroup(GroupInterface $group)
    {
        if($this->groups->contains($group)) {
            $this->groups->removeElement($group);
        }
        return $this;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * METHODS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @return bool
     */
    public function hasActivity()
    {
        return
            (!($this->wodActivities->isEmpty() &&
                $this->createdWods->isEmpty() &&
                $this->wodComments->isEmpty() &&
                $this->followedWods->isEmpty() &&
                $this->assignedWods->isEmpty()));
    }
}
