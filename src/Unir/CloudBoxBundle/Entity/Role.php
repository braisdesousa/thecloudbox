<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/29/14
 * Time: 9:55 AM
 */

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="role")
 * @ORM\Entity()
 * @UniqueEntity("role", message="No se Puede Duplicar el Role")
 */

class Role implements RoleInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Admin ROLE
     */
    const ROLE_ADMIN = "ROLE_ADMIN";

    /**
     * Owner Role text
     */
    const ROLE_OWNER = "ROLE_OWNER";

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="role", type="string", length=255,unique=true)
     * @Groups({"base_data"})
     *
     */
    protected $role;

    /**
      *@ORM\ManyToMany(targetEntity="User", mappedBy="roles")
    */
    protected $users;

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * @param null|string $role_name
     */
    public function __construct($role_name = null)
    {
        $this->role = $role_name;
        $this->users = new ArrayCollection();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PROPERTIES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @param $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }


    /**
     * @param $users
     * @return $this
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
