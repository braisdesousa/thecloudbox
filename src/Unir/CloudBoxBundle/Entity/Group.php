<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 8/8/14
 * Time: 12:18 PM
 */

namespace Unir\CloudBoxBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\GroupInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="userGroup")
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\UserGroupRepository")
 * @UniqueEntity(
 *      fields={"enterprise","name"},
 *      errorPath="name",
 *      message="This Group Already exists for this Enterprise"
 * )
 */
class Group extends BaseEntity implements GroupInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var integer
     * @Groups({"userAll", "list","short","findWodgroup","findWorkGroupById", "idname","enterprise_preview","workGroup_preview","wodgroup_preview","workGroup_user_info","enterprise_user_info"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Groups({"list","short", "idname","enterprise_preview","workGroup_preview","workGroup_user_info"})
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groups",cascade={"persist"})
     */
    protected $users;

    /**
     * @var
     * @Groups({"list","short"})
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\Enterprise",inversedBy="groups")
     * @ORM\JoinColumn(name="enterprise_id", referencedColumnName="id", nullable=true)
     * @Assert\NotNull()
     */
    protected $enterprise;

    /**
     * @Groups({"userAll","idname", "list","short","findWodgroup","findWorkGroupById","enterprise_preview","workGroup_preview","wodgroup_preview","workGroup_user_info","enterprise_user_info"})
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $name;



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * 
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PROPERTIES
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }




    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User $user
     * @return Group
     */
    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }
        return $this;
    }
    /**
     * @param User $user
     * @return Group
     */
    public function removeUser(User $user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }
        return $this;
    }

    /**
     * @param mixed $users
     * @return Group
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }



    /**
     * @return Enterprise
     */
    public function getEnterprise()
    {
        return $this->enterprise;
    }

    /**
     * @param $enterprise
     * @return Group
     */
    public function setEnterprise($enterprise)
    {
        $this->enterprise = $enterprise;
        return $this;
    }

    /**
     * @param string $role
     *
     * @return self
     */
    public function addRole($role)
    {
        // TODO: Implement addRole() method.
    }

    /**
     * @param string $role
     *
     * @return boolean
     */
    public function hasRole($role)
    {
        // TODO: Implement hasRole() method.
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        // TODO: Implement getRoles() method.
    }

    /**
     * @param string $role
     *
     * @return self
     */
    public function removeRole($role)
    {
        // TODO: Implement removeRole() method.
    }

    /**
     * @param array $roles
     *
     * @return self
     */
    public function setRoles(array $roles)
    {
        // TODO: Implement setRoles() method.
    }

}
