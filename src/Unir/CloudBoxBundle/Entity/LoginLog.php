<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LoginLog
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class LoginLog
{

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime")
     *
     * @ A s sert\Datetime()
     */
    private $creationDate;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Unir\CloudBoxBundle\Entity\User",inversedBy="logins")
     * @Assert\NotNull()
     */
    private $user;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * COSNTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     *
     */
    public function __construct()
    {
        $this->creationDate = new \DateTime();
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PROPERTIES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return LoginLog
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }




    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return LoginLog
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }





    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
