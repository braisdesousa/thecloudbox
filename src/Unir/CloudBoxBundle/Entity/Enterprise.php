<?php

namespace Unir\CloudBoxBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Group;
use JMS\Serializer\Annotation\Groups;

/**
 * Enterprise
 *
 * @ORM\Table(name="enterprise")
 * @ORM\Entity(repositoryClass="Unir\CloudBoxBundle\Repository\EnterpriseRepository")
 */
class Enterprise extends BaseEntity
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @Groups({"base_data","userAll","enterprise_preview","enterprise_user_info"})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Groups({"base_data","userAll","enterprise_preview","enterprise_user_info"})
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(max="255",maxMessage = "The name cannot be longer than {{ limit }} characters length")
     */
    private $name;

    /**
     * @var
     * @Groups({"enterprise_preview"})
     * @ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\WorkGroup", mappedBy="enterprise")
     */
    private $workGroups;
    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="Unir\CloudBoxBundle\Entity\User")
     * @ORM\JoinTable(name="enterprise_users",
     *      joinColumns={@ORM\JoinColumn(name="enterprise_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")})
     * @Groups({"enterprise_preview","enterprise_user_info"})
     * @ORM\OrderBy({"username"="ASC"})
     */
    private $users;
    /**
     * @var
     * @ORM\ManyToMany(targetEntity="Unir\CloudBoxBundle\Entity\User")
     * @ORM\JoinTable(name="enterprise_admin_users",
     *      joinColumns={@ORM\JoinColumn(name="enterprise_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")})
     * @Groups({"enterprise_preview","enterprise_user_info"})
     * @ORM\OrderBy({"username"="ASC"})
     */
    private $adminUsers;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Unir\CloudBoxBundle\Entity\Group", mappedBy="enterprise")
     * @ORM\OrderBy({"name"="ASC"})
     * @Groups({"enterprise_preview","enterprise_user_info","idname"})
     */
    private $groups;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {

        $this->users = new ArrayCollection();
        $this->workGroups = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->adminUsers = new ArrayCollection();
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PROPERTIES
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * Set the id
     *
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }



    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Enterprise
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }




    /**
     * @return ArrayCollection
     */
    public function getWorkGroups()
    {
        return $this->workGroups;
    }

    /**
     * Adds a workGroup to the enterprise
     * @param WorkGroup $workGroup
     * @return $this
     */
    public function addWorkGroup(WorkGroup $workGroup)
    {
        if (!$this->workGroups->contains($workGroup)) {
            $this->workGroups->add($workGroup);
        }
        return $this;
    }

    /**
     * Removes a workGroup from the enterprise
     * @param WorkGroup $workGroup
     * @return $this
     */
    public function removeWorkGroup(WorkGroup $workGroup)
    {
        if ($this->workGroups->contains($workGroup)) {
            $this->workGroups->removeElement($workGroup);
        }

        return $this;
    }

    /**
     * @param mixed $workGroups
     */
    public function setWorkGroups($workGroups)
    {
        $this->workGroups = $workGroups;
    }



    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Adds an user to the enterprise
     * @param User $user
     * @return Enterprise
     */
    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }

        return $this;
    }

    /**
     * Removes an user from the enterprise
     * @param User $user
     * @return $this
     */
    public function removeUser(User $user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }


    public function isAdminUser(User $user){
        return $this->adminUsers->contains($user);
    }
    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param Group $group
     * @return $this
     * @return $this
     */
    public function addGroup(Group $group)
    {
        if (!$this->groups->contains($group)) {
            $this->groups->add($group);
        }
        return $this;
    }

    /**
     * @param Group $group
     */
    public function removeGroup(Group $group)
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
        }
    }
    public function removeUserFromGroups(User $user)
    {
        foreach($this->getGroups() as $group){
            $group->removeUser($user);
        }
    }
    /**
     * @param mixed $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }

    /**
     * @return ArrayCollection
     */
    
    public function getAdminUsers()
    {
        return $this->adminUsers;
    }

    /**
     * @param AdminUser $adminUser
     * @return $this
     */
    public function addAdminUser(User $adminUser)
    {
        if (!$this->adminUsers->contains($adminUser)) {
            $this->adminUsers->add($adminUser);
        }
        return $this;
    }

    /**
     * @param AdminUser $adminUser
     */
    public function removeAdminUser(User $adminUser)
    {
        if ($this->adminUsers->contains($adminUser)) {
            $this->adminUsers->removeElement($adminUser);
        }
    }

    /**
     * @param mixed $adminUsers
     */
    public function setAdminUsers($adminUsers)
    {
        $this->adminUsers = $adminUsers;
    }
    public function getAllUsers()
    {
        $AllUsers=new ArrayCollection($this->getUsers()->toArray());
        foreach ($this->getGroups() as $group){
            foreach ($group->getUsers() as $user) {
                if(!$AllUsers->contains($user)){
                    $AllUsers->add($user);
                }
            }
        }
        return $AllUsers;
    }
}
