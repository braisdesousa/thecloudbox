<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 8/1/14
 * Time: 8:20 AM
 */

namespace Unir\CloudBoxBundle\Security\Firewall;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Http\Firewall\ExceptionListener as BaseExceptionListener;


class ExceptionListener extends BaseExceptionListener
{
    protected function setTargetPath(Request $request)
    {
        // Return 403 if is Ajax Request.
        if ($request->isXmlHttpRequest() || 'GET' !== $request->getMethod()) {
            throw new HttpException(403,"User UnAuthenticated on Ajax Request");
        }

        parent::setTargetPath($request);
    }
}