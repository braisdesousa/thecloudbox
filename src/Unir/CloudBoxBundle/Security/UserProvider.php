<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/29/14
 * Time: 10:55 AM
 */

namespace Unir\CloudBoxBundle\Security;

use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface as SecurityUserInterface;
use \FOS\UserBundle\Security\UserProvider as BaseUserProvider;
use Unir\CloudBoxBundle\Entity\User;

class UserProvider extends BaseUserProvider  {

    private $myUserManager;
    public function __construct(UserManagerInterface $userManager)
    {
        $this->myUserManager=$userManager;
        parent::__construct($userManager);
    }
    public function refreshUser(SecurityUserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Expected an instance of FOS\UserBundle\Model\User, but got "%s".', get_class($user)));
        }

        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Expected an instance of %s, but got "%s".', $this->userManager->getClass(), get_class($user)));
        }

        if (null === $reloadedUser = $this->userManager->findUserBy(array('id' => $user->getId()))) {
            throw new UsernameNotFoundException(sprintf('User with ID "%d" could not be reloaded.', $user->getId()));
        }

        return $reloadedUser;
    }
    public function supportsClass($class)
    {
        return $class=='Unir\CloudBoxBundle\Entity\User';
    }
} 