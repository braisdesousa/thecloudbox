<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 26/08/14
 * Time: 11:29
 */

namespace Unir\CloudBoxBundle\Helper;

/**
 * Class CollectionHelper
 * @package Unir\CloudBoxBundle\Helper
 */
class CollectionHelper
{
    /**
     * Transforms a collection into an idArray
     * @param $collection
     * @return array
     */
    public static function toIdArray($collection)
    {
        $result = [];

        foreach ($collection as $item) {
            $result[] = $item->getId();
        }

        return $result;
    }

    /**
     * Reduce matrix to first elements into an array
     * @param array $items
     * @return array
     */
    public static function reduceIdMatrix(array $items)
    {
        $result = [];
        foreach ($items as $itm) {
            $result[] = $itm['id'];
        }

        return $result;
    }
}
