(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.directive('myTableWods', ['$log', 'jQuery', '$compile', '$timeout',
        'notificationService', 'helperService', tableWodsPopoverDirective]);

    /**
     * Directive for show popover on wods
     * @param $log
     * @param jQuery
     * @param $compile
     * @param notificationService
     * @returns {{restrict: string, templateUrl: string, scope: {wods: string, parentScope: string}, controller: Function, link: Function}}
     */
    function tableWodsPopoverDirective($log, jQuery, $compile, $timeout, notificationService, helperService) {


        //Controller function
        var controllerFn = function($scope, $rootScope, $location, $log, routing, wodsService, workGroupService) {

            //add loot_usersAssigned to wod
            var getFilteredUsersAssigned = function(wod) {

                if (!wod || !wod.users_assigned || wod.users_assigned == 0) {
                    return;
                }

                if (wod.loop_usersAssigned) {
                    return;
                }

                if(wod.users_assigned.length > 3) {

                    //Retrieve last 3
                    var result = wod.users_assigned.slice(0,3);
                    //add to head
                    result.push({"id":0,"username":"..."});

                    //return
                    wod.loop_usersAssigned = result;
                } else { //if dont have more than 3, just response
                    wod.loop_usersAssigned = wod.users_assigned;
                }
            };

            //Switchs a wod for another one
            var switchWod = function(newWod) {
                for(var i = 0; i < $scope.wods.length; i++) {
                    if($scope.wods[i].slug == newWod.slug) {
                        $scope.wods[i] = newWod;
                        getFilteredUsersAssigned(newWod);
                        break;
                    }
                };
            };

            /**
             * print difficulty
             * @param difficulty
             * @returns {string}
             */
            $scope.printColorDifficulty = function(difficulty){ return helperService.printColorDifficulty(difficulty); }

            /**
             * Print status icon
             * @param status
             * @returns {string}
             */
            $scope.printStatusIcon = function(status){ return helperService.printStatusIcon(status); }

            /**
             * Changes wod status
             * @param wod
             * @param newStatus
             */
            $scope.changeStatus = function(wod, newStatus) {

                if (wod.status == newStatus) return;

                var promise = wodsService.setStatus(wod.slug, newStatus);

                promise.success(function(data){
                    notificationService.notifySuccess('status changed');
                    wod.status = data.status;

                    //Emite status changed
                    $scope.$emit('wodStatusChanged',{wod: data});

                    //If we can, reload activity page
                    if ($scope.parentScope && $scope.parentScope.reloadActivityPage) {
                        if ($scope.parentScope.activities.current == 1) {
                            $scope.parentScope.reloadActivityPage();
                        }
                    }
                });

                promise.error(function(error){});
            };

            /**
             * Changes wod difficulty
             * @param wod
             * @param newDifficulty
             */
            $scope.changeDifficulty = function(wod, newDifficulty) {

                if (wod.difficulty == newDifficulty) return;

                var promise = wodsService.setDifficulty(wod.slug, newDifficulty);

                promise.success(function(data){
                    notificationService.notifySuccess('difficulty changed');
                    wod.difficulty = data.difficulty;


                    //Emit a scope change
                    $scope.$emit('wodDifficultyChanged',{wod: data});


                    //If we can, reload activity page
                    if ($scope.parentScope && $scope.parentScope.reloadActivityPage) {
                        if ($scope.parentScope.activities.current == 1) {
                            $scope.parentScope.reloadActivityPage();
                        }
                    }
                });

                promise.error(function(error){});
            };

            //Watch wods !
            $scope.$watchCollection('wods',function(){

                if(!$scope.wods || $scope.wods.length == 0) return;

                for(var i = 0; i < $scope.wods.length; i++) {
                    getFilteredUsersAssigned($scope.wods[i]);
                }
            });

            /**
             * Show wods
             * @param $event
             * @param wod
             * @returns {boolean}
             */
            $scope.showWod = function($event, wod) {
                //HACK if this directive is correctly used never will be used in wods screen

                //Same workGroup ? no problem
                if($rootScope.getMainData().workGroup.id == wod.workGroup.id) {
                    return;
                }

                var servicePromise = workGroupService.selectWorkGroup(wod.workGroup.id);

                servicePromise.success(function(data){
                    $rootScope.setMainData(data);
                    $location.path('/wods').hash(wod.slug);
                });

                //We don't want the link that continue working
                $event.preventDefault();
            };

            //Watch selectedWod changes
            $scope.$watchCollection('selectedWod',function(){

                if(!$scope.wods) { return; }
                //Loop all wods
                for(var i = 0; i < $scope.wods.length; i++) {
                    $scope.wods[i].selected = $scope.selectedWod && $scope.selectedWod.id == $scope.wods[i].id;
                }
            });

            //Liste ton status changed broadcast
            $scope.$on('wodStatusChanged_broadcast',function(evt, data){

                //Check if we have the wod
                for(var i = 0; i < $scope.wods.length; i++) {
                    if($scope.wods[i].id == data.wod.id) {
                        $scope.wods[i].status = data.wod.status;
                    }
                }
            });

            //Listen on difficulty changed broadcast
            $scope.$on('wodDifficultyChanged_broadcast', function(evt, data){
                //Check if we have the wod
                for(var i = 0; i < $scope.wods.length; i++) {
                    if($scope.wods[i].id == data.wod.id) {
                        $scope.wods[i].difficulty = data.wod.difficulty;
                    }
                }
            });
        };

        //Here we will register last opened popover
        var lastOpenedPopover = null;

        //if we have click on de body  we need to close popover
        jQuery(document).click(function(e){
            if (lastOpenedPopover == null) return;


            if (jQuery(e.target).closest(lastOpenedPopover).length == 0) {
                lastOpenedPopover.popover('destroy');
            }
        });

        var linkFn = function($scope, tElement, attr) {

            //If we have text for select wod, then put it on scope
            if(attr.selectWod) {
                $scope.selectWodFn = attr.selectWod;
            }

            var popoverPosition = attr.popoverPosition ? attr.popoverPosition : 'left';

            //Retrieve element as jQuery
            var jQElement = jQuery(tElement);

            //Finds a wod by slug
            var findWodBySlug = function(slug) {

                if(!$scope.wods || $scope.wods.length == 0) {
                    return null;
                }

                for(var i = 0; i < $scope.wods.length; i++) {
                    if($scope.wods[i].slug == slug) {
                        return $scope.wods[i];
                    }
                }

                return null;
            };


            //Markup for popover
            var popoverMarkup = [
                '<ul class="dropdown-menu menuPopover" data-wod-slug="{! wod.slug !}">',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeStatus(wod,\'open\')">',
                            '<span class="btn btn-xs btn-status btnStatusWod mr5">',
                                '<i class="text-muted"></i>',
                            '</span>',
                            'Open ',
                            '<i class="fa fa-check" ng-show="wod.status === \'open\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeStatus(wod,\'in progress\')">',
                            '<span class="btn btn-xs btn-status btnStatusWod mr5">',
                                '<i class="text-muted fa fa-ellipsis-h"></i>',
                            '</span>',
                            'In progress ',
                            '<i class="fa fa-check" ng-show="wod.status === \'in progress\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeStatus(wod,\'resolved\')">',
                            '<span class="btn btn-xs btn-status btnStatusWod mr5">',
                                '<i class="text-muted fa fa-check"></i>',
                            '</span>',
                            '<span class="text-muted">Resolved</span> ',
                            '<i class="fa fa-check" ng-show="wod.status === \'resolved\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeStatus(wod,\'closed\')">',
                            '<span class="btn btn-xs btn-status btnStatusWod mr5">',
                                '<i class="text-muted fa fa-lock"></i>',
                            '</span>',
                            '<span class="text-muted text-line-through">Closed</span> ',
                            '<i class="fa fa-check" ng-show="wod.status === \'closed\'" ></i>',
                        '</a>',
                    '</li>',

                    //Commented difficulty, no to delete
                    /*'<li class="divider">',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeDifficulty(wod,\'minor\')">',
                            'Minor ',
                            '<i class="fa fa-check" ng-show="wod.difficulty === \'minor\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeDifficulty(wod,\'low\')">',
                            'Low ',
                            '<i class="fa fa-check" ng-show="wod.difficulty === \'low\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeDifficulty(wod,\'normal\')">',
                            'Normal ',
                            '<i class="fa fa-check" ng-show="wod.difficulty === \'normal\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeDifficulty(wod,\'high\')">',
                            'High ',
                            '<i class="fa fa-check" ng-show="wod.difficulty === \'high\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeDifficulty(wod,\'critical\')">',
                            'Critical ',
                            '<i class="fa fa-check" ng-show="wod.difficulty === \'critical\'" ></i>',
                        '</a>',
                    '</li>', */
                '</ul>',
            ].join('');

            var container = jQuery('<div class="hide wodsPopoverContainer"></div>');

            //Create a new scope for that wod
            var newScope = $scope.$new();
            newScope.wod = {};
            newScope.setWod = function(wod){ newScope.wod = wod; };

            //Create element
            var element = jQuery(popoverMarkup);

            //Compile it
            $compile(element)(newScope);

            //Associate click element on td
            jQElement.on('click', '.difficultyTableWod button', function(){
                var td = jQuery(this);
                var slug = td.data('wod-slug');
                var wod = findWodBySlug(slug);

                //If there is any popover in dome opened, then hide it
                jQuery('.popover').popover('destroy');

                //Change newScope wod
                newScope.$apply(function(){
                    newScope.setWod(wod);
                });


                lastOpenedPopover = td.popover({
                    content: element,
                    html: true,
                    placement: popoverPosition
                });

                //Show popover
                lastOpenedPopover.popover('show');


                //Popover animations
                element.one('mouseenter', function(){

                    var promise = null;
                    element.on('mouseleave',function(){

                        promise = $timeout(function(){
                            lastOpenedPopover.popover('destroy');

                        }, 1000);

                        element.one('mouseenter', function(){
                            if(promise) {
                                $timeout.cancel(promise);
                            }
                        });
                    });
                })
            });
        };

        var directiveItem = {
            restrict: 'E', //Restrict only for elements
            templateUrl: '/table-wods-dashboard.tpl',
            scope: {
                wods: '=elements',
                parentScope: '=parentScope',
                group: '=elementGroup',
                selectedWod: '=selectedWod'
            },
            controller : controllerFn,
            link: linkFn
        };


        return directiveItem;
    };

})(angular);