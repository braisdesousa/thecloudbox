(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.directive('myEllipsisMultiLine', ['$log', 'jQuery', '$compile', '$timeout',
        multiLineEllipsisDirective]);

    /**
     * Directive for show popover on wods
     * @param $log
     * @param jQuery
     * @param $compile
     * @param notificationService
     * @returns {{restrict: string, templateUrl: string, scope: {wods: string, parentScope: string}, controller: Function, link: Function}}
     */
    function multiLineEllipsisDirective($log, jQuery, $compile, $timeout) {

        var anchor_view_more = 'View more';
        var anchor_view_less = 'View less';

        /**
         * Cuts text
         * @param jText
         * @param maxHeight
         */
        var cutTextElement  = function(jText, maxHeight) {

            while(needToCutText(jText, maxHeight)) {
                jText.text(jText.text().replace(/\W*\s(\S)*$/, '...'));
            }

            jText.text(jText.text().replace(/\W*\s(\S)*$/, '...'));
        };

        /**
         * Function that indicates if is needed to measure
         * @returns {boolean}
         */
        var needToCutText = function(jText, maxHeight) {
            return  jText.parent().outerHeight() > maxHeight;
        };

        /**
         * Link function
         * @param $scope
         * @param element
         * @param attr
         */
        var linkFn = function($scope, element, attr) {

            var maxHeight = attr.height;

            var jElement = jQuery(element);

            var jContent = jQuery('<div></div>')
                .addClass('divCommentEllipsis')
                .appendTo(jElement);

            var jAnchor = jQuery('<a></a>')
                .addClass('commentViewMore')
                .appendTo(jElement);

            var jText = jQuery('<span></span>')
                .appendTo(jContent);

            $scope.$watch('originalText',function(newValue) {

                //First we set jText the new value
                jText.html(newValue);

                //If we dont need to measure...
                if(! needToCutText(jText, maxHeight)) {
                    jAnchor.hide(); //Hide anchor
                    return;
                }

                //Else first of all, show anchor
                jAnchor
                    .text(anchor_view_more)
                    .addClass('commentViewMore')
                    .show()
                    .off('click'); //Remove event handlers

                // if html or text
                if ( jText.children().length > 0 ) {

                    jContent
                        .addClass('showCommentBigHtml')
                        .css('height',maxHeight+'px');
                    jAnchor.addClass('commentViewMoreHtml');

                    //Now on anchor click we need switch
                    jAnchor.on('click',function() {

                        //IF is all text
                        if(jContent.hasClass('showCommentBigHtml')) {
                            jContent
                                .removeClass('showCommentBigHtml')
                                .css('height','');
                            jAnchor.text(anchor_view_less);
                        } else { //if is reduced one
                            jContent
                                .addClass('showCommentBigHtml')
                                .css('height',maxHeight+'px');
                            jAnchor.text(anchor_view_more);
                        }

                    });

                } else {
                    //We need to get the textReduced
                    cutTextElement(jText, maxHeight);

                    var reducedText = jText.text();

                    //Now on anchor click we need switch
                    jAnchor.on('click',function() {

                        //IF is all text
                        if(jText.text() == newValue) {
                            jText.text(reducedText);
                            jAnchor.text(anchor_view_more)

                        } else { //if is reduced one
                            jText.text(newValue);
                            jAnchor.text(anchor_view_less)
                        }

                    });

                }// end if html or text

            });
        };

        return {
            restrict: 'E', //Restrict only for elements
            link: linkFn,
            scope: {
                originalText: '=ngText'
            }
        };
    };

})(angular);