/**
 * author : Andres
 * Print chart Doughnut
 */
(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.directive('myDoughnutChart', ['$log', 'jQuery', '$compile', '$timeout',
        myDoughnutChartDirective]);

    /**
     * Directive for show myDoughnutChart
     * @param $log
     * @param jQuery
     * @param $compile
     * @param notificationService
     * @returns {{restrict: string, templateUrl: string, scope: {wods: string, parentScope: string}, controller: Function, link: Function}}
     */
    function myDoughnutChartDirective($log, jQuery, $compile, $timeout) {


        /**
         * Link function
         * @param $scope
         * @param element
         * @param attr
         */
        var linkFn = function($scope, element, attr) {

            // isNumber or '0'
            var auxCount = $scope.count == '' || $scope.count == null  ? 0 : parseInt($scope.count);
            var auxTotal =  $scope.total == '' || $scope.total == null  ? 0 : parseInt($scope.total);

            // default size
            $scope.sizeCanvas = $.isNumeric( $scope.sizeCanvas) ? $scope.sizeCanvas : 100;

            // element directive
            var jElement = jQuery(element);

            // print element canvas
            var jCanvas = jQuery('<canvas>')
                .attr('width',$scope.sizeCanvas)
                .attr('height',$scope.sizeCanvas)
                .attr('id','myDoughnutChart')
                .appendTo(jElement);

            // contexct canvas
            var ctx = document.getElementById("myDoughnutChart").getContext("2d");

            // new chart
            var chart = new Chart( ctx );

            // data init chart
            var data = [
                {
                    value: parseInt( auxCount ),
                    color: "#4D5360",
                    highlight: "#616774",
                    label: "Wods"
                },
                {
                    value: parseInt( auxTotal - auxCount ),
                    color: "#949FB1",
                    highlight: "#A8B3C5",
                    label: "Rest"
                }
            ];
            var options = {
                segmentShowStroke : false,
                animateRotate : true,
                animateScale : false,
                animationEasing: "easeOutQuart",
                animationSteps : 50
            }

            // create a new chart Doughnut
            chart = new Chart(ctx).Doughnut(data,options);

            // update chart from new data
            var updateChart = function(){

                // isNumber or '0'
                auxCount = $scope.count == '' || $scope.count == null  ? 0 : parseInt($scope.count);
                auxTotal = $scope.total == '' || $scope.total == null  ? 0 : parseInt($scope.total);

                // update chart
                chart.segments[0].value = parseInt( auxCount ) ;
                chart.segments[1].value = parseInt( auxTotal - auxCount );
                chart.update();
            };

            // watch
            $scope.$watch('count', updateChart);
            $scope.$watch('total', updateChart);
        };

        return {
            restrict: 'E', //Restrict only for elements
            link: linkFn,
            scope: {
                total: '=total',
                count: '=count',
                sizeCanvas: '=size'
            }
        };
    };

})(angular);