/**
 * author : Andres
 * when scrolled call function
 */
(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.directive('myWhenScrolled', ['$log', 'jQuery', '$compile', '$timeout',
        myWhenScrolledDirective]);

    /**
     * Directive myWhenScrolled
     * @param $log
     * @param jQuery
     * @param $compile
     * @param notificationService
     * @returns {{restrict: string, templateUrl: string, scope: {wods: string, parentScope: string}, controller: Function, link: Function}}
     */
    function myWhenScrolledDirective($log, jQuery, $compile, $timeout) {


        /**
         * Link function
         * @param $scope
         * @param element
         * @param attr
         */
        var linkFn = function($scope, element, attr) {

            var raw = element[0];

            // when the scroll reaches the end, it calls the function
            element.bind('scroll', function() {
                if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight ) {
                    //$scope.$apply(attr.myWhenScroll);
                    $scope.myWhenScrolled();
                }
            });


        };

        return {
            restrict: 'A', //Restrict only for attribute
            link: linkFn,
            scope: {
                myWhenScrolled:'&'
            }
        };
    };

})(angular);