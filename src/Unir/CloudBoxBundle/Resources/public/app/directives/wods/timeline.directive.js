/**
 * Directiva para la timeline
 *
 * order creciente en fecha
 * los mas recientes abajo
 * y mostrar los 5 ultimos y si hya mas ocultos con un boton para mostrarlos
 */
(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.directive('myActivitiesTimelineWod', ['$log', '$sanitize', 'jQuery', activitiesTimeLineWod]);

    /**
     * Second timeline for activities
     * @param $log
     * @param $sanitize
     * @param jQuery
     * @returns {{restrict: string, templateUrl: string, scope: {activities: string, parentScope: string}, link: Function, controller: Function}}
     */
    function activitiesTimeLineWod($log, $sanitize, jQuery) {

        //Fn for order activities
        function orderActivities(activities) {

            if(!activities) return [];

            //Clone and sort
            return activities.slice(0).sort(function(a,b) {
                return a.created.getTime() - b.created.getTime();
            });
        };

        //Controller function
        var controllerFn = function($scope, $log) {

            $scope.showMoreActivities = false;

            var numMaxActive = 5;

            //Fn that will put the activities on the scope
            var presentActivities = function() {

                //Sort em
                var items = orderActivities($scope.items);

                //We are in reduced view?
                if (!$scope.showMoreActivities) {

                    //Control if there are more to see
                    $scope.thereAreMore = items.length > numMaxActive;

                    //Reduce the items for only watch the last elements
                    if(items.length > numMaxActive) {
                        items = items.slice(items.length - numMaxActive);
                    }
                } else { //Else, thereAreMore must be undefined
                    $scope.thereAreMore = undefined;
                }

                //Set items on scope
                $scope.activities = items;
            };

            $scope.showAll = function() {  $scope.showMoreActivities = true; };
            $scope.showLess = function() {  $scope.showMoreActivities = false; };

            $scope.$watchCollection('items', presentActivities);
            $scope.$watch('showMoreActivities', presentActivities);
        };

        var linkFn = function ($scope, element, attr) {
            $scope.showEllipsis = attr.noEllipsis !== undefined ? false : true;
        };

        return {
            restrict: 'E', //Restrict only for elements
            templateUrl: '/timelineWod.tpl', //Load timeline.tpl
            scope: {
                items: '=elements', //We bind elements attribute
                parentScope: '=parentScope'
            },
            link: linkFn,
            controller: controllerFn
        }
    };


})(angular);