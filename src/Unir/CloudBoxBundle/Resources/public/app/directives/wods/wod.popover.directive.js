(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.directive('myWodPopoverDirective', ['$log', 'jQuery', '$compile', '$timeout',
        'notificationService', 'helperService', wodPopoverDirective]);


    /**
     * This directive will show popover for element
     */
    function wodPopoverDirective($log, jQuery, $compile, $timeout, notificationService, helperService){

        var controllerFn = function($scope, $log, wodsService){

            $scope._name = 'my-wod-popover-directive';

            /**
             * Changes wod status
             * @param wod
             * @param newStatus
             */
            $scope.changeStatus = function(wod, newStatus) {

                if (wod.status == newStatus) return;

                var promise = wodsService.setStatus(wod.slug, newStatus);

                promise.success(function(data){
                    notificationService.notifySuccess('status changed');
                    wod.status = data.status;

                    //Emit a scope change
                    $scope.$emit('wodStatusChanged',{wod: data});
                });

                promise.error(function(error){});
            };

            /**
             * Changes wod difficulty
             * @param wod
             * @param newDifficulty
             */
            $scope.changeDifficulty = function(wod, newDifficulty) {

                if (wod.difficulty == newDifficulty) return;

                var promise = wodsService.setDifficulty(wod.slug, newDifficulty);

                promise.success(function(data){
                    notificationService.notifySuccess('difficulty changed');
                    wod.difficulty = data.difficulty;

                    //Emit a scope change
                    $scope.$emit('wodDifficultyChanged',{wod: data});
                });

                promise.error(function(error){});
            };
        };

        //Here we will register last opened popover
        var lastOpenedPopover = null;

        //if we have click on de body  we need to close popover
        jQuery(document).click(function(e){
            if (lastOpenedPopover == null) return;


            if (jQuery(e.target).closest(lastOpenedPopover).length == 0) {
                lastOpenedPopover.popover('destroy');
            }
        });

        //Link function
        var linkFn = function($scope, tElement, attr){

            var jQElement = jQuery(tElement);

            var popoverPosition = attr.popoverPosition ? attr.popoverPosition : 'left';

            //Markup for popover
            var popoverMarkup = [
                '<ul class="dropdown-menu menuPopover" data-wod-slug="{! wod.slug !}" >',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeStatus(wod,\'open\')">',
                            '<span class="btn btn-xs btn-status btnStatusWod mr5">',
                                '<i class="text-muted"></i>',
                            '</span>',
                            'Open ',
                            '<i class="fa fa-check" ng-show="wod.status === \'open\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeStatus(wod,\'in progress\')">',
                            '<span class="btn btn-xs btn-status btnStatusWod mr5">',
                                '<i class="text-muted fa fa-ellipsis-h"></i>',
                            '</span>',
                            'in progress ',
                            '<i class="fa fa-check" ng-show="wod.status === \'in progress\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeStatus(wod,\'resolved\')">',
                            '<span class="btn btn-xs btn-status btnStatusWod mr5">',
                                '<i class="text-muted fa fa-check"></i>',
                            '</span>',
                            '<span class="text-muted">Resolved</span> ',
                            '<i class="fa fa-check" ng-show="wod.status === \'resolved\'" ></i>',
                        '</a>',
                    '</li>',
                    '<li>',
                        '<a class="cursor_pointer" ng-click="changeStatus(wod,\'closed\')">',
                            '<span class="btn btn-xs btn-status btnStatusWod mr5">',
                                '<i class="text-muted fa fa-lock"></i>',
                            '</span>',
                            '<span class="text-muted text-line-through">Closed</span> ',
                            '<i class="fa fa-check" ng-show="wod.status === \'closed\'" ></i>',
                        '</a>',
                    '</li>',
                '</ul>',
                '<!-- <h5 ng-show="!wod.canEdit">You cannot edit this wod</h5> -->',
            ].join('');

            //Create jQuery popover
            var jPopover = jQuery(popoverMarkup);

            //Compile popover in actual scope
            $compile(jPopover)($scope);

            //When we click on the button
            jQElement.on('click', function(){

                //If there is any popover in dome opened, then hide it
                jQuery('.popover').popover('destroy');

                lastOpenedPopover = jQElement.popover({
                    content: jPopover,
                    html: true,
                    placement: popoverPosition
                });

                //Show popover
                lastOpenedPopover.popover('show');


                //Popover animations
                jPopover.one('mouseenter', function(){

                    var promise = null;
                    jPopover.on('mouseleave',function(){

                        promise = $timeout(function(){
                            lastOpenedPopover.popover('destroy');

                        }, 1000);

                        jPopover.one('mouseenter', function(){
                            if(promise) {
                                $timeout.cancel(promise);
                            }
                        });
                    });
                })
            });
        };

        var directiveItem = {
            restrict: 'A', //Restrict only for elements
            scope: {
                wod: '=myWodPopoverDirective'
            },
            controller : controllerFn,
            link: linkFn
        };


        return directiveItem;
    };

})(angular);