(function(ng){

    var mainApp = ng.module('mainApp')
        .filter('characters', truncateCharacters)
        .filter('words', truncateWords);


    /**
     * Truncate characters
     * BASED on : https://github.com/sparkalow/angular-truncate
     * @returns {Function}
     */
    function truncateCharacters(){
        return function (input, chars, breakOnWord) {
            if (isNaN(chars)) return input;
            if (chars <= 0) return '';
            if (input && input.length > chars) {
                input = input.substring(0, chars);

                if (!breakOnWord) {
                    var lastspace = input.lastIndexOf(' ');
                    //get last space
                    if (lastspace !== -1) {
                        input = input.substr(0, lastspace);
                    }
                }else{
                    while(input.charAt(input.length-1) === ' '){
                        input = input.substr(0, input.length -1);
                    }
                }
                return input + '...';
            }
            return input;
        };
    };


    /**
     * Truncate words
     * BASED ON: https://github.com/sparkalow/angular-truncate
     * @returns {Function}
     */
    function truncateWords(){
        return function (input, words) {
            if (isNaN(words)) return input;
            if (words <= 0) return '';
            if (input) {
                var inputWords = input.split(/\s+/);
                if (inputWords.length > words) {
                    input = inputWords.slice(0, words).join(' ') + '...';
                }
            }
            return input;
        };
    }

})(angular);