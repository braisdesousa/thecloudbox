(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.modals.confirmation.ctrl', [
        '$log', '$scope', '$q', '$modalInstance', confirmationModalCtrl]);


    /**
     * New workGroup modal controller
     */
    function confirmationModalCtrl($log, $scope, $q, $modalInstance, reload){

        $scope._name = 'mainApp.modals.confirmation.ctrl';
        $scope.ok = function() {
            $modalInstance.close();
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };
    };
})(angular);