(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.wods.new.ctrl',[
        '$log', '$scope', '$q', 'wodsService',
        'helperService', '$modalInstance', 'wodGroups', 'enterpriseUsersAndGroups', wodNewCtrl]);

    //Simple function that will order the users
    var orderUsers = function(users) {
        var fnOrder = function(a,b) {
            return a.username.localeCompare(b.username);
        };
        return users.sort(fnOrder);
    };

    //Simple function that will order groups
    var orderGroups = function(groups) {
        var fnOrder = function(a,b) {
            return a.name.localeCompare(b.name);
        };
        return groups.sort(fnOrder);
    };

    /**
     * New wod Controller
     */
    function wodNewCtrl($log, $scope, $q, wodsService,
                         helperService, $modalInstance, wodGroups, enterpriseUsersAndGroups) {

        $scope._name = 'mainApp.wods.new.ctrl';
        $scope.title = 'New wod';

        var formName = 'unir_ticketmanagerbundle_wod';

        $scope.wodGroups = wodGroups;
        $scope.users = orderUsers(enterpriseUsersAndGroups.users.concat(enterpriseUsersAndGroups.admin_users));

        $scope.formData = {};
        $scope.formData[formName] = {};
        $scope.formData[formName]['status'] = 'open';
        $scope.formData[formName]['difficulty'] = 'normal';
        $scope.formData[formName]['wodGroup'] = wodGroups[0].id;

        //When user press send button
        $scope.formSubmit = function(formData){
            var deferred = $q.defer();

            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            if(!formData[formName].usersAssigned) {
                formData[formName].usersAssigned = [];
            }

            if(!formData[formName].usersFollower) {
                formData[formName].usersFollower = [];
            }

            var servicePromise = wodsService.create(formData);

            servicePromise.success(function(data){
                $modalInstance.close(data);
                deferred.resolve();
            });


            servicePromise.error(function(error){
                $log.error(error);

                deferred.reject(error);
            });


            return deferred.promise;
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };

        $scope.tinyMCEOptions = helperService.tinyMCEOptions;
    };

})(angular);