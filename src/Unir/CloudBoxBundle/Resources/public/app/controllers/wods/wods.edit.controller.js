(function(ng){

    var mainApp = ng.module('mainApp');
    mainApp.controller('mainApp.wods.edit.ctrl', [
        '$log', '$scope', '$q', 'wodsService', 'helperService',
        '$modalInstance', 'wod', 'wodGroups', wodEditCtrl]);

    /**
     * Wod edit controller
     */
    function wodEditCtrl($log, $scope, $q, wodsService, helperService,
                          $modalInstance, wod, wodGroups) {

        $scope._name = 'mainApp.wods.edit.ctrl';
        $scope.title = 'Edit wod: <strong>' + wod.title + '</strong>';
        var formName = 'unir_ticketmanagerbundle_wod_short';

        $scope.formData = {};
        $scope.formData[formName] = {};
        $scope.formData[formName].title = wod.title;
        $scope.formData[formName].description = wod.description;
        $scope.formData[formName].viewBy = wod.view_by;
        $scope.formData[formName].wodGroup = wod.wod_group.id;
        $scope.formData[formName].estimationTime = wod.estimation_time;

        $scope.wodGroups = wodGroups;

        //When user press send button
        $scope.formSubmit = function(formData){
            var deferred = $q.defer();

            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = wodsService.edit(wod.slug, formData);

            servicePromise.success(function(data){
                $modalInstance.close(data);
                deferred.resolve();
            });


            servicePromise.error(function(error){
                $log.error(error);
                deferred.reject(error);
            });


            return deferred.promise;
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };

        $scope.tinyMCEOptions = helperService.tinyMCEOptions;
    };

})(angular);