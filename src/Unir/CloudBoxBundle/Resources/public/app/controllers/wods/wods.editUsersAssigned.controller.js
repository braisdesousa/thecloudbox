(function(ng){

    var mainApp = ng.module('mainApp');
    mainApp.controller('mainApp.wods.editUsersAssigned.ctrl', ['$log', '$scope', '$q', 'wodsService',
        '$modalInstance', 'wod', 'allUsers', wodEditUsersAssignedCtrl]);

    /**
     * edit users assigned controller
     * @param $log
     * @param $scope
     * @param $q
     * @param wodsService
     * @param $modalInstance
     * @param wod
     */
    function wodEditUsersAssignedCtrl($log, $scope, $q, wodsService, $modalInstance, wod, allUsers) {

        $scope._name = 'mainApp.wods.editUsersAssigned.ctrl';
        $scope.title = 'Edit users assgigned for: <strong>' + wod.title + '</strong>';
        var formName = 'unir_ticketmanagerbundle_wod_assigned';

        $scope.wod = wod;
        $scope.allUsers = allUsers;


        var usersId = [];
        ng.forEach(wod.users_assigned, function(user) { usersId.push(user.id); });

        $scope.formData = {};
        $scope.formData[formName] = {};
        $scope.formData[formName].usersAssigned = usersId;

        //When form is submitted
        $scope.formSubmit = function(formData){
            var deferred = $q.defer();

            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = wodsService.setUsersAssigned(wod.slug, formData);

            servicePromise.success(function(data) {
                deferred.resolve();
                $modalInstance.close(data);
            });

            servicePromise.error(function(error){
                $log.error(error);
                deferred.reject(error);
            });

            return deferred.promise;
        };


        //When user cancel modal
        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };
    };

})(angular);