(function(ng){

    var mainApp = ng.module('mainApp')
        .controller('mainApp.wods.main.ctrl',
        ['$log', '$scope', '$timeout', '$q', '$modal', '$location', 'wodsService', 'helperService', 'translator',
            'wodGroupService', 'notificationService', 'localStorageService', 'routing', 'data', 'wod',  mainController]);

    /**
     *
     * @param $log
     */
    function mainController($log, $scope, $timeout, $q, $modal, $location,  wodsService, helperService, translator,
          wodGroupService, notificationService, localStorageService, routing, data, wod) {

        $scope._name = 'mainApp.wods.main.ctrl';

        //
        // INIT DATA
        //--------------------------------------------------------------------------------------------------------------
        $scope.setTitle('Wods').setMenuActive('wods');

        //Sets data on scope
        $scope.data = data;

        //Check if there is no workGroup, if there is no workGroup, redirect
        if($scope.getMainData().workGroups.length == 0) {
            $location.path(routing.generate('ng_no_workGroups'));
        }

        //Should show the wod in full screen
        var showWodInFullScreen = true;

        //Indicates if is first time that the filters have changed
        var isFirstTimeThatFiltersChanged = false;

        //LoadGroups promise, will help to know if is or not loading groups
        var loadGroupsHttpExtendedPromise = undefined;

        //  -------------- Local storage keys -------------- //
        var localStorage_showWodInFullScreen = 'wods_controller_showWodInFullScreen';
        var localStorage_wodGroupsFilterKey = 'wods_controller_wodGroupsFilter';
        var localStorage_difficultyFilterKey = 'wods_controller_difficultyFilter';
        var localStorage_statusFilterKey = 'wods_controller_statusFilter';
        var localStorage_sortByFilterKey = 'wods_controller_sortByFilter';

        //  --------------  End Local storage keys -------------- //

        //
        // FILTERS MANIPULATION
        //--------------------------------------------------------------------------------------------------------------

        //First of all we need to create new object on scope, thats what filters now are saying that we have
        $scope.filters = {
            difficulties : [],
            sortBy :  {
                field: null,
                order: null
            },
            statuses: [],
            groups: []
        };

        //Fn that will clone scope data groups into the filter
        var cloneDataFilters = function(){

            $scope.filters.difficulties = $scope.data.filters.difficulties;
            $scope.filters.statuses = $scope.data.filters.statuses;

            var dataField = null;
            var dataOrder = null;
            for(var key in $scope.data.filters.sortBy) {
                dataField = key;
                dataOrder = $scope.data.filters.sortBy[key];
            }

            $scope.filters.sortBy.field = dataField;
            $scope.filters.sortBy.order = dataOrder;

            if (!$scope.filters.sortBy.field) {
                $scope.filters.sortBy.field = 'difficulty';
            }

            if (!$scope.filters.sortBy.order) {
                $scope.filters.sortBy.order = 'asc';
            }


            $scope.filters.groups = [];
            for(var i = 0; i < data.filters.groups.length; i++) {
                if($scope.data.filters.groups[i].selected) {
                    $scope.filters.groups.push($scope.data.filters.groups[i].id);
                }

            }
        };

        //Fn that will clone scope filters in data filters
        var cloneDataFiltersBack = function() {

            $scope.data.filters.difficulties = $scope.filters.difficulties;
            $scope.data.filters.statuses = $scope.filters.statuses;

            $scope.data.filters.sortBy = { };
            $scope.data.filters.sortBy[$scope.filters.sortBy.field] = $scope.filters.sortBy.order;

            for(var i = 0; i < $scope.data.filters.groups.length; i++) {

                var cGroup = $scope.data.filters.groups[i];
                var isSelected = false;

                for (var j = 0; j < $scope.filters.groups.length; j++) {
                    if(cGroup.id == $scope.filters.groups[j]) {
                        isSelected = true;
                        break;
                    }
                };
                cGroup.selected = isSelected;
            }

        };

        //Manipulates sortBy to make it into query
        var sortByToQuery = function() {

            var field = $scope.filters.sortBy.field;
            var order = $scope.filters.sortBy.order;

            var obj = {};
            obj[field] = order;

            return obj;
        };

        //Fn that will store filters on local storage
        var storeFiltersOnLocalStorage = function(){

            localStorageService.set(localStorage_difficultyFilterKey, $scope.filters.difficulties);
            localStorageService.set(localStorage_statusFilterKey, $scope.filters.statuses);
            localStorageService.set(localStorage_sortByFilterKey, sortByToQuery());

            //Retrieve from localStorage the wodGroups filter
            var wodGroupsFilter = localStorageService.get(localStorage_wodGroupsFilterKey);

            //No exists? no problem
            if (!wodGroupsFilter)  wodGroupsFilter = {};

            //Extend it with the current workGroup id
            wodGroupsFilter[$scope.getMainData().workGroup.id] = $scope.filters.groups;

            //Store again on localstorage
            localStorageService.set(localStorage_wodGroupsFilterKey, wodGroupsFilter);
        };

        //Fn executed for the watchCollection that will try to watch for changes on filters
        var filtersChanged_watchFn = function(){
            var watchArr = [];

            watchArr.push($scope.filters);
            watchArr.push($scope.filters.difficulties);
            watchArr.push($scope.filters.statuses);
            watchArr.push($scope.filters.groups);

            if ($scope.filters.sortBy) {

                watchArr.push($scope.filters.sortBy.field);
                watchArr.push($scope.filters.sortBy.order);
            }

            return watchArr;
        };

        //When the filters change...
        var onFiltersChangedFn = function() {

            //Always first propagation occurrs when page is load
            if(!isFirstTimeThatFiltersChanged) {
                isFirstTimeThatFiltersChanged = true;
                return;
            }
            //Load groups
            loadGroups();
        };

        //We are going to watch changes on filter
        $scope.$watchCollection(filtersChanged_watchFn ,onFiltersChangedFn);

        /**
         * Will select the class for the sort by arrow
         * @returns {string}
         */
        $scope.sortByOrderArrowClass = function() {
            if (!$scope.filters.sortBy.order || $scope.filters.sortBy.order == 'asc') {
                return 'fa-arrow-up';
            } else {
                return 'fa-arrow-down';
            }
        };

        /**
         * Fn that will toggle sort by
         */
        $scope.toggleSortByOrder = function(){

            if (!$scope.filters.sortBy.order || $scope.filters.sortBy.order == 'asc') {
                $scope.filters.sortBy.order = 'desc';
            } else {
                $scope.filters.sortBy.order = 'asc';
            }
        };

        //Clone groups into the filter
        cloneDataFilters();

        //
        // Wods petitions
        //--------------------------------------------------------------------------------------------------------------

        //Indicates if filters differ
        var filtersDiffer = function() {

            //Ok this is hard, we are going to make some changes...

            var dataFilters = $scope.data.filters;
            var nowFilters = $scope.filters;


            //Difficulties
            if (!nowFilters.difficulties) { nowFilters.difficulties = []; }
            if (dataFilters.difficulties.length != nowFilters.difficulties.length) return true;

            for(var i = 0; i < dataFilters.difficulties.length; i++) {
                if (dataFilters.difficulties[i] != nowFilters.difficulties[i]) return true;
            }

            //Statuses
            if (!nowFilters.statuses) { nowFilters.statuses = []; }
            if (dataFilters.statuses.length != nowFilters.statuses.length) return true;

            for(var i = 0; i < dataFilters.statuses.length; i++) {
                if (dataFilters.statuses[i] != nowFilters.statuses[i]) return true;
            }

            //Groups
            var groups = [];
            for(var i = 0; i < $scope.data.filters.groups.length; i++) {
                if($scope.data.filters.groups[i].selected) {
                    groups.push($scope.data.filters.groups[i].id);
                }
            }

            if (nowFilters.groups.length != groups.length) return true;

            for(var i = 0; i < nowFilters.groups.length; i++) {
                if (nowFilters.groups[i] != groups[i]) return true;
            }

            //Order by
            var dataField = null;
            var dataOrder = null;
            for(var key in dataFilters.sortBy) {
                dataField = key;
                dataOrder = dataFilters.sortBy[key];
            }
            if(dataField != nowFilters.sortBy.field) return true;
            if(dataOrder != nowFilters.sortBy.order) return true;

            return false;
        };

        // Load groups data when filters are changed
        var loadGroups = function() {

            var deferred = $q.defer();

            //Any filter change will make a petition without
            if (!filtersDiffer()) {
                deferred.resolve();
                return deferred.promise;
            }

            //Clone filters back, that will be prevent an endless loop
            cloneDataFiltersBack();

            //Store the filters on localStorage
            storeFiltersOnLocalStorage();


            loadGroupsHttpExtendedPromise = wodsService.getWods(
                $scope.filters.groups,
                $scope.filters.difficulties,
                $scope.filters.statuses,
                sortByToQuery()
            );

            loadGroupsHttpExtendedPromise.success(function(data){
                $scope.data.groups = data.groups;
                $scope.data.filters = data.filters;

                cloneDataFilters();

                deferred.resolve();
            });

            loadGroupsHttpExtendedPromise.error(function(error){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Same as before, but in scope
        $scope.forceLoadGroups = function(){

            var deferred = $q.defer();

            loadGroupsHttpExtendedPromise = wodsService.getWods(
                $scope.filters.groups,
                $scope.filters.difficulties,
                $scope.filters.statuses,
                sortByToQuery()
            );

            loadGroupsHttpExtendedPromise.success(function(data){
                $scope.data.groups = data.groups;
                $scope.data.filters = data.filters;

                cloneDataFilters();

                deferred.resolve();
            });

            loadGroupsHttpExtendedPromise.error(function(error){
                $log.error(error);
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * This function will paginate a group
         * @param groupId
         * @param numPage
         */
        $scope.paginateGroup = function(group, numPage) {
            var deferred = $q.defer();

            var servicePromise = wodsService.getWodsByGroup(
                group.id,
                $scope.filters.difficulties,
                $scope.filters.statuses,
                sortByToQuery(),
                numPage
            );

            servicePromise.success(function(data){

                var foundGroup = null;

                //We loop till we get the group
                for (var i = 0; i < $scope.data.groups.length; i++) {
                    if ($scope.data.groups[i].id == group.id) {
                        foundGroup = $scope.data.groups[i];
                        break;
                    }
                }

                //Here now we have the group...
                foundGroup.pagination = data.pagination;
                foundGroup.wods = data.wods;
            });


            servicePromise.error(function(error){
                $log.error(error);
                deferred.reject();
            })


            return deferred.promise;
        };

        /**
         * Indicates is groups are loading...
         */
        $scope.isLoadingGroups = function(){
            return loadGroupsHttpExtendedPromise && loadGroupsHttpExtendedPromise.isDone();
        };

        //
        // Wods single page
        //--------------------------------------------------------------------------------------------------------------

        //This method will merge wods and comments
        var mergeActivityAndComments = function(wod) {


            var activities = wod.wod_activities;
            var comments = wod.wod_comments;

            if (!activities) activities = [];
            if (!comments) comments = [];

            var reducedWod = {};
            reducedWod.created = new Date(wod.updated);
            reducedWod.id = wod.id;
            reducedWod.difficulty = wod.difficulty;
            reducedWod.slug = wod.slug;
            reducedWod.status = wod.status;
            reducedWod.wod_group = wod.wod_group;
            reducedWod.title = wod.title;
            reducedWod.users_assigned = wod.users_assigned;
            reducedWod.users_follower = wod.users_follower;


            var mergedActivitiesAndComments = [];

            for(var i = 0; i < activities.length; i++) {

                var act = activities[i];
                var itm = {};
                itm.action = act.action;
                itm.created =  new Date(act.created);
                itm.description = act.description;
                itm.id = act.id;
                itm.wod = reducedWod;
                itm.user = act.user;


                mergedActivitiesAndComments.push(itm);
            };

            for (var i = 0; i < comments.length; i++) {

                var comm = comments[i];
                var itm = {};
                itm.comment = comm.comment;
                itm.created = new Date(comm.created);
                itm.id = comm.id;
                itm.wod = reducedWod;
                itm.user = comm.user;


                mergedActivitiesAndComments.push(itm);
            };

            //Sort items
            mergedActivitiesAndComments = mergedActivitiesAndComments.sort(function(a, b){

                var aTime = a.created.getTime();
                var bTime = b.created.getTime();

                return aTime > bTime ? -1 : 1;
            });


            wod.activitiesAndComments = {};
            wod.activitiesAndComments.elements = mergedActivitiesAndComments;

            //Now make functions por paginate

            //Items per page
            wod.activitiesAndComments.itemsPerPage = 10;

            //Retrieve total items
            wod.activitiesAndComments.totalItems = function() {
                return wod.activitiesAndComments.elements.length;
            };

            //Retrieve total pages
            wod.activitiesAndComments.totalPages = function() {
                return Math.ceil(wod.activitiesAndComments.elements.length  / wod.activitiesAndComments.itemsPerPage);
            };

            //Get items
            wod.activitiesAndComments.get = function(page) {
                //page out of index, return
                if (page <= 0 || page > wod.activitiesAndComments.totalPages()) return [];

                var startingIndex = (page - 1) * wod.activitiesAndComments.itemsPerPage;
                var maxIndex = startingIndex + wod.activitiesAndComments.itemsPerPage;

                if (maxIndex > wod.activitiesAndComments.elements.length) {
                    maxIndex = wod.activitiesAndComments.elements.length;
                }

                var itms = [];

                for (var i = startingIndex; i < maxIndex; i++) {
                    itms.push(wod.activitiesAndComments.elements[i]);
                }

                return itms;
            }
        };

        //This will upgrade the wod
        var upgradeWod = function(wod) {
            //First set activity and comments
            mergeActivityAndComments(wod);

            var currentUser = $scope.getMainData().user;

            //Set if wod is current user is followed or assigned
            wod.isCurrentUserAssigned = false;
            wod.isCurrentUserFollower = false;

            for(var i = 0; i < wod.users_assigned.length; i++) {
                if (wod.users_assigned[i].id == currentUser.id) {
                    wod.isCurrentUserAssigned = true;
                    break;
                }
            }

            for(var i = 0; i < wod.users_follower.length; i++) {
                if(wod.users_follower[i].id == currentUser.id) {
                    wod.isCurrentUserFollower = true;
                    break;
                }
            }
        };

        //This will change url for set the wod slug in url
        var changeUrlForWod = function(wod) {
            $location.hash(wod.slug);
            $location.replace();
        };

        //Remove wod Url
        var removeWodUrl = function() {
            $location.hash('');
            $location.replace();
        };

        //If we have wod
        if (wod) {
            upgradeWod(wod);
            //Set page title
            $scope.setTitle(wod.title);
        }

        $scope.selectedWod = wod;

        //Check in localStorage if we have the value
        if(localStorageService.get(localStorage_showWodInFullScreen) !== undefined) {
            showWodInFullScreen = localStorageService.get(localStorage_showWodInFullScreen) == 'true';
        }

        $scope.getSelectedWod = function(){ return $scope.selectedWod; };
        $scope.isShowWodInFullScreen = function(){ return showWodInFullScreen; };


        //this function will be called when a wod is selected
        $scope.selectWod = function(wod, group) {

            var deferred = $q.defer();

            var servicePromise = wodsService.getWod(wod.slug);

            servicePromise.success(function(data){
                $scope.selectedWod = data;
                $scope.setTitle(wod.title); //Set page title
                upgradeWod($scope.selectedWod);
                changeUrlForWod($scope.selectedWod);

                deferred.resolve();
            });

            servicePromise.error(function(error){
                $log.error(error);
                deferred.reject(error);
            })

            return deferred.promise;
        };

        //This function will close selected wod
        $scope.closeSelectedWod = function(){
            $scope.selectedWod = null;
            $scope.setTitle('Wods'); //Remove wods title
            removeWodUrl();
        };

        //This indicate that need to show wod in full screen
        $scope.toggleFullScreen = function(){
            showWodInFullScreen = !showWodInFullScreen;
            $scope.selectedWod = null;
            $scope.setTitle('Wods'); //Remove wods title
            removeWodUrl();

            localStorageService.set(localStorage_showWodInFullScreen, showWodInFullScreen);
        };


        //Indicates when full screen wod is visible
        $scope.isWodFullScreenVisible = function(){

            //Full screen is visible when we are showing a wod in fullScreen option
            return showWodInFullScreen && $scope.selectedWod != null;
        };

        //Indicates when the three columns panel is visible
        $scope.isThreeColumnPanelVisible = function() {

            //Three panel column is visible when we don't have selectedWod and we are on full screen
            return showWodInFullScreen && $scope.selectedWod == null;

        };

        //Indicates when the wodScreen filters is visible
        $scope.isWodScreenFiltersVisible = function(){

            if (!showWodInFullScreen) {
                return true;
            }

            return $scope.selectedWod == null;
        };

        $scope.isSingleColumnAndPreviewPanelVisible = function() {
            return !showWodInFullScreen;
        };

        /**
         * Delete a wod
         * @param wod
         * @returns {*}
         */
        $scope.deleteWod = function(wod) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'deleteWodConfirmationBox.tpl',
                controller: 'mainApp.modals.confirmation.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {}
            });

            modal.result.then(function() {

                var servicePromise = wodsService.delete(wod.slug);

                servicePromise.success(function () {

                    var promise2 = loadGroups();

                    promise2.then(function () {
                        $scope.selectedWod = null;

                        //We need to loop group wods till we foind the wod

                        for(var i = 0; i < $scope.data.groups.length; i++) {

                            var idx = -1;

                            for(var j = 0; j < $scope.data.groups[i].wods.length; j++) {

                                if($scope.data.groups[i].wods[j].id == wod.id) {
                                    idx = j;
                                    break;
                                }
                            }

                            if(idx >= 0) {
                                $scope.data.groups[i].wods.splice(idx, 1);
                            }
                        }

                        deferred.resolve();
                    });

                    promise2.catch(function (error) {
                        $log.error(error);
                        deferred.reject(error);
                    });
                });

                servicePromise.error(function (error) {
                    $log.error(error);
                    deferred.reject(error);
                });
            });

            modal.result.catch(function () {
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * Assigns a wod or unassign it
         * @param wod
         * @returns {*}
         */
        $scope.assignWod = function(wod) {
            var deferred = $q.defer();

            var servicePromise = wodsService.assign(wod.slug);

            servicePromise.success(function(data){
                upgradeWod(data);

                $scope.selectedWod = data;

                notificationService.notifySuccess($scope.selectedWod.isCurrentUserAssigned
                        ? 'You are now assigned'
                        : 'You have been unassigned'
                );
                deferred.resolve();
            });

            servicePromise.error(function(error){
                $log.error(error);
                deferred.reject();
            });


            return deferred.promise;
        };

        /**
         * Follow or unfollow a wod
         * @param wod
         * @returns {*}
         */
        $scope.followWod = function(wod) {
            var deferred = $q.defer();

            var servicePromise = wodsService.follow(wod.slug);

            servicePromise.success(function(data){
                upgradeWod(data);

                $scope.selectedWod = data;

                notificationService.notifySuccess($scope.selectedWod.isCurrentUserFollower
                        ? 'You are now follower'
                        : 'You are no longer follower'
                );
                deferred.resolve();
            });

            servicePromise.error(function(error){
                $log.error(error);
                deferred.reject();
            });


            return deferred.promise;
        };

        /**
         * Presents modal for new wod
         * @returns {*}
         */
        $scope.new = function() {
            var deferred = $q.defer();


            //Open modal and get the promise
            var modalPromise = $modal.open({
                templateUrl: 'create-wod-modal.tpl',
                controller: 'mainApp.wods.new.ctrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    wodGroups: function(wodGroupService, $q){
                        var deferred = $q.defer();

                        var servicePromise = wodGroupService.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    enterpriseUsersAndGroups: function(enterpriseService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = enterpriseService.users.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    }
                }
            }).result;

            //When modal is confirmed
            modalPromise.then(function(data){

                //We reload all groups
                var secondPromise = $scope.forceLoadGroups();

                secondPromise.then(function(){

                    //Upgrade wod
                    upgradeWod(data);
                    //We mark selectedWod as big one
                    $scope.selectedWod = data;
                    //Set page title
                    $scope.setTitle(data.title);

                    //Change the url for the wod...
                    changeUrlForWod($scope.selectedWod);

                    //Then resolve...
                    deferred.resolve();
                });

                secondPromise.catch(function(error){
                    $log.error(error);
                });


            });

            //When modal is dismissed
            modalPromise.catch(function(){
                deferred.reject();
            })



            return deferred.promise;
        };

        /**
         * Edit a wod
         * @param wod
         */
        $scope.edit = function(wod) {
            var deferred = $q.defer();


            //Open modal and get the promise
            var modalPromise = $modal.open({
                templateUrl: 'edit-wod-modal.tpl',
                controller: 'mainApp.wods.edit.ctrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    wod : function(){ return wod; },
                    wodGroups: function(wodGroupService, $q){
                        var deferred = $q.defer();

                        var servicePromise = wodGroupService.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    }
                }
            }).result;

            //When modal is confirmed
            modalPromise.then(function(data){
                notificationService.notifySuccess('wod edited');

                upgradeWod(data);
                $scope.selectedWod = data;

                deferred.resolve();
            });

            //When modal is dismissed
            modalPromise.catch(function(){
                deferred.reject();
            })



            return deferred.promise;
        };


        //Simple function that will order the users
        var orderUsers = function(users) {
            var fnOrder = function(a,b) {
                return a.username.localeCompare(b.username);
            };
            return users.sort(fnOrder);
        };


        /**
         * Edit users assigned
         * @param wod
         */
        $scope.editUsersAssigned = function(wod) {
            var deferred = $q.defer();


            //Open modal and get the promise
            var modalPromise = $modal.open({
                templateUrl: 'edit-usersAssigned-wod-modal.tpl',
                controller: 'mainApp.wods.editUsersAssigned.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {
                    wod : function(){ return wod; },
                    allUsers: function(enterpriseService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = enterpriseService.users.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(orderUsers(data.users.concat(data.admin_users)));
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });
                        return deferred.promise;
                    }
                }
            }).result;

            //When modal is confirmed
            modalPromise.then(function(data){

                notificationService.notifySuccess('assigned users changed');

                upgradeWod(data);
                $scope.selectedWod = data;


                deferred.resolve();
            });

            //When modal is dismissed
            modalPromise.catch(function(){
                deferred.reject();
            })



            return deferred.promise;
        };

        /**
         * Edit users assigned
         * @param wod
         */
        $scope.editUsersFollower = function(wod) {
            var deferred = $q.defer();


            //Open modal and get the promise
            var modalPromise = $modal.open({
                templateUrl: 'edit-usersFollower-wod-modal.tpl',
                controller: 'mainApp.wods.editUsersFollower.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {
                    wod : function(){ return wod; },
                    allUsers: function(enterpriseService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = enterpriseService.users.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(orderUsers(data.users.concat(data.admin_users)));
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });


                        return deferred.promise;
                    }
                }
            }).result;

            //When modal is confirmed
            modalPromise.then(function(data){
                notificationService.notifySuccess('assigned users changed');

                upgradeWod(data);
                $scope.selectedWod = data;

                deferred.resolve();
            });

            //When modal is dismissed
            modalPromise.catch(function(){
                deferred.reject();
            })



            return deferred.promise;
        };

        /**
         * Edit wod difficulty
         * @param wod
         * @param newDifficulty
         */
        $scope.changeDifficulty = function(wod, newDifficulty) {

            var deferred = $q.defer();

            if (wod.difficulty == newDifficulty) {
                deferred.resolve();
                return deferred.promise;
            };


            var changeDifficultyPromise = wodsService.setDifficulty(wod.slug, newDifficulty);

            changeDifficultyPromise.success(function(data){

                //We broadcast down
                $scope.$broadcast('wodDifficultyChanged_broadcast', {wod: data});

                var retrieveWodPromise = wodsService.getWod(wod.slug);

                retrieveWodPromise.success(function(data){
                    $scope.selectedWod = data;
                    $scope.setTitle(wod.title); //Set page title
                    upgradeWod($scope.selectedWod);
                    changeUrlForWod($scope.selectedWod);

                    notificationService.notifySuccess('difficulty changed');

                    deferred.resolve();
                });

                retrieveWodPromise.error(function(error){
                   deferred.reject();
                });
            });

            changeDifficultyPromise.error(function(error){
                deferred.reject();
            });

            return deferred.promise;
        }

        /**
        * get text translation difficulty
        * @param difficulty
        * @returns {*}
        */
        $scope.getTranslationDifficulty = function(difficulty) {
            return translator.trans('difficulty.' + difficulty, {}, 'generic');
        }

        //
        // EVENTS
        //--------------------------------------------------------------------------------------------------------------

        //On wodStatus changed...
        $scope.$on('wodStatusChanged',function(evt, data){

            //If we received notification from the selected wod...
            if ($scope.selectedWod && $scope.selectedWod.id == data.wod.id) {

                //We broadcast down
                $scope.$broadcast('wodStatusChanged_broadcast', data);


                var servicePromise = wodsService.getWod(data.wod.slug);

                servicePromise.success(function(data){
                    $scope.selectedWod = data;
                    $scope.setTitle(data.title); //Set page title
                    upgradeWod($scope.selectedWod);
                    changeUrlForWod($scope.selectedWod);
                });

                servicePromise.error(function(error){
                    $log.error(error);
                });
            }
        });

        //On wod difficulty changed
        $scope.$on('wodDifficultyChanged', function(evt, data){
            //If we received notification from the selected wod...
            if ($scope.selectedWod && $scope.selectedWod.id == data.wod.id) {

                //We broadcast down
                $scope.$broadcast('wodDifficultyChanged_broadcast', data);

                var servicePromise = wodsService.getWod(data.wod.slug);

                servicePromise.success(function(data){
                    $scope.selectedWod = data;
                    $scope.setTitle(wod.title); //Set page title
                    upgradeWod($scope.selectedWod);
                    changeUrlForWod($scope.selectedWod);
                });

                servicePromise.error(function(error){
                    $log.error(error);
                });
            }
        });

        //
        // Comments
        //--------------------------------------------------------------------------------------------------------------

        //For comment, we will set it in here
        $scope.comment = {
            commentText : ''
        };
        //TinyMCE options
        $scope.tinyMCEOptions = helperService.tinyMCEOptions;

        /**
         * Does a comment
         * @param wod
         * @param comment
         * @returns {*}
         */
        $scope.doComment = function(wod) {
            var deferred = $q.defer();

            var commentText =  $scope.comment.commentText;

            var servicePromise = wodsService.addComment(wod.slug, commentText);

            servicePromise.success(function(data){

                //Now we are going to retrieve the wod again
                var servicePromise2 = wodsService.getWod(wod.slug);

                servicePromise2.success(function(data){
                    notificationService.notifySuccess('comment added');

                    $scope.selectedWod = data;
                    upgradeWod($scope.selectedWod);
                    $scope.comment.commentText = '';

                    deferred.resolve();
                });
            });

            servicePromise.error(function(error){
                $log.error(error);
                deferred.reject(error);
            });


            return deferred.promise;
        };

        //
        // Helpers
        //--------------------------------------------------------------------------------------------------------------

        //Helper function that will sprint status icon
        $scope.printStatusIcon = function(status){ return helperService.printStatusIcon(status); }
        //Helper function that will print color
        $scope.printColorDifficulty = function(difficulty) { return helperService.printColorDifficulty(difficulty); };

        //
        // Subwods
        //--------------------------------------------------------------------------------------------------------------

        //Controls the subwod title
        $scope.subWodInputTitle = '';
        //Controls visibility for subwod form
        $scope.showFormNewSubWod = false;

        /**
         * Retrieves the percent of wods that are closed
         */
        $scope.getPercentClosed = function() {
            var wod = $scope.getSelectedWod();

            //No wod? return 0
            if(!wod) return 0;

            var numClosed = 0;
            var total = wod.sub_wods.length;

            for(var i = 0; i < total; i++) {
                if (wod.sub_wods[i].status != 'open') numClosed++;
            }

            var percent = numClosed * 100 / total;

            return Math.floor(percent);
        };

        /**
         * Creates a new subwod
         * @param title
         * @returns {*}
         */
        $scope.createSubWod = function(title) {
            var deferred = $q.defer();

            var currentWod = $scope.getSelectedWod();

            var servicePromise = wodsService.subwods.create(currentWod.slug, title);

            //When the subwod is created...
            servicePromise.success(function(data){

                currentWod.sub_wods.push(data);
                $scope.subWodInputTitle = '';
                $scope.showFormNewSubWod = false;
                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        /**
         * Toggles status of a subwod
         * @param subwod
         */
        $scope.toggleStatusSubWod = function(subwod) {
            var deferred = $q.defer();

            var servicePromise;

            if(subwod.status == 'open') {
                servicePromise = wodsService.subwods.close(subwod.id);
            } else {
                servicePromise = wodsService.subwods.open(subwod.id);
            }

            servicePromise.success(function(data){

                var idx = -1;
                for (var i = 0; i < $scope.selectedWod.sub_wods.length; i++) {
                    if ($scope.selectedWod.sub_wods[i].id == data.id) {
                        idx = i;
                        break;
                    }
                }

                $scope.selectedWod.sub_wods[i] = data;

                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        /**
         * Deletes a subwod
         * @param subwod
         * @returns {*}
         */
        $scope.deleteSubWod = function(subwod) {
            var deferred = $q.defer();

            var currentWod = $scope.getSelectedWod();
            var servicePromise = wodsService.subwods.delete(subwod.id);

            servicePromise.success(function(data){
                var idx = -1;

                for(var i = 0; i < currentWod.sub_wods.length; i++) {
                    if(currentWod.sub_wods[i].id == subwod.id) {
                        idx = i;
                        break;
                    }
                }
                //Delete
                currentWod.sub_wods.splice(idx, 1);


                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        //
        // Wod groups
        //--------------------------------------------------------------------------------------------------------------

        //Edit a wodGroup
        $scope.editWodGroup = function(wodGroup) {

            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-wod-group-modal.tpl',
                controller: 'mainApp.wodGroup.edit.modalCtrl',
                size: '',
                backdrop: 'static',
                resolve: {
                    wodGroup: function(wodGroupService, $q){
                        var deferred = $q.defer();

                        var servicePromise = wodGroupService.get(wodGroup.id);

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    workGroups: function(workGroupService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = workGroupService.getAllWithUsersInfo();

                        servicePromise.success(function(data){
                            deferred.resolve(data.workGroups);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    enterpriseUsersAndGroups: function(enterpriseService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = enterpriseService.users.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                }
            });

            modal.result.then(function(data){

                //We must search wodGroup in data.groups
                for(var i = 0; i < $scope.data.groups.length; i++) {
                    if($scope.data.groups[i].id == wodGroup.id) {
                        $scope.data.groups[i].name = data.name;
                        break;
                    }
                }

                //data.filters.groups
                for(var i = 0; i < $scope.data.filters.groups.length; i++) {
                    if($scope.data.filters.groups[i].id == wodGroup.id) {
                        $scope.data.filters.groups[i] = data;
                        break;
                    }
                }

                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Deletes a wodgroup
        $scope.deleteWodGroup = function(wodGroup) {

            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'deleteWodGroupConfirmationBox.tpl',
                controller: 'mainApp.modals.confirmation.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {}
            });

            modal.result.then(function() {
                var servicePromise = wodGroupService.delete(wodGroup.id);

                servicePromise.then(function(){

                    //We must search wodGroup in data.groups
                    var idx = -1;
                    for(var i = 0; i < $scope.data.groups.length; i++) {
                        if($scope.data.groups[i].id == wodGroup.id) {
                            idx = i;
                            break;
                        }
                    }
                    if(idx >= 0)  $scope.data.groups.splice(idx, 1);

                    //Remove from data.filters.groups
                    idx = -1;
                    for(var i = 0; i < $scope.data.filters.groups.length; i++) {
                        if($scope.data.filters.groups[i].id == wodGroup.id) {
                            idx = i;
                            break;
                        }
                    }
                    if(idx >= 0)  $scope.data.filters.groups.splice(idx, 1);


                    //Remove from filtersGroups
                    idx = -1;
                    for(var i = 0; i < $scope.filters.groups.length; i++) {
                        if($scope.filters.groups[i] == wodGroup.id) {
                            idx = i;
                            break;
                        }
                    }
                    if (idx >= 0) $scope.filters.groups.splice(idx, 1);

                    deferred.resolve();

                });

                servicePromise.catch(function(){
                    deferred.reject();
                });
            });

            modal.result.catch(function () {
                deferred.reject();
            });

            return deferred.promise;
        };

    };

})(angular);