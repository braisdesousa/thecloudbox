(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.errors.noWorkGroups.ctrl', [
        '$log', '$scope', '$q', '$modal', '$window', 'routing', noWorkGroupsCtrl
    ]);

    //No workGroups available controller
    function noWorkGroupsCtrl($log, $scope, $q, $modal, $window, routing)
    {
        $scope._name = '';
        $scope.setTitle('There are no workGroups available');

        /**
         * Opens new workGroup modal
         * @returns {*}
         */
        $scope.newWorkGroup = function(){
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-workGroup-modal.tpl',
                controller: 'mainApp.workGroup.new.modalCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    reload: function(){ return true; }
                }
            });

            //Really we never are going to reach this
            modal.result.then(function(data){
                deferred.resolve();
                $window.location = routing.generate('ng-select_workGroup', {'id' : data.id});
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * Opens new enterprise modal
         * @returns {*}
         */
        $scope.newEnterprise = function(){
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-enterprise-modal.tpl',
                controller: 'mainApp.enterprise.new.modalCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    shouldReload : function(){ return true; }
                }
            });

            //Really we never are going to reach this
            modal.result.then(function(data){
                deferred.resolve();
                $window.location = routing.generate('ng-select_enterprise',{'id' : data.id});
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };
    };

})(angular);