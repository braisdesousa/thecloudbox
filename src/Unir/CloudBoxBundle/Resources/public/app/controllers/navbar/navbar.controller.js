(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.navBar.mainCtrl',['$log', '$scope', '$q', '$location', '$timeout', 'routing',
        '$route', '$window', '$location', '$modal', 'notificationService', navBarMainCtrl]);

    /**
     * Controller for navBar
     */
    function navBarMainCtrl($log, $scope, $q, $location, $timeout, routing,
                            $route, $window, $location, $modal, notificationService) {
        //Scope name
        $scope._name = 'mainApp.navBar.mainCtrl';

        /**
         * opens ne wod modal
         * @returns {*}
         */
        $scope.newWod = function() {

            var deferred = $q.defer();


            //Open modal and get the promise
            var modalPromise = $modal.open({
                templateUrl: 'create-wod-modal.tpl',
                controller: 'mainApp.wods.new.ctrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    wodGroups: function(wodGroupService, $q){
                        var deferred = $q.defer();

                        var servicePromise = wodGroupService.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    enterpriseUsersAndGroups: function(enterpriseService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = enterpriseService.users.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    }
                }
            }).result;

            //When modal is confirmed
            modalPromise.then(function(data){
                //Go to the wod page
                $location.path('/wods/').hash(data.slug);
                deferred.resolve();
            });


            //When modal is dismissed
            modalPromise.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * Opens new wodgroup modal
         * @returns {*}
         */
        $scope.newWodGroup = function() {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-wod-group-modal.tpl',
                controller: 'mainApp.wodGroup.new.modalCtrl',
                size: '',
                backdrop: 'static',
                resolve: {
                    currentWorkGroup : function(){
                        return $scope.getMainData().workGroup;
                    },
                    workGroups: function(workGroupService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = workGroupService.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data.workGroups);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    enterpriseUsersAndGroups: function(enterpriseService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = enterpriseService.users.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    shouldReload: function(){ return true; }
                }
            });

            modal.result.then(function(data){

                notificationService.notifySuccess('You have created the wodGroup');

                //This will be prevented cuz we aren't going to close modal if is in another workGroup...
                if ($scope.getMainData().workGroup.id != data.workGroup_id) {
                    $window.location = routing.generate('ng-select_workGroup', {'id' : data.workGroup_id});
                } else {


                    //Retrieve current scope
                    var currentScope = $route.current.scope;

                    //We are on wods screen?
                    if(currentScope._name && currentScope._name == 'mainApp.wods.main.ctrl') {

                        var itm = {
                            id: data.id,
                            name: data.name,
                            selected : true
                        };

                        //We need put the current group in filter groups data
                        currentScope.data.filters.groups.push(itm);
                        currentScope.filters.groups.push(itm.id);

                        currentScope.forceLoadGroups();

                    } else {
                        $location.path(routing.generate('ng_wods'));
                    }
                }

                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * Opens new workGroup modal
         * @returns {*}
         */
        $scope.newWorkGroup = function(){
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-workGroup-modal.tpl',
                controller: 'mainApp.workGroup.new.modalCtrl',
                size: '',
                backdrop: 'static',
                resolve: {
                    reload: function(){ return true; }
                }
            });

            //Really we never are going to reach this
            modal.result.then(function(data){
                deferred.resolve();
                $window.location = routing.generate('ng-select_workGroup', {'id' : data.id});
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * Opens new user modal
         * @returns {*}
         */
        $scope.newUser = function(){
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-user-modal.tpl',
                controller: 'mainApp.user.new.modalCtrl',
                size: '',
                backdrop: 'static'
            });

            modal.result.then(function(data){

                notificationService.notifySuccess('We have sended an email to <b>' + data.email + '</b>');
                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * Opens new enterprise modal
         * @returns {*}
         */
        $scope.newEnterprise = function(){
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-enterprise-modal.tpl',
                controller: 'mainApp.enterprise.new.modalCtrl',
                size: '',
                backdrop: 'static',
                resolve: {
                    shouldReload : function(){ return true; }
                }
            });

            //Really we never are going to reach this
            modal.result.then(function(data){
                deferred.resolve();
                $window.location = routing.generate('ng-select_enterprise',{'id' : data.id});
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

    };

})(angular);