(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.wodGroup.edit.modalCtrl', [
        '$log', '$scope', '$q', '$window', 'routing', '$timeout',
        'wodGroupService', 'helperService', '$modalInstance',
        'wodGroup', 'workGroups', 'enterpriseUsersAndGroups', wodGroupNewModalCtrl]);


    //Simple function that will order the users
    var orderUsers = function(users) {
        var fnOrder = function(a,b) {
            return a.username.localeCompare(b.username);
        };
        return users.sort(fnOrder);
    };

    //Simple function that will order groups
    var orderGroups = function(groups) {
        var fnOrder = function(a,b) {
            return a.name.localeCompare(b.name);
        };
        return groups.sort(fnOrder);
    };

    /**
     * New WodGroup modal controller
     */
    function wodGroupNewModalCtrl($log, $scope, $q, $window, routing, $timeout,
            wodGroupService, helperService, $modalInstance,
            wodGroup, workGroups, enterpriseUsersAndGroups)
    {

        $scope._name = 'mainApp.wodGroup.edit.modalCtrl';
        $scope.title = 'Edit wod group: <strong>' + wodGroup.name + '</strong>';
        var formName = 'unir_ticketmanagerbundle_wodgroup';

        $scope.formData = {};
        $scope.formData[formName] = {};

        $scope.workGroups = workGroups;
        $scope.userGroups = orderGroups(enterpriseUsersAndGroups.groups);
        $scope.users = orderUsers(enterpriseUsersAndGroups.users.concat(enterpriseUsersAndGroups.admin_users));


        $scope.formSubmit = function(formData){
            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = wodGroupService.update(wodGroup.id, formData);

            servicePromise.success(function(data){
                $modalInstance.close(data);
                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };

        $scope.init = function() {

            $scope.formData = {};
            $scope.formData[formName] = {};
            $scope.formData[formName]['name'] = wodGroup.name;
            $scope.formData[formName]['workGroup'] = wodGroup.workGroup.id;
            $scope.formData[formName]['availableViewers'] = wodGroup.view_by;
            $scope.formData[formName]['description'] = wodGroup.description;
            $scope.formData[formName]['users'] = [];
            $scope.formData[formName]['groupMembers'] = [];


            for(var i = 0; i < wodGroup.users.length; i++) {
                $scope.formData[formName]['users'].push(wodGroup.users[i].id);
            }

            for(var i = 0; i < wodGroup.group_members.length; i++) {
                $scope.formData[formName]['groupMembers'].push(wodGroup.group_members[i].id);
            }
        };

        $scope.tinyMCEOptions = helperService.tinyMCEOptions;
    };

})(angular);