(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.wodGroup.new.modalCtrl', [
        '$log', '$scope', '$q', '$window', 'routing',
        'wodGroupService', 'helperService', '$modalInstance',
        'currentWorkGroup', 'workGroups', 'enterpriseUsersAndGroups', 'shouldReload', wodGroupNewModalCtrl]);

    //Simple function that will order the users
    var orderUsers = function(users) {
        var fnOrder = function(a,b) {
            return a.username.localeCompare(b.username);
        };
        return users.sort(fnOrder);
    };

    //Simple function that will order groups
    var orderGroups = function(groups) {
        var fnOrder = function(a,b) {
            return a.name.localeCompare(b.name);
        };
        return groups.sort(fnOrder);
    };

    /**
     * New WodGroup modal controller
     */
    function wodGroupNewModalCtrl($log, $scope, $q, $window, routing,
            wodGroupService, helperService, $modalInstance,
            currentWorkGroup, workGroups, enterpriseUsersAndGroups, shouldReload)
    {

        $scope._name = 'mainApp.wodGroup.new.modalCtrl';
        $scope.title = 'New wodGroup';
        var formName = 'unir_ticketmanagerbundle_wodgroup';

        $scope.workGroups = workGroups;
        $scope.userGroups = orderGroups(enterpriseUsersAndGroups.groups);
        $scope.users = orderUsers(enterpriseUsersAndGroups.users.concat(enterpriseUsersAndGroups.admin_users));



        $scope.formData = {};
        $scope.formData[formName] = {};
        $scope.formData[formName]['workGroup'] = currentWorkGroup.id;
        $scope.formData[formName]['availableViewers'] = 'all';

        $scope.formSubmit = function(formData){
            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = wodGroupService.create(formData);

            servicePromise.success(function(data){

                data.workGroup_id = formData[formName]['workGroup'];

                //IF we need to reload and isn't the same workGroup we redirect to the properly workGroup page
                if(shouldReload && currentWorkGroup.id != data.workGroup_id) {
                    $window.location = routing.generate('ng-select_workGroup', {'id' : data.workGroup_id});
                } else { //Else close modal
                    $modalInstance.close(data);
                    deferred.resolve();
                }
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };

        $scope.tinyMCEOptions = helperService.tinyMCEOptions;

    };

})(angular);