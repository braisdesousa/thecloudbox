(function(ng){
    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.wodGroup.list.ctrl',[
        '$log', '$scope', '$modal', '$q', 'wodGroupService', 'notificationService', 'wodGroups', wodGroupListCtrl]);

    function wodGroupListCtrl($log, $scope, $modal, $q, wodGroupService,  notificationService, wodGroups) {

        $scope._name = 'mainApp.wodGroup.list.ctrl';
        $scope.wodGroups = wodGroups;
        $scope.setTitle('Wod Groups List').setMenuActive('wodGroups');

        //Creates a new wodGroup
        $scope.new = function() {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-wod-group-modal.tpl',
                controller: 'mainApp.wodGroup.new.modalCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    currentWorkGroup : function(){
                        return $scope.getMainData().workGroup;
                    },
                    workGroups: function(workGroupService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = workGroupService.getAllWithUsersInfo();

                        servicePromise.success(function(data){
                            deferred.resolve(data.workGroups);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    enterpriseUsersAndGroups: function(enterpriseService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = enterpriseService.users.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    shouldReload: function(){ return false; }
                }
            });

            modal.result.then(function(data){

                notificationService.notifySuccess('You have created the wodGroup');

                //If we have created a wodGroup on another workGroup
                if ($scope.getMainData().workGroup.id != data.workGroup_id) {
                    //Don't do nothing
                }

                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Edits a wodGroup
        $scope.edit = function(wodGroup) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-wod-group-modal.tpl',
                controller: 'mainApp.wodGroup.edit.modalCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    wodGroup: function(wodGroupService, $q){
                        var deferred = $q.defer();

                        var servicePromise = wodGroupService.get(wodGroup.id);

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    workGroups: function(workGroupService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = workGroupService.getAllWithUsersInfo();

                        servicePromise.success(function(data){
                            deferred.resolve(data.workGroups);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    enterpriseUsersAndGroups: function(enterpriseService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = enterpriseService.users.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                }
            });

            modal.result.then(function(data){

                //If the wodGroup have switched the workGroup
                if ($scope.getMainData().workGroup.id != data.workGroup_id) {

                    //We must search wodGroup in wodGroups
                    var idx = -1;
                    //Remove wodGroup from scope
                    for(var i = 0; i < $scope.wodGroups.length; i++) {
                        if($scope.wodGroups[i].id == wodGroup.id) {
                            idx = i;
                            break;
                        }
                    }
                    if (idx >= 0) $scope.wodGroups.splice(idx, 1);

                } else {
                    //We must search wodGroup in data.groups
                    for(var i = 0; i < $scope.wodGroups.length; i++) {
                        if($scope.wodGroups[i].id == wodGroup.id) {
                            $scope.wodGroups[i].name = data.name;
                            break;
                        }
                    }
                }



                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Deletes a wodGroup
        $scope.delete = function(wodGroup) {

            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'deleteWodGroupConfirmationBox.tpl',
                controller: 'mainApp.modals.confirmation.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {}
            });

            modal.result.then(function() {
                var servicePromise = wodGroupService.delete(wodGroup.id);

                servicePromise.then(function(){

                    //We must search wodGroup in wodGroups
                    var idx = -1;
                    //Remove wodGroup from scope
                    for(var i = 0; i < $scope.wodGroups.length; i++) {
                        if($scope.wodGroups[i].id == wodGroup.id) {
                            idx = i;
                            break;
                        }
                    }
                    if (idx >= 0) $scope.wodGroups.splice(idx, 1);

                    deferred.resolve();

                });

                servicePromise.catch(function(){
                    deferred.reject();
                });
            });

            modal.result.catch(function () {
                deferred.reject();
            });

            return deferred.promise;
        };
    };

})(angular);