(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.userGroup.new.modalCtrl', [
        '$log', '$scope', '$q', '$timeout', 'usersService', '$modalInstance', userNewModalCtrl]);

    /**
     * New User Group modal controller
     * @param $log
     * @param $scope
     * @param $q
     * @param $modalInstance
     */
    function userNewModalCtrl($log, $scope, $q, $timeout, usersService, $modalInstance){

        $scope._name = 'mainApp.userGroup.new.modalCtrl';
        $scope.title = 'Create new user group';
        var formName = 'unir_ticketmanagerbundle_group';

        $scope.formData = {};
        $scope.formData[formName] = {};

        $scope.formSubmit = function(formData){

            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = usersService.userGroups.create(formData);


            servicePromise.success(function(data){
                deferred.resolve();
                $modalInstance.close(data);
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;

        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };
    };

})(angular);