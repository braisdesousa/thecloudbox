(function(ng){

    var mainApp = ng.module('mainApp');
    mainApp.controller('mainApp.user.changePassword.modalCtrl',[
        '$log', '$scope', '$q', 'usersService', '$modalInstance', 'user', userEditController]);

    function userEditController($log, $scope, $q, usersService, $modalInstance, user){

        $scope._name = 'mainApp.user.changePassword.modalCtrl';
        $scope.title = 'Change password for user: <strong>' + user.username+ '</strong>';
        var formName = 'unir_ticketmanaget_update_password';

        $scope.formData = {};


        $scope.formSubmit = function(formData){

            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();


            var clonedData = ng.copy(formData);

            var servicePromise = usersService.changePassword(user.id, clonedData);

            servicePromise.success(function(data){
                $modalInstance.close(data);
                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;

        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };

    };

})(angular);