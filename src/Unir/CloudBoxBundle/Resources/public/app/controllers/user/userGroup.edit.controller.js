(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.userGroup.edit.modalCtrl', [
        '$log', '$scope', '$q', '$timeout', 'usersService', '$modalInstance', 'userGroup', userNewModalCtrl]);

    /**
     * New User Group modal controller
     * @param $log
     * @param $scope
     * @param $q
     * @param $modalInstance
     */
    function userNewModalCtrl($log, $scope, $q, $timeout, usersService, $modalInstance, userGroup){

        $scope._name = 'mainApp.userGroup.edit.modalCtrl';
        $scope.title = 'Edit user group: <strong>' + userGroup.name+ '</strong>';
        var formName = 'unir_ticketmanagerbundle_group';

        $scope.formData = {};
        $scope.formData[formName] = {};

        $scope.formSubmit = function(formData){

            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = usersService.userGroups.update(userGroup.id, formData);


            servicePromise.success(function(data){
                deferred.resolve();
                $modalInstance.close(data);
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;

        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };

        $scope.init = function(){
            $scope.formData = {};
            $scope.formData[formName] = {};
            $scope.formData[formName]['name'] = userGroup.name;
            $scope.formData[formName]['users'] = [];

            for (var i = 0; i < userGroup.users.length; i++) {
                $scope.formData[formName]['users'].push(userGroup.users[i].id);
            }

            //Fuk yu
            $timeout(function(){

                jQuery("[name='" + formName + "[users][]']")
                    .val($scope.formData[formName]['users'])
                    .multipleSelect('refresh');

            }, 500);
        };
    };

})(angular);