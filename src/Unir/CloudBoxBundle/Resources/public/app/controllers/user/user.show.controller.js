(function(ng){

    var mainApp = ng.module('mainApp');
    mainApp.controller('mainApp.user.show.ctrl', [
        '$log', '$scope', '$q', '$modal', '$window', '$timeout',
        'notificationService', 'usersService' , 'user', userShowCtrl]);

    /**
     * User show
     */
    function userShowCtrl($log, $scope, $q, $modal, $window, $timeout,
                          notificationService, usersService, user) {

        $scope._name = 'mainApp.user.show.ctrl';

        $scope.user = user;
        $scope.userActivities = mergeActivitiesAndComments($scope.user);
        $scope.setTitle(user.username).setMenuActive('user_show');

        //Returns if current user can edit another user
        $scope.currentUserCanEdit = function(user) {
            //TODO here is the logic for determine if user can edit another user or self

            // if userShow == userLogged
            if ($scope.getMainData().user.id == user.id ) {
                return true;
            }

            // if admin
            if($scope.getMainData().user.isRoleAdmin()) {
                return true;
            }

            // if owner
            if ($scope.getMainData().user.isRoleOwner()){
                return true;
            }

            return false;
        };

        //Show edit user modal
        $scope.edit = function(user) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'edit-user-modal.tpl',
                controller: 'mainApp.user.edit.modalCtrl',
                size: '',
                backdrop: 'static',
                resolve: {
                    user: function() {
                        return user;
                    }
                }
            });

            modal.result.then(function(data){
                $scope.user = data;
                $scope.userActivities = mergeActivitiesAndComments($scope.user);;
                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Deletes an user
        $scope.delete = function(user) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'deleteUserConfirmationBox.tpl',
                controller: 'mainApp.modals.confirmation.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {}
            });

            modal.result.then(function() {

                var servicePromise = usersService.delete(user.id);
                servicePromise.success(function () {
                    deferred.resolve();
                    //Redirect back
                    $window.history.back();
                });
                servicePromise.error(function () {
                    deferred.reject();
                });
            });

            modal.result.catch(function () {
                deferred.reject();
            });

            return deferred.promise;
        };

        //Changes user password
        $scope.changePassword = function(user){
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'change-password-user-modal.tpl',
                controller: 'mainApp.user.changePassword.modalCtrl',
                size: '',
                backdrop: 'static',
                resolve: {
                    user: function() {
                        return user;
                    }
                }
            });

            modal.result.then(function(data){
                notificationService.notifySuccess('Password changed');
                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;

        };

        //Fn that will help to merge activities and comments
        function mergeActivitiesAndComments(user) {

            var mergedActivitiesAndComments = [];

            if(!user || !user.wod_activities || !user.wod_comments) {
                return {};
            }

            for (var i = 0; i < user.wod_activities.length; i++) {
                var act =  user.wod_activities[i];
                var itm = {};
                itm.action = act.action;
                itm.created =  new Date(act.created);
                itm.description = act.description;
                itm.id = act.id;
                itm.wod = act.wod;
                itm.user = user;

                mergedActivitiesAndComments.push(itm);
            }

            for (var i = 0; i < user.wod_comments.length; i++) {

                var comm = user.wod_comments[i];
                var itm = {};
                itm.comment = comm.comment;
                itm.created = new Date(comm.created);
                itm.id = comm.id;
                itm.wod = comm.wod;
                itm.user = comm.user;


                mergedActivitiesAndComments.push(itm);
            };

            //Sort items
            mergedActivitiesAndComments = mergedActivitiesAndComments.sort(function(a, b){

                var aTime = a.created.getTime();
                var bTime = b.created.getTime();

                return aTime > bTime ? -1 : 1;
            });


            var result = {};
            result.elements = mergedActivitiesAndComments;

            //Now make functions por paginate


            //Items per page
            result.itemsPerPage = 10;

            //Retrieve total items
            result.totalItems = function() { return result.elements.length; };

            //Retrieve total pages
            result.totalPages = function() {
                return Math.ceil(result.elements.length  / result.itemsPerPage);
            };

            //Get items
            result.get = function(page) {
                //page out of index, return
                if (page <= 0 || page > result.totalPages()) return [];

                var startingIndex = (page - 1) * result.itemsPerPage;
                var maxIndex = startingIndex + result.itemsPerPage;

                if (maxIndex > result.elements.length) {
                    maxIndex = result.elements.length;
                }

                var itms = [];

                for (var i = startingIndex; i < maxIndex; i++) {
                    itms.push(result.elements[i]);
                }

                return itms;
            }


            return result;
        };

    };

})(angular);