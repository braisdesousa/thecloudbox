(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.user.list.ctrl', [
        '$log', '$scope', '$q', '$modal', 'notificationService', 'usersService' ,'users', 'groups', userListCtrl]);

    /**
     * List users controller
     */
    function userListCtrl($log, $scope, $q, $modal, notificationService, usersService, users, groups){

        $scope._name = 'mainApp.user.list.ctrl';
        $scope.setTitle('users list').setMenuActive('users');

        //Initialize scope
        $scope.users = users;
        $scope.groups = groups;
        $scope.groupSelected = null;
        $scope.allUsers = users;

        //Sorts users
        var sortUsersFn = function(usersArr) {

            var sortFn = function(a,b) {
                return a.username.localeCompare(b.username);
            };

            return usersArr.sort(sortFn);
        };

        $scope.users = sortUsersFn($scope.users);

        /**
         * select group users
         * @param group
         */
        $scope.selectGroupUsers = function (group) {
            if (group) {
                $scope.groupSelected = group;
                $scope.users = group.users;
            } else {
                $scope.groupSelected = null;
                $scope.users = $scope.allUsers;
            }

            $scope.users = sortUsersFn($scope.users);
        }

        //New user modal
        $scope.newUser = function(){
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-user-modal.tpl',
                controller: 'mainApp.user.new.modalCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {}
            });


            modal.result.then(function(data){
                notificationService.notifySuccess('We have sended an email to <b>' + data.email + '</b>');
                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Creates new user gorup
        $scope.newUserGroup = function() {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-group-users-modal.tpl',
                controller: 'mainApp.userGroup.new.modalCtrl',
                size: '',
                backdrop: 'static',
                resolve: {}
            });


            modal.result.then(function(data){
                //Put group on groups scope
                $scope.groups.push(data);
                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Edits an user group
        $scope.editUserGroup = function(userGroup) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-group-users-modal.tpl',
                controller: 'mainApp.userGroup.edit.modalCtrl',
                size: '',
                backdrop: 'static',
                resolve: {
                    userGroup: function(){ return userGroup; }
                }
            });


            modal.result.then(function(data){

                for(var i = 0; i < $scope.groups.length; i++) {
                    if($scope.groups[i].id == userGroup.id) {
                        $scope.groups[i] = data;
                        break;
                    }
                }

                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Deletes user group
        $scope.deleteUserGroup = function(userGroup) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'deleteGroupUserConfirmationBox.tpl',
                controller: 'mainApp.modals.confirmation.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {}
            });

            modal.result.then(function() {
                var servicePromise = usersService.userGroups.delete(userGroup.id);

                servicePromise.then(function(){

                    var idx = -1;
                    for(var i = 0; i < $scope.groups.length; i++) {
                        if ($scope.groups[i].id == userGroup.id) {
                            idx = i;
                            break;
                        }
                    };

                    $scope.groups.splice(idx, 1);
                    if ($scope.groupSelected && $scope.groupSelected.id == userGroup.id) {
                        $scope.groupSelected = null;
                        $scope.users = $scope.allUsers;
                    }

                    deferred.resolve();

                });

                servicePromise.catch(function(){
                   deferred.reject();
                });
            });

            modal.result.catch(function () {
                deferred.reject();
            });

            return deferred.promise;
        };
    };

})(angular);