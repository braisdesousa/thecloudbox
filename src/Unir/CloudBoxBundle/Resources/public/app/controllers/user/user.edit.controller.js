(function(ng){

    var mainApp = ng.module('mainApp');
    mainApp.controller('mainApp.user.edit.modalCtrl',[
        '$log', '$scope', '$q', 'usersService', '$modalInstance', 'user', userEditController]);

    function userEditController($log, $scope, $q, usersService, $modalInstance, user){

        $scope._name = 'mainApp.user.edit.modalCtrl';
        $scope.title = 'Edit user: <strong>' + user.username+ '</strong>';
        var formName = 'unir_ticketmanagerbundle_user_mail';

        $scope.formData = {};
        $scope.formData[formName] = {};
        $scope.formData[formName]['username'] = user.username;
        $scope.formData[formName]['email'] = user.email;

        $scope.formSubmit = function(formData){

            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = usersService.update(user.id, formData);

            servicePromise.success(function(data){
                $modalInstance.close(data);
                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;

        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };

    };

})(angular);