(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.user.new.modalCtrl', [
        '$log', '$scope', '$q', 'usersService', '$modalInstance', userNewModalCtrl]);

    /**
     * New User modal controller
     * @param $log
     * @param $scope
     * @param $q
     * @param $modalInstance
     */
    function userNewModalCtrl($log, $scope, $q, usersService, $modalInstance){

        $scope._name = 'mainApp.user.new.modalCtrl';
        $scope.title = 'Create new user';

        var formName = 'unir_ticketmanagerbundle_user';

        $scope.formData = {};
        $scope.formData[formName] = {};

        $scope.formSubmit = function(formData){

            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = usersService.create(formData);

            servicePromise.success(function(data){

                var result = {
                    data : data,
                    email: formData[formName]['email']
                };

                $modalInstance.close(result);
                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;

        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };
    };

})(angular);