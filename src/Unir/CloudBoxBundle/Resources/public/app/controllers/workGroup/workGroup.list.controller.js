(function(ng){

    var mainApp = ng.module('mainApp');
    mainApp.controller('mainApp.workGroup.list.ctrl', [
        '$log', '$scope', '$q', '$modal', '$location', 'notificationService', 'routing', 'workGroupService' , 'workGroups', workGroupListCtrl]);

    /**
     * List workGroups controller
     */
    function workGroupListCtrl($log, $scope, $q, $modal, $location, notificationService, routing, workGroupService, workGroups){

        $scope._name = 'mainApp.workGroup.list.ctrl';

        $scope.workGroups = workGroups;

        $scope.setTitle('Projects List').setMenuActive('workGroups');

        //Open new workGroup modal
        $scope.new = function() {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-workGroup-modal.tpl',
                controller: 'mainApp.workGroup.new.modalCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    reload : function(){ return false;}
                }
            });

            //Really we never are going to reach this
            modal.result.then(function(data){
                $scope.workGroups.push(data);
                notificationService.notifySuccess('WorkGroup ' + data.name + ' created');
                //Put workGroup on mainDataScope
                $scope.getMainData().workGroups.push({
                    id : data.id,
                    name: data.name
                });
                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Open modal for edit workGroup
        $scope.edit = function(workGroup) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-workGroup-modal.tpl',
                controller: 'mainApp.workGroup.edit.modalCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    workGroup: function(){ return workGroup; }
                }
            });

            //Really we never are going to reach this
            modal.result.then(function(data){

                var idx = -1;
                //Find workGroup in the elements
                for(var i = 0; i < $scope.workGroups.length; i++) {
                    if ($scope.workGroups[i].id == workGroup.id) {
                        idx = i;
                        break;
                    }
                }

                $scope.workGroups[idx] = data;

                //Same with main data
                for(var i = 0; i < $scope.getMainData().workGroups.length; i++) {
                    if ($scope.getMainData().workGroups[i].id == workGroup.id) {
                        idx = i;
                        break;
                    }
                }
                $scope.getMainData().workGroups[idx] = {
                    id : data.id,
                    name: data.name
                };

                notificationService.notifySuccess('WorkGroup ' + workGroup.name + ' updated');
                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * Delete workGroup
         */
        $scope.delete = function(workGroup) {

            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'deleteProjectConfirmationBox.tpl',
                controller: 'mainApp.modals.confirmation.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {}
            });

            modal.result.then(function () {

                var servicePromise = workGroupService.delete(workGroup.id);
                servicePromise.success(function () {
                    var idx = -1;
                    //Find workGroup in the elements
                    for (var i = 0; i < $scope.workGroups.length; i++) {
                        if ($scope.workGroups[i].id == workGroup.id) {
                            idx = i;
                            break;
                        }
                    }

                    $scope.workGroups.splice(idx, 1);

                    //Same with main data
                    for (var i = 0; i < $scope.getMainData().workGroups.length; i++) {
                        if ($scope.getMainData().workGroups[i].id == workGroup.id) {
                            idx = i;
                            break;
                        }
                    }
                    $scope.getMainData().workGroups.splice(idx, 1);

                    notificationService.notifySuccess('WorkGroup ' + workGroup.name + ' deleted');

                    //Once we are here, we must consider if the deleted workGroup is the same as is active
                    if ($scope.getMainData().workGroup.id == workGroup.id) {

                        //There are more workGroups
                        if ($scope.getMainData().workGroups.length > 0) {
                            //Pick first and reload
                            $scope.goToRoute('ng-select_workGroup', {'id': $scope.getMainData().workGroups[0].id}, true);
                        } else {
                            $location.path(routing.generate('ng_no_workGroups'));
                        }
                    }
                });
            });

            modal.result.catch(function () {
                deferred.reject();
            });

            return deferred.promise;
        }
    };
})(angular);