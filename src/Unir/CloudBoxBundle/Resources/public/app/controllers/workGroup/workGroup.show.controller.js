(function(ng){

    var mainApp = ng.module('mainApp');
    mainApp.controller('mainApp.workGroup.show.ctrl', [
        '$log', '$scope', '$q', '$modal', '$window', '$location', 'notificationService', 'routing', 'workGroupService' ,'workGroup', workGroupShowCtrl]);

    //Show a workGroup
    function workGroupShowCtrl($log, $scope, $q, $modal, $window, $location, notificationService, routing, workGroupService, workGroup){

        $scope._name = 'mainApp.workGroup.show.ctrl';

        $scope.workGroup = workGroup;

        $scope.setTitle(workGroup.name).setMenuActive('workGroup_show');

        //Open modal for edit workGroup
        $scope.edit = function(workGroup) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-workGroup-modal.tpl',
                controller: 'mainApp.workGroup.edit.modalCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    workGroup: function(){ return workGroup; }
                }
            });

            //Really we never are going to reach this
            modal.result.then(function(data){

                $scope.workGroup = data;

                var idx = -1
                //Search workGroup in main data
                for(var i = 0; i < $scope.getMainData().workGroups.length; i++) {
                    if ($scope.getMainData().workGroups[i].id == workGroup.id) {
                        idx = i;
                        break;
                    }
                }
                $scope.getMainData().workGroups[idx] = {
                    id : data.id,
                    name: data.name
                };

                notificationService.notifySuccess('WorkGroup ' + workGroup.name + ' updated');
                deferred.resolve();
            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * Delete workGroup
         */
        $scope.delete = function(workGroup) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'deleteWorkGroupConfirmationBox.tpl',
                controller: 'mainApp.modals.confirmation.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {}
            });

            modal.result.then(function() {

                var servicePromise = workGroupService.delete(workGroup.id);
                servicePromise.success(function () {

                    var idx = -1;
                    //Same with main data
                    for (var i = 0; i < $scope.getMainData().workGroups.length; i++) {
                        if ($scope.getMainData().workGroups[i].id == workGroup.id) {
                            idx = i;
                            break;
                        }
                    }

                    $scope.getMainData().workGroups.splice(idx, 1);

                    notificationService.notifySuccess('WorkGroup ' + workGroup.name + ' deleted');

                    //Once we are here, we must consider if the deleted workGroup is the same as is active
                    if ($scope.getMainData().workGroup.id == workGroup.id) {

                        //There are more workGroups
                        if ($scope.getMainData().workGroups.length > 0) {
                            //Pick first and reload
                            $scope.goToRoute('ng-select_workGroup', {'id': $scope.getMainData().workGroups[0].id}, true);
                        } else {
                            $location.path(routing.generate('ng_no_workGroups'));
                        }
                    } else { //Is not the active workGroup
                        //Redirect back
                        $window.history.back();
                    }
                })

            });

            modal.result.catch(function () {
                deferred.reject();
            });

            return deferred.promise;
        };
    };

})(angular);