(function(ng){

    var mainApp = ng.module('mainApp');
    mainApp.controller('mainApp.workGroup.edit.modalCtrl', [
        '$log', '$scope', '$q', '$window', 'jQuery', '$timeout', 'routing',
        'workGroupService', 'helperService', '$modalInstance', 'workGroup', workGroupEditModalCtrl]);

    /**
     * Edit workGroup modal controller
     */
    function workGroupEditModalCtrl($log, $scope, $q, $window, jQuery, $timeout, routing,
                                  workGroupService, helperService, $modalInstance, workGroup) {

        $scope._name = 'mainApp.workGroup.edit.modalCtrl';
        $scope.title = 'Edit workGroup: <strong>' + workGroup.name + '</strong>';
        var formName = 'unir_ticketmanagerbundle_workGroup';

        $scope.workGroup = workGroup;

        $scope.formSubmit = function(formData){
            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = workGroupService.update(workGroup.id, formData);

            servicePromise.success(function(data){
                $modalInstance.close(data);
                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };

        $scope.init = function() {

            $scope.formData = {};
            $scope.formData[formName] = {};
            $scope.formData[formName]['name'] = workGroup.name;
            $scope.formData[formName]['description'] = workGroup.description;

        };

        $scope.tinyMCEOptions = helperService.tinyMCEOptions;
    }

})(angular);