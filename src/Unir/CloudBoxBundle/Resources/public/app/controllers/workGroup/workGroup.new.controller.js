(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.workGroup.new.modalCtrl', [
        '$log', '$scope', '$q', '$window', 'routing',
        'workGroupService', 'helperService', '$modalInstance', 'reload', workGroupNewModalCtrl]);


    /**
     * New workGroup modal controller
     */
    function workGroupNewModalCtrl($log, $scope, $q, $window, routing,
                                 workGroupService, helperService, $modalInstance, reload){

        $scope._name = 'mainApp.workGroup.new.modalCtrl';
        $scope.title = 'Create new workGroup';
        var formName = 'unir_ticketmanagerbundle_workGroup';

        $scope.formData = {};
        $scope.formData[formName] = {};

        $scope.formSubmit = function(formData){
            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = workGroupService.create(formData);

            servicePromise.success(function(data){

                if(reload) {
                    $window.location = routing.generate('ng-select_workGroup', {'id' : data.id});
                } else {
                    $modalInstance.close(data);
                    deferred.resolve();
                }
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };

        $scope.tinyMCEOptions = helperService.tinyMCEOptions;
    };

})(angular);