(function(ng){


    ng.module('mainApp')
        .controller('mainApp.dashboard.mainCtrl',
            ['$log', '$q', '$scope', '$modal', '$location', 'activitiesService', 'notificationService', 'wodsService', mainCtrl]);

    /**
     * main controller for dashboard
     */
    function mainCtrl($log, $q,  $scope, $modal, $location, activitiesService, notificationService, wodsService) {

        //
        // INIT DATA
        //--------------------------------------------------------------------------------------------------------------

        //First of all, we set title and active Menu
        $scope.setTitle('Dashboard').setMenuActive('dashboard');


        $scope.activities = {};
        $scope.activities.elements = [];
        var currentPage = 0;

        $scope.wods = {};

        /**
         * Get activities page
         * @param numPage
         */
        $scope.loadMoreActivities = function(){
            var deferred = $q.defer();

            var promise = activitiesService.paginated(++currentPage);

            promise.then(function(response){
                for(var i = 0; i < response.data.activities.elements.length; i++) {
                    $scope.activities.elements.push(response.data.activities.elements[i]);
                }
                deferred.resolve();
            });

            promise.error(function(data){
                deferred.reject();
            });

            return deferred.promise;
        };

        /**
         * opens ne wod modal
         * @returns {*}
         */
        $scope.new = function() {

            var deferred = $q.defer();


            //Open modal and get the promise
            var modalPromise = $modal.open({
                templateUrl: 'create-wod-modal.tpl',
                controller: 'mainApp.wods.new.ctrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    wodGroups: function(wodGroupService, $q){
                        var deferred = $q.defer();

                        var servicePromise = wodGroupService.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    },
                    enterpriseUsersAndGroups: function(enterpriseService, $q) {
                        var deferred = $q.defer();

                        var servicePromise = enterpriseService.users.getAll();

                        servicePromise.success(function(data){
                            deferred.resolve(data);
                        });

                        servicePromise.error(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                    }
                }
            }).result;

            //When modal is confirmed
            modalPromise.then(function(data){

                //Go to the wod page
                $location.path('/wods/').hash(data.slug);
                deferred.resolve();
            });


            //When modal is dismissed
            modalPromise.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };



        /**
         * Get wods page
         * @param numPage
         */
        $scope.getWodsPage = function(numPage){

            var promise = wodsService.paginated(numPage);

            promise.then(function(response){
                $scope.wods = response.data.wods;
            });

            promise.error(function(data){
                throw data;
            });
        };

        $scope.loadMoreActivities();
        $scope.getWodsPage(1);

        //Reload activity page
        $scope.reloadActivityPage = function() {
            $scope.getActivitiesPage($scope.activities.current);
        };
    };

})(angular);