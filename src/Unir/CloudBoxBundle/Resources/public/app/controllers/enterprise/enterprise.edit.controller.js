(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.enterprise.edit.modalCtrl', [
        '$log', '$scope', '$q', '$window', 'routing', '$timeout',
        '$modalInstance', 'enterpriseService', 'enterprise', enterpriseEditModalCtrl]);

    /**
     * Enterprise edit modal Ctrl
     */
    function enterpriseEditModalCtrl($log, $scope, $q, $window, routing, $timeout,
                                     $modalInstance, enterpriseService, enterprise){

        var formName = 'unir_ticketmanagerbundle_enterprise';

        $scope._name = 'mainApp.enterprise.edit.modalCtrl';
        $scope.title = 'Edit enterprise: <strong>' + enterprise.name+ '</strong>';

        $scope.formData = {};
        $scope.formData[formName] = {};
        $scope.formData[formName]['name'] = enterprise.name;


        $scope.formSubmit = function(formData){
            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = enterpriseService.update(enterprise.id, formData);

            servicePromise.success(function(data){
                $modalInstance.close(data);
                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });


            return deferred.promise;
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };
    };

})(angular);