(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.enterprise.show.ctrl', [
        '$log', '$scope', '$q', '$modal', '$window', 'notificationService',
        'enterpriseService' , 'enterprise', enterpriseShowCtrl]);

    /**
     * Enterprise show
     */
    function enterpriseShowCtrl($log, $scope, $q, $modal, $window, notificationService,
                                enterpriseService, enterprise) {

        $scope._name = 'mainApp.enterprise.show.ctrl';
        $scope.enterprise = enterprise;

        $scope.setTitle(enterprise.name).setMenuActive('enterprise_show');

        //Edits an enterprise
        $scope.edit = function(enterprise) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'create-enterprise-modal.tpl',
                controller: 'mainApp.enterprise.edit.modalCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve:{
                    enterprise: function(){ return enterprise; }
                }
            });

            //Really we never are going to reach this
            modal.result.then(function(data){


                //We must update the enterprise in mainData list
                for(var i = 0; i < $scope.getMainData().companies.length; i++) {
                    if($scope.getMainData().companies[i].id == enterprise.id) {

                        if( $scope.getMainData().companies[i].is_selected_enterprise) {
                            $scope.getMainData().selected_company.name = data.name;
                        }

                        $scope.getMainData().companies[i] = data;


                        break;
                    }
                }

                $scope.enterprise = data;
                deferred.resolve();

            });

            modal.result.catch(function(){
                deferred.reject();
            });

            return deferred.promise;
        };

        //Deletes an enterprise
        $scope.delete = function(enterprise) {
            var deferred = $q.defer();

            var modal = $modal.open({
                templateUrl: 'deleteEnterpriseConfirmationBox.tpl',
                controller: 'mainApp.modals.confirmation.ctrl',
                size: '',
                backdrop: 'static',
                resolve: {}
            });

            modal.result.then(function() {

                var servicePromise = enterpriseService.delete(enterprise.id);
                servicePromise.success(function () {

                    var idx = -1;
                    var isActive = undefined;
                    //Same with main data
                    for (var i = 0; i < $scope.getMainData().companies.length; i++) {
                        if ($scope.getMainData().companies[i].id == enterprise.id) {

                            isActive = $scope.getMainData().companies[i].is_selected_enterprise
                                ? true
                                : false;

                            idx = i;
                            break;
                        }
                    }

                    $scope.getMainData().companies.splice(idx, 1);

                    notificationService.notifySuccess('Enterprise ' + enterprise.name + ' deleted');

                    //Once we are here, we must consider if the deleted enterprise is the same as is active
                    if (isActive) {

                        //There are more enterprise
                        if ($scope.getMainData().companies.length > 0) {
                            //Pick first and reload
                            $scope.goToRoute('ng-select_enterprise',
                                {'id': $scope.getMainData().companies[0].id},
                                true
                            );
                        } else {
                            $location.path('/no/enterprises/');
                        }
                    } else { //Is not the active workGroup
                        //Redirect back
                        $window.history.back();
                    }
                })
            });

            modal.result.catch(function () {
                deferred.reject();
            });

            return deferred.promise;
        };
    };

})(angular);