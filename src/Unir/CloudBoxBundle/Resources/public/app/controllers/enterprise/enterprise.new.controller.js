(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.enterprise.new.modalCtrl', [
        '$log', '$scope', '$q', '$window', 'routing',
        '$modalInstance', 'enterpriseService', 'shouldReload', enterpriseNewModalCtrl]);

    /**
     * New enterprise modal controller
     */
    function enterpriseNewModalCtrl($log, $scope, $q, $window, routing,
                                    $modalInstance, enterpriseService, shouldReload){

        var formName = 'unir_ticketmanagerbundle_enterprise';

        $scope._name = 'mainApp.enterprise.new.modalCtrl';
        $scope.title = 'New enterprise';

        $scope.formData = {};
        $scope.formData[formName] = {};

        $scope.formSubmit = function(formData){
            var deferred = $q.defer();

            //Put the token in the form
            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            var servicePromise = enterpriseService.create(formData);

            servicePromise.success(function(data){

                if(shouldReload) {
                    $window.location = routing.generate('ng-select_enterprise',{'id' : data.id});
                } else {
                    $modalInstance.close(data);
                    deferred.resolve();
                };
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };
    };

})(angular);