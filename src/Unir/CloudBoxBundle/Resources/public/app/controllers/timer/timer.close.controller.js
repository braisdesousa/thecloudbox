(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.controller('mainApp.timer.close.ctrl',['$log', '$scope', '$q', '$modalInstance', 'timerService', 'helperService',
        'notificationService', 'wod', 'startDate', 'endDate', timerStopCtrl]);


    /**
     * Controller for timer stop
     */
    function timerStopCtrl($log, $scope, $q,  $modalInstance, timerService, helperService, notificationService, wod,  startDate, endDate){

        $scope._name = 'mainApp.timer.close.ctrl';
        $scope.title = 'Close timer for: <strong>' + wod.title + '</strong>';
        var formName = 'unir_ticketmanagerbundle_wodtiming';

        $scope.wod = wod;

        $scope.formData = {};
        $scope.formData.start = {};
        $scope.formData.end = {};


        $scope.formData.start.date = startDate;
        $scope.formData.end.date = endDate;


        $scope.formData[formName] = {};

        $scope.formData[formName]['date_start'] = {};
        $scope.formData[formName]['date_start']['date'] = {};
        $scope.formData[formName]['date_start']['time'] = {};

        $scope.formData[formName]['date_end'] = {};
        $scope.formData[formName]['date_end']['date'] = {};
        $scope.formData[formName]['date_end']['time'] = {};

        $scope.formData[formName]['date_start']['date']['year'] = null;
        $scope.formData[formName]['date_start']['date']['month'] = null;
        $scope.formData[formName]['date_start']['date']['day'] = null;
        $scope.formData[formName]['date_start']['time']['hour'] = null;
        $scope.formData[formName]['date_start']['time']['minute'] = null;

        $scope.formData[formName]['date_end']['date']['year'] = null;
        $scope.formData[formName]['date_end']['date']['month'] = null;
        $scope.formData[formName]['date_end']['date']['day'] = null;
        $scope.formData[formName]['date_end']['time']['hour'] = null;
        $scope.formData[formName]['date_end']['time']['minute'] = null;

        $scope.formData[formName]['comment'] = null;

        //On form submit
        $scope.formSubmit = function(formData) {

            var deferred = $q.defer();

            if($scope.formData.start.date > $scope.formData.end.date) {

                notificationService.notifyError('End date must be greater than start date');

                deferred.resolve();
                return deferred.promise;
            }

            if ($scope.formData[formName]['comment'].trim().length == 0) {
                notificationService.notifyError('You must specify a comment');

                deferred.resolve();
                return deferred.promise;
            }

            formData[formName]._token = ng.element("[name='" + formName + "[_token]']").val();

            formData[formName]['date_start']['date']['year'] = $scope.formData.start.date.getFullYear();
            formData[formName]['date_start']['date']['month'] = $scope.formData.start.date.getMonth();
            formData[formName]['date_start']['date']['day'] = $scope.formData.start.date.getDate();
            formData[formName]['date_start']['time']['hour'] = $scope.formData.start.date.getHours();
            formData[formName]['date_start']['time']['minute'] = $scope.formData.start.date.getMinutes();

            formData[formName]['date_end']['date']['year'] = $scope.formData.end.date.getFullYear();
            formData[formName]['date_end']['date']['month'] = $scope.formData.end.date.getMonth();
            formData[formName]['date_end']['date']['day'] = $scope.formData.end.date.getDate();
            formData[formName]['date_end']['time']['hour'] = $scope.formData.end.date.getHours();
            formData[formName]['date_end']['time']['minute'] = $scope.formData.end.date.getMinutes();


            var clonedData = ng.copy(formData);
            delete clonedData.start;
            delete clonedData.end;

            var servicePromise = timerService.stop(wod.slug, clonedData);

            servicePromise.success(function(data){
                notificationService.notifySuccess('You have ended <b>' + wod.title + '</b> wod succesfully' );
                $modalInstance.close(data);
                deferred.resolve();
            });

            servicePromise.error(function(error){
                deferred.reject(error);
            });

            deferred.resolve();

            return deferred.promise;
        };

        $scope.cancel = function(){
            $modalInstance.dismiss('cancel');
        };


        $scope.tinyMCEOptions = helperService.tinyMCEOptions;

    }

})(angular);