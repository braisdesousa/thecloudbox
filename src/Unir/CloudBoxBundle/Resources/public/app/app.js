(function(ng, jQuery, moment, Routing, tinyMCE, Translator){

    //Set tinyMCE config
    tinyMCE.baseURL = '/bundles/unircloudbox/bower/tinymce';

    var onErrorMakeRedirection = true;

    //Module declaration
    var mainApp = ng.module('mainApp', [
        'ngRoute',
        'ngResource',
        'ngAnimate',
        'ngSanitize',
        'angularMoment',
        'angular-loading-bar',
        'ui.bootstrap',
        'ui.tinymce',
        'ngSiwebNotifications',
        'localytics.directives',
        'ngMultipleSelect',
        'ngSpecialClicks',
        'ngSiwebForm',
        'ngSiwebClock',
        'LocalStorageModule',
        'ui.bootstrap.datetimepicker',
        'ngHttpExtension'
    ]).constant('moment', moment)
        .constant('jQuery', jQuery)
        .constant('routing', Routing)
        .constant('translator', Translator)

    //Disable loadingBar spinner
    mainApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider){
        cfpLoadingBarProvider.includeSpinner = false;
    }]);

    // Establish basic configuration to angular
    mainApp.config(function($interpolateProvider, $locationProvider){

        //Use html5 Routing mode
        $locationProvider.html5Mode(true);

        //Change angular simbols for don't conflict with twig ones
        $interpolateProvider.startSymbol('{!').endSymbol('!}');
    });

    //Exception Handling
    mainApp.config(['$provide',function($provide){
        $provide.decorator('$exceptionHandler',
            ['$delegate', '$window', '$log', function($delegate, $window, $log){
                return function (exception, cause) {

                    //Log to console
                    $log.error(exception);

                    //Redirect to 500
                    //if (onErrorMakeRedirection) {
                    //    $window.location = '/500/';
                    //}
                    //
                    //$delegate(exception, cause);
                }
            }]);
    }]);

    //Http interceptor
    mainApp.config(function($httpProvider){

        $httpProvider.interceptors.push(function($q, $log, $window, $location, notificationService){
            return {

                //When we have a request....
                'request': function(config) {
                    //Put is angular header, this will tell server that we
                    // are requesting from angular
                    config.headers['X-Is-Angular'] = true;
                    return config;
                },

                //If we have a response...
                'response' : function(response) {


                    //If we have a message, then print it
                    if(response && response.data && response.data.message) {
                        notificationService.notifySuccess(response.data.message);
                    }

                    switch(response.status) {

                        //Response 200 ? all OK !
                        case 200:
                            return response;
                            break;

                        //In case we cannot attend...
                        default:
                            return response;
                            break;
                    }
                },
                //ResponseError
                'responseError': function(rejection) {

                    //if rejection have message, log it
                    if( rejection && rejection.data && rejection.data.message) {
                        notificationService.notifyError(rejection.data.message);

                        //If data is an array...
                    } else if(rejection && rejection.data && Array.isArray(rejection.data)) {

                        //Loop array
                        for( var i = 0; i < rejection.data.length; i++ ) {

                            //If element is string...
                            if(typeof rejection.data[i] == 'string') {
                                notificationService.notifyError(rejection.data[i]);
                            }

                            //If is an object and have message property
                            else if(rejection.data[i] && rejection.data[i].message) {
                                notificationService.notifyError(rejection.data[i].message);
                            }
                        }

                    } else if(rejection && rejection.status > 0) { //Only log if status is greater than 0
                        notificationService.notifyError('There was an error, we are working on it');
                    }

                    switch(rejection.status) {

                        //Session invalid, redirect to /Login
                        case 403:
                            $window.location = '/login';
                            break;

                        //In case we don't know http Status...
                        default:

                            return $q.reject(rejection);
                            break;
                    }
                }
            }
        });
    });

    //Enable debug mode for httpExtension
    mainApp.config(['$httpExtensionProvider', '$injector', function($httpExtensionProvider, $injector){

        $httpExtensionProvider.disableDebugMode();
    }]);

})(angular, jQuery, moment, Routing, tinyMCE, Translator);