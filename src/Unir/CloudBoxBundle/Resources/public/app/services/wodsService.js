(function(ng){


    var mainApp = ng.module('mainApp');

    mainApp.service('wodsService', ['$log', '$resource', '$q', '$http', '$httpExtension', 'routing', wodsService])

    /**
     * Wods service
     * @param $log
     * @param $resource
     * @param $q
     * @param $http
     */
    function wodsService($log, $resource, $q, $http, $httpExtension, routing) {

        var getWods_httpPromise = undefined;


        /**
         * Retrieve dashboard paginated wods
         */
        this.paginated =  function(page) {
            return $httpExtension({
                method: 'GET',
                url: routing.generate('ng-paginated_wods',{page: page})
            });
        };

        /**
         * Retrieve wods...
         */
        this.getWods = function(groups, difficulties, statuses, sortBy) {

            var deferred = $q.defer();

            //We are going to make that deferred's promise object seems http Object
            var returnPromise = deferred.promise;
            returnPromise.success = returnPromise.then;
            returnPromise.error = returnPromise.catch;

            //Just for ensure that all works at expected we are going to clone groups array and filter
            var clonedGroups = [];

            if (groups && groups.length > 0) {
                for( var i = 0; i < groups.length; i++) {
                    var gr = groups[i];

                    if(!gr) {
                        continue;
                    }

                    clonedGroups.push(gr);
                }
            }

            //If we had a petition in course, cancel it
            if(getWods_httpPromise != undefined && !getWods_httpPromise.isDone()) {
                getWods_httpPromise.cancel();
            }

            //Here we make http petition
            getWods_httpPromise = $httpExtension({
                method: 'POST',
                url: routing.generate('ng-wods'),
                data: {
                    groups : clonedGroups,
                    difficulties: difficulties,
                    statuses: statuses,
                    sortBy: sortBy
                }
            });

            //ON success
            getWods_httpPromise.success(function(data) {

                var reorganizedData = {};

                reorganizedData.filters = data.filters;
                reorganizedData.groups = [];


                //Group filters need one aditional field
                for(var i = 0; i < reorganizedData.filters.groups.length; i++) {
                    reorganizedData.filters.groups[i].selected = false;
                    delete reorganizedData.filters.groups[i].created;
                }

                //If sortBy is an array convert to object
                if(Array.isArray(reorganizedData.filters.sortBy)) {
                    reorganizedData.filters.sortBy = {};
                }


                //Loop object to create an array
                for ( var key in data.groups) {
                    reorganizedData.groups.push(data.groups[key]);
                }

                var getGroup = function(groupId) {
                    for(var i = 0; i < reorganizedData.groups.length; i++) {


                        if(reorganizedData.groups[i].id == groupId) {
                            return reorganizedData.groups[i];
                        }
                    }
                    return null;
                };

                var setSelectedGroupFilter = function(groupId) {
                    for(var i = 0; i < reorganizedData.filters.groups.length; i++) {
                        if(reorganizedData.filters.groups[i].id == groupId) {
                            reorganizedData.filters.groups[i].selected = true;
                        }
                    }
                };


                //Now we loop group_wods
                for(var groupId in data.group_wods) {

                    setSelectedGroupFilter(groupId);
                    var wodsObj = data.group_wods[groupId];
                    var group = getGroup(groupId);

                    group.pagination = {
                        current : wodsObj.current,
                        numItemsPerPage : wodsObj.numItemsPerPage,
                        totalElements : wodsObj.totalElements,
                        totalPages : wodsObj.totalPages
                    };

                    group.wods = wodsObj.elements;
                }

                deferred.resolve(reorganizedData);
            });

            //On error
            getWods_httpPromise.error(function(error) {
                deferred.reject(error);
            });

            //Also we are going to extend returnPromise with isDone Fn
            returnPromise.isDone = getWods_httpPromise.isDone;

            return returnPromise;
        };

        /**
         * Retrieves a wod
         */
        this.getWod = function(slug) {
            return $httpExtension({
                method: 'GET',
                url: routing.generate('ng-wod', {'slug': slug})
            });
        };

        /**
         * Retrieve wods per group
         * @param groupId
         * @param difficulties
         * @param statuses
         * @param shortBy
         * @param page
         * @returns {*}
         */
        this.getWodsByGroup = function(groupId, difficulties, statuses, shortBy, page) {

            //We are going to transform response for seem as getWods
            var deferred = $q.defer();

            //We are going to make that deferred's promise object seems http Object
            var returnPromise = deferred.promise;
            returnPromise.success = returnPromise.then;
            returnPromise.error = returnPromise.catch;

            var httpPromise = $httpExtension({
                method: 'POST',
                url: routing.generate('ng-wodsbygroup'),
                data: {
                    'group_id' : groupId,
                    page: page,
                    difficulties: difficulties,
                    statuses: statuses,
                    shortBy: shortBy
                }
            });

            //On success..
            httpPromise.success(function(data){

                var resultObj = {};

                resultObj.pagination = {
                    current : data.current,
                    numItemsPerPage : data.numItemsPerPage,
                    totalElements : data.totalElements,
                    totalPages : data.totalPages
                };

                resultObj.wods = data.elements;


                deferred.resolve(resultObj);

            });


            //On error
            httpPromise.error(function(error) {
                deferred.reject(error);
            });

            return returnPromise;
        };

        /**
         * Sets wod status
         */
        this.setStatus = function(wodSlug, newStatus) {
            return $httpExtension({
                method: 'PATCH',
                url: routing.generate('ng-wod-patch-status', { 'slug': wodSlug, 'status': newStatus})
            });
        };

        /**
         * Sets wod difficulty
         */
        this.setDifficulty = function(wodSlug, newDifficulty) {
            return $httpExtension({
                method: 'PATCH',
                url: routing.generate('ng-wod-patch-difficulty', { 'slug': wodSlug, 'difficulty': newDifficulty})
            });
        };

        /**
         * Adds a comment to a wod
         */
        this.addComment = function(wodSlug, commentText){
            return $httpExtension({
                url: routing.generate('ng-create-comment', {'slug': wodSlug}),
                method: 'POST',
                data: {
                    comment: commentText
                }
            })
        };

        /**
         * Creates a new wod
         */
        this.create = function(formData) {
            return $httpExtension({
                url: routing.generate('ng-wod-post'),
                method: 'POST',
                data: formData
            })
        };

        /**
         * Edit wod
         */
        this.edit = function(wodSlug, formData) {
            return $httpExtension({
                url: routing.generate('ng-wod_short-put', {'slug' : wodSlug}),
                method: 'PUT',
                data: formData
            });
        };

        /**
         * Deletes a wod
         * @param wodSlug
         * @returns {*}
         */
        this.delete = function(wodSlug) {
            return $httpExtension({
                url: routing.generate('ng-wod-delete', {'slug': wodSlug}),
                method: 'DELETE'
            });
        };

        /**
         * Assign or desassing the wod
         * @param wodSlug
         */
        this.assign = function(wodSlug) {
            return $httpExtension({
                url: routing.generate('ng-wod-assign', {'slug': wodSlug}),
                method: 'PATCH'
            });
        };

        /**
         * Follow or unfollow the wod
         * @param wodSlug
         */
        this.follow = function(wodSlug) {
            return $httpExtension({
                url: routing.generate('ng-wod-follow', {'slug': wodSlug}),
                method: 'PATCH'
            });
        };

        /**
         * Sets users assigned
         * @param wodSlug
         * @param formData
         */
        this.setUsersAssigned = function(wodSlug, formData) {
            return $httpExtension({
                url: routing.generate('ng-wod_assigned-put', {'slug': wodSlug}),
                method: 'PUT',
                data: formData
            });
        };

        /**
         * Sets users followers
         * @param wodSlug
         * @param formData
         */
        this.setUsersFollowers = function(wodSlug, formData) {
            return $httpExtension({
                url: routing.generate('ng-wod_follower-put', {'slug': wodSlug}),
                method: 'PUT',
                data: formData
            });
        };

        /*
         * SUBTASKS
         * -------------------------------------------------------------------------------------------------------------
         */

        this.subwods = {};

        /**
         * Retrieve subwod by an id
         * @param id
         * @returns {*}
         */
        this.subwods.get = function(id) {
            return $httpExtension({
                url: routing.generate('ng-subwod',{'id': id}),
                method: 'GET'
            });
        };

        /**
         * Creates a new subwod
         * @param wodSlug
         * @param title
         * @returns {*}
         */
        this.subwods.create = function(wodSlug, title) {
            return $httpExtension({
                url: routing.generate('ng-create-subwod', {'slug' : wodSlug}),
                data: { 'title' : title },
                method: 'POST'
            });
        };

        /**
         * close a subwod
         * @param id
         * @returns {*}
         */
        this.subwods.close = function(id) {
            return $httpExtension({
                url: routing.generate('ng-subwod-close',{'id': id}),
                method: 'PATCH'
            });
        };

        /**
         * Opens a subwod
         * @param id
         * @returns {*}
         */
        this.subwods.open = function(id) {
            return $httpExtension({
                url: routing.generate('ng-subwod-open',{'id': id}),
                method: 'PATCH'
            });
        };

        /**
         * Delete a subwod
         * @param id
         * @returns {*}
         */
        this.subwods.delete = function(id) {
            return $httpExtension({
                url: routing.generate('ng-subwod-delete',{'id': id}),
                method: 'DELETE'
            });
        }

    };

})(angular);