(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.service('helperService',[helperService]);

    /**
     * Helper Service
     */
    function helperService() {

        /**
         * Prints status icon
         * @param status
         * @returns {string}
         */
        this.printStatusIcon = function(status) {

            if (!status) return '';

            switch (status.toLowerCase()) {
                case 'closed':
                    return "fa fa-lock";
                    break;
                case 'resolved' :
                    return "fa fa-check";
                    break;
                case 'in progress':
                    return "fa fa-ellipsis-h";
                    break;
                case 'open' :
                    return ""; //fa fa-unlock
                    break;
                default:
                    return "";
            }
        };

        /**
         * Prints color difficulty
         * @param difficulty
         * @returns {string}
         */
        this.printColorDifficulty = function(difficulty){

            if (!difficulty) return '';

            switch (difficulty.toLowerCase()) {
                case 'low' :
                    return "success";
                    break;
                case 'minor' :
                    return "info";
                    break;
                case 'high' :
                    return "warning";
                    break;
                case 'critical' :
                    return "danger";
                    break;
                default:
                    return "default";
            }
        };

        //TinyMCE Options
        this.tinyMCEOptions = {
            plugins: ['link', 'autolink'],
            toolbar1: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link",
            toolbar2: "",
            menubar: false,
            statusbar: false,
            image_advtab: false,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],
            extended_valid_elements: 'a[href|target=_blank]',
            valid_elements: 'a[href|target=_blank],strong/b,div[align],br,p'
        };

    };

})(angular);