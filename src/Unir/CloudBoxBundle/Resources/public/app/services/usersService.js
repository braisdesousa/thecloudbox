(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.service('usersService',['$log', '$resource', '$q', '$httpExtension', 'routing', usersService]);

    /**
     * Users service
     * @param $log
     * @param $resource
     * @param $q
     * @param $http
     * @param routing
     */
    function usersService($log, $resource, $q, $httpExtension, routing) {

        /**
         * Gets an user
         * @param userId
         * @returns {*}
         */
        this.get = function(userId) {
            return $httpExtension({
                url: routing.generate('ng-user', {'id' : userId}),
                method: 'GET'
            });
        }

        /**
         * Retrieve all users from the current enterprise
         * @returns {*}
         */
        this.getAll = function(){
            return $httpExtension({
                url: routing.generate('ng-selected-workGroup-users'),
                method: 'GET'
            });
        };

        /**
         * get all users by workGroup
         * @param workGroupId
         * @returns {*}
         */
        this.getAllByWorkGroup = function(workGroupId) {
            return $httpExtension({
                url: routing.generate('ng-workGroup-users', {'id' : workGroupId}),
                method: 'GET'
            });
        };

        /**
         * get all users by group
         * @param groupId
         * @returns {*}
         */
        this.getAllByUserGroup = function(groupId) {
            return $httpExtension({
                url: routing.generate('ng-workGroup-usersgroups', {'id' : groupId}),
                method: 'GET'
            });
        };


        /**
         * Will create an user
         * @param formData
         * @returns {*}
         */
        this.create = function(formData) {
            return $httpExtension({
                url: routing.generate('ng-post-user'),
                method: 'POST',
                data: formData
            });
        };

        //Updates an user
        this.update = function(id, formData) {
            return $httpExtension({
                url: routing.generate('ng-user-put', {'id': id}),
                method: 'PUT',
                data: formData
            });
        };

        //Delete user
        this.delete = function(userId) {
            return $httpExtension({
                url: routing.generate('ng-user-delete', {'id': userId}),
                method: 'DELETE'
            });
        };

        //Changes user password
        this.changePassword = function(userId, formData) {
            return $httpExtension({
                url: routing.generate('ng-user-put-password', {'id': userId}),
                method: 'PUT',
                data: formData
            });
        };

        this.userGroups = {};

        //Retrieve all userGroups from the current enterprise
        this.userGroups.getAll = function(){
            return $httpExtension({
                url: routing.generate('ng-enterprise-users-by-group'),
                method: 'GET'
            });
        };

        //Creates new userGroup
        this.userGroups.create = function(formData) {
            return $httpExtension({
                url: routing.generate('ng-usersgroup-post'),
                method: 'POST',
                data: formData
            });
        };

        //Updates userGroup
        this.userGroups.update = function(id, formData) {
            return $httpExtension({
                url: routing.generate('ng-usersgroup-put',{'id': id}),
                method: 'PUT',
                data: formData
            });
        };

        //Removes an user group
        this.userGroups.delete = function(id) {
            return $httpExtension({
                url: routing.generate('ng-usersgroup-delete', {'id': id}),
                method: 'DELETE'
            })
        };

    };

})(angular);