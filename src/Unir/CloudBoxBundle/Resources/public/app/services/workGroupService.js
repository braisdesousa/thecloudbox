(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.service('workGroupService', ['$log', '$q', '$httpExtension', 'routing', workGroupService]);

    function workGroupService($log, $q, $httpExtension, routing) {

        /**
         * Retrieves a workGroup by its id
         */
        this.get = function(id) {
            return $httpExtension({
                url: routing.generate('ng-workGroup', {'id': id}),
                method: 'GET'
            });
        };

        /**
         * Retrieves all workGroups
         */
        this.getAll = function() {
            return $httpExtension({
                url: routing.generate('ng-workGroups'),
                method: 'GET'
            });
        };


        this.getAllWithUsersInfo = function() {
            return $httpExtension({
                url: routing.generate('ng-workGroups-user-info'),
                method: 'GET'
            });
        };

        /**
         * Creates new workGroup
         */
        this.create = function(formData) {
            return $httpExtension({
                url: routing.generate('ng-workGroup-post'),
                method: 'POST',
                data : formData
            });
        };

        /**
         * Updates a workGroup
         */
        this.update = function(id, formData) {
            return $httpExtension({
                url: routing.generate('ng-workGroup-put', {'id': id}),
                method: 'PUT',
                data : formData
            });
        };

        /**
         * Retrieves workGroups by an user
         */
        this.getByUser = function(userId) {
            return $httpExtension({
                url: routing.generate('ng-user-workGroups', {'id' : userId}),
                method: 'GET'
            })
        };

        /**
         * Removes a workGroup
         */
        this.delete = function(id) {
            return $httpExtension({
                url: routing.generate('ng-workGroup-delete', {'id' : id}),
                method: 'DELETE'
            });
        };

        /**
         * Selects a new active workGroup
         */
        this.selectWorkGroup = function(id) {
            return $httpExtension({
                url: routing.generate('ng-select_workGroup_no-redirect', {'id' : id}),
                method: 'GET'
            });
        };
    };

})(angular);