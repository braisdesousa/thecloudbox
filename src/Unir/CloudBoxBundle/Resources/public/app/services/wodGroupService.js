(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.service('wodGroupService', ['$log', '$q', '$httpExtension', 'routing', wodGroupService]);

    function wodGroupService($log, $q, $httpExtension, routing) {

        //Retrieves a full wodGroup
        this.get = function(id) {
            return $httpExtension({
                url: routing.generate( 'ng-wodgroup', {'id': id}),
                method: 'GET'
            });
        };

        //Retrieves all wodGroups
        this.getAll = function() {
            return $httpExtension({
                url: routing.generate('ng-wodgroups'),
                method: 'GET'
            });
        };

        /**
         * Creates new wodGroup
         */
        this.create = function(formData) {
            return $httpExtension({
                url: routing.generate('ng-wodgroup-post'),
                method: 'POST',
                data : formData
            });
        };

        //Updates a wodGroup
        this.update = function(id, formData){
            return $httpExtension({
                url: routing.generate('ng-wodgroup-put', {'id' : id}),
                method: 'PUT',
                data: formData
            });
        };

        //Deletes a wodgroup
        this.delete  = function(id) {
            return $httpExtension({
                url: routing.generate('ng-wodgroup-delete', {'id' : id}),
                method: 'DELETE'
            });
        };
    };

})(angular);