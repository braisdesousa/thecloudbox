(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.service('timerService',['$log', '$resource', '$q', '$httpExtension', 'routing', timerService]);

    /**
     * Timer service
     */
    function timerService($log, $resource, $q, $httpExtension, routing) {

        /**
         * Starts timing
         * @param wodSlug
         * @returns {*}
         */
        this.start = function(wodSlug) {
            return $httpExtension({
                url: routing.generate('ng-start_wodtiming', {'slug': wodSlug}),
                method: 'POST'
            });
        };

        /**
         * Stops timing
         * @param wodSlug
         * @param formData
         * @returns {*}
         */
        this.stop = function(wodSlug, formData) {
            return $httpExtension({
                url: routing.generate('ng-end_wodtiming', {'slug': wodSlug}),
                method: 'POST',
                data: formData
            });
        };
    };

})(angular);