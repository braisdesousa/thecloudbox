(function(ng){


    ng.module('mainApp')
        .factory('activitiesService',['$log', '$resource', '$q', '$httpExtension', activitiesService]);

    function activitiesService($log, $resource, $q, $httpExtension){
        return {

            //Retrieve dashboardActivities
            'paginated': function(page) {
                return $httpExtension({
                   method: 'GET',
                   url: Routing.generate('ng-paginated_activities',{page: page})
                });
            }
        };
    };
})(angular);