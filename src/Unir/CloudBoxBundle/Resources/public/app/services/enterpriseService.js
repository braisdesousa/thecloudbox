(function(ng){

    var mainApp = ng.module('mainApp');

    mainApp.service('enterpriseService', ['$log', '$q', '$httpExtension', 'routing', enterpriseService]);

    function enterpriseService($log, $q, $httpExtension, routing) {

        this.get = function(id){
            return $httpExtension({
                url: routing.generate('ng-enterprise', {'id' : id}),
                method: 'GET'
            });
        };

        this.getAll = function() {
            return $httpExtension({
                url: routing.generate('ng-enterprises'),
                method: 'GET'
            });
        };

        this.create = function(formData) {
            return $httpExtension({
                url: routing.generate('ng-enterprise-post'),
                method: 'POST',
                data: formData
            });
        };

        this.update = function(id, formData) {
            return $httpExtension({
                url: routing.generate('ng-enterprise-put',{'id': id}),
                method: 'PUT',
                data: formData
            });
        };

        this.delete = function(id) {
            return $httpExtension({
                url: routing.generate('ng-enterprise-delete', {'id': id}),
                method: 'DELETE'
            });
        };

        this.users = {};

        //Retrieve all users for the current enterprise
        this.users.getAll = function(){
            return $httpExtension({
                url: routing.generate('ng-enterprise-users-info'),
                method: 'GET'
            });
        };
    };

})(angular);