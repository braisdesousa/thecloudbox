(function(ng){

    ng.module('mainApp')
        .directive('myLog', ['$log', logDirective])
        .directive('myUserIcon', ['$log', 'jQuery', '$compile', 'routing' ,userIconDirective])
        .directive('myActivitiesTimeline', ['$log', '$sanitize', 'jQuery', activitiesTimeLine]);

    /**
     * Directives for log
     * @param $log
     * @returns {{restrict: string, replace: boolean, template: string}}
     */
    function logDirective($log) {
        return {
            restrict: 'E',
            replace: true,
            template: "<ul class='list-unstyled'><li ng-repeat='l in getLast10Logs()'>[{{l.date|date:'HH:mm:ss'}}] {{l.text}}</li></ul>"
        };
    };

    /**
     * user Icon directive
     * @param $log
     * @param jQuery
     * @returns {{restrict: string, link: link}}
     */
    function userIconDirective($log, jQuery, $compile, routing) {

        //Colors array
        var colors = [
            "avatar-darkblue",  "avatar-yellowgreen",  "avatar-brown",     "avatar-darkred",   "avatar-green",
            "avatar-orangered", "avatar-dodgerblue",   "avatar-seagreen",   "avatar-chocolate", "avatar-indigo",
            "avatar-steelblue", "avatar-saddlebrown",  "avatar-darkkhaki",  "avatar-teal",      "avatar-olive",
            "avatar-cadetblue", "avatar-darkslategray","avatar-lightgreen", "avatar-plum",      "avatar-tan",
            "avatar-limegreen", "avatar-lightsteelblue"];

        //get colors
        var getColorClass = function(name) {
            if (name == "More Users" || !name || name.length == 0) {
                return "avatar-ellipsis";
            }
            var numericString = 0;
            for(var i = 0; i < name.length; i++) {
                numericString += name.charCodeAt(i);
            }

            var pseudo_random = count_chars(name, 3).length;

            return colors[(numericString * pseudo_random) % colors.length];
        };

        //count chars as php function
        var count_chars = function(str, mode) {
            //  discuss at: http://phpjs.org/functions/count_chars/
            // original by: Ates Goral (http://magnetiq.com)
            // improved by: Jack
            // bugfixed by: Onno Marsman
            // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            //    input by: Brett Zamir (http://brett-zamir.me)
            //  revised by: Theriault
            //   example 1: count_chars("Hello World!", 3);
            //   returns 1: " !HWdelor"
            //   example 2: count_chars("Hello World!", 1);
            //   returns 2: {32:1,33:1,72:1,87:1,100:1,101:1,108:3,111:2,114:1}

            var result = {},
                resultArr = [],
                i;

            str = ('' + str)
                .split('')
                .sort()
                .join('')
                .match(/(.)\1*/g);

            if ((mode & 1) == 0) {
                for (i = 0; i != 256; i++) {
                    result[i] = 0;
                }
            }

            if (mode === 2 || mode === 4) {

                for (i = 0; i != str.length; i += 1) {
                    delete result[str[i].charCodeAt(0)];
                }
                for (i in result) {
                    result[i] = (mode === 4) ? String.fromCharCode(i) : 0;
                }

            } else if (mode === 3) {

                for (i = 0; i != str.length; i += 1) {
                    result[i] = str[i].slice(0, 1);
                }

            } else {

                for (i = 0; i != str.length; i += 1) {
                    result[str[i].charCodeAt(0)] = str[i].length;
                }

            }
            if (mode < 3) {
                return result;
            }

            for (i in result) {
                resultArr.push(result[i]);
            }
            return resultArr.join('');
        };

        /**
         * Hlpr for retrieve sizee
         * @param size
         * @returns {string}
         */
        var retrieveSizeClass = function(size) {
            switch (size) {
                case 'normal':
                    return 'avatarNormal';
                    break;
                case 'big':
                    return 'avatarBig';
                    break;
                case 'small':
                    return 'avatarSmall';
                    break;
                default:
                    return 'avatarNormal';
                    break;
            };
        };

        //Link function
        var linkFn = function($scope, element, attr) {

            var name = attr.name;
            var wodSlug = attr.wodslug;
            var link = attr.link;
            var size = attr.size ? attr.size : 'normal';
            var elem = attr.element ? attr.element : 'span';

            var ngNameExpr = attr.ngName;
            var ngUserId = attr.ngUserId;



            var item = jQuery('<' + elem + '></' + elem + '>');

            //We set default attributes and classes
            item.attr({
                'tooltip': name,
                'tooltip-placement': 'top',
                "target": "_self"
            }).addClass('avatarUser cursor_pointer');

            //Append item to the element
            element.append(item);
            //We compile for get the tooltip
            $compile(item)($scope);

            //Here if we have a binding we must stablish data when scope changes

            if(ngNameExpr != null) {

                //Watch function
                var watchFn = function() {
                    //We must watch on name changes
                    return $scope.$eval(ngNameExpr);
                };

                $scope.$watch(watchFn, function(newValue, oldValue){
                    //In newValue we have the text...
                    //Here we are not going to do ellipsis sith

                    var userId = $scope.$eval(ngUserId);

                    //Get the capitalLetter
                    var capitalLetter =  !newValue || newValue.length == 0
                        ? ''
                        : newValue.toUpperCase()[0];

                    item.addClass(getColorClass(newValue))
                        .addClass(retrieveSizeClass(size))
                        .addClass(attr.class)
                        .text(capitalLetter)
                        .attr('href', routing.generate("ng_user_show", {"id": userId}));

                    if(element == 'a' && userId) {
                        item.attr({'href': '/user/' + userId});
                    }
                });

            } else {

                var haveEllipsis = attr.name == "...";

                var capitalLetter = '';

                if(haveEllipsis) {
                    capitalLetter = name;
                    name = 'More Users';
                    //size = 'small';
                } else {
                    capitalLetter = name.toUpperCase()[0];
                }

                item.addClass(getColorClass(name))
                    .addClass(retrieveSizeClass(size))
                    .addClass(attr.class)
                    .text(capitalLetter)
                    .attr('href', haveEllipsis
                        ? routing.generate("ng_wod_show", {"slug": wodSlug})
                        : routing.generate("ng_user_show", {"id": link}));

                if(element == 'a' && link) {
                    item.attr({'href': '/user/' + link});
                }


            }
        };



        return {
            restrict: 'E',
            link: linkFn,
        }
    };

    /**
     * Timeline for activities
     * @param $log
     * @param jQuery
     * @returns {{restrict: string, template: string, scope: {activities: string}}}
     */
    function activitiesTimeLine($log, $sanitize, jQuery) {
        return {
            restrict: 'E', //Restrict only for elements
            templateUrl: '/timeline.tpl', //Load timeline.tpl
            scope: {
                activities: '=elements', //We bind elements attribute
                parentScope: '=parentScope',

            },
            link: function($scope, element, attr) {
                $scope.showEllipsis = attr.noEllipsis !== undefined ? false : true;
            },
            controller: function($scope,$log) {
                /**
                 * Print color action
                 * @param status
                 * @returns {string}
                 */
                $scope.printColorAction = function(action){
                    switch (action) {
                        case 'Closed':
                            return "label-warning";
                            break;
                        case 'Resolved' :
                            return "label-warning";
                            break;
                        case 'In progress':
                            return "label-success";
                            break;
                        case 'Open' :
                            return "label-success";
                            break;
                        case 'Critical' :
                            return "label-danger";
                            break;
                        case 'High' :
                            return "label-danger";
                            break;
                        case 'low' :
                            return "label-info";
                            break;
                        case 'minor' :
                            return "label-info";
                            break;
                        case 'Update':
                            return "label-primary";
                            break;
                        default:
                            return "label-default";
                    }
                };
            }
        }
    };



})(angular);