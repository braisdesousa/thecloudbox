(function(ng){

    ng.module('mainApp')
        .config(['$routeProvider', 'routing', setRoutes]);

    /**
     * Sets application routes
     * @param $routeProvider
     */
    function setRoutes($routeProvider, routing) {

        /*
         * Dashboard
         *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

        //Dashboard route config
        var dashboardRouteData = {
            templateUrl: '/partials/_dashboard/',
            controller: 'mainApp.dashboard.mainCtrl',
            controllerAs: 'dashboardMainCtrl',
            resolve: {}
        };

        /*
         * Wods
         *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

        //Wod route data
        var wodsRouteData = {
            templateUrl: '/partials/wods/_index',
            controller: 'mainApp.wods.main.ctrl',
            controllerAs: 'wodMainCtrl',
            reloadOnSearch: false,
            resolve: {
                data : function($log, $q, wodsService, localStorageService, $window, $rootScope) {

                    var localStorage_wodGroupsFilterKey = 'wods_controller_wodGroupsFilter';
                    var localStorage_difficultyFilterKey = 'wods_controller_difficultyFilter';
                    var localStorage_statusFilterKey = 'wods_controller_statusFilter';
                    var localStorage_sortByFilterKey = 'wods_controller_sortByFilter';

                    var deferred = $q.defer();

                    //this is the function that we will execute...
                    var fn = function() {


                        //We need to check localStorage for  watch if items are in there....
                        var groupsFilter = [];
                        var difficultyFilter = ['low', 'minor', 'normal', 'high', 'critical'];
                        var statusFilter = ['open', 'in progress'];
                        var orderByFilter = [];

                        //Retrieve elements from localStorage

                        var ls_groupsFilters = localStorageService.get(localStorage_wodGroupsFilterKey);
                        var ls_difficultyFilter = localStorageService.get(localStorage_difficultyFilterKey);
                        var ls_statusFilter = localStorageService.get(localStorage_statusFilterKey);
                        var ls_orderedByFilter = localStorageService.get(localStorage_sortByFilterKey);


                        //Assign to filters if needed

                        if (ls_difficultyFilter) difficultyFilter = ls_difficultyFilter;
                        if (ls_statusFilter) statusFilter = ls_statusFilter;
                        if (ls_orderedByFilter) orderByFilter = ls_orderedByFilter;

                        if (ls_groupsFilters && ls_groupsFilters[$rootScope.getMainData().workGroup.id]) {
                            groupsFilter = ls_groupsFilters[$rootScope.getMainData().workGroup.id];
                        }


                        //Start to load...
                        var promise = wodsService.getWods(
                            groupsFilter,
                            difficultyFilter,
                            statusFilter,
                            orderByFilter
                        );

                        promise.success(function (data) {
                            deferred.resolve(data);
                        });

                        promise.error(function (error) {
                            deferred.reject();
                        });
                    };

                    //The condition is $rootScope.getMainData
                    var mainData = $rootScope.getMainData();

                    //if mainData is a promise...
                    if(mainData.then) {
                        //Loop promises
                        mainData.then(fn);
                    } else fn();

                    return deferred.promise;
                },
                wod: function($log, $q, wodsService, $window, $location) {

                    //If we have a wod hash, then store it
                    var deferred = $q.defer();
                    var wodSlug = $location.hash();

                    if(!wodSlug) {
                        deferred.resolve(null);
                    } else {

                        var promise = wodsService.getWod(wodSlug);

                        promise.success(function(data){
                            deferred.resolve(data);
                        });

                        promise.error(function(error) {

                            $location.hash('');
                            deferred.resolve(null);
                        });
                    }

                    return deferred.promise;
                }
            }
        };

        /*
         * Users
         *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

        //Route data for users
        var userListRouteData = {
            templateUrl:  '/partials/user/_index',
            controller: 'mainApp.user.list.ctrl',
            resolve: {
                users: function($q, usersService) {
                    var deferred = $q.defer();

                    var servicePromise = usersService.getAll();

                    servicePromise.success(function(data){
                        deferred.resolve(data);
                    });

                    servicePromise.error(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                },
                groups: function($q, usersService) {
                    var deferred = $q.defer();

                    var servicePromise = usersService.userGroups.getAll();

                    servicePromise.success(function(data){
                        deferred.resolve(data);
                    });

                    servicePromise.error(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            }
        };

        //Route for show an user
        var userShowRouteData = {
            templateUrl:  '/partials/user/_show',
            controller: 'mainApp.user.show.ctrl',
            resolve: {
                user: function($q, usersService, $route) {
                    var deferred = $q.defer();

                    var servicePromise = usersService.get($route.current.params.id)

                    servicePromise.success(function(data){
                        deferred.resolve(data);
                    });

                    servicePromise.error(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            }
        };

        /*
         * WodGroups
         *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

        var wodGroupListRouteData = {
            templateUrl:  '/partials/wodGroup/_index',
            controller: 'mainApp.wodGroup.list.ctrl',
            resolve: {
                wodGroups: function($q, wodGroupService) {
                    var deferred = $q.defer();

                    var servicePromise = wodGroupService.getAll()

                    servicePromise.success(function(data){
                        deferred.resolve(data);
                    });

                    servicePromise.error(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            }
        };


        /*
         * Enterprises
         *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

        //Route data for
        var enterpriseListRouteData = {
            templateUrl: '/partials/enterprise/_index',
            controller: 'mainApp.enterprise.list.ctrl',
            resolve: {
                enterprises: function($q, enterpriseService) {
                    var deferred = $q.defer();

                    var servicePromise = enterpriseService.getAll();

                    servicePromise.success(function(data){
                        deferred.resolve(data);
                    });

                    servicePromise.error(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            }
        };

        var enterpriseShowRouteData = {
            templateUrl: '/partials/enterprise/_show',
            controller: 'mainApp.enterprise.show.ctrl',
            resolve: {
                enterprise: function($q, enterpriseService, $route) {
                    var deferred = $q.defer();

                    var servicePromise = enterpriseService.get($route.current.params.id)

                    servicePromise.success(function(data){
                        deferred.resolve(data);
                    });

                    servicePromise.error(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            }
        }
        /*
         * WorkGroups
         *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

        //List workGroups route
        var workGroupsListRouteData = {
            templateUrl:  '/partials/workGroup/_index',
            controller: 'mainApp.workGroup.list.ctrl',
            resolve: {
                workGroups: function($q, workGroupService) {
                    var deferred = $q.defer();

                    var servicePromise = workGroupService.getAll();

                    servicePromise.success(function(data){
                        deferred.resolve(data.workGroups);
                    });

                    servicePromise.error(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }
            }
        };

        //Show workGroup route
        var workGroupShowRouteData = {
            templateUrl:  '/partials/workGroup/_show',
            controller: 'mainApp.workGroup.show.ctrl',
            resolve: {
                 workGroup: function($q, workGroupService, $route) {
                     var deferred = $q.defer();

                     var servicePromise = workGroupService.get($route.current.params.id)

                     servicePromise.success(function(data){
                         deferred.resolve(data);
                     });

                     servicePromise.error(function(error){
                         deferred.reject(error);
                     });

                     return deferred.promise;
                 }
            }
        };

        /*
         * 500, 404 Etc.
         *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

        //Error 500 route data
        var error500RouteData = {
            templateUrl: '/partials/errors/_500/',
            controller: function($scope) {
                $scope.setTitle('Error');
                $scope.setMenuActive('500/');
            }
        };

        //Error 404 route Data
        var error404RouteData = {
            templateUrl: '/partials/errors/_404/',
            controller : function($scope){
                $scope.setTitle('No encontrado');
                $scope.setMenuActive('404/');
            }
        };

        var noWorkGroupsAvailableRouteData = {
            templateUrl: '/partials/errors/_noWorkGroups/',
            controller: 'mainApp.errors.noWorkGroups.ctrl'
        };

        var noEnterprisesAvailableRouteData = {
            templateUrl: '/partials/errors/_noEnterprises/',
            controller: function($scope) {

            }
        };

        /*
         * Tsts Route
         *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/


        var debugRouteData = {
            templateUrl: '/partials/debug/_debug/',
            controller: function($scope, $httpExtension) {
                $scope
                    .setTitle('Debug window, if you entered here, a big and bad monster will came to catchya')
                    .setMenuActive('Debug');
                $scope.getAJAXHistory = function(){
                    return $httpExtension.getHistory();
                }
            }
        }

        var testsRouteData = {
            template: '<pre>{! log | json !}</pre>',
            controller: function($log, $scope, $http, $httpExtension) {

                $scope.log = $httpExtension.getHistory();
            }
        };


        $routeProvider
            .when(routing.generate('ng_index'), dashboardRouteData)
            .when(routing.generate('ng_dashboard'), dashboardRouteData)
            .when(routing.generate('ng_wods'), wodsRouteData)

            .when(routing.generate('ng_workGroup_list'), workGroupsListRouteData)
            .when('/workGroup/:id', workGroupShowRouteData)

            .when(routing.generate('ng_user_list'), userListRouteData)
            .when('/user/:id', userShowRouteData)

            .when(routing.generate('ng_enterprise_list'), enterpriseListRouteData)
            .when('/enterprise/:id', enterpriseShowRouteData)

            .when(routing.generate('ng_wodGroup_list'), wodGroupListRouteData)

            .when(routing.generate('ng_error_404'), error404RouteData)
            .when(routing.generate('ng_error_500'), error500RouteData)
            .when(routing.generate('ng_no_workGroups'), noWorkGroupsAvailableRouteData)
            .when(routing.generate('ng_no_enterprises'), noEnterprisesAvailableRouteData)


            .when('/debug', debugRouteData)
            .when('/test', testsRouteData)
            .otherwise(error404RouteData);
    };

})(angular);