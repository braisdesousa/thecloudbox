(function(ng){

    ng.module('mainApp')
        .controller('mainApp.mainCtrl', ['$log', '$scope', '$rootScope', '$window', '$location', '$httpExtension',
            '$q', '$modal', 'translator', 'timerService', 'notificationService',  'routing',  mainCtrl]);

    /**
     * Main controller, manages app and is root scope
     */
    function mainCtrl($log, $scope, $rootScope, $window, $location, $httpExtension,
                      $q, $modal, translator, timerService, notificationService,  routing) {

        $scope._name = 'mainApp.mainCtrl';

        //
        // TITLE
        // -------------------------------------------------------------------------------------------------------------

        var title = '';

        $scope.getTitle = function(){ return title; }
        $scope.setTitle = function(newTitle){ title = newTitle; return $scope; }

        //
        // MENU ACTIVE
        // -------------------------------------------------------------------------------------------------------------

        var activeMenu = '';

        $scope.isMenuActive = function(menu) { return activeMenu == menu; }
        $scope.setMenuActive = function(newMenu) { activeMenu = newMenu; return $scope; }

        //
        // Main Data
        // -------------------------------------------------------------------------------------------------------------

        $rootScope.mainData = {};
        var blockRoutes = false;

        //Retrieves mainData (When it's avaliable)
        $scope.getMainData = function(){ return $rootScope.mainData; };
        $rootScope.getMainData = $scope.getMainData;

        //sets mainData on rootScope
        $rootScope.setMainData = function(data) {

            //We have no workGroups? redirect
            if(data.workGroups.length == 0) {
                $location.path(routing.generate('ng_no_workGroups'));
            }

            $rootScope.mainData = data;
            $rootScope.mainData.haveWorkGroups = function(){ return $rootScope.mainData.n_workGroups > 0; };
            $rootScope.mainData.haveWodGroups = function(){ return $rootScope.mainData.n_wodGroups > 0; };
            $rootScope.mainData.haveWods = function(){ return $rootScope.mainData.n_wods > 0; };


            for(var i = 0; i < $rootScope.mainData.companies.length; i++) {
                if ($rootScope.mainData.companies[i].is_selected_enterprise) {
                    $rootScope.mainData.selected_company = $rootScope.mainData.companies[i];
                    break;
                }
            }


            for (var i = 0; i < $rootScope.mainData.workGroups.length; i++) {
                if ($rootScope.mainData.workGroups[i].is_selected_workGroup) {
                    $rootScope.mainData.workGroup = $rootScope.mainData.workGroups[i];
                }
            }

            //User is role owner?
            $rootScope.mainData.user.isRoleOwner = function() {
                return false;
            };

            //User is role_admin ?
            $rootScope.mainData.user.isRoleAdmin = function() {

                return $rootScope.mainData.user.is_admin;
            };

            setStartDateCorrectly();
        };

        /**
         * Function that will reload main Data
         */
        $rootScope.reloadMainData = function() {

            var deferred = $q.defer();

            var promise = $httpExtension({
                url: Routing.generate('ng-base_data'),
                method: 'GET'
            });

            $rootScope.mainData = promise;

            promise.success(function(data){

                $rootScope.setMainData(data);
                deferred.resolve($rootScope.mainData);
            });

            promise.error(function(x){
                deferred.reject(x);
                throw x;
            });


            return deferred.promise;

        };

        //Reload main data
        $scope.reloadMainData();

        //
        // Routes
        // -------------------------------------------------------------------------------------------------------------

        //Traslates a text
        $scope.translate = function(text, arg1, arg2) {
            return translator.trans(text, arg1, arg2);
        };

        //
        // Routes
        // -------------------------------------------------------------------------------------------------------------

        /**
         * Generates a new route
         * @param routeName
         * @param params
         * @returns {*|string}
         */
        $scope.generateRoute = function(routeName, params) {
            return routing.generate(routeName, params);
        };

        //goes to a route
        $scope.goToRoute = function(routeName, params, reloadPage) {
            var route = routing.generate(routeName, params);

            if(reloadPage == undefined) reloadPage = false;

            if(!reloadPage) {
                $location.path(route);
            } else {
                $window.location = route;
            }

        };

        //
        // LOGS
        // -------------------------------------------------------------------------------------------------------------
        // The idea is that we can print easy logs on screen


        var logs =  [];

        //This will retrieve 10 last logs
        $scope.getLast10Logs = function() {
            var results = [];
            for(var i = logs.length - 1, j = 0; i >= 0 && j < 10; i--, j++) {
                results.push(logs[i]);
            }
            return results;
        };

        //Make a log
        $scope.log = function(msg) {
            logs.push({date: new Date(), text: msg});
        };



        //
        // ERROR HANDLING
        // -------------------------------------------------------------------------------------------------------------

        //When a route have an error on load
        $scope.$on('$routeChangeError',function(evt, route1, route2, httpResponse){

            //If we have an status of 421, template couldn't be found
            switch (httpResponse.status) {
                case 421:
                    $location.path('/ng/404/').replace();
                    break;
            }
        });

        //
        // Timing
        // -------------------------------------------------------------------------------------------------------------

        //Retrieves current wod that is in timing
        $scope.getCurrentTimingWod = function(){ return $rootScope.mainData.wod_timing; };

        //Sets start data for can bre read with JS
        var setStartDateCorrectly = function() {

            if(!$rootScope.mainData || !$rootScope.mainData.wod_timing) return;

            $rootScope.mainData.wod_timing.date_start_js = new Date($rootScope.mainData.wod_timing.date_start);
        };


        //Starts timer
        $scope.startTimer = function(wod) {
            var deferred = $q.defer();

            var servicePromise = timerService.start(wod.slug);

            servicePromise.success(function(data){
                $rootScope.mainData.wod_timing = data;

                notificationService.notifySuccess('You have started <b>' + data.wod.title + '</b> succesfully');
                setStartDateCorrectly();
                deferred.resolve();
            });

            servicePromise.error(function(error){
                $log.error(error);
                deferred.reject(error);
            });

            return deferred.promise;
        };

        //Stops timer
        $scope.stopTimer = function() {
            var deferred = $q.defer();

            //Open modal and get the promise
            var modalPromise = $modal.open({
                templateUrl: 'timer-close-wod.tpl',
                controller: 'mainApp.timer.close.ctrl',
                size: 'lg',
                resolve: {
                    startDate : function() {
                        return $rootScope.mainData.wod_timing.date_start_js;
                    },
                    endDate : function() {
                        return new Date();
                    },
                    wod: function(){
                        return $rootScope.mainData.wod_timing.wod;
                    }
                }
            }).result;

            //When modal is confirmed
            modalPromise.then(function(data){
                delete $rootScope.mainData.wod_timing;
                deferred.resolve();
            });

            //When modal is dismissed
            modalPromise.catch(function(){
                deferred.reject();
            })

            //Return value
            return deferred.promise;
        };

    };

    /**
     * NavBar controller
     * @param $log
     * @param $scope
     */
    function navBarCtrl($log, $scope) {

    };

})(angular);