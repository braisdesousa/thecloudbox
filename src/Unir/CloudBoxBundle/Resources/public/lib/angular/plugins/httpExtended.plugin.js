/*
 * HTTP Extension:
 *
 * Extengsion of http for some aditional features:
 *  - Cancelation
 *
 * ---------------------------------------------------------------------------------------------------------------------
 */
(function(ng){

    var mainModule = ng.module('ngHttpExtension', []);

    mainModule.provider('$httpExtension', httpExtensionProvider);

    //Provider for the extension
    function httpExtensionProvider () {

        //Debug enabled?
        var enable_debug = false;

        //Ids for promises
        var id = 0;

        //here we put all the httpExtension created
        var httpExtensionLog = [];

        //Enables debug model
        this.enableDebugMode = function () {
            enable_debug = true;
        };

        this.disableDebugMode = function () {
            enable_debug = false;
        };

        //Retrieve fn
        this.$get = ['$log', '$http', '$q', function ($log, $http, $q) {

            //Log function (if is not in debug will not log)
            function log(msg) {
                if (enable_debug) {
                    $log.debug('{httpExtension} ' + msg);
                }
            };

            //Creates short methods
            function createShortMethods(httpPromiseWrapper, names) {
                ng.forEach(names, function (name) {
                    httpPromiseWrapper[name] = function (url, config) {
                        return httpPromiseWrapper(ng.extend(config || {}, {
                            method: name,
                            url: url
                        }));
                    }
                });
            };

            //Creates short methods with data
            function createShortMethodsWithData(httpPromiseWrapper, names) {
                ng.forEach(names, function (name) {
                    httpPromiseWrapper[name] = function (url, data, config) {
                        return httpPromiseWrapper(ng.extend(config || {}, {
                            method: name,
                            url: url,
                            data: data
                        }));
                    }
                });
            };

            //Indicates if debug is enabled
            function isDebugEnabled() {
                return enable_debug;
            }

            //adds the element to log
            function addToLog(element) {
                if (isDebugEnabled()) {
                    httpExtensionLog.push(element);
                }
            }

            //Extend the element
            function extendHttpExtension($httpExtension) {

                //Create short methods
                createShortMethods($httpExtension, ['get', 'delete', 'head', 'jsonp']);
                createShortMethodsWithData($httpExtension, ['post', 'put', 'patch']);

                $httpExtension.getHistory = function(){
                    return httpExtensionLog;
                };

                return $httpExtension;
            }

            //This is the main function, will
            var $httpExtension = function (config) {

                //We create a cancelPromise
                var cancelPromise = $q.defer();

                //Extend config
                config.timeout = cancelPromise.promise;

                //create the httpPromise
                var httpPromise = $http(config);

                //Creates a new wrapper
                var httpPromiseWrapper = new function () {

                    var $this = this;

                    this.$$id = ++id;
                    this.$$isCompleted = false;
                    this.$$isCanceled = false;
                    this.$$originalPromise = httpPromise;
                    this.$$cancelPromise = cancelPromise;
                    this.$$config = config;
                    this.$$start = new Date();
                    this.$$end = undefined;
                    this.$$response = undefined;
                    this.$$hasError = undefined;

                    var endWod = function () {
                        $this.$$end = new Date();
                        return $this.$$end.getTime() - $this.$$start.getTime();
                    };

                    log('generated [' + this.$$id + '] url: ' + config.url);

                    /**
                     * Cancel petition
                     */
                    this.cancel = function () {

                        if (this.$$isCanceled || this.$$isCompleted) return;

                        log('canceling [' + this.$$id + '] ' + endWod() + 'ms');
                        this.$$isCanceled = true;
                        this.$$cancelPromise.resolve('cancel');
                    };

                    /**
                     * Check if is completed
                     * @returns {boolean}
                     */
                    this.isCompleted = function () {
                        return this.$$isCompleted;
                    };

                    /**
                     * Check if httpPromise is canceled
                     * @returns {boolean}
                     */
                    this.isCanceled = function () {
                        return this.$$isCanceled;
                    };

                    /**
                     * Petition is canceled or completed?
                     * @returns {boolean}
                     */
                    this.isDone = function () {
                        return this.$$isCanceled || this.$$isCompleted;
                    }

                    /**
                     * Check if has start
                     * @returns {boolean}
                     */
                    this.hasStart = function () {
                        return true;
                    };

                    this.then = this.$$originalPromise.then;
                    this.catch = this.$$originalPromise.catch;
                    this.error = this.$$originalPromise.error;
                    this.finally = this.$$originalPromise.finally;
                    this.success = this.$$originalPromise.success;

                    //On success...
                    this.$$originalPromise.then(function (response) {
                        log('completed [' + $this.$$id + '] - success ' + endWod() + 'ms');
                        $this.$$isCompleted = true;
                        $this.$$hasError = false;
                        $this.$$response = response;
                    });

                    //On error...
                    this.$$originalPromise.catch(function (error) {

                        if (error.status == 0) {
                            return;
                        }

                        log('completed [' + $this.$$id + '] - error ' + endWod() + 'ms');
                        $this.$$isCompleted = true;
                        $this.$$hasError = true;
                        $this.$$response = error;
                    });
                }();

                //Add to log the element
                addToLog(httpPromiseWrapper);

                //Return the new and great promise
                return httpPromiseWrapper;
            };

            //Extend httpExtension
            extendHttpExtension($httpExtension);

            return $httpExtension;
        }];
    };


})(angular);
