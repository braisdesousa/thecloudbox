(function(ng, jQuery){

    //We are going to use formDirective module
    var module = ng.module('ngSiwebForm', ['ngSiwebNotifications'])
        .constant('jQuery', jQuery);

    //Declare the directive in the form
    module.directive('formSave', ['$log', 'jQuery', 'notificationService', '$timeout', formSaveDirective]);


    /**
     * Directive for forms
     * @param $log
     * @param jQuery
     * @returns {{strict: string, link: Function}}
     */
    function formSaveDirective ($log, jQuery, notificationService, $timeout){

        //Clears control div error
        var clearDivErrors = function(control) {
            //Hide messages
            for(var msg in control.errorControls) {
                control.errorControls[msg].hide();
                control.control.$setValidity(msg, true);
            }

            control.formGroupDiv.removeClass('has-error');

            if(control.tab) {
                control.tab.jQNav.removeClass('has-tab-error');
                control.tab.jQTab.removeClass('has-tab-error');
            }

        };

        //This function helps retrieving controls from a formController
        var retrieveControls = function(jQForm, formController) {
            var result = [];

            for(var key in formController) {

                //If key have name, we have found a control
                if(formController[key] && formController[key].$name) {

                    var itm = {};

                    itm.name = key;
                    itm.control = formController[key];
                    itm.element = jQuery('[name=\'' + itm.control.$name + '\']', jQForm)[0];
                    itm.formGroupDiv = jQuery(itm.element).parents('.form-group');
                    itm.form = jQForm;
                    itm.errorControls = {};

                    //We are going to check if form have tabs and wich tab contains element
                    var tab = jQuery('.tab-pane', jQForm).has(itm.element);

                    //If item has tab
                    if (tab.length > 0 ) {

                        //We need the idx of tab in parent
                        var idx = tab.parent().children().index(tab);

                        //Now we need to traverse till nav-tabs and get the index
                        var navTab = jQuery('.nav-tabs', jQForm).children().get(idx);

                        itm.tab = {
                            jQNav : jQuery(navTab),
                            jQTab : jQuery(tab.get(0)),
                            idx: idx
                        };

                        if (!itm.tab.jQNav.data('controls')) {
                            itm.tab.jQNav.data('controls', []);
                            itm.tab.jQNav.addClass('form-tab-nav');
                        }

                        if (!itm.tab.jQTab.data('controls')) {
                            itm.tab.jQTab.data('controls', []);
                            itm.tab.jQTab.addClass('form-tab-pane');
                        }

                        itm.tab.jQNav.data('controls').push(itm);
                        itm.tab.jQTab.data('controls').push(itm);

                    }


                    jQuery("[data-form-save-error]", itm.formGroupDiv).each(function(){
                        var jItem = jQuery(this);
                        itm.errorControls[jItem.data('form-save-error')] = jItem;
                        jItem.hide();
                    });

                    //Store saveDirective on data
                    jQuery(itm.element).data('saveDirective', itm);

                    var fnClearDivErrors = function() {
                        clearDivErrors(jQuery(this).data('saveDirective'));
                    };

                    //If user changes input, need to clear errors
                    jQuery(itm.element).on("keyup", fnClearDivErrors);
                    jQuery(itm.element).on("change", fnClearDivErrors);

                    result.push(itm);
                }
            }

            return result;
        };

        /**
         * Will decorate tab
         * @param tabItem
         */
        function decorateTab(tabElement) {

            if(!tabElement || !tabElement.jQNav || !tabElement.jQTab) {
                return;
            }

            var isTabValid = true;
            var controls = tabElement.jQNav.data('controls');

            for(var i = 0; i < controls.length; i++) {
                if (!controls[i].control.$valid) {
                    isTabValid = false;
                    break;
                }
            }

            if (isTabValid) {
                tabElement.jQNav.removeClass('has-tab-error');
                tabElement.jQTab.removeClass('has-error');
            } else {
                tabElement.jQNav.addClass('has-tab-error');
                tabElement.jQTab.addClass('has-tab-error');

                //Extract scope
                var scope = tabElement.jQNav.data('$isolateScope');

                //Select tab...
                scope.$apply(function(){
                    scope.select();
                });
            }
        }

        //Decorates div if control is valid or not, and also show validation errors
        function decorateDiv(control) {

            //Hide all messages
            for(var msg in control.errorControls) {
                control.errorControls[msg].hide();
            }

            //If is valid, remove class
            if(control.control.$valid) {
                control.formGroupDiv.removeClass('has-error');
                decorateTab(control.tab);
            } else { //Not valid

                //Add error class
                control.formGroupDiv.addClass('has-error');
                decorateTab(control.tab);

                //Loop control errors
                for(var errorName in control.control.$error) {
                    if(control.errorControls && control.errorControls[errorName] && control.control.$error[errorName]) {
                        control.errorControls[errorName].show();
                    }
                }
            }
        };

        //Disables elements and show Spinner
        function disableElements(controls, buttons) {

            for(var i = 0; i < controls.length; i++) {
                jQuery(controls[i].element).attr('disabled', 'disabled');
            }

            buttons.cancelButton.attr('disabled', 'disabled');
            buttons.submitButton.attr('disabled', 'disabled');
            buttons.loadingSpinner.show();
        };

        //Enables elements
        function enableElements(controls, buttons) {
            for(var i = 0; i < controls.length; i++) {
                jQuery(controls[i].element).removeAttr('disabled');
            }

            buttons.cancelButton.removeAttr('disabled');
            buttons.submitButton.removeAttr('disabled');
            buttons.loadingSpinner.hide();
        };

        /**
         * Will show validationErrors when there are no translations
         * @param errorData
         */
        function highlighErrors(errorData, controls, elem) {

            var formName = jQuery(elem).attr('name');

            var childs = null;

            if (errorData.form ) {
                childs = errorData.form;
            } else if( errorData.errors ) {
                childs = errorData.errors;
            }

            for( var propName in childs.children) {

                var errorElement = childs.children[propName];

                //If error element haven't nothing, then continue
                if (!errorElement.errors  || !errorElement.errors.length) {
                    continue;
                }

                //Retrieve the control
                var control = null;
                for ( var i = 0; i < controls.length; i++) {

                    //Same as name...
                    if(controls[i].name == propName) {
                        control = controls[i];
                        break;
                        //Same as name with formName
                    } else if(controls[i].name == (formName + '[' + propName +']')) {
                        control = controls[i];
                        break;
                        //Last condition, just imagine that the element is an array form "formName[formElement][]"
                    } else if(controls[i].name == (formName + '[' + propName + '][]')) {
                        control = controls[i];
                        break;
                    }
                }

                //TODO: What happens if we don't have the control??
                if(!control) {
                    continue;
                }

                var errorMessage = '';

                for(var x = 0; x < errorElement.errors.length; x++ ) {
                    errorMessage += errorElement.errors[x] + ' <br />';
                }

                //Set server error to false
                control.control.$setValidity('serverError', false);
                control.errorControls['serverError'].html(errorMessage);

                //Decorate the div for print the error
                decorateDiv(control);
            }

        };

        /**
         * Will show validation errors when there are translations
         * @param errorData
         * @param controls
         */
        function highlighErrorTranslations(errorData, controls, elem) {

            var formName = jQuery(elem).attr('name');

            var errorItm = errorData.form
                ? errorData.form.children.translations.children
                : errorData.errors.children.translations.children;

            //first start with defaultLocale
            for (var language in errorItm.defaultLocale.children) {
                for(var propName in errorItm.defaultLocale.children[language].children) {

                    var errorElement = errorItm.defaultLocale.children[language].children[propName];

                    //If error element haven't nothing, then continue
                    if (!errorElement.errors  || !errorElement.errors.length) {
                        continue;
                    }

                    //Retrieve the control
                    var control = null;

                    ///Wich name will have the control
                    var controlName = formName + '[translations][defaultLocale][' + language + '][' + propName + ']';

                    for ( var i = 0; i < controls.length; i++) {
                        if (controls[i].name == controlName) {
                            control = controls[i];
                            break;
                        }
                    }

                    //TODO: What happens if we don't have the control??
                    if(!control) {
                        continue;
                    }

                    var errorMessage = '';

                    for(var x = 0; x < errorElement.errors.length; x++ ) {
                        errorMessage += errorElement.errors[x] + ' <br />';
                    }

                    //Set server error to false
                    control.control.$setValidity('serverError', false);
                    control.errorControls['serverError'].html(errorMessage);

                    //Decorate the div for print the error
                    decorateDiv(control);

                }
            }

            //Now translations
            for (var language in errorItm.translations.children) {
                for(var propName in errorItm.translations.children[language].children) {

                    var errorElement = errorItm.translations.children[language].children[propName];

                    //If error element haven't nothing, then continue
                    if (!errorElement.errors  || !errorElement.errors.length) {
                        continue;
                    }

                    //Retrieve the control
                    var control = null;

                    ///Wich name will have the control
                    var controlName = formName + '[translations][translations][' + language + '][' + propName + ']';

                    for ( var i = 0; i < controls.length; i++) {
                        if (controls[i].name == controlName) {
                            control = controls[i];
                            break;
                        }
                    }

                    //TODO: What happens if we don't have the control??
                    if(!control) {
                        continue;
                    }

                    var errorMessage = '';

                    for(var x = 0; x < errorElement.errors.length; x++ ) {
                        errorMessage += errorElement.errors[x] + ' <br />';
                    }

                    //Set server error to false
                    control.control.$setValidity('serverError', false);
                    control.errorControls['serverError'].html(errorMessage);

                    //Decorate the div for print the error
                    decorateDiv(control);

                }
            }
        }

        //Directive return
        return {
            strict: 'A',
            link: function($scope, elem, attrs) {

                //Retrieve formController
                var formController = elem.data('$formController');

                //Retrieve controls...
                var controls = retrieveControls(jQuery(elem), formController);

                //Retrieve buttons
                var buttons = {
                    'cancelButton': jQuery('[form-save-cancel-button]', jQuery(elem)),
                    'submitButton': jQuery('[form-save-submit-button]', jQuery(elem)),
                    'loadingSpinner': jQuery('[form-save-loading-spinner]', jQuery(elem))
                };

                //Hide spinner
                buttons.loadingSpinner.hide();


                //If there are no controls then set a timeout
                if (!controls || !controls.length) {
                    $timeout(function(){
                        controls = retrieveControls(jQuery(elem), formController);
                    }, 500);
                }


                elem.on('submit',function(event){
                    //Prevent normal submit
                    event.preventDefault();

                    //Try to retrieve parentScope if there is any
                    var parentScope = null;
                    if(attrs.parentScope) {
                        parentScope = $scope.$eval(attrs.parentScope);
                    }

                    //Foreach control try to decorate divs (that will print error or not)
                    for(var i = 0; i < controls.length; i++) {
                        decorateDiv(controls[i]);
                    }


                    //If form isn't valid...
                    if ( !formController.$valid ) {

                        notificationService.notifyError('There are validation errors');
                        return false;
                    }

                    //Disable elements till is loading..
                    disableElements(controls, buttons);

                    //If is valid, then call method
                    var responsePromise = $scope.$eval(attrs.formSave);

                    //If we don't have a promise, then enable elements again
                    if(!responsePromise || !responsePromise.then) {
                        enableElements(controls, buttons);
                        return false;
                    }


                    //If is correct, then...
                    responsePromise.then(function(msg){

                        //First of all we enable the elements
                        enableElements(controls, buttons);

                        //if is a string
                        if(typeof msg == 'string') {
                            notificationService.notifySuccess(msg);
                        }  //If is an object
                        else if(typeof msg == 'object') {

                            //Have message property ?
                            if(msg.message) {
                                notificationService.notifySuccess(msg.message);
                            }
                        }

                        return false;
                    });



                    //If we are handling an error..
                    responsePromise.catch(function(errorData){

                        //First of all we enable the elements
                        enableElements(controls, buttons);

                        //If our errorData is a string, just print if we can
                        if (errorData && typeof errorData == 'string') {

                            notificationService.notifyError(errorData);
                            return false;
                        }

                        //If our errorData is an object
                        if ( errorData && typeof errorData == 'object') {

                            //If we have errorData and errors
                            if (errorData &&
                                (( errorData.form && errorData.form.children ) ||
                                (errorData.errors && errorData.errors.children))) {


                                if (errorData.form && errorData.form.children && errorData.form.children.translations) {

                                    highlighErrorTranslations(errorData, controls, elem);

                                } else if(errorData.errors && errorData.errors.children && errorData.errors.children.translations) {

                                    highlighErrorTranslations(errorData, controls, elem);

                                } else if( (errorData.form && errorData.form.children()) || (errorData.errors && errorData.errors.children)) {

                                    highlighErrors(errorData, controls, elem);

                                }


                            }

                            return false;
                        }

                    });
                });
            }
        }
    };
})(angular, jQuery);