(function(ng, jQuery){

    var app = ng.module('ngSpecialClicks', [])
        .constant('jQuery', jQuery);


    app.directive('ngClickLoadReplace', ['$log', clickAndReplaceWithSpinner]);


    function clickAndReplaceWithSpinner($log) {

        return {
            restrict : 'A',
            link: function($scope, elem, attrs) {

                //We get the link fn
                var linkFn = attrs.ngClickLoadReplace;
                var jElement = jQuery(elem);

                //On element click
                jElement.on('click', function(){
                    //We need to apply on scope...

                    $scope.$apply(function(){

                        var promise =  $scope.$eval(linkFn);

                        //If we don't have a promise, then don't do nothing
                        if(!promise || !promise.then) {
                            return;
                        }

                        //Here we know that we have a promise

                        //First of all, add disable
                        jElement.attr('disabled', 'disabled');

                        //Get current html
                        var html = jElement.html();

                        //Replace for spinner
                        jElement.html('<i class=\'fa fa-spinner fa-spin\'></i>');

                        //When promise finish
                        promise.then(function(){

                            //Set again the html and enable it
                            jElement.html(html);
                            jElement.removeAttr('disabled');

                            if(!$scope.$$phase && !$scope.$root.$$phase) {
                                $scope.$apply();
                            }
                        });

                        //If there is an error...
                        promise.catch(function(){
                            //Set again the html and enable it
                            jElement.html(html);
                            jElement.removeAttr('disabled');

                            if(!$scope.$$phase && !$scope.$root.$$phase) {
                                $scope.$apply();
                            }
                        });

                    });
                });
            }
        }

    };

})(angular, jQuery);