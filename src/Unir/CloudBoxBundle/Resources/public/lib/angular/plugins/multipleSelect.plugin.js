(function(ng, jQuery){

    var app = ng.module('ngMultipleSelect', [])
        .constant('jQuery', jQuery);

    app.directive('ngMultipleSelect', ['$log', 'jQuery', multipleSelectDirective]);


    /**
     * Directive for multiple select
     * @param $log
     * @param jQuery
     * @returns {{restrict: string, link: Function}}
     */
    function multipleSelectDirective($log, jQuery) {

        var NG_OPTIONS_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?(?:\s+group\s+by\s+(.*))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+(.*?)(?:\s+track\s+by\s+(.*?))?$/;

        var _getCollection = function(collection, indexer) {

            var result = collection;

            //Split per point
            var lvls = indexer.split('.');

            for(var i = 0; i < lvls.length; i++) {

                if(result == undefined) {
                    break;
                }

                result = result[lvls[i]];
            }

            return result;
        };

        return {
            restrict: 'A',
            require : 'ngModel',
            link:  function($scope, element, attr, ngModel) {

                //Without ngModel
                if (!ngModel) {
                    return;
                }

                var originalCollectionStr, originalCollection, setterProperty;

                if(attr.ngOptions) {
                    var match = attr.ngOptions.match(NG_OPTIONS_REGEXP);

                    if(match[1]) {
                        setterProperty = match[1].split('.');
                        setterProperty.shift();
                        setterProperty = setterProperty.join('.')
                    }

                    originalCollectionStr = match[7];
                    originalCollection = _getCollection($scope, originalCollectionStr);
                } else {
                    originalCollectionStr = attr.ngModel;
                    originalCollection = _getCollection($scope, attr.ngModel);
                }





                var jElement = jQuery(element);

                var jElementData = jElement.data();
                var extension = {
                    onClose : function(){

                        jElement.trigger('change');
                        $scope.$apply(function(){

                            if (attr.ngOptions && setterProperty) {

                                var match = attr.ngOptions.match(NG_OPTIONS_REGEXP);
                                originalCollectionStr = match[7];
                                originalCollection = _getCollection($scope, originalCollectionStr);

                                var indexes = jElement.val();
                                var trueValues = [];

                                if(!indexes) {
                                    ngModel.$setViewValue([]);
                                    return;
                                }

                                for (var i = 0; i < indexes.length; i++) {
                                    trueValues.push(_getCollection(originalCollection[indexes[i]], setterProperty));
                                }
                                ngModel.$setViewValue(trueValues);
                            } else {
                                ngModel.$setViewValue(jElement.val());
                            }

                        });
                    }
                };

                jElementData = ng.extend(jElementData, extension);

                //We need to set select width to full width
                jElement.css('width','100%').multipleSelect(jElementData);

                $scope.$watchCollection(attr.ngModel, function(newValue, oldValue) {
                    jElement.multipleSelect('refresh');
                });
                $scope.$watchCollection(originalCollectionStr, function(newValue, oldValue) {
                    jElement.multipleSelect('refresh');
                });
            }
        }
    }

})(angular, jQuery);