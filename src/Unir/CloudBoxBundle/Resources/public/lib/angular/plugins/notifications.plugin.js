(function(ng, jQuery){

    var app = ng.module('ngSiwebNotifications',[])
        .constant('jQuery', jQuery);

    //Register service
    app.service('notificationService', ['$log', 'jQuery', notificationsService]);

    //Register directive
    app.directive('ngShowNotifications', ['$log', notificationsDirective]);

    //We must only have one notifications queue
    var notifications = [];

    //Watchers to notifications (Each directive created is a watcher)
    var watchers = [];

    //Adds a watcher
    var addWatcher = function(watcherFn) {
        watchers.push(watcherFn);
    };

    //Wake up watchers
    var notifyWatchers = function() {

        for(var i = 0; i < watchers.length; i++) {
            if (watchers[i]) {
                watchers[i]();
            }
        }

    };

    //Adds a notification and call watchers
    var addNotification = function(data) {
        notifications.push(data);
        notifyWatchers();
    };

    //Clears all notifications
    var clearNotifications = function() {
        notifications.splice(0, notifications.length);
    };


    /**
     * Service for notifications, see that use notifications as closure
     * that iwll ensure that will only one notifications
     * @param $log
     */
    function notificationsService($log, jQuery) {


        /**
         * Retrieve all notifications
         * @returns {Array}
         */
        this.getNotifications = function() {
            return notifications;
        };

        /**
         * Perfoms a notification, the avaliable data parameters are:
         *
         * Uses http://ned.im/noty/
         *
         * text: message to show
         * type: message type (alert, success, error, warning, information)
         * container: id of container that you want to show in, undefined for show in general
         * layout: top, topCenter, topRight, topLeft, etc..
         * timeout: avaliable time wich message will show
         * modal: message is modal
         *
         * @param data
         * @param type
         */
        this.notify = function(data, type) {

            //If we have an string, we can handle it
            if(typeof data == 'string' || data instanceof String) {
                data = {
                    text: data
                };
            }

            //Here we extend data with default data
            data = angular.extend({
                type: 'alert',
                layout: 'topCenter',
                timeout: 3000,
                modal: false
            }, data);

            if (type) {
                data.type = type;
            }

            if(!data.text) throw 'You must specify a message';

            //Add notification
            addNotification(data);
        };

        /**
         * Notify error
         * Like notify function
         * @param data
         */
        this.notifyError = function(data) {
            this.notify(data, 'error');
        };

        /**
         * Notify warning
         * Like notify function
         * @param data
         */
        this.notifyWarning = function(data) {
            this.notify(data, 'warning');
        };

        /**
         * Notify success
         * Like notify function
         * @param data
         */
        this.notifySuccess = function(data) {
            this.notify(data, 'success');
        };

        /**
         * Closes all notifications
         */
        this.notifyCloseAll = function() {

            if(jQuery.noty && jQuery.noty.closeAll) {
                jQuery.noty.closeAll();
            }

        };
    };

    /**
     * Directive for show notifications
     * @param $log
     * @returns {{restrict: string, link: Function}}
     */
    function notificationsDirective($log, jQuery) {
        return {
            restrict: 'EA',
            link: function(scope, element, attr) {

                //Add this to watchers
                addWatcher(function(){


                    //There is not need to use apply because we aren't using scope

                    if(notifications.length == 0) return;

                    for(var i = 0; i < notifications.length; i++) {

                        var notification = notifications[i];

                        if (notification.container) {
                            jQuery('#' + notification.container).noty(notification);
                        } else {
                            noty(notification);
                        }
                    };


                    //When all notifications are shown, then we must clear all
                    clearNotifications();

                });
            }
        };
    };

})(angular, jQuery);