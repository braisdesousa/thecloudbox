jQuery(document).ready(function($){

    $("[data-trigger]").click(function (){
        if ($(this).attr("data-trigger")=="disableUser"){
            disableUser($(this));
        }
        if ($(this).attr("data-trigger")=="enableUser"){
            enableUser($(this));
        }
        if ($(this).attr("data-trigger")=="removeUser"){
            removeUser($(this));
        }
    });
    
    
    function enableUser(element)
    {
        $.ajax({
            type: "PATCH",
            url: "/user/",
            data: { id: $(element).attr("data-id"), method: "enable" },
            context: $(element),
            success: function (info)
            {
                if (info.error)
                {
                    alert("Error");
                } else {
                    $(element).removeClass("btn-success").addClass("btn-warning");
                    $(element).html("Disable User");
                    $(element).attr("data-trigger","disableUser")
                }
            }
        });
    }
    function disableUser(element)
    {
        $.ajax({
            type: "PATCH",
            url: "/user/",
            data: { id: $(element).attr("data-id"), method: "disable" },
            context: $(element),
            success: function (info)
            {
                if (info.error)
                {
                    alert("Error");
                } else {
                    $(element).removeClass("btn-warning").addClass("btn-success");
                    $(element).html("Enable User");
                    $(element).attr("data-trigger","enableUser")
                }
            }
        });   
    }
    function removeUser(element)
    {
        $.ajax({
            type: "DELETE",
            url: "/user/",
            data: { id: $(element).attr("data-id")},
            context: $(element),
            success: function (info)
            {
                if (info.error)
                {
                    alert("Error");
                } else {
                    $(element).parents("tr").remove();
                    location.reload(true)
                }
            }
        });
    }

    $('.viewUser').on('click','.editUserProfile',function(e){
        e.stopPropagation();
        e.preventDefault();
        var id = $(this).data('id');
        var name = $(this).data('name');

        var $content = $('<div>', {class:'div_contentDialog modalEditUser'});
        $content.append(
            $('<div>',{class:'row'}).append(
                $('<div>',{class:'col-md-6'}).append(
                    $('<label>').text('UserName'),
                    $('<input>',{class:'form-control username',type:'text',name:'username',value:''})
                ),
                $('<div>',{class:'col-md-6'}).append(
                    $('<label>').text('Email'),
                    $('<input>',{class:'form-control email',type:'text',name:'email',value:''})
                )
            ),
            $('<div>',{class:'row mt10'}).append(
                $('<div>',{class:'col-md-12'}).append(
                    $('<h3>',{class:'cursor_pointer mt0'}).text('Wod Groups where the user is member').append($('<small>',{class:'fa fa-chevron-down'}))
                        .on('click',function(e){
                            $('.wodGroupsMember').slideToggle('slow');
                            if($(this).find('.fa').hasClass('fa-chevron-down')){
                                $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            }else{
                                $(this).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                            }
                        }),
                    $('<div>',{class:'wodGroupsMember borderSolidSilver pAll10 '})
                )
            ),
            $('<div>',{class:'row mt10'}).append(
                $('<div>',{class:'col-md-12'}).append(
                    $('<h3>',{class:'cursor_pointer mt0'}).text('Wod Groups where the user is follower').append($('<small>',{class:'fa fa-chevron-right'}))
                        .on('click',function(e){
                            $('.wodGroupsFollower').slideToggle('slow');
                            if($(this).find('.fa').hasClass('fa-chevron-down')){
                                $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            }else{
                                $(this).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                            }
                        }),
                    $('<div>',{class:'wodGroupsFollower borderSolidSilver pAll10 '}).hide()
                )
            ),
            $('<div>',{class:'row mt10'}).append(
                $('<div>',{class:'col-md-12'}).append(
                    $('<h3>',{class:'cursor_pointer mt0'}).text('WorkGroups ').append($('<small>',{class:'fa fa-chevron-right'}))
                        .on('click',function(e){
                            $('.workGroups').slideToggle('slow');
                            if($(this).find('.fa').hasClass('fa-chevron-down')){
                                $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            }else{
                                $(this).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                            }
                        }),
                    $('<div>',{class:'workGroups borderSolidSilver pAll10'}).hide()
                )
            ),
            $('<div>',{class:'row mt10'}).append(
                $('<div>',{class:'col-md-12'}).append(
                    $('<h3>',{class:'cursor_pointer mt0'}).text('User Groups ').append($('<small>',{class:'fa fa-chevron-right'}))
                        .on('click',function(e){
                            $('.userGroups').slideToggle('slow');
                            if($(this).find('.fa').hasClass('fa-chevron-down')){
                                $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            }else{
                                $(this).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                            }
                        }),
                    $('<div>',{class:'userGroups borderSolidSilver pAll10'}).hide()
                )
            )
        );

        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'Edit User',
            buttons: {
                def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                confirm: {  label: 'Update',  className: "btn-primary confirm",  callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

        $bootbox.find('.confirm').attr('disabled','disabled');

        // find users and print
        $.ajax({
            type: "POST",
            url: "/json/findUserById/"+id,
            dataType: 'json',
            data: {},
            success: function(response){
                if(response.error != undefined &&  response.error >0 ){
                    console.log(response.error_message)
                }else{
                    response.user = JSON.parse(response.user);
                    response.userGroups= JSON.parse(response.userGroups);
                    response.wodGroups= JSON.parse(response.wodGroups);
                    response.workGroups = JSON.parse(response.workGroups);
                    $.each(response.userGroups, function(i,v){ response.userGroups[i]['text'] = v.name; });
                    $.each(response.wodGroups, function(i,v){ response.wodGroups[i]['text'] = v.name; });
                    $.each(response.workGroups, function(i,v){ response.workGroups[i]['text'] = v.name; });
                    console.log(response)
                    $('.modalEditUser .username').val(response.user.username);
                    $('.modalEditUser .email').val(response.user.email);
                    printMultiSelectGenericModal(
                        $content.find('.wodGroupsMember'),
                        '.wodGroupsMember',
                        response.wodGroups,
                        response.user.member_wod_groups,
                        'WodGroups available',
                        'Member of',
                        ''
                    );
                    printMultiSelectGenericModal(
                        $content.find('.wodGroupsFollower'),
                        '.wodGroupsFollower',
                        response.wodGroups,
                        response.user.followed_wod_groups,
                        'WodGroups available',
                        'Follower of',
                        ''
                    );
                    printMultiSelectGenericModal(
                        $content.find('.workGroups'),
                        '.workGroups',
                        response.workGroups,
                        response.user.member_workGroups,
                        'WorkGroups available',
                        'WorkGroups of',
                        name
                    );
                    printMultiSelectGenericModal(
                        $content.find('.userGroups'),
                        '.userGroups',
                        response.userGroups,
                        response.user.groups,
                        'WorkGroups available',
                        'WorkGroups of',
                        name
                    );
                    $bootbox.find('.confirm').removeAttr('disabled');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                //consoel.log("Failure: " + errorThrown);
                noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
            }
        });

        $bootbox.find('.confirm').off('click').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            $bootbox.find('.confirm').attr('disabled','disabled');

            var url = "/user/updateAsync/"+id;
            var params = new Object;
            params['username'] = $content.find('.username').val();
            params['email'] = $content.find('.email').val();
            $content.find('.wodGroupsMember #ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function (index) {
                params['idsWodGroupsMember['+index+']'] = $(this).data('id');
            });
            $content.find('.wodGroupsFollower #ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function (index) {
                params['idsWodGroupsFollower['+index+']'] = $(this).data('id');
            });
            $content.find('.workGroups #ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function (index) {
                params['idsWorkGroups['+index+']'] = $(this).data('id');
            });
            $content.find('.userGroups #ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function (index) {
                params['idsUserGroups['+index+']'] = $(this).data('id');
            });
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: params,
                success: function(response){
                    if(response.error != undefined &&  response.error >0 ){
                        $bootbox.find('.confirm').removeAttr('disabled');
                        var errors = JSON.parse(response.errors)
                        $.each(errors,function(i,v){
                            $('.modalEditUser').find('[name*="'+ v.property_path+'"]').closest('div')
                                .addClass('has-error')
                                .append($('<span>',{class:'text-danger messageError'}).text(v.message));
                            $('.modalEditUser').find('[name*="'+ v.property_path+'"]').on('change',function(e){
                                $(this).closest('div').removeClass('has-error').find('.messageError').remove();
                            });
                        });
                    }else{
                        location.reload(true);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    //consoel.log("Failure: " + errorThrown);
                    noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
                }
            });
        });


    });// end edit User


});
