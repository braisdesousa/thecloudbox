Date.prototype.format = function(format)
{
  var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(),    //day
    "h+" : this.getHours(),   //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
    "S" : this.getMilliseconds() //millisecond
  }

  if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
    (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  for(var k in o)if(new RegExp("("+ k +")").test(format))
    format = format.replace(RegExp.$1,
      RegExp.$1.length==1 ? o[k] :
        ("00"+ o[k]).substr((""+ o[k]).length));
  return format;
}

//trimming space from both side of the string
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
 
//trimming space from left side of the string
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
 
//trimming space from right side of the string
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}

//pads left
String.prototype.lpad = function(padString, length) {
	var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}
 
//pads right
String.prototype.rpad = function(padString, length) {
	var str = this;
    while (str.length < length)
        str = str + padString;
    return str;
}

//pads left
Number.prototype.lpad = function(padString, length) {
	var str = this.toString();
    while (str.length < length)
        str = padString + str;
    return str;
}
 
//pads right
Number.prototype.rpad = function(padString, length) {
	var str = this.toString();
    while (str.length < length)
        str = str + padString;
    return str;
}


//Table latin-1 -> ASCII
var CODE_NO_DIACRITICS = [

          0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
         10,  11,  12,  13,  14,  15,  16,  17,  18,  19,
         20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
         30,  31,  32,  33,  34,  35,  36,  37,  38,  39,
         40,  41,  42,  43,  44,  45,  46,  47,  48,  49,
         50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
         60,  61,  62,  63,  64,  65,  66,  67,  68,  69,
         70,  71,  72,  73,  74,  75,  76,  77,  78,  79,
         80,  81,  82,  83,  84,  85,  86,  87,  88,  89,
         90,  91,  92,  93,  94,  95,  96,  65,  66,  67,
         68,  69,  70,  71,  72,  73,  74,  75,  76,  77,
         78,  79,  80,  81,  82,  83,  84,  85,  86,  87,
         88,  89,  90, 123, 124, 125, 126, 127, 128, 129,
        130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
        140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
        150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
        160, 161, 162, 163, 164, 165, 166, 167, 168, 169,
        170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
        180, 181, 182, 183, 184, 185, 186, 187, 188, 189,
        190, 191,  65,  65,  65,  65,  65,  65, 198,  67,
         69,  69,  69,  69,  73,  73,  73,  73,  68,  78,
         79,  79,  79,  79,  79, 215,  79,  85,  85,  85,
         85,  89, 222, 223,  65,  65,  65,  65,  65,  65,
        230,  67,  69,  69,  69,  69,  73,  73,  73,  73,
        240,  78,  79,  79,  79,  79,  79, 247,  79,  85,
         85,  85,  85,  89, 254,  89
  ];

//Igualación invariante
String.prototype.equalsIgnoreCase = function(anotherString){
	
	if(!this && !anotherString) return true;
	if(this.length == 0 && anotherString.length == 0) return true;
	
	//For each character... 
	for(var i = 0; i < this.length && anotherString.length; i++){
		
		var cmp1 = CODE_NO_DIACRITICS[this.charCodeAt(i)];
		var cmp2 = CODE_NO_DIACRITICS[anotherString.charCodeAt(i)];
		
		if(cmp1 - cmp2 > 0) return false;
	}
	
	return this.length == anotherString.length;
};

//Comparación invariante
String.prototype.compareIgnoreCase = function(anotherString){
	
	return this.slugify() > anotherString.slugify() ? 1 : -1;
}

//Sluglify (Transformar a ASCII)
String.prototype.slugify = function(){
	
	var newStr = "";
	
	for(var i = 0; i < this.length; i++){
		newStr += String.fromCharCode(CODE_NO_DIACRITICS[this.charCodeAt(i)]);
	}
	
	return newStr;
};

//Contains
String.prototype.containsIgnoreCase = function(anotherString){
	
	var str1 = anotherString.slugify();
	var str2 = this.slugify();
	
	var reg = new RegExp(str1);
	
	return reg.test(str2);
}; 

/**
 * Formatea por los patrones de referencia... 
 * @param pattern
 */
String.prototype.formatRef = function(pattern){
	
	//Cojemos el patron y hacemos un split por la , cojemos el tercer elemento y separamos por ; pillando el primer elemento
	var witchPattern = pattern.split(',')[2].split(';')[0]; 
	
	//Ahora algo mas complejo, quitamos el numero de caracteres por la derecha que tenemos en el string actual y agregamos el string actual
	return witchPattern.substr(0, witchPattern.length - this.length) + this;
};
