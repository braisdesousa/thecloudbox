$(document).on('ready', function(){

    $('.viewWod').on('click','.viewAllUsers',function(e){
        $(this).closest('p').find('.avatarUser').removeClass('hide').addClass('mt10');
        $(this).addClass('hide');
    });

    //add new comment async
    $('.viewWod').on('submit','.newCommentDiv form',function(e){
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var idWod = $('.newCommentDiv').data('idwod');
        var url = "/wodcomments/createAsync";
        var params = new Object;
        $(this).find('button').attr('disabled','disabled');

        $('.newCommentDiv form').find('select, input, textarea').each(function(i){
            params[$(this).attr('name')] = $(this).val()
        });
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: params,
            success: function(response,textStatus,jqXHR){
                $this.find('.wodComment').val('');
                //console.log($.isEmptyObject(response))
                if(response.error == undefined){
                    var url = "/view/commentsWodById/" + idWod;
                    var params = new Object;
                    params['id'] = idWod;
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: 'html',
                        data: params,
                        success: function(response){
                            $('.viewWod .wodComments').empty().append(response);
                            $('.newCommentDiv').find('button').removeAttr('disabled');
                        },
                        error: function(){
                            $('.newCommentDiv').find('button').removeAttr('disabled');
                        }
                    });
                }else{
                    console.log('error');
                }
            },
            error: function(jqXHR,textStatus,errorThrown){
                console.log('error');
            }
        });
    });// end submit form comments add new  comment async

    // edit description and title wod
    $('.viewWod').on('click','.editTitleAndDescription', function(e){
        var idWod = $(this).data('idwod');
        var field = $(this).data('field');
        var slug = $(this).data('slug');
        var title = $(this).data('title');
        var description = $(this).data('description');
        var viewBy = $(this).data('view');

        var params = new Object;
        params['id'] = idWod;
        params['field'] = field;

        var $content = $('<div>', {class:'div_contentDialog modalUpdateWod'});
        $content.append(
            $('<div>').append(
                $('<label>').text('Title'),
                $('<input>',{class:'form-control newTitleWod',type:'text',name:'title',value:title})
            ),
            $('<div>',{class:'mt10'}).append(
                $('<label>').text('Description'),
                $('<textarea>',{class:'form-control newDescriptionWod',cols:'5',name:'description'}).text(description)
            ),
            $('<div>',{class:'mt10'}).append(
                $('<label>').text('View By'),
                $('<select>',{class:'form-control newViewByWod displayBlock',type:'text',name:'viewBy'}).append(
                    $('<option>',{val:'all',class:'all'}).text('All'),
                    $('<option>',{val:'assigned_and_followers',class:'assigned_and_followers'}).text('Assigned and follower'),
                    $('<option>',{val:'assigned',class:'assigned'}).text('Assigned')
                )
            )
        );
        $content.find('.newViewByWod').val(viewBy);
        $content.find('.newViewByWod').chosen();
        $content.find('.newViewByWod').closest('div').find('.chosen-container').css('width','50%').css('display','block');
        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'Update '+field+' wod',
            buttons: {
                def: { label: 'Cancel', className: "btn-default pull-left", callback: function() {} },
                confirm: { label: 'Update', className: "btn-primary confirm", callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

        $bootbox.find('.confirm').off('click').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var url = "/wod/updateFieldWod/" + slug;
            var params = new Object;
            params['id'] = idWod;
            params['field'] = field;
            params['title'] =  $('.newTitleWod').val();
            params['description'] =  $('.newDescriptionWod').val();
            params['viewBy'] =  $('.newViewByWod').val();
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: params,
                success: function(response){
                    var wod = JSON.parse(response.wod);
                    if(response.error > 0){
                        $('.newValueWod').closest('div').addClass('has-error');
                        var errors = JSON.parse(response.errors);
                        $.each(errors,function(i,v){
                            $('.newValueWod').closest('div').append($('<span>',{class:'text-danger messageError'}).text(v.message));

                            $('.newValueWod').on('change',function(e){
                                $(this).closest('div').removeClass('has-error').find('.messageError').remove();
                            });
                        });
                    } else {

                        //All application using window reload, and here you don't wanted to?
                        // ok, i am going to do for user xperience...

                        if (response.mustRedirect) {
                            window.location = '/';
                        } else {

                            //Check if HTML replaceState is avaliable
                            if (window.history && window.history.replaceState) {
                                window.history.replaceState({}, 'loading... ', '/wod/' + wod.slug);

                                window.location.reload(true);
                            } else {
                                window.location = '/wod/' + wod.slug;
                            }
                        }


                        //Lines below are ok, but user xperience is so bad cuz don't know what is happening
                        // Also you didn't mantain same markup
                        /*
                        window.history.pushState('', "ChangeTitle",wod.slug);
                        $('.viewWod .editTitleAndDescription')
                            .data('slug',wod.slug)
                            .data('title',wod.title)
                            .data('description',wod.description);
                        textViewBy = '';
                        textViewBy = wod.view_by == 'all' ? 'All':textViewBy;
                        textViewBy = wod.view_by == 'assigned_and_followers' ? 'Assigned and follower':textViewBy;
                        textViewBy = wod.view_by == 'assigned' ? 'Assigned':textViewBy;

                        $('.viewWod .viewByOfWod').text(textViewBy);
                        $('.viewWod .wodDescription').text(wod.description).addClass('well well-sm');
                        $('.viewWod .wodTitle').html( wod.title + ' <small>#'+idWod+'</small>' );
                        $bootbox.find('.close').trigger('click');// cerrar modal */
                    }

                }
            });

        });
    });// end edit title and description wod


    if(window.registerEditWodDropDown === undefined) {

        window.registerEditWodDropDown = true;

        // update status and difficulty wod
        $('body').on('click','.editWodDropdown', function(e){
            var idWod = $(this).closest('ul').data('idwod');
            var slug = $(this).closest('ul').data('slug');
            var field = $(this).data('field');
            var value = $(this).data('value');
            var params = new Object;
            params['field'] = field;

            if(value == 'Closed'){
                var $content = $('<div>', {class:'div_contentDialog modalUpdateWod'});
                $content.append(
                    $('<p>').text('Are you sure you want to change '+field+' to "'+value+'"?')
                );
                // custom dialog
                var $bootbox = bootbox.dialog({
                    animate:false,
                    message: $content,
                    title: 'Update '+field+' wod',
                    buttons: {
                        def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                        confirm: {  label: 'Update',  className: "btn-primary confirm",  callback: function() {} }
                    }
                });//findel custom dialog
                //$bootbox.find('.modal-dialog').addClass('modal-lg');

                $bootbox.find('.confirm').off('click').on('click', function(e){
                    e.preventDefault();
                    e.stopPropagation();

                    var url = "/wod/updateFieldWod/" + slug;
                    var params = new Object;
                    params['id'] = idWod;
                    params['field'] = field;
                    params['value'] =  value;
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: 'json',
                        data: params,
                        success: function(response){
                            $bootbox.find('.close').trigger('click');// cerrar modal
                            location.reload(true);
                        }
                    });
                });
            }else{
                var url = "/wod/updateFieldWod/" + slug;
                var params = new Object;
                params['field'] = field;
                params['value'] =  value;
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: params,
                    success: function(response){
                        location.reload(true);
                    }
                });
            }

        });// end update status and difficulty wod

    }
    // edit users
    $('.viewWod').on('click','.editUsersWod', function(e){
        var idWod = $(this).data('idwod');
        var slug = $(this).data('slug');
        var field = $(this).data('field');
        var values = $(this).data('value');
        var users =  $(this).data('users');
        var params = new Object;
        params['id'] = idWod;
        params['field'] = field;

        var $content = $('<div>', {class:'div_contentDialog modalUpdateWod'});
        $content.append($('<label>').text('Users Assigned'));
        $content.append($('<br>'));
        var $select = $('<select>',{class:' selectUsersAssigned multiGeneric',multiple:'multiple'});
        var uA = [];
        $.each(values,function(i,v){ uA.push(v.id); });
        $.each(users,function(i,v){
            if($.inArray(v.id,uA)>=0){
                $select.append( $('<option>',{value: v.id,selected:'selected'}).text(v.username) );
            }else{
                $select.append( $('<option>',{value: v.id}).text(v.username) );
            }
        });
        $select.appendTo($content);
        $select.css('width','50%').removeClass('hide').multipleSelect({ filter: true });
        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'Update '+field+' wod',
            buttons: {
                def: { label: 'Cancel', className: "btn-default pull-left", callback: function() {} },
                confirm: { label: 'Update', className: "btn-primary confirm", callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

        $bootbox.find('.confirm').off('click').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var url = "/wod/updateFieldWod/" + slug;
            var params = new Object;
            params['field'] = field;
            params['value'] =  $('.selectUsersAssigned').val() == null ? [] : $('.selectUsersAssigned').val();

            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: params,
                success: function(response){
                    var wod = JSON.parse(response.wod);
                    location.reload(true);
                    $bootbox.find('.close').trigger('click');// cerrar modal
                }
            });

        });
    });// end edit users

    //UnassignMe button
    $('.viewWod').on('click','.assignMe', function(e){
        var idWod = $(this).data('idwod');
        var slug = $(this).data('slug');
        var field = $(this).data('field');
        var value = $(this).data('value');
        var params = new Object;
        params['field'] = field;


        var url = "/wod/updateFieldWod/" + slug;
        var params = new Object;
        params['field'] = field;
        params['value'] =  value;
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: params,
            success: function(response){

                //If we get response, check mustRedirect
                if(response.mustRedirect) {
                    window.location = '/';
                } else { //Else, a simple reload will be fine
                    window.location.reload(true);
                }

            }
        });
    });// end follow and assing me

    // edit wod group
    $('.viewWod').on('click','.editWodGroup', function(e){
        var idWod = $(this).data('idwod');
        var slug = $(this).data('slug');
        var field = $(this).data('field');
        var value = $(this).data('value');
        var params = new Object;
        params['id'] = idWod;
        params['field'] = field;
        params['value'] = value;

        var $content = $('<div>', {class:'div_contentDialog modalUpdateWodGroup'});
        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'Update '+field+' wod',
            buttons: {
                def: { label: 'Cancel', className: "btn-default pull-left", callback: function() {} },
                confirm: { label: 'Update', className: "btn-primary confirm", callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

        $.ajax({
            type: "POST",
            url: '/json/findWodGroups/',
            dataType: 'json',
            success: function(response){
                response.wodGroups = JSON.parse(response.wodGroups);
                $content.append($('<label>').text('Wod Group'));
                $content.append($('<br>'));
                var $select = $('<select>',{class:' wodGroup chosenGeneric'});

                $.each(response.wodGroups ,function(i,v){
                    if(value== v.id){
                        $select.append( $('<option>',{value: v.id,selected:'selected'}).text(v.name) );
                    }else{
                        $select.append( $('<option>',{value: v.id}).text(v.name) );
                    }
                });
                $select.appendTo($content);

                $select.chosen();
                $select.closest('div').find('.chosen-container').css('width','50%');
            }
        });

        $bootbox.find('.confirm').off('click').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var url = "/wod/updateFieldWod/" + slug;
            var params = new Object;
            params['field'] = field;
            //params['value'] = value;
            params['value'] =  $('select.wodGroup').val();

            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: params,
                success: function(response){
                    if(response.error > 0){
                        $('.newValueWod').closest('div').addClass('has-error');
                        var errors = JSON.parse(response.errors);
                        $.each(errors,function(i,v){
                            $('.select.wodGroup').closest('div').append($('<span>',{class:'text-danger messageError'}).text(v.message));

                            $('.select.wodGroup').on('change',function(e){
                                $(this).closest('div').removeClass('has-error').find('.messageError').remove();
                            });
                        });
                    }else{
                        location.reload(true);
                        $bootbox.find('.close').trigger('click');// cerrar modal
                    }
                }
            });

        });
    });// end edit wod group

});//end ready