
$(document).on('ready', function(){

    $.ajaxSetup({
        error: function (x, status, error) {
            console.log(x)
            if(x.status>0){
                noty({text: "Failure: " + error, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
            }
            if (x.status == 403) {
                window.location.reload(true);
            }
        }
    });
    $('.chosenGeneric').chosen();
    $('.chosenGeneric').closest('div').find('.chosen-container').css('width','100%');
    $('.tooltipGeneric').tooltip();
    $(".multiGeneric").css('width','100%').removeClass('hide').multipleSelect({
        filter: true
    });

    $('body').on('click','*',function(e){
        if( $(this).hasClass('popoverGeneric') ){
            e.preventDefault();
            e.stopPropagation();
            $('.popoverGeneric').popover('destroy');
            $(this).popover('show');
            $('.popover').on('mouseleave',function(e){
                $('.popoverGeneric').popover('destroy');
            });
        }else{
            $('.popoverGeneric').popover('destroy');
        }
    });
    $("[data-trigger='delete']").click(function (){
        bootbox.dialog({
           message: "Confirm deletion of '"+$(this).attr("data-name")+"'",
           title: "Delete "+$(this).attr("data-name"),
           buttons: {
               delete:{
                   label: "Delete",
                   className: "btn-danger",
                   callback:function(){
                       $(".delete-form button").click();
                   }
               },
               cancel:{
                   label: "Cancel",
                   className: "btn-info"
               }
           }

        });
    });
});