$(document).on('ready', function(){

    countWodsEntity(true);
    findWodsInEntityFilters();
    $('.filterWodsShowEntity').on('change','input', function(e){
        countWodsEntity(false);
        findWodsInEntityFilters();
        $('.tooltipGeneric').tooltip();
    });

    if(window.registerEditWodDropDown === undefined) {

        window.registerEditWodDropDown = true;

        //  update difficulty and status for wod
        $('body').on('click','.editWodDropdown',function(e){
            var idWod = $(this).closest('ul').data('idwod');
            var slug = $(this).closest('ul').data('slug');
            var field = $(this).data('field');
            var value = $(this).data('value');
            var params = new Object;
            params['field'] = field;
            params['value'] =  value;
            if( value == 'Closed' ){
                var $content = $('<div>', {class:'div_contentDialog modalUpdateWod'});
                $content.append(
                    $('<p>').text('Are you sure you want to change?')
                );
                // custom dialog
                var $bootbox = bootbox.dialog({
                    animate:false,
                    message: $content,
                    title: 'Update title wod',
                    buttons: {
                        def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                        confirm: {  label: 'Update',  className: "btn-primary confirm",  callback: function() {} }
                    }
                });//findel custom dialog
                //$bootbox.find('.modal-dialog').addClass('modal-lg');

                $bootbox.find('.confirm').off('click').on('click', function(e){
                    e.preventDefault();
                    e.stopPropagation();

                    var url = "/wod/updateFieldWod/" + slug;
                    var params = new Object;
                    params['field'] = field;
                    params['value'] =  value;
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: 'html',
                        data: params,
                        success: function(response){
                            findWodsInEntityFilters();
                            countWodsEntity(false);
                            $('.tooltipGeneric').tooltip();
                            $bootbox.find('.close').trigger('click');// cerrar modal
                        }
                    });
                });
            }else{
                var url = "/wod/updateFieldWod/" + slug;
                var params = new Object;
                params['field'] = field;
                params['value'] =  value;
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'html',
                    data: params,
                    success: function(response){
                        findWodsInEntityFilters();
                        countWodsEntity(false);
                        $('.tooltipGeneric').tooltip();
                    }
                });
            }

        });// update status and difficulty

    }
});//end ready
var ctx = document.getElementById("chart-area").getContext("2d");
var myDoughnutChart = new Chart(ctx);

function requestParams(){
    var params = new Object;
    params['id'] = $('.filterWodsShowEntity').data().id;
    params[$('.filterWodsShowEntity').data().idname] = $('.filterWodsShowEntity').data().id;
    params['status'] = [];
    params['difficulty'] = [];
    $('.filterWodsShowEntity .status').each(function(i){
        if( $(this).is(':checked')){
            params['status'].push($(this).val())
        }
    })
    $('.filterWodsShowEntity .difficulty').each(function(i){
        if( $(this).is(':checked')){
            params['difficulty'].push($(this).val())
        }
    })
    return params;
}

function countWodsEntity(first){
    var url = "/json/countWods/";
    var params = requestParams();
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: params,
        success: function(response){
            var count = parseInt(response.count == null ? 0 : response.count );
            var all = parseInt(response.all == null ? 0 : response.all );
            if(first){
                var data = [
                    {
                        value: count,
                        color: "#4D5360",
                        highlight: "#616774",
                        label: "Wods"
                    },
                    {
                        value: parseInt(all- count),
                        color: "#949FB1",
                        highlight: "#A8B3C5",
                        label: "Rest"
                    }
                ];
                var options = {
                    segmentShowStroke : false,
                    animateRotate : true,
                    animateScale : false,
                    animationEasing: "easeOutQuart",
                    animationSteps : 50
                }
                myDoughnutChart = new Chart(ctx).Doughnut(data,options);
            }else{
                myDoughnutChart.segments[0].value = count ;
                myDoughnutChart.segments[1].value = parseInt(all - count) ;
                myDoughnutChart.update();
            }
            if(all > 0){
                $('.numWodsTitle').text(
                    count +' de '+all
                        +' ('+(count*100/all).toFixed(0)+'% / '+(100-(count*100/all).toFixed(0))+'%)'
                );
            }
            $('.tooltipGeneric').tooltip();
            //$('#canvas-holder').css('width','10px');
        }
    });
}


function findWodsInEntityFilters(){
    var url = "/view/tableLisWods/";
    var params = requestParams();
    $('.listWods').empty().hide();

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'html',
        data: params,
        success: function(response){
            $('.listWods').append(response).scrollTop(0).fadeIn();
            $('.tooltipGeneric').tooltip();
        },
        error: function(x){
            if(x.status>0){
                console.log(' error find wods entity filter');
                noty({text: "Failure: error find wods entity filter", layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
            }
        }
    });
}// end findWodsInGroupFilters

