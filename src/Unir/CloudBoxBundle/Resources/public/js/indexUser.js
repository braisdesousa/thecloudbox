$(document).on('ready', function(){

    /**
     * groupUsers
     */


    $('.indexUser').on('click','.btn_newGroupUser',function(e){

        var $content = $('<div>', {class:'div_contentDialog modalAddGroupUser'});
        $content.append(
            $('<div>',{class:'row'}).append(
                $('<div>',{class:'col-md-6 col-md-offset-3'}).append(
                    $('<label>').text('Group Name'),
                    $('<input>',{class:'form-control nameGroup',type:'text',name:'name'})
                )
            )
        );
        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'New Group',
            buttons: {
                def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                confirm: {  label: 'Create',  className: "btn-primary confirm",  callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

        $bootbox.find('.confirm').off('click').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var url = "/group/createAsync";
            var params = new Object;
            params['name'] = $content.find('.nameGroup').val();
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: params,
                success: function(response){
                    if(response.error != undefined &&  response.error >0 ){
                        var errors = JSON.parse(response.errors)
                        $.each(errors,function(i,v){
                            $('.modalAddGroupUser').find('[name*="'+ v.property_path+'"]').closest('div')
                                .addClass('has-error')
                                .append($('<span>',{class:'text-danger messageError'}).text(v.message));
                            $('.modalAddGroupUser').find('[name*="'+ v.property_path+'"]').on('change',function(e){
                                $(this).closest('div').removeClass('has-error').find('.messageError').remove();
                            });
                        });
                    }else{
                        location.reload(true);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    //consoel.log("Failure: " + errorThrown);
                    noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
                }
            });
        });

    });// add group user

    // group active
    $('.userGroupId'+$('.inputIdGroupUserList').val()).addClass('list-group-item-info');
    $('.nameGroupList').text($('.userGroupId'+$('.inputIdGroupUserList').val()).data('name'));
    //loadUsersInGroup($('.inputIdGroupUserList').val());
    $('.listUserGroups').on('click','.dropDownStop',function(e){
        e.stopPropagation();
        $('.dropdown').removeClass('open');
        $(this).addClass('open');
    });
    $('.listUserGroups').on('click','.itemUserGroup',function(e){
        $('.inputIdGroupUserList').val($(this).data('id'));
        $('.nameGroupList').text($(this).data('name'));
        $('.listUserGroups .itemUserGroup').removeClass('list-group-item-info');
        $(this).addClass('list-group-item-info');
        loadUsersInGroup($(this).data('id'));
    });
    // end group active

    //edit group
    $('.listUserGroups').on('click','.editUserGroup',function(e){
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this);
        var id = $this.closest('.itemUserGroup').data('id');
        var name = $this.closest('.itemUserGroup').data('name');

        var $content = $('<div>', {class:'div_contentDialog modalEditGroupUser'});
        $content.append(
            $('<div>',{class:'row'}).append(
                $('<div>',{class:'col-md-6'}).append(
                    $('<label>').text('Group Name'),
                    $('<input>',{class:'form-control nameGroup',type:'text',name:'name',value:name})
                )
            ),
            $('<div>',{class:'row mt10'}).append(
                $('<div>',{class:'col-md-12'}).append(
                    $('<h2>',{class:'cursor_pointer'}).text('Users ').append($('<small>',{class:'fa fa-chevron-down'}))
                        .on('click',function(e){
                            $('.divUsersInGroup').slideToggle('slow');
                            if($(this).find('.fa').hasClass('fa-chevron-down')){
                                $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            }else{
                                $(this).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                            }
                        }),
                    $('<div>',{class:'divUsersInGroup borderSolidSilver pAll10 '})
                )
            ),
            $('<div>',{class:'row mt10'}).append(
                $('<div>',{class:'col-md-12'}).append(
                    $('<h2>',{class:'cursor_pointer'}).text('WorkGroups ').append($('<small>',{class:'fa fa-chevron-right'}))
                        .on('click',function(e){
                            $('.divWorkGroupsToGroup').slideToggle('slow');
                            if($(this).find('.fa').hasClass('fa-chevron-down')){
                                $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            }else{
                                $(this).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                            }
                        }),
                    $('<div>',{class:'divWorkGroupsToGroup borderSolidSilver pAll10'}).hide()
                )
            )
        );
        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'Edit Group',
            buttons: {
                def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                confirm: {  label: 'Update',  className: "btn-primary confirm",  callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

        // find users and print
        $.ajax({
            type: "POST",
            url: "/json/userGroup/"+id,
            dataType: 'json',
            data: {},
            success: function(response){
                if(response.error != undefined &&  response.error >0 ){
                    console.log(response.error_message)
                }else{
                    response.users = JSON.parse(response.users);
                    response.userGroup = JSON.parse(response.userGroup);
                    response.workGroups = JSON.parse(response.workGroups);
                    $.each(response.users, function(i,v){ response.users[i]['text'] = v.username; });
                    $.each(response.workGroups, function(i,v){ response.workGroups[i]['text'] = v.name; });
                    printMultiSelectGenericModal(
                        $content.find('.divUsersInGroup'),
                        '.divUsersInGroup',
                        response.users,
                        response.userGroup.users,
                        'Users available',
                        'Users of',
                        name
                    );
                    printMultiSelectGenericModal(
                        $content.find('.divWorkGroupsToGroup'),
                        '.divWorkGroupsToGroup',
                        response.workGroups,
                        response.userGroup.member_workGroups,
                        'WorkGroups available',
                        'WorkGroups of',
                        name
                    );
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                //consoel.log("Failure: " + errorThrown);
                noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
            }
        });

        $bootbox.find('.confirm').off('click').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var url = "/group/updateAsync/"+id;
            var params = new Object;
            params['name'] = $content.find('.nameGroup').val();
            $content.find('.divUsersInGroup #ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function (index) {
                params['ids['+index+']'] = $(this).data('id');
            });
            $content.find('.divWorkGroupsToGroup #ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function (index) {
                params['idsWorkGroups['+index+']'] = $(this).data('id');
            });
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: params,
                success: function(response){
                    if(response.error != undefined &&  response.error >0 ){
                        var errors = JSON.parse(response.errors)
                        $.each(errors,function(i,v){
                            $('.modalAddGroupUser').find('[name*="'+ v.property_path+'"]').closest('div')
                                .addClass('has-error')
                                .append($('<span>',{class:'text-danger messageError'}).text(v.message));
                            $('.modalAddGroupUser').find('[name*="'+ v.property_path+'"]').on('change',function(e){
                                $(this).closest('div').removeClass('has-error').find('.messageError').remove();
                            });
                        });
                    }else{
                        location.reload(true);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    //consoel.log("Failure: " + errorThrown);
                    noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
                }
            });
        });

    });// end edit group

    //delete user group
    $('.listUserGroups').on('click','.deleteUserGroup',function(e){
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this);

        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: 'Are you sure you want to delete the group?',
            title: 'Delete Group',
            buttons: {
                def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                confirm: {  label: 'Delete',  className: "btn-danger confirm",  callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

        $bootbox.find('.confirm').off('click').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var url = "/group/deleteAsync/"+$this.closest('.itemUserGroup').data('id');
            var params = new Object;
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'html',
               // data: params,
                success: function(response){
                    if(response.error != undefined &&  response.error >0 ){
                        console.log('error')
                    }else{
                        location.reload(true);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    //consoel.log("Failure: " + errorThrown);
                    noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
                }
            });
        });

    });// end delete user group

    // end userGroups

    /**
     * users
     */

    $('.listUsersInGroup').on('click','.editUser',function(e){
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this);
        var id = $this.closest('.itemUser').data('id');
        var name = $this.closest('.itemUser').data('username');

        var $content = $('<div>', {class:'div_contentDialog modalEditUser'});

        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'Edit User: '+ name,
            buttons: {
                def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                confirm: {  label: 'Update',  className: "btn-primary confirm",  callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

    });


    // end users


});// end ready

function loadUsersInGroup(idGroup){
    $('.listUsersInGroup').empty().hide();
    var url = "/view/listUsersByGroup/" + idGroup;
    var params = new Object;
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'html',
        data: params,
        success: function(response){
            $('.listUsersInGroup').append(response).fadeIn();
        }
    });
}