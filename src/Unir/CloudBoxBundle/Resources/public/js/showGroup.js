$(document).on('ready', function(){
    countWodsGroup(true);
    findWodsInGroupFilters();
    $('.filterWodsGroup').on('change','input', function(e){
        countWodsGroup(false);
        findWodsInGroupFilters();
    });
});//end ready
var ctx = document.getElementById("chart-area").getContext("2d");
var myDoughnutChart = new Chart(ctx);

function requestParams(){
    var params = {};
    params['id'] = $('.filterWodsGroup').data().id;
    params['idGroup'] = $('.filterWodsGroup').data().id;
    params['status'] = [];
    params['difficulty'] = [];
    $('.filterWodsGroup .status').each(function(i){
        if( $(this).is(':checked')){
            params['status'].push($(this).val())
        }
    })
    $('.filterWodsGroup .difficulty').each(function(i){
        if( $(this).is(':checked')){
            params['difficulty'].push($(this).val())
        }
    })
    return params;
}

function countWodsGroup(first){
    var url = "/json/countWods/";
    var params = requestParams();
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: params,
        success: function(response){
            if(first){
                var data = [
                    {
                        value: parseInt(response.count),
                        color: "#4D5360",
                        highlight: "#616774",
                        label: "Wods"
                    },
                    {
                        value: parseInt(response.all - response.count),
                        color: "#949FB1",
                        highlight: "#A8B3C5",
                        label: "Rest"
                    }
                ];
                var options = {}
                myDoughnutChart = new Chart(ctx).Doughnut(data,options);
            }else{
                myDoughnutChart.segments[0].value = parseInt(response.count) ;
                myDoughnutChart.segments[1].value = parseInt(response.all - response.count) ;
                myDoughnutChart.update();
            }
        }
    });
} // end countWodsGroup

function findWodsInGroupFilters(){
    var url = "/json/getWods/";
    var params = requestParams();
    $('.listWodsInGroup').empty().hide();
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: params,
        success: function(response){
            response.wods = JSON.parse(response.wods)

            $.each(response.wods,function(i,v){
                $('.listWodsInGroup').append(
                    $('<li>',{}).append($('<a>',{href:'/wod/view/'+ v.id}).html('<span class="fa fa-eye"></span> '+v.title))
                );
            });

            $('.listWodsInGroup').scrollTop(0).fadeIn();

        }
    });
}// end findWodsInGroupFilters