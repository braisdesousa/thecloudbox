/**
 * Autor: Andres Vieitez Fontan
 * 
 */

/**
 * construct select multiple generic
 * @param $modal
 * @param selector
 * @param allList
 * @param listInclude
 * @param titleAvailable
 * @param titleSelected
 */
function printMultiSelectGenericModal($modal,selector,allList,listInclude,titleAvailable,titleSelected,name){
    var ids = [];
    $.each(listInclude,function(i,v){
        ids[i] = v.id;
    });
    var $div = $('<div>') ;
    $('<h4>', {class:'mt0'}).text(titleAvailable).appendTo($div);
    var $input = $('<input>', {class:' form-control inputSearchGenericModal mb10',type:'text',placeholder:'search'})
    $input.on('input keyup',function(e){
        var $this = $(this);
        $(selector).find('#ms-pre-selected-options .ms-selectable .ms-list li:not(.ms-selected)').each(function(e){
            if($(this).find('span').text().containsIgnoreCase($this.val())){
                $(this).closest('li').removeClass('hidden');
            }else{
                $(this).closest('li').addClass('hidden');
            }
        })
    });
    $input.appendTo($div);
    var $div2 = $('<div>') ;
    $('<h4>', {class:'mt0'}).text(titleSelected+': ' + name).appendTo($div2);
    var $input2 = $('<input>', {class:' form-control inputSearchGenericModal mb10',type:'text',placeholder:'search'})
    $input2.on('input keyup',function(e){
        var $this = $(this);
        $(selector).find('#ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function(e){
            if($(this).find('span').text().containsIgnoreCase($this.val())){
                $(this).closest('li').removeClass('hidden');
            }else{
                $(this).closest('li').addClass('hidden');
            }
        })
    });
    $input2.appendTo($div2)
    var $select = $('<select>', {id:'pre-selected-options', multiple:'multiple',name:'ids'});
    $.each(allList, function(i,v){
        var $option = $('<option>', {value:v.id, 'data-id':v.id, 'data-name':v.text}).text( v.text)

        if($.inArray(v.id,ids)>=0){
            $option.attr('selected','selected');
        }
        $option.appendTo($select);
    });
    $select.appendTo($modal);
    var $selectAll =  $('<a>', {id:'select-all', class:'mt5 ml10 a_decoration_underline_generic cursorPointer a_decoration_none text-info' })
        .text('Select All')
        .click(function(){
            $select.multiSelect('select_all');
            return false;
        });
    var $deselectAll =  $('<a>', {id:'deselect-all', class:'mt5 ml10 a_decoration_underline_generic cursorPointer a_decoration_none text-info' })
        .text('Remove All')
        .click(function(){
            $select.multiSelect('deselect_all');
            return false;
        });
    $select.multiSelect({	selectableHeader:$div,       selectionHeader:$div2,
        selectableFooter:$selectAll, selectionFooter:$deselectAll,
        cssClass:'ms-container-modal width100',dblClick:false});
}