jQuery(function($){


    $('.tableList').removeClass('hide');

    /**
     * filterWod change
     */
    $('.wodListContainer').on('change','select.filterWod',function(e){
        var url = "/view/wodsFilter/";
        var params = new Object;


        $('.filterWod').each(function(i){
            if($(this).val()){
                if($(this).data().filter =='sort'){
                    if($('.filterWodOrder').data('order')==undefined){
                        $('.filterWodOrder').data('order','asc');
                        $('.filterWodOrder')
                            .removeClass('fa-arrow-down')
                            .addClass('fa-arrow-up');
                    }
                    params[$(this).data().filter+'['+$(this).val()+']'] = $('.filterWodOrder').data('order');
                }else if($(this).data().filter != undefined){
                    params[$(this).data().filter] = $(this).val();
                }
            }
        });


        $('.tableList').empty().hide();
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'html',
            data: params,
            success: function(response){
                $('.tableList').append(response).fadeIn();
                $('.itemListGroups').removeClass('list-group-item-info');
                //$('.itemListGroups .fa').removeClass('loader').removeClass('show').removeClass('text-muted');
                $('.listWodGroups .idGroup' + $('.groupSelect').val()).addClass('list-group-item-info');
                countWodsList();
                $('.tooltipGeneric').tooltip();
            }
        });
    });// en change filter wod

    // trigger change, to retrieve the automatic selection of symfony to reload
    $('.groupSelect').trigger('change');
    $('.listWodGroups .idGroup' + $('.groupSelect').val()).addClass('list-group-item-info');
    //Why in the hell if you are going to update later you are going to update now????
    //countWodsList();

    //itemListGroups
    $('.wodListContainer').on('click','.itemListGroups',function(e){
        $('.itemListGroups').removeClass('list-group-item-info');
        $(this).addClass('list-group-item-info');
        $('.groupSelect').val($(this).data('id'));
        $('.groupSelect').trigger("chosen:updated")
        $('.groupSelect').trigger('change');
        //$(this).find('.fa').addClass('show loader text-muted')
        //$('.groupSelect').val($(this).data('id')).trigger('change').chosen('update');.trigger("chosen:updated")
    });

    // select sort, change arrow
    $('.wodListContainer').on('click','.filterWodOrder',function(e){
        if($(this).data('order')=='asc'){
            $(this).data('order','desc');
            $(this)
                .removeClass('fa-arrow-up')
                .addClass('fa-arrow-down');
        }else{
            $(this).data('order','asc');
            $(this)
                .removeClass('fa-arrow-down')
                .addClass('fa-arrow-up');
        }
        $(this).closest('div').find('select').trigger('change');
        countWodsList();
    });

    //click on preview wod
    $('.wodListContainer').on('click','.previewWod',function(e){
        e.preventDefault();
        e.stopPropagation();


        var showLoader = setInterval(function(){$('.contentPreviewWod').empty().append($('<div>',{class:'show loader loader-centered'}));},500);

        var url = "/view/previewWodById/" + $(this).data().id;
        var params = new Object;
        params['id'] = $(this).data().id;

        $('.contentPreviewWod').empty().hide()
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'html',
            data: params,
            success: function(response){
                clearInterval(showLoader);
                $('.contentPreviewWod').append(response).fadeIn();
                $('.tooltipGeneric').tooltip();
            }
        });
    });// end click preview wod


    $('.wodListContainer').on('submit','.newCommentDiv form',function(e){
        e.preventDefault();
        e.stopPropagation();

        var idWod = $('.newCommentDiv').data('idwod');
        var url = "/wodcomments/createAsync";
        var params = new Object;

        $(this).find('button').attr('disabled','disabled');

        $('.newCommentDiv form').find('select, input, textarea').each(function(i){
            params[$(this).attr('name')] = $(this).val()
        });
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: params,
            success: function(response,textStatus,jqXHR){
                console.log($.isEmptyObject(response))
                if(response.error == undefined){
                    var url = "/view/previewWodById/" + idWod;
                    var params = new Object;
                    params['id'] = idWod;
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: 'html',
                        data: params,
                        success: function(response){
                            $('.contentPreviewWod').empty().append(response);
                            $('.tooltipGeneric').tooltip();
                            $('.newCommentDiv').find('button').removeAttr('disabled');
                        },
                        error: function(){
                            $('.newCommentDiv').find('button').removeAttr('disabled');
                        }
                    });
                }else{
                    console.log('error new comment');
                }
            },
            error: function(jqXHR,textStatus,errorThrown){
                console.log('error call');
                noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
            }
        });
    });// end submit form comments

    // edit title and description for wod
    $('.wodListContainer').on('click','.editTitleAndDescription',function(e){
        var idWod = $(this).data('idwod');
        var field = $(this).data('field');
        var slug = $(this).data('slug');
        var title = $(this).data('title');
        var description = $(this).data('description');

        var params = new Object;
        params['id'] = idWod;
        params['field'] = field;
        var $content = $('<div>', {class:'div_contentDialog modalUpdateWod'});
        $content.append(
            $('<div>').append(
                $('<label>').text('Title'),
                $('<input>',{class:'form-control newTitleWod',type:'text',name:'title',value:title})
            ),
            $('<div>').append(
                $('<label>').text('Description'),
                $('<textarea>',{class:'form-control newDescriptionWod',type:'text',row:'8',name:'description'}).text(description)
            )
        );
        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'Update title and description wod',
            buttons: {
                def: { label: 'Cancel', className: "btn-default pull-left", callback: function() {} },
                confirm: { label: 'Update', className: "btn-primary confirm", callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

        $bootbox.find('.confirm').off('click').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var url = "/wod/updateFieldWod/" + slug;
            var params = new Object;
            params['id'] = idWod;
            params['field'] = field;
            params['title'] =  $('.newTitleWod').val();
            params['description'] =  $('.newDescriptionWod').val();

            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: params,
                success: function(response){
                    if(response.error > 0){
                        $('.newValueWod').closest('div').addClass('has-error');
                        var errors = JSON.parse(response.errors);
                        $.each(errors,function(i,v){
                            $content.find('[name*="'+ v.property_path+'"]').closest('div')
                                .addClass('has-error')
                                .append($('<span>',{class:'text-danger messageError'}).text(v.message));
                            $content.find('[name*="'+ v.property_path+'"]').on('change',function(e){
                                $(this).closest('div').removeClass('has-error').find('.messageError').remove();
                            });
                        });
                    }else{
                        wod = response.wod;
                        $('.wodListContainer .editTitleAndDescription').data('title',wod.title).data('description',wod.description);
                        var showLoader = setInterval(function(){$('.contentPreviewWod').empty().append($('<div>',{class:'show loader loader-centered'}));},500);
                        var url = "/view/previewWodById/" + idWod;
                        var params = new Object;
                        params['id'] = idWod;
                        $.ajax({
                            type: "POST",
                            url: url,
                            dataType: 'html',
                            data: params,
                            success: function(response){
                                clearInterval(showLoader);
                                $('.contentPreviewWod').empty().append(response);
                                $('.tooltipGeneric').tooltip();
                            }
                        });
                        $bootbox.find('.close').trigger('click');// cerrar modal
                        countWodsList();
                    }
                }
            });
        });// confirm

    });// update title and description wod

    //  update difficulty and status for wod
    $('body').on('click','.editWodDropdown',function(e){
        var idWod = $(this).closest('ul').data('idwod');
        var slug = $(this).closest('ul').data('slug');
        var field = $(this).data('field');
        var value = $(this).data('value');
        var params = new Object;
        console.log(slug)
        params['field'] = field;
        params['value'] =  value;
        if( value == 'Closed' ){
            var $content = $('<div>', {class:'div_contentDialog modalUpdateWod'});
            $content.append(
                $('<p>').text('Are you sure you want to change?')
            );
            // custom dialog
            var $bootbox = bootbox.dialog({
                animate:false,
                message: $content,
                title: 'Update title wod',
                buttons: {
                    def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                    confirm: {  label: 'Update',  className: "btn-primary confirm",  callback: function() {} }
                }
            });//findel custom dialog
            //$bootbox.find('.modal-dialog').addClass('modal-lg');

            $bootbox.find('.confirm').off('click').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $('.contentPreviewWod').empty().append($('<div>',{class:'show loader loader-centered'}));
                var url = "/wod/updateFieldWod/" + slug;
                var params = new Object;
                params['slug']  = slug;
                params['field'] = field;
                params['value'] =  value;
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'html',
                    data: params,
                    success: function(response){
                        var url = "/view/previewWodById/" + idWod;
                        var params = new Object;
                        params['id'] = idWod;
                        $.ajax({
                            type: "POST",
                            url: url,
                            dataType: 'html',
                            data: params,
                            success: function(response){
                                $('.contentPreviewWod').empty().append(response);
                                $('.tooltipGeneric').tooltip();
                                $('.groupSelect').trigger('change');
                            }
                        });
                        $bootbox.find('.close').trigger('click');// cerrar modal
                        countWodsList();
                    }
                });
            });
        }else{
            $('.contentPreviewWod').empty().append($('<div>',{class:'show loader loader-centered'}));
            var url = "/wod/updateFieldWod/" + slug;
            var params = new Object;
            params['field'] = field;
            params['value'] =  value;
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'html',
                data: params,
                success: function(response){
                    var url = "/view/previewWodById/" + idWod;
                    var params = new Object;
                    params['id'] = idWod;
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: 'html',
                        data: params,
                        success: function(response){
                            $('.contentPreviewWod').empty().append(response);
                            $('.tooltipGeneric').tooltip();
                            $('.groupSelect').trigger('change');
                            countWodsList();
                        }
                    });
                }
            });
        }

    });// update status and difficulty

    //  add wod async
    $('.wodListContainer').on('click','.addWod',function(e){
        e.preventDefault();
        e.stopPropagation();
        var params = new Object;
        var $content = $('<div>', {class:'div_contentDialog modalAddWod'});
        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'Add new wod',
            buttons: {
                def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                confirm: {  label: 'Add',  className: "btn-primary confirm",  callback: function() {} }
            }
        });//findel custom dialog
        $bootbox.find('.modal-dialog').addClass('modal-lg');



        var url = "/wod/newAsync";
        var params = new Object;
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'html',
            data: params,
            success: function(response){
                $('.modalAddWod').empty().append(response);
                $('.chosenGeneric').chosen();
                $('.chosenGeneric').closest('div').find('.chosen-container').css('width','100%');
                $('.tooltipGeneric').tooltip();

                $('.modalAddWod').find('form').on('submit',function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    $bootbox.find('.confirm').trigger('click');
                });
            }
        });

        $bootbox.find('.confirm').off('click').on('click', function(e){

            var url = "/wod/createAsync";
            var params = new Object;

            $('.modalAddWod form').find('select, input, textarea').each(function(i){
                if($(this).attr('name')!=undefined) {
                    if($(this).val()=='' || $(this).val()==null){
                        //params[$(this).attr('name')] = '';
                    }else{
                        params[$(this).attr('name')] = $(this).val();
                    }
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: params,
                success: function(response){
                    if(response.error != undefined &&  response.error >0 ){
                        var errors = JSON.parse(response.errors)
                        $.each(errors,function(i,v){
                            $('.modalAddWod').find('[name*="'+ v.property_path+'"]').closest('div')
                                .addClass('has-error')
                                .append($('<span>',{class:'text-danger messageError'}).text(v.message));
                            $('.modalAddWod').find('[name*="'+ v.property_path+'"]').on('change',function(e){
                                $(this).closest('div').removeClass('has-error').find('.messageError').remove();
                            });
                        });
                    }else{
                        //location.reload();
                        $('.groupSelect').trigger('change');
                        $bootbox.find('.close').trigger('click');// cerrar modal
                        countWodsList();
                        listGroupsCounts();

                        // show preview wod
                        var showLoader = setInterval(function(){$('.contentPreviewWod').empty().append($('<div>',{class:'show loader loader-centered'}));},200);

                        var url = "/view/previewWodById/" + response.id;
                        var params = new Object;
                        params['id'] = response.id;
                        $.ajax({
                            type: "POST",
                            url: url,
                            dataType: 'html',
                            data: params,
                            success: function(response){
                                clearInterval(showLoader);
                                $('.contentPreviewWod').empty().append(response);
                                $('.tooltipGeneric').tooltip();
                            }
                        });
                        var url = "/view/previewWodById/" + response.id;
                        var params = new Object;
                        params['id'] = response.id;
                        $.ajax({
                            type: "POST",
                            url: url,
                            dataType: 'html',
                            data: params,
                            success: function(response){
                                clearInterval(showLoader);
                                $('.contentPreviewWod').empty().append(response);
                                $('.tooltipGeneric').tooltip();
                            }
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    console.log("Failure: " + errorThrown);
                    noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
                }
            });
        });

    });// addWod async

    // change input search wod
    $('.wodListContainer').on('keyup','.searchWodDashboard',function(e){
        var text = $(this).val();
        $('.tableList tr').show();
        $('.tableList .previewWod').each(function(i){
            var eq = $(this).text().containsIgnoreCase(text);
            if( ! eq ){
                $(this).closest('tr').hide();
            }
        });
    });// end search wod
    $('.searchWodDashboard').val('');


    /**
     * collapse counts
     */
    $('.wodListContainer').on('click','.collapseCounts',function(e){
        $(this).addClass('expandCounts').removeClass('collapseCounts').html('&gg;');
        $('.wodListContainer .wodCounts ul').hide();
        $('.wodListContainer .wodCounts ').animate({width:"1%",padding:'0px 0px 0px 5px'},'fast');
        $('.wodListContainer .contentPreviewWod').animate({width:"62%"},'fast');
    });// end collapse counts
    /**
     * expand counts
     */
    $('.wodListContainer').on('click','.expandCounts',function(e){
        $(this).addClass('collapseCounts').removeClass('expandCounts').html('&ll;');
        $('.wodListContainer .wodCounts ul').show();
        $('.wodListContainer .contentPreviewWod').animate({width:"50%"},'fast');
        $('.wodListContainer .wodCounts ').animate({width:"16%",padding:'0px 15px'},'fast');
    });// end expand counts



function requestParams(){
    var params = new Object;
    $('.filterWod').each(function(i){
        if($(this).val()){
            if($(this).data().filter =='sort'){
                if($('.filterWodOrder').data('order')==undefined){
                    $('.filterWodOrder').data('order','asc');
                    $('.filterWodOrder')
                        .removeClass('fa-arrow-down')
                        .addClass('fa-arrow-up');
                }
                params[$(this).data().filter+'['+$(this).val()+']'] = $('.filterWodOrder').data('order');
            }else if($(this).data().filter != undefined){
                params[$(this).data().filter] = $(this).val();
            }
        }
    });
    return params;
}


function countWodsList(){
    var url = "/json/countWods/";
    var params = requestParams();
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: params,
        success: function(response){
            var count = parseInt(response.count == null ? 0 : response.count );
            var all = parseInt(response.all == null ? 0 : response.all );
            if(all > 0){
                $('.numWodsTitle').text(
                    count +' de '+all
                        +' ('+(count*100/all).toFixed(0)+'% / '+(100-(count*100/all).toFixed(0))+'%)'
                );
            }else{
                $('.numWodsTitle').text('');
            }
            $('.tooltipGeneric').tooltip();
        }
    });
}//countWodsList

function listGroupsCounts(){
    var url = "/view/listGroupsCounts/";
    var params = {};
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'html',
        data: params,
        success: function(response){
            $('.listWodGroups').empty().append(response);
            $('.listWodGroups .idGroup' + $('.groupSelect').val()).addClass('list-group-item-info');
            $('.tooltipGeneric').tooltip();
        }
    });
}//countWodsList

})(jQuery);// end ready