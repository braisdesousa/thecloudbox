$(document).on('ready', function(){

    //edit users in group
    $('.showWodGroup').on('click','.btnEditUsersTAskGroup',function(e){
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this);
        var id = $this.data('id');
        var name = $this.data('name');

        var $content = $('<div>', {class:'div_contentDialog modalEditUsersInGroup'});
        $content.append(
            $('<div>',{class:'row mt10'}).append(
                $('<div>',{class:'col-md-12'}).append(
                    $('<h2>',{class:'cursor_pointer'}).text('Members ').append($('<small>',{class:'fa fa-chevron-down'}))
                        .on('click',function(e){
                            $('.divMembers').slideToggle('slow');
                            $('.divGroupMembers').slideUp('slow').closest('.col-md-12').find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            $('.divFollowers').slideUp('slow').closest('.col-md-12').find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            if($(this).find('.fa').hasClass('fa-chevron-down')){
                                $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            }else{
                                $(this).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                            }
                        }),
                    $('<div>',{class:'divMembers borderSolidSilver pAll10 '})
                )
            ),
            $('<div>',{class:'row mt10'}).append(
                $('<div>',{class:'col-md-12'}).append(
                    $('<h2>',{class:'cursor_pointer'}).text('Group members ').append($('<small>',{class:'fa fa-chevron-right'}))
                        .on('click',function(e){
                            $('.divGroupMembers').slideToggle('slow');
                            $('.divMembers').slideUp('slow').closest('.col-md-12').find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            $('.divFollowers').slideUp('slow').closest('.col-md-12').find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            if($(this).find('.fa').hasClass('fa-chevron-down')){
                                $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            }else{
                                $(this).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                            }
                        }),
                    $('<div>',{class:'divGroupMembers borderSolidSilver pAll10'}).hide()
                )
            ),
            $('<div>',{class:'row mt10'}).append(
                $('<div>',{class:'col-md-12'}).append(
                    $('<h2>',{class:'cursor_pointer'}).text('Followers ').append($('<small>',{class:'fa fa-chevron-right'}))
                        .on('click',function(e){
                            $('.divFollowers').slideToggle('slow');
                            $('.divMembers').slideUp('slow').closest('.col-md-12').find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            $('.divGroupMembers').slideUp('slow').closest('.col-md-12').find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            if($(this).find('.fa').hasClass('fa-chevron-down')){
                                $(this).find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
                            }else{
                                $(this).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
                            }
                        }),
                    $('<div>',{class:'divFollowers borderSolidSilver pAll10'}).hide()
                )
            )
        );
        // custom dialog
        var $bootbox = bootbox.dialog({
            animate:false,
            message: $content,
            title: 'Edit Group',
            buttons: {
                def: {  label: 'Cancel', className: "btn-default pull-left",  callback: function() {} },
                confirm: {  label: 'Update',  className: "btn-primary confirm",  callback: function() {} }
            }
        });//findel custom dialog
        //$bootbox.find('.modal-dialog').addClass('modal-lg');

        // find users and print
        $.ajax({
            type: "POST",
            url: "/json/findWodGroupById/"+id,
            dataType: 'json',
            data: {},
            success: function(response){
                if(response.error != undefined &&  response.error >0 ){
                    console.log(response.error_message)
                }else{
                    response.wodGroup = JSON.parse(response.wodGroup);
                    response.users = JSON.parse(response.users);
                    response.groups = JSON.parse(response.groups);
                    console.log(response);
                    $.each(response.users, function(i,v){ response.users[i]['text'] = v.username; });
                    $.each(response.groups, function(i,v){ response.groups[i]['text'] = v.name; });
                    printMultiSelectGenericModal(
                        $content.find('.divMembers'),
                        '.divMembers',
                        response.users,
                        response.wodGroup.user_members,
                        'Users available',
                        'Users members of',
                        name
                    );
                    printMultiSelectGenericModal(
                        $content.find('.divGroupMembers'),
                        '.divGroupMembers',
                        response.groups,
                        response.wodGroup.group_members,
                        'Groups available',
                        'Groups members of',
                        name
                    );
                    printMultiSelectGenericModal(
                        $content.find('.divFollowers'),
                        '.divFollowers',
                        response.users,
                        response.wodGroup.user_followers,
                        'Users available',
                        'Users followers of',
                        name
                    );
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                //consoel.log("Failure: " + errorThrown);
                noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
            }
        });

        $bootbox.find('.confirm').off('click').on('click', function(e){
            e.preventDefault();
            e.stopPropagation();

            var url = "/wodgroup/updateFieldAsync/"+id;
            var params = new Object;
            params['field'] = 'membersAndFollowers';
            params['name'] = $content.find('.nameGroup').val();
            $content.find('.divMembers #ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function (index) {
                params['idsMembers['+index+']'] = $(this).data('id');
            });
            $content.find('.divGroupMembers #ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function (index) {
                params['idsGroupMembers['+index+']'] = $(this).data('id');
            });
            $content.find('.divFollowers #ms-pre-selected-options .ms-selection .ms-list li.ms-selected').each(function (index) {
                params['idsFollowers['+index+']'] = $(this).data('id');
            });
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: params,
                success: function(response){
                    if(response.error != undefined &&  response.error >0 ){
                        var errors = JSON.parse(response.errors)
                        $.each(errors,function(i,v){
                            $('.modalEditUsersInGroup').find('[name*="'+ v.property_path+'"]').closest('div')
                                .addClass('has-error')
                                .append($('<span>',{class:'text-danger messageError'}).text(v.message));
                            $('.modalEditUsersInGroup').find('[name*="'+ v.property_path+'"]').on('change',function(e){
                                $(this).closest('div').removeClass('has-error').find('.messageError').remove();
                            });
                        });
                    }else{
                        location.reload(true);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    //consoel.log("Failure: " + errorThrown);
                    noty({text: "Failure: " + errorThrown, layout:'topCenter', type:'error_andres',timeout:10000, closeOnSelfOver:true});
                }
            });
        });

    });// end edit users in group
});