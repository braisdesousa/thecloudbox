<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Route;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\Wod;

/**
 * @Route("/ng/~usergroup")
 * */
class UserGroupController extends FOSRestController {


    /**
     * @Get("/workGroup/{id}", name="ng-workGroup-usersgroups",requirements={"id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function getWorkGroupUserGroupsAction($id)
    {
        if (!$workGroup = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WorkGroup")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gt01",sprintf("Given id '%s' does not represent a workGroup",$id));
        }
        return $workGroup->getGroupMembers();
    }
    /**
     * @Get("/", name="ng-usersgroups", defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function getUserGroupsAction()
    {
        return $this->get("unir.repository.usergroups_repository_service")->findByEnterprise($this->get("unir.common.common_service")->getCompany());
    }
    /**
     * @Get("/{id}", name="ng-usersgroup",requirements={"id" = "\d+"}, defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function getUserGroupAction($id)
    {
        if(!$group=$this->getDoctrine()->getRepository("UnirCloudBoxBundle:Group")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gug01",sprintf("Given id '%id' does not represent an UserGroup"));
        }

        return $group;
    }
    /**
     * @Post("/", name="ng-usersgroup-post",defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function postUserGroupAction(Request $request)
    {
        $group = new Group();
        $group->setEnterprise($this->get("unir.common.common_service")->getCompany());

        $form= $this->createForm("unir_ticketmanagerbundle_group",$group,["method"=>"POST"]);
        $form->handleRequest($request);

        if ($form->isValid()){
            $this->getDoctrine()->getManager()->persist($group);
            foreach ($group->getUsers() as $user)
            {
                $user->addGroup($group);
                $this->getDoctrine()->getManager()->persist($user);

            }
            $this->getDoctrine()->getManager()->flush();
            return $group;
        }

        return $this->get("unir.rest.error")->formErrorView("421",$form);
    }
    /**
     * @Put("/{id}", name="ng-usersgroup-put",requirements={"id" = "\d+"},defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function putUserGroupAction(Request $request,$id)
    {
        if(!$group=$this->getDoctrine()->getRepository("UnirCloudBoxBundle:Group")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gug01",sprintf("Given id '%id' does not represent an UserGroup"));
        }
        $past_users=$group->getUsers()->toArray();
        $form= $this->createForm("unir_ticketmanagerbundle_group",$group,["method"=>"PUT"]);
        $form->handleRequest($request);

        if ($form->isValid()){
            $new_users= $group->getUsers()->toArray();

            $newUsers= array_diff($new_users,$past_users);
            $removeUsers=array_diff($past_users,$new_users);
            foreach ($newUsers as $user) {
                $user->addGroup($group);
                $this->getDoctrine()->getManager()->persist($user);
            }
            foreach ($removeUsers as $user) {
                $user->removeGroup($group);
                $this->getDoctrine()->getManager()->persist($user);
            }

            $this->getDoctrine()->getManager()->persist($group);
            $this->getDoctrine()->getManager()->flush();
            return $group;
        }

        return $this->get("unir.rest.error")->formErrorView("421",$form);
    }
    /**
     * @Delete("/{id}", name="ng-usersgroup-delete",requirements={"id" = "\d+"},defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function deleteUserGroupAction($id)
    {
        if(!$group=$this->getDoctrine()->getRepository("UnirCloudBoxBundle:Group")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gug01",sprintf("Given id '%id' does not represent an UserGroup"));
        }
        $this->getDoctrine()->getManager()->remove($group);
        $this->getDoctrine()->getManager()->flush();

        return [];
    }
}