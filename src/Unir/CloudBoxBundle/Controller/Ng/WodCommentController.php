<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Route;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\WodTiming;
use Unir\CloudBoxBundle\Form\WodTimingType;

/**
 * @Route("/ng/wodcomment/")
 * */
class WodCommentController extends FOSRestController {


    /**
     * @Post("/{slug}/comment", name="ng-create-comment",requirements={"slug"="^[a-z0-9-]+$"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function createWodCommentAction(Request $request,$slug)
    {
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->get("unir.rest.error")->errorView(421,"cc01",sprintf("Given slug '%s' does not represent a wod",$slug));
        }
        if (!$title=$request->request->get("comment")){
            return $this->get("unir.rest.error")->errorView(421,"cc02","No se ha introducido comentario");
        }

        $comment= new WodComments();
        $comment->setWod($wod);
        $comment->setUser($this->getUser());
        $comment->setComment($title);

        $this->get("doctrine.orm.entity_manager")->persist($comment);
        $this->get("doctrine.orm.entity_manager")->flush();

        return $comment;
    }
    /**
     * @Get("/{slug}/", name="ng-get-comments",requirements={"slug"="^[a-z0-9-]+$"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function getWodCommentsAction($slug){
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->get("unir.rest.error")->errorView(421,"cc01",sprintf("Given slug '%s' does not represent a wod",$slug));
        }
        return $wod->getWodComments();
    }
} 