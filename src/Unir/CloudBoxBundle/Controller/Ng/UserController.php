<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Route;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\User;

/**
 * @Route("/ng/~user")
 * */
class UserController extends FOSRestController {


    /**
     * @Get("/workGroup/{id}", name="ng-workGroup-users",requirements={"id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function getWorkGroupUsersAction($id)
    {
        if (!$workGroup = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WorkGroup")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gt01",sprintf("Given id '%s' does not represent a workGroup",$id));
        }
        return $this->get("unir.repository.users_repository_service")->findByWorkGroup($workGroup);
    }
    /**
     * @Get("/enterprise/", name="ng-enterprise-users",defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function getEntepriseUsersAction()
    {
        $enterprise= $this->get("unir.common.common_service")->getCompany();

        return $this->get("unir.repository.users_repository_service")->findByCompany($enterprise);
    }
    /**
     * @Get("/workGroup/", name="ng-selected-workGroup-users",defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function getDefaultWorkGroupUsersAction()
    {
        /**
         * @var Enterprise
         */
        $enterprise=$this->get("unir.common.common_service")->getSelectedWorkGroup()->getEnterprise();

        return $this->get("unir.repository.users_repository_service")->findByCompany($enterprise,true);
    }
    /**
     * @Get("/by_group/", name="ng-enterprise-users-by-group",defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idname"})
     */
    public function getEntepriseUsersByGroupAction()
    {


        return $this->get("unir.common.common_service")->getCompany()->getGroups();
    }
    /**
     * @Get("/{id}", name="ng-user",requirements={"id" = "\d+"} ,defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"userAll"})
     */
    public function getUserAction($id)
    {
        if (!$user = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:User")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gt01",sprintf("Given id '%s' does not represent an user",$id));
        }

        $user->setWodTiming($this->get("unir.common.wodTiming_repository_service")->getUserWods($user));
        return $user;
    }
    /**
     * @Put("/{id}", name="ng-user-put",requirements={"id" = "\d+"} ,defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"userAll"})
     */
    public function putUserAction(Request $request,$id)
    {
        if (!$user = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:User")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gt01",sprintf("Given id '%s' does not represent an user",$id));
        }

        $form=$this->createForm("unir_ticketmanagerbundle_user_mail",$user,["method"=>"PUT"]);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $this->get("fos_user.user_manager")->updateUser($user,true);
            return $user;
        }


        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    /**
     * @Put("/{id}/password", name="ng-user-put-password",requirements={"id" = "\d+"} ,defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"userAll"})
     */
    public function putUserPasswordAction(Request $request,$id)
    {
        if (!$user = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:User")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"pp01",sprintf("Given id '%s' does not represent an user",$id));
        }

        $form=$this->createForm("unir_ticketmanaget_update_password",$user,["method"=>"PUT"]);
        $form->handleRequest($request);
        if(!$this->isValidPassword($user,$form->get("actual_password")->getData())){
            return $this->get("unir.rest.error")->errorView(421,"pp02",sprintf("Given password is not valid"));
        }
        if ($form->isValid())
        {
            $this->get("fos_user.user_manager")->updateUser($user,true);
            return $user;
        }
        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    private function isValidPassword(User $user,$old_password)
    {
        $encoder=$this->get("security.encoder_factory")->getEncoder($user);
        return $encoder->isPasswordValid($user->getPassword(),$old_password,$user->getSalt());
    }
    /**
     * @Post("/", name="ng-post-user",defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"idnamemail"})
     */
    public function createAction(Request $request)
    {


        $user = new User();
        $form= $this->createForm("unir_ticketmanagerbundle_user",$user,["method"=>"POST"]);

        $user->setUsername(Urlizer::urlize(base64_encode($this->container->get("security.secure_random")->nextBytes("24"))));
        $user->setPlainPassword(base64_encode($this->container->get("security.secure_random")->nextBytes("24")));
        $user->setExpired(true);

        $form->handleRequest($request);



        if ($existing_user=$this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:User")->findOneByEmail($user->getEmail())){
            $enterprise=$this->get("unir.common.common_service")->getCompany();
            $a=$form->get("roles")->getData();

            if($form->get("roles")->getData()){

                $enterprise->addAdminUser($existing_user);
            } else {
                $enterprise->addUser($existing_user);
            }
            $this->get("doctrine.orm.entity_manager")->persist($enterprise);
            $this->get("doctrine.orm.entity_manager")->flush();
            $this->get("unir.mailer.mail_service")->mailExistingUser($existing_user,$enterprise);
            return $existing_user;
        }




        if ($form->isValid()) {

            $this->get("fos_user.user_manager")->updateUser($user);
            $this->get("doctrine.orm.entity_manager")->persist($user);

            $this->get("doctrine.orm.entity_manager")->flush();
            $this->get("unir.mailer.mail_service")->mailNewUser($user);

            $enterprise=$this->get("unir.common.common_service")->getCompany();
            $a=$form->get("roles")->getData();
            if($form->get("roles")->getData()){
                $enterprise->addAdminUser($user);
            } else {
                $enterprise->addUser($user);
            }
            $this->get("doctrine.orm.entity_manager")->persist($enterprise);
            $this->get("doctrine.orm.entity_manager")->flush();
//            $this->get("session")->getFlashBag()->add("created",sprintf("El usuario ha sido creado satisfactoriamente. Correo de confirmacion enviado a: %s",$user->getEmail()));
            return $user;
        }
        return $this->get("unir.rest.error")->formErrorView("421",$form);
    }

    /**
     * @Delete("/{id}", name="ng-user-delete",requirements={"id" = "\d+"} ,defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"userAll"})
     */
    public function deleteAction($id)
    {
        if (!$user = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:User")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gt01",sprintf("Given id '%s' does not represent an user",$id));
        }
        $this->get("unir.entity_delete.service")->deleteUser($user);
        return [];
    }
}