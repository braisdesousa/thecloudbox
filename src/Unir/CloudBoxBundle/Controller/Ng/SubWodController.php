<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Delete;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\SubWod;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodTiming;
use Unir\CloudBoxBundle\Form\WodTimingType;

/**
 * @Route("/ng/~subwod/")
 * */
class SubWodController extends FOSRestController {


    /**
     * @Get("{slug}/", name="ng-subwods",requirements={"slug"="^[a-z0-9-]+$"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod"})
     */
    public function getWodSubWodsAction(Request $request,$slug)
    {
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->get("unir.rest.error")->errorView(421,"gst01",sprintf("Given slug '%s' does not represent a wod",$slug));
        }

        if (!$this->get("unir.can_view_or_edit.service")->canEditWod($wod)){
            return $this->get("unir.rest.error")->errorView(421,"gst02",sprintf("This user '%s' is not allowed to se this wod '%s'",$this->getUser()->getUsername(),$slug));
        }

        return $wod->getSubWods();
    }
    /**
     * @Post("{slug}/", name="ng-create-subwod",requirements={"slug"="^[a-z0-9-]+$"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod"})
     */
    public function createSubWodsAction(Request $request,$slug)
    {
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->get("unir.rest.error")->errorView(421,"cst01",sprintf("Given slug '%s' does not represent a wod",$slug));
        }
        if (!$title=$request->request->get("title")){
            return $this->get("unir.rest.error")->errorView(421,"cst02","No se ha introducido título");
        }

        $subWod= new SubWod();
        $subWod->setWod($wod);
        $subWod->setTitle($title);

        $this->get("doctrine.orm.entity_manager")->persist($subWod);
        $this->get("doctrine.orm.entity_manager")->flush();

        return $subWod;
    }
    /**
     * @Get("{id}", name="ng-subwod",requirements={"id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod"})
     */
    public function getSubWodAction($id){
        if (!$subWod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:SubWod")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gst03",sprintf("Given id '%s' does not represent a subWod",$id));
        }
        return $subWod;
    }
    /**
     * @Patch("{id}/open", name="ng-subwod-open",requirements={"id" = "\d+"} , defaults={"_format"="json","status"="open"},options={"expose"=true})
     * @Patch("{id}/close", name="ng-subwod-close",requirements={"id" = "\d+"} , defaults={"_format"="json","status"="closed"},options={"expose"=true})
     * @View(serializerGroups={"wod"})
     */
    public function patchSubWodAction($id,$status){

        if (!$subWod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:SubWod")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"pst01",sprintf("Given id '%s' does not represent a subWod",$id));
        }
        if (!in_array($status,[SubWod::STATUS_CLOSED,SubWod::STATUS_OPEN])){
            return $this->get("unir.rest.error")->errorView(421,"pst02",sprintf("Given status '%s' is not allowed",$status));
        }
        if (!$this->get("unir.can_view_or_edit.service")->canEditWod($subWod->getWod())){
            return $this->get("unir.rest.error")->errorView(421,"pst03",sprintf("Given status '%s' is not allowed",$status  ));
        }

        $subWod->setStatus($status);

        $this->getDoctrine()->getManager()->persist($subWod);
        $this->getDoctrine()->getManager()->flush();


        return $subWod;
    }
    /**
     * @Delete("{id}", name="ng-subwod-delete",requirements={"id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     */
    public function deleteSubWod($id){
        if (!$subWod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:SubWod")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"ds01",sprintf("Given id '%s' does not represent a subWod",$id));
        }

        $this->getDoctrine()->getManager()->remove($subWod);
        $this->getDoctrine()->getManager()->flush();
        return [];
    }
} 