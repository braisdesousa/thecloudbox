<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 12/09/14
 * Time: 16:25
 */

namespace Unir\CloudBoxBundle\Controller\Ng;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * This controller will help loading partial views for Angular
 * Class AngularPartialController
 * @package Unir\CloudBoxBundle\Controller\Ng
 * @Route("/")
 * @Security("is_granted('ROLE_USER')")
 */

class AngularPartialController extends FOSRestController
{

    /**
    * Used for display angular main page...
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function indexAction()
    {
        return $this->render("::angular_base.html.twig");
    }
    /**
     * Renders a partial file for Angular
     * @Get("/partials/_{file}/", name="ng_ticketmanager_render_partial", defaults={"_format"="html"}, requirements={"file"="^[a-z A-Z 0-9]{3,25}$"},options={"expose"=true})
     * @param $file
     * @return \Symfony\Component\HttpFoundation\Response
     * @View()
     */
    public function renderPartialAction($file)
    {
        $filePath = "@UnirCloudBox/Ng/Partials/$file.html.twig";
        //Resource exists?
        if( $this->get('templating')->exists($filePath)) {
            return $this->render($filePath);
        } else {
            return $this->errorView(421);

        }
    }

    /**
     *
     * @Get(
     *      "/partials/{folder}/_{file}/",
     *      name="ng_ticketmanager_render_folder_partial",
     *      requirements={"file"="^[a-z A-Z 0-9]{3,25}$", "folder"="^[a-z A-Z 0-9]{3,25}$"},
     *      options={"expose"=true})
     * @View()
     */
    public function renderFolderPartialAction($folder, $file)
    {
        $filePath = "@UnirCloudBox/Ng/Partials/$folder/$file.html.twig";


        //Resource exists?
        if( $this->get('templating')->exists($filePath)) {
            return $this->render($filePath);
        } else {
            return $this->errorView(421);
        }

    }

    private function errorView($httpCode)
    {
        $view = \FOS\RestBundle\View\View::create()->setStatusCode($httpCode);
        $view->setFormat("json");
        return $view;
    }
} 