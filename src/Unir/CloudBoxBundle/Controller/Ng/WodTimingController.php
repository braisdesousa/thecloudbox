<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Route;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodTiming;
use Unir\CloudBoxBundle\Form\WodTimingType;

/**
 * @Route("/ng/timing/")
 * */
class WodTimingController extends FOSRestController {


    /**
     * @Post("/{slug}/~start", name="ng-start_wodtiming",requirements={"slug"="^[a-z0-9-]+$"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"base_data"})
     */
    public function startWodsTimingAction(Request $request,$slug)
    {
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->errorView(421,sprintf("Given slug '%s' does not represent a wod",$slug));
        }
        if ($this->get("unir.common.wodTiming_repository_service")->hasWorkingWods($this->getUser())){
            return $this->errorView(421,sprintf("This user '%s' is not allowed to se this wod '%s'",$this->getUser()->getUsername(),$slug));
        }
        if (!$this->get("unir.can_view_or_edit.service")->canEditWod($wod)){
            return $this->errorView(421,sprintf("This user '%s' is not allowed to se this wod '%s'",$this->getUser()->getUsername(),$slug));
        }

        $wodTiming=new WodTiming();
        $wodTiming->setWod($wod);
        $wodTiming->setUser($this->getUser());

        $this->get("doctrine.orm.entity_manager")->persist($wodTiming);
        $this->get("doctrine.orm.entity_manager")->flush();
        return $wodTiming;

    }
    /**
     * @Get("/{slug}/", name="ng-wodtiming_slug",requirements={"slug"="^[a-z0-9-]+$"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wodTiming_list"})
     */
    public function getWodTimingsAction($slug){
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->errorView(421,sprintf("Given slug '%s' does not represent a wod",$slug));
        }
        return $this->getDoctrine()->getRepository("UnirCloudBoxBundle:WodTiming")->findByWod($wod);
    }
    /**
     * @Get("/{user_id}/", name="ng-wodtiming_user",requirements={"user_id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wodTiming_list"})
     */
    public function getWodTimingsByUserAction($user_id){
        if (!$user = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:User")->find($user_id)){
            return $this->errorView(421,sprintf("Given id '%s' does not represent an user",$user_id));
        }
        return $this->getDoctrine()->getRepository("UnirCloudBoxBundle:WodTiming")->findByUser($user);
    }

    /**
     * @Post("/{slug}/~end", name="ng-end_wodtiming",requirements={"slug"="^[a-z0-9-]+$"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wodTiming_list"})
     */
    public function endWodTimingAction(Request $request,$slug)
    {
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->errorView(421,sprintf("Given slug '%s' does not represent a wod",$slug));
        }
        if (!$wodTiming = $this->get("unir.common.wodTiming_repository_service")->getWorkingWod($this->getUser(),$wod)){
            return $this->errorView(421,sprintf("Given wod %s has not been starterd yet",$slug));
        }
        if (!$this->get("unir.can_view_or_edit.service")->canEditWod($wod)){
            return $this->errorView(421,sprintf("This user '%s' is not allowed to se this wod '%s'",$this->getUser()->getUsername(),$slug));
        }

        $form= $this->createForm(new WodTimingType(),$wodTiming);
        $form->handleRequest($request);

            if ($form->isValid()) {
                $this->get("doctrine.orm.entity_manager")->persist($wodTiming);
                $this->get("doctrine.orm.entity_manager")->flush();
                return $wodTiming;

            }

        return $form;

    }
    private function errorView($httpCode,$message=null)
    {
        $view = \FOS\RestBundle\View\View::create()->setStatusCode($httpCode);
        $view->setData(["message"=>$message]);
        $view->setFormat("json");
        return $view;
    }
} 