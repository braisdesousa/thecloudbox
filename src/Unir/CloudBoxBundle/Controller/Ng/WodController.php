<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Route;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\Wod;

/**
 * @Route("/ng")
 * */
class WodController extends FOSRestController {


    /**
     * @Get("/~wod/{slug}", name="ng-wod",requirements={"slug"="^[a-z0-9-]+$"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function getWodAction($slug)
    {
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->get("unir.rest.error")->errorView(421,"gt01",sprintf("Given slug '%s' does not represent a wod",$slug));
        }
        return $wod;
    }
    /**
     * @Post("/~wods", name="ng-wods" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"dashboard_list"})
     */
    public function getWodsAction(Request $request)
    {
        $groups=$request->request->get("groups",[]);
        $groups=empty($groups)?$this->get("unir.repository.wodgroup_repository_service")->getLastGroups(3):$groups;
        $difficulties=$request->get("difficulties",[]);
        $statuses=$request->get("statuses",[]);
        $sortBy=$request->get("sortBy",[]);

        $response=[];
        $response["filters"]=["difficulties"=>$difficulties,"sortBy"=>$sortBy,"statuses"=>$statuses,"groups"=>$this->get("unir.repository.wodgroup_repository_service")->findByWorkGroup()];
        foreach ($groups as $group) {
            if ($group=$this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WodGroup")->find($group)){
                if($this->get("unir.can_view_or_edit.service")->canViewWodGroup($group)){
                    $response["groups"][$group->getId()]=$group;
                    $response["group_wods"][$group->getId()]=$this->getPaginatedWods($this->get("unir.repository.wod_repository_service")->findByGroup($group,$statuses,$difficulties,$sortBy));
                }
            }
        }

    return $response;
    }
    /**
     * @Post("/~wodsbygroup", name="ng-wodsbygroup" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"dashboard_list"})
     */
    public function getWodByGroupAction(Request $request)
    {
        $group_id=$request->request->get("group_id",false);
        $difficulties=$request->get("difficulties",[]);
        $statuses=$request->get("statuses",[]);
        $sortBy=$request->get("sortBy",[]);
        $page=$request->get("page",1);

        if ($group=$this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WodGroup")->find($group_id)){
                $wods = $this->get('knp_paginator')->paginate(
                            $this->get("unir.repository.wod_repository_service")->findByGroup($group,$statuses,$difficulties,$sortBy),
                            $page,
                            15,
                            ['pageParameterName' => 'wodPage']
                        );
                $pagination_data=$wods->getPaginationData();
                $json_wods=[
                    "totalElements"=> $pagination_data["totalCount"],
                    "current"=> $pagination_data["current"],
                    "totalPages"=> $pagination_data["pageCount"],
                    "numItemsPerPage"=>$pagination_data["numItemsPerPage"],
                    "elements"=>$wods->getItems()
                ];

            return $json_wods;
        }
        return ["totalElements"=> 0, "current"=> 0, "totalPages"=> 0,  "numItemsPerPage"=>15, "elements"=>[]];
    }
    /**
     * @Patch("/~wod/{slug}/edit/status/{status}", name="ng-wod-patch-status", defaults={"_format"="json"},
     * requirements={"slug"="^[a-zA-Z0-9-]+$","status"="^[a-zA-Z ]+$"},options={"expose"=true})
     * @View(serializerGroups={"dashboard_list"})
     */
    public function patchStatusAction($slug,$status)
    {
        if (!$wod=$this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)) {
            return $this->get("unir.rest.error")->errorView("420","t01",sprintf("Wod with given slug '%s' does not exist"));
        }
        if (!$this->get("unir.can_view_or_edit.service")->canEditWod($wod)){
            return $this->get("unir.rest.error")->errorView("423","t02",sprintf("User not Allowed to edit this wod"));
        }
        if (!in_array(strtolower($status),Wod::getStatuses())) {
            return $this->get("unir.rest.error")->errorView("421","t03",sprintf("Status %s is not allowed",$status));
        }
        $wod->setStatus($status);
        $this->get("doctrine.orm.entity_manager")->persist($wod);
        $this->get("doctrine.orm.entity_manager")->flush();
        $this->get("unir.mailer.mail_service")->editWodStatus($wod);


        return $wod;

    }
    /**
     * @Patch("/~wod/{slug}/edit/difficulty/{difficulty}", name="ng-wod-patch-difficulty", defaults={"_format"="json"},
     * requirements={"slug"="^[a-zA-Z0-9-]+$","difficulty"="^[a-zA-Z ]+$"},options={"expose"=true})
     * @View(serializerGroups={"dashboard_list"})
     */
    public function patchDifficultyAction($slug,$difficulty)
    {
        if (!$wod=$this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)) {
            return $this->get("unir.rest.error")->errorView("420","t01",sprintf("Wod with given slug '%s' does not exist"));
        }
        if (!$this->get("unir.can_view_or_edit.service")->canEditWod($wod)){
            return $this->get("unir.rest.error")->errorView("423","t02",sprintf("User not Allowed to edit this wod"));
        }
        if (!in_array(strtolower($difficulty),Wod::getDifficulties())) {
            return $this->get("unir.rest.error")->errorView("421","t03",sprintf("Status %s is not allowed",$difficulty));
        }
        $wod->setDifficulty($difficulty);
        $this->get("doctrine.orm.entity_manager")->persist($wod);
        $this->get("doctrine.orm.entity_manager")->flush();
        $this->get("unir.mailer.mail_service")->editWodDifficulty($wod);
        return $wod;
    }
    /**
     * @Post("/~wod/", name="ng-wod-post", defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function postWodAction(Request $request){


        $wod= new Wod();
        $wod->setCreationUser($this->getUser());
        $form=$this->get("form.factory")->create("unir_ticketmanagerbundle_wod",$wod,["method"=>"POST"]);
        $form->handleRequest($request);


        if ($form->isValid()){
            //Save Data
            $this->getDoctrine()->getManager()->persist($wod);
            $this->getDoctrine()->getManager()->flush();
            //Generate ACL List and Send Mail
            $this->get("unir.acl_wod")->generateAclForWod($wod);
            $this->get("unir.mailer.mail_service")->mailWod($wod);

            return $wod;
        }
        echo((string)$this->get("validator")->validate($wod));
        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    /**
     * @Put("/~wod/{slug}", name="ng-wod-put",requirements={"slug"="^[a-zA-Z0-9-]+$"}, defaults={"_format"="json","form_name"="unir_ticketmanagerbundle_wod"},options={"expose"=true})
     * @Put("/~wod/s/{slug}", name="ng-wod_short-put",requirements={"slug"="^[a-zA-Z0-9-]+$"}, defaults={"_format"="json","form_name"="unir_ticketmanagerbundle_wod_short"},options={"expose"=true})
     * @Put("/~wod/a/{slug}", name="ng-wod_assigned-put",requirements={"slug"="^[a-zA-Z0-9-]+$"}, defaults={"_format"="json","form_name"="unir_ticketmanagerbundle_wod_assigned"},options={"expose"=true})
     * @Put("/~wod/f/{slug}", name="ng-wod_follower-put",requirements={"slug"="^[a-zA-Z0-9-]+$"}, defaults={"_format"="json","form_name"="unir_ticketmanagerbundle_wod_follower"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function putWodAction(Request $request,$slug,$form_name){

        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->get("unir.rest.error")->errorView(421,"pt01",sprintf("Given slug '%s' does not represent a wod",$slug));
        }

        $form=$this->get("form.factory")->create($form_name,$wod,["method"=>"PUT"]);
        $form->handleRequest($request);
        if ($form->isValid()){

            //Save Data
            $this->getDoctrine()->getManager()->persist($wod);
            $this->getDoctrine()->getManager()->flush();
            //Generate ACL List and Send Mail
            $this->get("unir.acl_wod")->generateAclForWod($wod);
            $this->get("unir.mailer.mail_service")->mailWod($wod);

            return $wod;
        }

        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    /**
     * @Delete("/~wod/{slug}", name="ng-wod-delete",requirements={"slug"="^[a-zA-Z0-9-]+$"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deleteWodAction($slug)
     {
         if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
             return $this->get("unir.rest.error")->errorView(421,"dt01",sprintf("Given slug '%s' does not represent a wod",$slug));
         }

         $this->get("unir.acl_wod")->removeAclForWod($wod);
         $this->get("unir.entity_delete.service")->deleteWod($wod);
         //$this->getDoctrine()->getManager()->flush();
         return [];

     }

    /**
     * @Patch("/~wod/{slug}/assign", name="ng-wod-assign", defaults={"_format"="json"},
     * requirements={"slug"="^[a-zA-Z0-9-]+$"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function assignAction($slug){
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->get("unir.rest.error")->errorView(421,"pat01",sprintf("Given slug '%s' does not represent a wod",$slug));
        }
        if (!$this->get("unir.can_view_or_edit.service")->canViewWod($wod)){
            return $this->get("unir.rest.error")->errorView(421,"pat02",sprintf("You are not allow to edit '%s'",$slug));
        }
        if ($wod->getUsersAssigned()->contains($this->getUser())){
            $wod->removeUsersAssigned($this->getUser());
        }else {
            $wod->addUsersAssigned($this->getUser());
        }

        $this->getDoctrine()->getManager()->persist($wod);
        $this->getDoctrine()->getManager()->flush();
        return $wod;
    }
    /**
     * @Patch("/~wod/{slug}/follow", name="ng-wod-follow", defaults={"_format"="json"},
     * requirements={"slug"="^[a-zA-Z0-9-]+$"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function followAction($slug){
        if (!$wod = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($slug)){
            return $this->get("unir.rest.error")->errorView(421,"pft01",sprintf("Given slug '%s' does not represent a wod",$slug));
        }
        if (!$this->get("unir.can_view_or_edit.service")->canViewWod($wod)){
            return $this->get("unir.rest.error")->errorView(421,"pft02",sprintf("You are not allow to edit '%s'",$slug));
        }
        if ($wod->getUsersFollower()->contains($this->getUser())){
            $wod->removeUsersFollower($this->getUser());
        }else {
            $wod->addUsersFollower($this->getUser());
        }

        $this->getDoctrine()->getManager()->persist($wod);
        $this->getDoctrine()->getManager()->flush();
        return $wod;
    }
    private function getPaginatedWods($wods,$page=1)
    {
        $wods = $this->get('knp_paginator')->paginate(
            $wods ,
            $page,
            15,
            ['pageParameterName' => 'wodPage']
        );
        $pagination_data=$wods->getPaginationData();
        $json_wods=[
            "totalElements"=> $pagination_data["totalCount"],
            "current"=> $pagination_data["current"],
            "totalPages"=> $pagination_data["pageCount"],
            "numItemsPerPage"=>$pagination_data["numItemsPerPage"],
            "elements"=>$wods->getItems()
        ];
        return $json_wods;
    }
} 