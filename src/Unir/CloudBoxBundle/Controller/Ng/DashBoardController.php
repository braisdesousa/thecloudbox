<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 10:28
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Route;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Model\User;

/**
 * @Route("/")
 * */
class DashBoardController extends FOSRestController {

    /**
     * @Get("/~scope-data", name="ng-base_data" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"base_data"})
     */
    public function dashboardAction()
    {

        return [
            "user"=>$this->getUser(),
            "workGroups"=>$this->get("unir.repository.workGroup_repository_service")->findAll(),
            "companies"=>$this->get("unir.repository.enterprise_repository_service")->findAll(),
            "wod_timing"=>$this->get("unir.common.wodTiming_repository_service")->hasWorkingWods($this->getUser())
        ];

    }
    /**
     * @Get("/~paginatedActivities/{page}", requirements={"page"="\d+"},name="ng-paginated_activities" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"dashboard_list"})
     */
    public function getPaginatedActivitiesAction($page=1)
    {
        return ['activities' => $this->getPaginatedActivities($page)];
    }
    /**
     * @Get("/~paginatedWods/{page}",requirements={"page"="\d+"}, name="ng-paginated_wods" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"dashboard_list"})
     */
    public function getPaginatedWodsAction($page=1)
    {
        return ['wods' => $this->getPaginatedWods($page)];
    }
    /**
     * @Get("/~select_workGroup/{id}",requirements={"id"="\d+"}, name="ng-select_workGroup" , defaults={"_format"="json","angular"=false},options={"expose"=true})
     * @Get("/~select_workGroup/{id}/no-redirect",requirements={"id"="\d+"}, name="ng-select_workGroup_no-redirect" , defaults={"_format"="json","angular"=true},options={"expose"=true})
     * @View(serializerGroups={"base_data"})
     */
    public function selectWorkGroupAction(Request $request, $id,$angular)
    {
        if (!$workGroup=$this->get("doctrine.orm.default_entity_manager")->getRepository("UnirCloudBoxBundle:WorkGroup")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"sp_01",sprintf("WorkGroup '%s' is not an existing workGroup"));
        }
        $this->get("unir.common.common_service")->setSelectedWorkGroup($workGroup);
        if($angular){
            return [
                "user"=>$this->getUser(),
                "workGroups"=>$this->get("unir.repository.workGroup_repository_service")->findAll(),
                "companies"=>$this->get("unir.repository.enterprise_repository_service")->findAll(),
                "wod_timing"=>$this->get("unir.common.wodTiming_repository_service")->hasWorkingWods($this->getUser())
            ];
        }else{
            return $this->redirect($this->generateUrl("ng_index"));
        }
    }
    /**
     *
     * @Get("/~select_enterprise/{id}", name="ng-select_enterprise",requirements={"id"="\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function selectEnterpriseAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$entity = $em->getRepository('UnirCloudBoxBundle:Enterprise')->find($id)) {
            return $this->get("unir.rest.error")->errorView(421,"sp_01",sprintf("Enterprise '%s' is not an existing enterprise"));
        }

        $this->get("unir.common.common_service")->unsetSelectedWorkGroup();
        $workGroups=$em->getRepository("UnirCloudBoxBundle:WorkGroup")->findByEnterprise($entity);
        if (!empty($workGroups)){
            $this->get("unir.common.common_service")->setSelectedWorkGroup($workGroups[0]);
        } else {
            $this->get("unir.common.common_service")->setSelectedEnterprise($entity);
        }

        return $this->redirect($this->generateUrl("ng_index"));
    }
    private function getPaginatedWods($page)
    {
        /**
         * @var User
         */

        $wods=$this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Wod")->findAll([Wod::STATUS_OPEN,Wod::STATUS_INPROGRESS],null,["updated"=>"DESC"]);
        $wodResult= [];
        foreach ($wods as $wod)
        {
            if($this->get("unir.can_view_or_edit.service")->canViewWod($wod)){
                $wodResult[]=$wod;
            }
        }
        $wods=$wodResult;




        //$wods = array_merge($user->getAssignedWods()->toArray(),$user->getFollowedWods()->toArray());

        usort($wods, function($a, $b){
            return $b->getCreated()->getTimestamp() - $a->getCreated()->getTimestamp();
        });


        $wods = $this->get('knp_paginator')->paginate(
            $wods ,
            $page,
            15,
            ['pageParameterName' => 'wodPage']
        );
        $pagination_data=$wods->getPaginationData();
        $json_wods=[
            "totalElements"=> $pagination_data["totalCount"],
            "current"=> $pagination_data["current"],
            "totalPages"=> $pagination_data["pageCount"],
            "numItemsPerPage"=>$pagination_data["numItemsPerPage"],
            "elements"=>$wods->getItems()
        ];
        return $json_wods;
    }
    private function getPaginatedActivities($page)
    {
        /**
         * @var EntityManager
         */
        $em = $this->get("doctrine.orm.default_entity_manager");

        $commonService = $this->get('unir.common.common_service');
        $canViewOrEditService = $this->get('unir.can_view_or_edit.service');
        $wodActivities=$this->getDoctrine()->getRepository("UnirCloudBoxBundle:WodActivity")->findAll();
        $allWodActivity=[];
        $wodComments=$this->getDoctrine()->getRepository("UnirCloudBoxBundle:WodComments")->findAll();
        $allWodComments=[];

        foreach($wodActivities as $wodActivity){
            if($canViewOrEditService->canViewWod($wodActivity->getWod())){
                $allWodActivity[]=$wodActivity;
            }
        }
        foreach($wodComments as $wodComment){
            if($canViewOrEditService->canViewWod($wodComment->getWod())){
                $allWodActivity[]=$wodComment;
            }
        }




        //Merge wodComments and wodActivity and sort em
        $result = array_merge($allWodComments, $allWodActivity);
        usort($result, function ($a, $b) {
                return $b->getCreated()->getTimestamp() - $a->getCreated()->getTimestamp();
            });


        $activities = $this->get('knp_paginator')->paginate(
            $result,
            $page,
            5,
            ['pageParameterName' =>'activitypage']
        );

        $pagination_data=$activities->getPaginationData();
        $json_activities=[
            "totalElements"=> $pagination_data["totalCount"],
            "current"=> $pagination_data["current"],
            "totalPages"=> $pagination_data["pageCount"],
            "numItemsPerPage"=>$pagination_data["numItemsPerPage"],
            "elements"=>$activities->getItems()
        ];

        return $json_activities;
    }
}