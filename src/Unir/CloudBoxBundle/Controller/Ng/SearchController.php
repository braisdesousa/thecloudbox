<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Route;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\Wod;

/**
 * @Route("/ng/search")
 * */
class WodController extends FOSRestController {


    /**
     * @Post("/search", name="ng-search",defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function SearchAction(Request $request,$slug)
    {
        if (!$text = $request->request->get("search")){
            return $this->get("unir.rest.error")->errorView(421,"s01","No search Text Was sent");
        }


        //Search on Wod By Title
        $wodByTitle= $this->getDoctrine()->getManager()->getRepository("UnirCloudBoxBundle:Wod")->findByTitleText($text,$this->get("unir.common.common_service")->getSelectedWorkGroup());
        //Search on Wod By Comment
        $wodByDescription= $this->getDoctrine()->getManager()->getRepository("UnirCloudBoxBundle:Wod")->findByDescriptionText($text,$this->get("unir.common.common_service")->getSelectedWorkGroup());
        //Search on WodGroup by Title
        $wodGroupByTitle= $this->getDoctrine()->getManager()->getRepository("UnirCloudBoxBundle:WodGroup")->findByTitleText($text,$this->get("unir.common.common_service")->getSelectedWorkGroup());
        //Search on WodGroup by Comment
        $wodGroupByDescription= $this->getDoctrine()->getManager()->getRepository("UnirCloudBoxBundle:WodGroup")->findByDescriptionText($text,$this->get("unir.common.common_service")->getSelectedWorkGroup());

        return ["wodByTitle"=>$wodByTitle,"wodByDescription"=>$wodByDescription,"wodGroupByTitle"=>$wodGroupByTitle,"wodGroupByDescription"=>$wodGroupByDescription];
    }

} 