<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Delete;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;

/**
 * @Route("/ng/~wodgroup")
 * */
class WodGroupController extends FOSRestController {


    /**
     * @Get("/{id}", name="ng-wodgroup",requirements={"id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wodgroup_preview"})
     */
    public function getWodGroupAction($id)
    {
        if (!$wodGroup = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WodGroup")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gtg01",sprintf("Given id '%s' does not represent a WodGroup",$id));
        }
        return $wodGroup;
    }
    /**
     * @Get("/", name="ng-wodgroups" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"dashboard_list"})
     */
    public function getWodGroupsAction()
    {
        return $this->get("unir.repository.wodgroup_repository_service")->findByWorkGroup();
    }
    /**
     * @Post("/", name="ng-wodgroup-post", defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function postWodAction(Request $request){

        $wodGroup= new WodGroup();
        $form=$this->createForm("unir_ticketmanagerbundle_wodgroup",$wodGroup,["method"=>"POST"]);
        $form->handleRequest($request);
        if ($form->isValid()){


            //Save Data
            $this->getDoctrine()->getManager()->persist($wodGroup);

            //Create DefaultWod
            $this->get("unir.repository.wod_repository_service")->createDefault($wodGroup,$this->getUser());

            $this->getDoctrine()->getManager()->flush();
            //Generate ACL List and Send Mail
            $this->get("unir.acl_wod")->generateAclForWodGroupWods($wodGroup);

            return $wodGroup;
        }

        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    /**
     * @Put("/{id}", name="ng-wodgroup-put",requirements={"id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"wod_preview"})
     */
    public function putWodAction(Request $request,$id){

        if (!$wodGroup = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WodGroup")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gtg01",sprintf("Given id '%s' does not represent a WodGroup",$id));
        }

        $form=$this->get("form.factory")->create("unir_ticketmanagerbundle_wodgroup",$wodGroup,["method"=>"PUT"]);
        $form->handleRequest($request);

        if ($form->isValid()){

            //Save Data
            $this->getDoctrine()->getManager()->persist($wodGroup);
            $this->getDoctrine()->getManager()->flush();
            //Generate ACL List;
            $this->get("unir.acl_wod")->generateAclForWodGroupWods($wodGroup);

            return $wodGroup;
        }

        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    /**
     * @Delete("/{id}", name="ng-wodgroup-delete",requirements={"id" = "\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deleteWodAction($id)
    {
        if (!$wodGroup = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WodGroup")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gtg01",sprintf("Given id '%s' does not represent a WodGroup",$id));
        }
        $this->get("unir.entity_delete.service")->deleteWodGroup($wodGroup);
        $this->get("unir.acl_wod")->removeAclForWodGroupWods($wodGroup);
        $this->getDoctrine()->getManager()->flush();
        return [];
    }

} 