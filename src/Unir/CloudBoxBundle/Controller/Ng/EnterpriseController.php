<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Route;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\Wod;

/**
 * @Route("/ng/~enterprise")
 * */
class EnterpriseController extends FOSRestController {


    /**
     * @Get("/{id}", name="ng-enterprise",requirements={"id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"enterprise_preview"})
     */
    public function getEnterpiseAction($id)
    {
        if (!$enterprise = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Enterprise")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gt01",sprintf("Given id '%s' does not represent a enterprise",$id));
        }
        return $enterprise;
    }
    /**
     * @Get("/", name="ng-enterprises" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"enterprise_preview"})
     */
    public function getEnterprisesAction()
    {
        return $this->get("unir.repository.enterprise_repository_service")->findAll();
    }
    /**
     * @Get("/user-info", name="ng-enterprise-users-info" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"enterprise_user_info"})
     */
    public function getEnterprisesUserInfoAction()
    {
        return $this->get("unir.common.common_service")->getCompany();
    }
    /**
     * @Post("/", name="ng-enterprise-post", defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"enterprise_preview"})
     */
    public function postEnterpriseAction(Request $request){


        $form=$this->get("form.factory")->create("unir_ticketmanagerbundle_enterprise",null,["method"=>"POST"]);
        $form->handleRequest($request);


        if ($form->isValid()){

            $enterprise=$form->getData();
            //Save Data
            $this->getDoctrine()->getManager()->persist($enterprise);

            $workGroup=$this->get("unir.repository.workGroup_repository_service")->createDefault($enterprise);
            $tg=$this->get("unir.repository.wodgroup_repository_service")->createDefault($workGroup);
            $this->get("unir.repository.wod_repository_service")->createDefault($tg,$this->getUser());

            $this->getDoctrine()->getManager()->flush();
            //Generate ACL List and Send Mail

            return $enterprise;
        }

        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    /**
     * @Put("/{id}", name="ng-enterprise-put",requirements={"id" = "\d+"}, defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"enterprise_preview"})
     */
    public function putEnterpriseAction(Request $request,$id){

        if (!$enterprise = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Enterprise")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"pt01",sprintf("Given id '%s' does not represent an Enterprsie",$id));
        }

        $form=$this->get("form.factory")->create("unir_ticketmanagerbundle_enterprise",$enterprise,["method"=>"PUT"]);
        $form->handleRequest($request);

        if ($form->isValid()){

            //Save Data
            $this->getDoctrine()->getManager()->persist($enterprise);
            $this->getDoctrine()->getManager()->flush();
            //Generate ACL List and Send Mail
            return $enterprise;
        }

        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    /**
     * @Delete("/{id}", name="ng-enterprise-delete",requirements={"id" = "\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deleteWodAction($id)
     {
         if (!$enterprise = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:Enterprise")->find($id)){
             return $this->get("unir.rest.error")->errorView(421,"pt01",sprintf("Given id '%s' does not represent an enterprise",$id));
         }


         $this->get("unir.entity_delete.service")->deleteEnterprise($enterprise);
         $this->get("unir.acl_wod")->removeAclForCompanyWods($enterprise);
         $this->getDoctrine()->getManager()->flush();
         return [];

     }
} 