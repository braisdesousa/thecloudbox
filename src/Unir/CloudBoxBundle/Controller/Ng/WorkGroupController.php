<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 18/09/14
 * Time: 13:23
 */

namespace Unir\CloudBoxBundle\Controller\Ng;


use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Route;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;

/**
 * @Route("/ng/~workGroup")
 * */
class WorkGroupController extends FOSRestController {


    /**
     * @Get("/{id}", name="ng-workGroup",requirements={"id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"workGroup_preview"})
     */
    public function getWorkGroupAction($id)
    {
        if (!$workGroup = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WorkGroup")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gt01",sprintf("Given id '%s' does not represent a workGroup",$id));
        }
        return $workGroup;
    }
    /**
     * @Get("/user/{id}", name="ng-user-workGroups",requirements={"id" = "\d+"} , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"workGroup_preview"})
     */
    public function getUserWorkGroupAction($id)
    {
        if (!$user = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:User")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"gt01",sprintf("Given id '%s' does not represent an user",$id));
        }
        return ["workGroups"=>$user->getWorkGroups()];
    }
    /**
     * @Get("/", name="ng-workGroups" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"workGroup_preview"})
     */
    public function getWorkGroupsAction()
    {
        return ["workGroups"=>$this->get("unir.repository.workGroup_repository_service")->findAll()];
    }
    /**
     * @Get("/user-info", name="ng-workGroups-user-info" , defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"workGroup_user_info"})
     */
    public function getWorkGroupsUserInfoAction()
    {
        return ["workGroups"=>$this->get("unir.repository.workGroup_repository_service")->findAll()];
    }
    /**
     * @Post("/", name="ng-workGroup-post", defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"workGroup_preview"})
     */
    public function postWorkGroupAction(Request $request){


        $workGroup = new WorkGroup();
        $workGroup->setEnterprise($this->get("unir.common.common_service")->getCompany());
        $form=$this->get("form.factory")->create("unir_ticketmanagerbundle_workGroup",$workGroup,["method"=>"POST"]);
        $form->handleRequest($request);


        if ($form->isValid()){

            $workGroup=$form->getData();
            //Save Data
            $this->getDoctrine()->getManager()->persist($workGroup);

            $tg=$this->get("unir.repository.wodgroup_repository_service")->createDefault($workGroup);
            $this->get("unir.repository.wod_repository_service")->createDefault($tg,$this->getUser());

            $this->getDoctrine()->getManager()->flush();
            //Generate ACL List and Send Mail
            $this->get("unir.acl_wod")->generateAclForWorkGroupWods($workGroup);

            return $workGroup;
        }

        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    /**
     * @Put("/{id}", name="ng-workGroup-put",requirements={"id" = "\d+"}, defaults={"_format"="json"},options={"expose"=true})
     * @View(serializerGroups={"workGroup_preview"})
     */
    public function putWorkGroupAction(Request $request,$id){

        if (!$workGroup = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WorkGroup")->find($id)){
            return $this->get("unir.rest.error")->errorView(421,"pt01",sprintf("Given id '%s' does not represent a workGroup",$id));
        }

        $form=$this->get("form.factory")->create("unir_ticketmanagerbundle_workGroup",$workGroup,["method"=>"PUT"]);
        $form->handleRequest($request);

        if ($form->isValid()){

            //Save Data
            $this->getDoctrine()->getManager()->persist($workGroup);
            $this->getDoctrine()->getManager()->flush();
            //Generate ACL List and Send Mail
            $this->get("unir.acl_wod")->removeAclForWorkGroupWods($workGroup);
            $this->get("unir.acl_wod")->generateAclForWorkGroupWods($workGroup);

            return $workGroup;
        }

        return $this->get("unir.rest.error")->formErrorView(421,$form);
    }
    /**
     * @Delete("/{id}", name="ng-workGroup-delete",requirements={"id" = "\d+"}, defaults={"_format"="json"},options={"expose"=true})
     */
    public function deleteWorkGroupAction($id)
     {
         if (!$workGroup = $this->get("doctrine.orm.entity_manager")->getRepository("UnirCloudBoxBundle:WorkGroup")->find($id)){
             return $this->get("unir.rest.error")->errorView(421,"pt01",sprintf("Given id '%s' does not represent a workGroup",$id));
         }
         $this->get("unir.entity_delete.service")->deleteWorkGroup($workGroup);
         $this->get("unir.acl_wod")->removeAclForWorkGroupWods($workGroup);
         $this->getDoctrine()->getManager()->flush();

         return [];

     }
} 