<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/21/14
 * Time: 10:58 AM
 */

namespace Unir\CloudBoxBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Finder\Exception\OperationNotPermitedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Validator\Constraints\Date;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Form\ConfirmUserType;
use Unir\CloudBoxBundle\Form\UserType;

/**
 * Enterprise controller.
 */
class RegisterController extends Controller{



    /**
     * Lists all Users
     *
     * @Route("/confirm-registration/{username}", name="confirm_registration")
     * @Method("GET")
     * @Template("UnirCloudBoxBundle:Public:confirm_registration.html.twig")
     */
    public function indexAction($username)
    {
        if (!$user=$this->get("fos_user.user_manager")->findUserByUsername($username))
        {
           throw new NotFoundHttpException(sprintf("No user was found with this token %s",$username));
        }
        if (!$user->getExpired())
        {

           throw new NotFoundHttpException(sprintf("Credentials Are Not Expired for user %s",$username));
        }

        $form= $this->createCreateForm($user);
        return ["form"=>$form->createView()];
    }

    /**
     * @Route("/confirm-registration/{username}", name="user_confirm_registration")
     * @Method("POST")
     * @Template("UnirCloudBoxBundle:Public:confirm_registration.html.twig")
    */
    public function confirmAction(Request $request,$username)
    {
        if (!$entity=$this->getDoctrine()->getManager()->getRepository("UnirCloudBoxBundle:User")->findOneByUsername($username))
        {
            throw new NotFoundHttpException(sprintf("No user was found with this token %s",$username));
        }
        if (!($entity instanceof User)){
            throw  new NotFoundHttpException(sprintf("No user was found with this token %s",$username));
        }
        if (!$entity->getExpired()) {
            throw new NotFoundHttpException(sprintf("Credentials Are Not Expired for user %s",$username));
        }
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();


            $entity->setExpired(false);
            $entity->setEnabled(true);
            $this->get("fos_user.user_manager")->updateUser($entity);
            $em->persist($entity);
            $em->flush();
            $em->refresh($entity);
            $token = new UsernamePasswordToken($entity, $entity->getPlainPassword(), "fos_userbundle", $entity->getRoles());
            $this->get("security.context")->setToken($token);
            // Fire the login event
            // Logging the user in above the way we do it doesn't do this automatically
            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

            return $this->redirect($this->generateUrl("ng_dashboard"));
        }
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );


    }
    private function createCreateForm(User $user)
    {
        $form = $this->createForm(new ConfirmUserType(), $user, array(
                'action' => $this->generateUrl('user_confirm_registration',["username"=>$user->getUsername()]),
                'method' => 'POST',
            ));
        $form->add('submit', 'submit', array('label' => 'Confirm'));

        return $form;
    }

} 