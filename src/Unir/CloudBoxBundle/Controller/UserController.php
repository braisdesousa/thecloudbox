<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/21/14
 * Time: 10:58 AM
 */

namespace Unir\CloudBoxBundle\Controller;


use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Finder\Exception\OperationNotPermitedException;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\Date;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Form\UserType;

/**
 * Enterprise controller.
 * @Security("is_granted('ROLE_ADMIN')")
 * @Route("/user")
 */
class UserController extends Controller{



    /**
     * Lists all Users
     *
     * @Route("/", name="user_list")
     * @Method("GET")
     * @Template("UnirCloudBoxBundle:User:index.html.twig")
     */
    public function indexAction()
    {

        if ($response=$this->get("unir.minimun_content.service")->hasMinimumContent("enterprise")){
            $enterprise=$this->get("unir.repository.enterprise_repository_service")->checkOrCreate();
        }
        if ($response=$this->get("unir.minimun_content.service")->hasMinimumContent("workGroup")){
            $enterprise=isset($enterprise)?$enterprise:$this->get("unir.common.common_service")->getCompany();
            $workGroup=$this->get("unir.repository.workGroup_repository_service")->createDefault($enterprise);
            $wodGroup=$this->get("unir.repository.wodgroup_repository_service")->createDefault($workGroup);
            $wod=$this->get("unir.repository.wod_repository_service")->createDefault($wodGroup,$this->getUser());

        }
        $this->getDoctrine()->getManager()->flush();

        $company=$this->get("unir.common.common_service")->getCompany();
        $users= $this->get("unir.repository.users_repository_service")->findByCompany($company,["enterprise"=>"ASC","username"=>"ASC"]);
        $usersGroups= $this->getDoctrine()->getManager()->getRepository("UnirCloudBoxBundle:Group")->findByCompany($company);

        return ["users"=>$users,"userGroups"=>$usersGroups];
    }
    /**
     * User Profile Show
     *
     * @Route("/_{id}/",requirements={"id"="\d+"}, name="user_profile_show", options={"expose"=true})
     * @Method("GET")
     * @Security("is_granted('ROLE_USER')")
     * @Template("UnirCloudBoxBundle:User:profile.html.twig")
     */
    public function userProfileAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$user=$this->get("doctrine.orm.default_entity_manager")->getRepository("UnirCloudBoxBundle:User")->find($id)){
            throw new NotFoundHttpException(sprintf("No user with given id %s",$id));
        }

        //If user haven't ROLE_OWNER, check the other user enterprise...
        if (!$this->getUser()->hasRole('ROLE_OWNER')) {
            $company = $this->get("unir.common.common_service")->getCompany();

            if ($company && !$company->getUsers()->contains($user)){
                throw new AccessDeniedException("No tienes permiso para ver a este Usuario este intento fraudulento será reportado");
            }
        }

        if (!$this->getUser()->hasRole("ROLE_ADMIN") && !$this->getUser()->hasRole("ROLE_OWNER")){
            if ($user->hasRole("ROLE_ADMIN") || $user->hasRole("ROLE_OWNER")){
                throw new AccessDeniedException("No tienes permiso para ver a este Usuario este intento fraudulento será reportado");
            }
         }
        if ($this->getUser()->hasRole("ROLE_ADMIN")){
            if ($user->hasRole("ROLE_OWNER")){
                throw new AccessDeniedException("No tienes permiso para ver a este Usuario este intento fraudulento será reportado");
            }
         }


        $workGroupsMember = $em->getRepository('UnirCloudBoxBundle:WorkGroup')->findByUserMember($user);
        $wodGroupMember = $em->getRepository('UnirCloudBoxBundle:WodGroup')->findByUser($user);

        $wodActivities=$this->get("doctrine.orm.default_entity_manager")->getRepository("UnirCloudBoxBundle:WodActivity")->findByUser($user);
        if (count($this->get("unir.repository.wod_repository_service")->findByUser($this->getUser()))==0){
            return $this->render("UnirCloudBoxBundle:Default:empty_dashboard.html.twig",["step"=>4]);
        }

        return [
            "user"             => $user,
            "wodActivities"   => $wodActivities,
            "workGroupsMember"   => $workGroupsMember,
            "wodGroupMember"  => $wodGroupMember,
        ];
    }

    /**
     * @Route("/updateAsync/{id}",requirements={"id"="\d+"}, name="user_update_async")
     * @Method("POST")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $id
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return JsonResponse
     */
    public function updateAsyncAction(Request $request, $id){
        $em = $this->get('doctrine.orm.default_entity_manager');
        $entity = $em->getRepository('UnirCloudBoxBundle:User')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Wod entity.');
        }
        //username
        $entity->setUsername($request->get('username'));
        //email
        $entity->setEmail($request->get('email'));
        //wodGroups member
        $entity = $this->get('unir.repository.users_repository_service')->setWodGroups($entity,$request->get('idsWodGroupsMember',[]));
        //wodGroups follower
        $entity = $this->get('unir.repository.users_repository_service')->setFollowedWodGroups($entity,$request->get('idsWodGroupsFollower',[]));
        //WorkGroups
        $entity = $this->get('unir.repository.users_repository_service')->setWorkGroups($entity,$request->get('idsWorkGroups',[]));
        //userGroups
        $entity = $this->get('unir.repository.users_repository_service')->setUserGroups($entity,$request->get('idsUserGroups',[]));

        $errors=$this->get("validator")->validate($entity);
        if (count($errors)>0){
            //return new JsonResponse(["error"=>count($errors)]);
            $data = $this->get("jms_serializer")->serialize($errors,"json",SerializationContext::create()->setGroups(array('list')));
            //return new Response($data);
            return new JsonResponse([
                "error"=>count($errors),'type'=>'validation','error_message'=>'',
                'errors'=>$data
            ]);
        }
        $em->persist($entity);
        $em->flush();
        //$this->sendEmail($entity);
        $data = $this->get("jms_serializer")->serialize($entity,"json",SerializationContext::create()->setGroups(array('userAll')));
        //return new Response($data);
        return new JsonResponse([
            "error"=>count($errors),'type'=>'response','error_message'=>'',
            'user'=>$data
        ]);
    }

    /**
     * @Route("/", name="user_patch")
     * @Method("PATCH")
     */
    public function pacthUserAction(Request $request)
    {
        $response_array=[];
        $id= $request->request->get("id",null);
        $method= $request->request->get("method",null);
        $em=$this->get("doctrine.orm.default_entity_manager");

        if (!$user=$em->getRepository("UnirCloudBoxBundle:User")->find($id)){
              $response_array=["error"=>1,"message"=>"No user with given id $id"];
            }
        if (empty($response_array))
            {
                switch ($method) {
                    case "disable": $user->setEnabled(false); break;
                    case "enable": $user->setEnabled(true); break;
                    default: $response_array = ["error"=>1,"message"=>"Method '$method' does not exist"];
                }
                if (empty($response_array))
                {
                    $em->persist($user);
                    $em->flush($user);
                    $response_array=["error"=>0];
                }
            }

        return new JsonResponse($response_array);
    }
    /**
     * @Route("/", name="user_remove")
     * @Method("DELETE")
     */
    public function removeUserAction(Request $request)
    {
        //Retrieve user Id
        $id = $request->request->get("id",null);

        $user = $this->get("doctrine.orm.default_entity_manager")
            ->getRepository("UnirCloudBoxBundle:User")
            ->find($id);

        if (!$user) {
            return new JsonResponse([
                "error" => 1,
                "message" => "Given id '$id' was not found"
            ]);
        }

        $deleteService = $this->get('unir.entity_delete.service');

        $deleteService->deleteUser($user);

        return new JsonResponse(['error' => 0]);
    }
    /**
     * @Route("/register", name="register")
     * @Method("GET")
     * @Template("UnirCloudBoxBundle:User:new.html.twig")
     */
    public function registerUserAction(Request $request)
    {

        $this->get("unir.repository.enterprise_repository_service")->checkOrCreate();
        $user = new User();
        $form   = $this->createCreateForm($user);

        return array(
            'entity' => $user,
            'form'   => $form->createView(),
        );

    }
    /**
     * @Route("/", name="user_register")
     * @Method("POST")
     * @Template("UnirCloudBoxBundle:User:new.html.twig")
    */
    public function createAction(Request $request)
    {

        $em=$this->getDoctrine()->getManager();
        $entity = new User();
        $form = $this->createCreateForm($entity);

        if (!$entity->getEnterprise()){
            $entity->setEnterprise($this->getUser()->getEnterprise());
        }
        $entity->setUsername(Urlizer::urlize(base64_encode($this->container->get("security.secure_random")->nextBytes("24"))));
        $entity->setPlainPassword(base64_encode($this->container->get("security.secure_random")->nextBytes("24")));
        $entity->setExpired(true);

        $form->handleRequest($request);
        if ($form->isValid()) {


            $this->get("fos_user.user_manager")->updateUser($entity);
            $em->persist($entity);
            $em->flush();
            $this->sendConfirmationMail($entity);

            $this->get("session")->getFlashBag()->add("created",sprintf("El usuario ha sido creado satisfactoriamente. Correo de confirmacion enviado a: %s",$entity->getEmail()));
            return $this->redirect($this->generateUrl('user_list'));
        }
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );

        //throw new \Exception('Something went wrong!');
    }

    private function sendConfirmationMail(User $user)
    {
        $this->get("unir.mailer.mail_service")->mailNewUser($user);
    }
    private function createCreateForm(User $user)
    {
        $form = $this->createForm(new UserType($this->getUser(),$this->get("doctrine.orm.default_entity_manager")), $user, array(
                'action' => $this->generateUrl('user_register'),
                'method' => 'POST',
            ));
        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }
} 