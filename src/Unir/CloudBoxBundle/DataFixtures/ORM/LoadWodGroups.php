<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 7/07/14
 * Time: 18:58
 */

namespace Unir\CloudBoxBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Helper\LoremProvider;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

/**
 * Class LoadWodGroups
 * @package Unir\CloudBoxBundle\DataFixtures\ORM
 */
class LoadWodGroups extends AbstractFixture implements
    FixtureInterface,
    OrderedFixtureInterface,
    ContainerAwareInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS AND CONSTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Class order for load
     */
    const ORDER = 6;

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @var ObjectManager
     */
    private $em;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * DEPENDENCY INJECTION
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Container injection
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return self::ORDER;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {

        $this->em = $em;

        if ($this->isTestEnvironment()) {
            $this->createWodGroupsTestEnvironment();
        } elseif ($this->isDevEnvironment()) {
            $this->createWodGroupsDevEnvironment();
        }
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Creates WodGroups for test environment
     */
    private function createWodGroupsTestEnvironment()
    {
        TestData::initialize();

        foreach (TestData::getWodGroups() as $dataWodGroup) {

            $wodGroup = new WodGroup();

            $wodGroup
                ->setId($dataWodGroup->getId())
                ->setName($dataWodGroup->getName())
                ->setDescription($dataWodGroup->getDescription())
                ->setWorkGroup($this->getWorkGroupsRepository()->find($dataWodGroup->getWorkGroup()->getId()));

            if ($dataWodGroup->getDeleted()) {
                $wodGroup->setDeleted();
            }

            //If is not valid, don't persist
            if (!$this->validate($wodGroup)) {
                continue;
            }

            $this->changeGeneratorStrategy($wodGroup);
            $this->em->persist($wodGroup);
            $this->em->flush();



            foreach ($dataWodGroup->getGroupMembers() as $groupMember) {
                $wodGroup->addGroupMember($this->getGroupsRepository()->find($groupMember->getId()));
            }

            $this->em->flush();
        }
    }

    /**
     * Creates WodGroups for dev environment
     */
    private function createWodGroupsDevEnvironment()
    {
        $factory = new Factory();
        $faker = $factory->create('es');
        $faker->addProvider(new LoremProvider($faker));
        $numToInsertPerWorkGroup = 7;

        $allEnterprises = $this->getEnterprisesRepository()->findAll();

        foreach ($allEnterprises as $enterprise) {

            //If the enterprise haven't users, then must continue
            foreach ($enterprise->getWorkGroups() as $workGroup) {
                for ($i = 0; $i < rand(1, $numToInsertPerWorkGroup); $i++) {


                    $wodGroup = new WodGroup();
                    $wodGroup
                        ->setName($faker->words(3, true))
                        ->setDescription($faker->sentence(5))
                        ->setWorkGroup($workGroup);
                    //If is not valid, don't persist
                    if (!$this->validate($wodGroup)) {
                        continue;
                    }

                    $this->em->persist($wodGroup);
                    $users=$enterprise->getUsers()->toArray();
                    $groups=$enterprise->getGroups()->toArray();
                    $rand= rand(1,count($users)-1);
                    $rand_keys=array_rand($users,$rand);
                    if (!is_array($rand_keys)){
                        $rand_keys=[$rand_keys];
                    }
                    foreach ($rand_keys as $key){
                        $wodGroup->addUser($users[$key]);
                    }
                    $rand= rand(1,count($groups)-1);
                    $rand_keys=array_rand($groups,$rand);
                    if (!is_array($rand_keys)){
                        $rand_keys=[$rand_keys];
                    }
                    foreach ($rand_keys as $key){
                        $wodGroup->addGroupMember($groups[$key]);
                    }
                    $this->em->persist($wodGroup);
                }
            }
        }

        $this->em->flush();
    }

    /**
     * Validates a WodGroup...
     * @param WodGroup $wodGroup
     * @return bool
     */
    private function validate(WodGroup $wodGroup)
    {
        $errors = $this->container->get('validator')->validate($wodGroup);

        if (count($errors) == 0) {
            return true;
        } else {
            $logger = $this->container->get('logger');

            if (!is_array($errors)) {
                $errors = [$errors];
            }

            $logger->error(implode(', ', $errors));
            return false;
        }
    }


    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getWorkGroupsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:WorkGroup');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getUsersRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:User');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getGroupsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Group');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getEnterprisesRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Enterprise');
    }


    /**
     * Changes the generator strategy for store correctly the id
     * @param $entity
     */
    private function changeGeneratorStrategy($entity)
    {
        $metadata = $this->em->getClassMetadata(get_class($entity));
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
    }

    /**
     * Checks if we are on test environment
     * @return bool
     */
    private function isTestEnvironment()
    {
        return 'test' == $this->getEnvironment();
    }

    /**
     * Check if we are on prod environment
     * @return bool
     */
    private function isProdEnvironment()
    {
        return 'prod' == $this->getEnvironment();
    }

    /**
     * Check if we are on dev environment
     * @return bool
     */
    private function isDevEnvironment()
    {
        return 'dev' == $this->getEnvironment();
    }

    /**
     * Return in what environment we are
     * @return mixed
     */
    private function getEnvironment()
    {
        return $this->container->get('kernel')->getEnvironment();
    }
}
