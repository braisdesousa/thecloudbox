<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 7/07/14
 * Time: 18:58
 */

namespace Unir\CloudBoxBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Helper\LoremProvider;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Doctrine\ORM\EntityManager;

/**
 * Class LoadWorkGroups
 * @package Unir\CloudBoxBundle\DataFixtures\ORM
 */
class LoadWorkGroups extends AbstractFixture implements
    FixtureInterface,
    OrderedFixtureInterface,
    ContainerAwareInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS AND CONSTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Class order for load
     */
    const ORDER = 5;

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @var EntityManager
     */
    private $em;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * DEPENDENCY INJECTION
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Container injection
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return self::ORDER;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $this->em = $em;

        if ($this->isTestEnvironment()) {
            $this->createWorkGroupsTestEnvironment();
        } elseif ($this->isDevEnvironment()) {
            $this->createWorkGroupsNonTestEnvironment();
        }

        $this->em->flush();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Creates workGroups for test environment
     */
    private function createWorkGroupsTestEnvironment()
    {
        TestData::initialize();

        foreach (TestData::getWorkGroups() as $dataWorkGroup) {

            $workGroup = new WorkGroup();
            $workGroup
                ->setId($dataWorkGroup->getId())
                ->setName($dataWorkGroup->getName())
                ->setDescription($dataWorkGroup->getDescription())
                ->setEnterprise($this->getEnterpriseRepository()->find($dataWorkGroup->getEnterprise()->getId()));

            if ($dataWorkGroup->getDeleted()) {
                $workGroup->setDeleted();
            }

            //If is not valid, just don't insert it
            if (!$this->validate($workGroup)) {
                continue;
            }

            $this->changeGeneratorStrategy($workGroup);

            $this->em->persist($workGroup);




        }

    }

    /**
     * Creates WorkGroups for non test environment
     */
    private function createWorkGroupsNonTestEnvironment()
    {
        $factory = new Factory();
        $faker = $factory->create('es');
        $faker->addProvider(new LoremProvider($faker));
        $maxNumToInsertPerEnterprise = 15;
        //Retrieves all enterprises
        $allEnterprises = $this->getEnterpriseRepository()->findAll();

        foreach ($allEnterprises as $enterprise) {
            //Insert the workGroups
            for ($i = 0; $i < rand(0, $maxNumToInsertPerEnterprise); $i++) {
                $workGroup = new WorkGroup();
                $workGroup
                    ->setName($faker->words(3, true))
                    ->setDescription($faker->sentence(6))
                    ->setEnterprise($enterprise);

                //Not valid, non continue
                if (!$this->validate($workGroup)) {
                    continue;
                }

                $this->em->persist($workGroup);
            }
        }

        $this->em->flush();
    }


    /**
     * Validates a workGroup...
     * @param WorkGroup $workGroup
     * @return bool
     */
    private function validate(WorkGroup $workGroup)
    {
        $errors = $this->container->get('validator')->validate($workGroup);

        if (count($errors) == 0) {
            return true;
        } else {
            $logger = $this->container->get('logger');

            if (!is_array($errors)) {
                $errors = [$errors];
            }

            $logger->error(implode(', ', $errors));
            return false;
        }
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getEnterpriseRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Enterprise');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getUsersRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:User');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getGroupsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Group');
    }


    /**
     * Changes the generator strategy for store correctly the id
     * @param $entity
     */
    private function changeGeneratorStrategy($entity)
    {
        $metadata = $this->em->getClassMetadata(get_class($entity));
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
    }

    /**
     * Checks if we are on test environment
     * @return bool
     */
    private function isTestEnvironment()
    {
        return 'test' == $this->getEnvironment();
    }

    /**
     * Check if we are on prod environment
     * @return bool
     */
    private function isProdEnvironment()
    {
        return 'prod' == $this->getEnvironment();
    }

    /**
     * Check if we are on dev environment
     * @return bool
     */
    private function isDevEnvironment()
    {
        return 'dev' == $this->getEnvironment();
    }

    /**
     * Return in what environment we are
     * @return mixed
     */
    private function getEnvironment()
    {
        return $this->container->get('kernel')->getEnvironment();
    }
}
