<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 12/08/14
 * Time: 17:12
 */

namespace Unir\CloudBoxBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Helper\LoremProvider;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Doctrine\ORM\EntityManager;
use Faker\Factory;

/**
 * Loads groups into database
 * Class LoadGroups
 * @package Unir\CloudBoxBundle\DataFixtures\ORM
 */
class LoadGroups extends AbstractFixture implements
    FixtureInterface,
    OrderedFixtureInterface,
    ContainerAwareInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS AND CONSTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Class order for load
     */
    const ORDER = 3;

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @var EntityManager
     */
    private $em;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * DEPENDENCY INJECTION
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Container injection
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return self::ORDER;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $this->em = $em;

        if ($this->isTestEnvironment()) {
            $this->createGroupsTestEnvironment();
        } elseif ($this->isDevEnvironment()) {
            $this->createGroupsDevEnvironment();
        }
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * creates groups in test environment
     */
    private function createGroupsTestEnvironment()
    {
        TestData::initialize();

        foreach (TestData::getGroups() as $group) {

            $this->changeGeneratorStrategy($group);

            if ($this->validate($group)) {

                //Sets group enterprise...
                $group->setEnterprise($this->getEnterpriseRepository()->find($group->getEnterprise()->getId()));

                $this->em->persist($group);
            }
        }

        $this->em->flush();
    }

    /**
     * creates groups in dev environment
     */
    private function createGroupsDevEnvironment()
    {
        $numGroupsPerEnterprise = 10;
        $factory = new Factory();
        $faker = $factory->create('es');
        $faker->addProvider(new LoremProvider($faker));
        $enterpriseRepo = $this->getEnterpriseRepository();

        //Retrieves all enterprises
        $allEnterprises = $enterpriseRepo->findAll();

        //foreach enterprise
        foreach ($allEnterprises as $enterprise) {

            $maxNum = rand(1, $numGroupsPerEnterprise);

            //we cycle till a random value with the max specified
            for ($i = 0; $i < $maxNum; $i++) {
                $group = new Group();
                $group
                    ->setName(implode(' ', $faker->words(3)))
                    ->setEnterprise($enterprise);

                if ($this->validate($group)) {
                    $this->em->persist($group);
                }
            }
        }

        $this->em->flush();
    }

    /**
     * Validates a group...
     * @param Group $group
     * @return bool
     */
    private function validate(Group $group)
    {
        $errors = $this->container->get('validator')->validate($group);

        if (count($errors) == 0) {
            return true;
        } else {
            $logger = $this->container->get('logger');
            $logger->error(implode(', ', $errors));
            return false;
        }
    }


    /**
     * Changes the generator strategy for store correctly the id
     * @param $entity
     */
    private function changeGeneratorStrategy($entity)
    {
        $metadata = $this->em->getClassMetadata(get_class($entity));
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
    }

    /**
     * Retrieves enterpriseRepository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getEnterpriseRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Enterprise');
    }

    /**
     * Checks if we are on test environment
     * @return bool
     */
    private function isTestEnvironment()
    {
        return 'test' == $this->getEnvironment();
    }

    /**
     * Check if we are on prod environment
     * @return bool
     */
    private function isProdEnvironment()
    {
        return 'prod' == $this->getEnvironment();
    }

    /**
     * Check if we are on dev environment
     * @return bool
     */
    private function isDevEnvironment()
    {
        return 'dev' == $this->getEnvironment();
    }

    /**
     * Return in what environment we are
     * @return mixed
     */
    private function getEnvironment()
    {
        return $this->container->get('kernel')->getEnvironment();
    }
}
