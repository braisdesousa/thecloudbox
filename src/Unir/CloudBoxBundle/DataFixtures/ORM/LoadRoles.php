<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 7/07/14
 * Time: 18:58
 */

namespace Unir\CloudBoxBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Doctrine\ORM\EntityManager;

/**
 * Class LoadRoles
 * @package Unir\CloudBoxBundle\DataFixtures\ORM
 */
class LoadRoles extends AbstractFixture implements
    FixtureInterface,
    OrderedFixtureInterface,
    ContainerAwareInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS AND CONSTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Class order for load
     */
    const ORDER = 1;

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @var EntityManager
     */
    private $em;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * DEPENDENCY INJECTION
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Container injection
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return self::ORDER;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $this->em = $em;

        if ($this->isTestEnvironment()) {
            $this->createRolesTestEnvironment();
        } else {
            $this->createRolesNonTestEnvironment(['ROLE_ADMIN' , 'ROLE_OWNER']);
        }
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Creates the roles in the test environment
     */
    private function createRolesTestEnvironment()
    {
        TestData::initialize();

        foreach (TestData::getRoles() as $role) {
            $this->changeGeneratorStrategy($role);

            if ($this->validate($role)) {
                $this->em->persist($role);
            }
        }

        $this->em->flush();
    }

    /**
     * Creates the roles in other environments
     */
    private function createRolesNonTestEnvironment(array $roleNames)
    {
        foreach ($roleNames as $roleName) {
            $role = new Role($roleName);

            if ($this->validate($role)) {
                $this->em->persist($role);
            }
        }

        $this->em->flush();
    }

    /**
     * Validates a role...
     * @param Role $role
     * @return bool
     */
    private function validate(Role $role)
    {
        $errors = $this->container->get('validator')->validate($role);

        if (count($errors) == 0) {
            return true;
        } else {
            $logger = $this->container->get('logger');
            $logger->error(implode(', ', $errors));
            return false;
        }
    }

    /**
     * Changes the generator strategy for store correctly the id
     * @param $entity
     */
    private function changeGeneratorStrategy($entity)
    {
        $metadata = $this->em->getClassMetadata(get_class($entity));
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
    }

    /**
     * Checks if we are on test environment
     * @return bool
     */
    private function isTestEnvironment()
    {
        return 'test' == $this->getEnvironment();
    }

    /**
     * Check if we are on prod environment
     * @return bool
     */
    private function isProdEnvironment()
    {
        return 'prod' == $this->getEnvironment();
    }

    /**
     * Check if we are on dev environment
     * @return bool
     */
    private function isDevEnvironment()
    {
        return 'dev' == $this->getEnvironment();
    }

    /**
     * Return in what environment we are
     * @return mixed
     */
    private function getEnvironment()
    {
        return $this->container->get('kernel')->getEnvironment();
    }
}
