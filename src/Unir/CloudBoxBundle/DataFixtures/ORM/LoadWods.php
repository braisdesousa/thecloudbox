<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 7/07/14
 * Time: 18:58
 */

namespace Unir\CloudBoxBundle\DataFixtures\ORM;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Helper\LoremProvider;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Doctrine\ORM\EntityManager;

/**
 * Class LoadWods
 * @package Unir\CloudBoxBundle\DataFixtures\ORM
 */
class LoadWods extends AbstractFixture implements
    FixtureInterface,
    OrderedFixtureInterface,
    ContainerAwareInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS AND CONSTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Class order for load
     */
    const ORDER = 7;

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @var EntityManager
     */
    private $em;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * DEPENDENCY INJECTION
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Container injection
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return self::ORDER;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $this->em = $em;

        if ($this->isTestEnvironment()) {
            $this->createWodsTestEnvironment();
        } elseif ($this->isDevEnvironment()) {
            $this->createWodsDevEnvironment();
        }
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Creates wods in test environment
     */
    private function createWodsTestEnvironment()
    {
        TestData::initialize();

        foreach (TestData::getWods() as $dataWod) {

            $wod = new Wod();

            $wod
                ->setId($dataWod->getId())
                ->setTitle($dataWod->getTitle())
                ->setDescription($dataWod->getDescription())
                ->setStatus($dataWod->getStatus())
                ->setDifficulty($dataWod->getDifficulty())
                ->setEstimationTime($dataWod->getEstimationTime())
                ->setWodGroup($this->getWodGroupsRepository()->find($dataWod->getWodGroup()->getId()))
                ->setCreationUser($this->getUsersRepository()->find($dataWod->getCreationUser()->getId()))
                ->setAvailableViewers($dataWod->getAvailableViewers());

            if ($dataWod->getDeleted()) {
                $wod->setDeleted();
            }

            //Not valid, not insert
            if (!$this->validate($wod)) {
                continue;
            }

            $this->changeGeneratorStrategy($wod);
            $this->em->persist($wod);
            $this->em->flush();




        }
    }

    /**
     * Creates wods in dev environment
     */
    private function createWodsDevEnvironment()
    {
        $factory = new Factory();
        $faker = $factory->create('es');
        $faker->addProvider(new LoremProvider($faker));
        $numMaxWodsPerWodGroup = 10;

        $enterprises = $this->getEnterprisesRepository()->findAll();

        foreach ($enterprises as $enterprise) {
            foreach ($enterprise->getWorkGroups() as $workGroup) {
                foreach ($workGroup->getWodGroups() as $wodGroup) {
                    $this->createRandomWods(
                        $numMaxWodsPerWodGroup,
                        $faker,
                        $enterprise,
                        $workGroup,
                        $wodGroup
                    );
                }
            }
        }

        $this->em->flush();
    }

    /**
     * creates random wods for simplicy...
     * @param $numMax
     * @param Generator $faker
     * @param Enterprise $enterprise
     * @param WorkGroup $workGroup
     * @param WodGroup $wodGroup
     */
    private function createRandomWods(
        $numMax,
        Generator $faker,
        Enterprise $enterprise,
        WorkGroup $workGroup,
        WodGroup $wodGroup
    ) {

        for ($i = 0; $i < rand(1, $numMax); $i++) {

            $wod = new Wod();

            $status = Wod::getStatuses()[array_rand(Wod::getStatuses())];
            $difficulty = Wod::getDifficulties()[array_rand(Wod::getDifficulties())];
            $viewOption = Wod::getViewOptions()[array_rand(Wod::getViewOptions())];
            $users=$enterprise->getUsers()->toArray();
            if (count($users)==0){
                throw new \Exception("Enteprise Without Users ".$enterprise->getName());
            }
            $users_array_rand_keys=array_rand($users,rand(1,count($users)-1));
            if (!is_array($users_array_rand_keys)){
                $users_array_rand_keys=[$users_array_rand_keys];
            }

            $wod
                ->setTitle($faker->words(2, true))
                ->setDescription($faker->text(1500))
                ->setStatus($status)
                ->setDifficulty($difficulty)
                ->setEstimationTime(rand(10, 1000))
                ->setWodGroup($wodGroup)
                ->setAvailableViewers($viewOption)
                ->setCreationUser($users[array_shift($users_array_rand_keys)]);


            //Not valid? continue
            if (!$this->validate($wod)) {
                continue;
            }
            $this->em->persist($wod);

            $users=$wodGroup->getUsers()->toArray();
            $groups=$wodGroup->getGroupMembers()->toArray();
            $rand= rand(1,count($users)-1);
            $rand_keys=array_rand($users,$rand);
            if (!is_array($rand_keys)){
                $rand_keys=[$rand_keys];
            }
            foreach ($rand_keys as $key){
                if (0==rand(0,2)){
                    $wod->addUsersFollower($users[$key]);
                } else {
                    $wod->addUsersAssigned($users[$key]);
                }
            }
            $this->em->persist($wod);

//            foreach($users_array_rand_keys as $user_key){
//                if(rand(0,1)==0){
//                    $wod->addUsersAssigned($users[$user_key]);
//                } else {
//                    $wod->addUsersFollower($user_key[$user_key]);
//                }
//            }
            $this->em->persist($wod);
            //Persists it
            $this->em->flush($wod);
        }
    }

    /**
     * Validates a Wod...
     * @param Wod $wod
     * @return bool
     */
    private function validate(Wod $wod)
    {
        $errors = $this->container->get('validator')->validate($wod);

        if (count($errors) == 0) {
            return true;
        } else {
            $logger = $this->container->get('logger');

            if (!is_array($errors)) {
                $errors = [$errors];
            }

            $logger->error(implode(', ', $errors));
            return false;
        }
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getWorkGroupsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:WorkGroup');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getWodGroupsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:WodGroup');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getUsersRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:User');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getGroupsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Group');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getEnterprisesRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Enterprise');
    }


    /**
     * Changes the generator strategy for store correctly the id
     * @param $entity
     */
    private function changeGeneratorStrategy($entity)
    {
        $metadata = $this->em->getClassMetadata(get_class($entity));
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
    }

    /**
     * Checks if we are on test environment
     * @return bool
     */
    private function isTestEnvironment()
    {
        return 'test' == $this->getEnvironment();
    }

    /**
     * Check if we are on prod environment
     * @return bool
     */
    private function isProdEnvironment()
    {
        return 'prod' == $this->getEnvironment();
    }

    /**
     * Check if we are on dev environment
     * @return bool
     */
    private function isDevEnvironment()
    {
        return 'dev' == $this->getEnvironment();
    }

    /**
     * Return in what environment we are
     * @return mixed
     */
    private function getEnvironment()
    {
        return $this->container->get('kernel')->getEnvironment();
    }
}
