<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 7/07/14
 * Time: 18:58
 */

namespace Unir\CloudBoxBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\SubWod;
use Unir\CloudBoxBundle\Helper\LoremProvider;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Doctrine\ORM\EntityManager;

/**
 * Class LoadRoles
 * @package Unir\CloudBoxBundle\DataFixtures\ORM
 */
class LoadSubWods extends AbstractFixture implements
    FixtureInterface,
    OrderedFixtureInterface,
    ContainerAwareInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS AND CONSTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Class order for load
     */
    const ORDER = 9;

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @var EntityManager
     */
    private $em;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * DEPENDENCY INJECTION
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Container injection
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return self::ORDER;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $this->em=$em;
        $factory = new Factory();
        $faker = $factory->create('es');
        $faker->addProvider(new LoremProvider($faker));
        foreach ($em->getRepository("UnirCloudBoxBundle:Wod")->findAll() as $wod)
        {
            $this->createSubWods($wod,$faker);
            $this->em->persist($wod);
        }
        $this->em->flush();
    }
    public function createSubWods($wod,Generator $faker)
    {
        $rand= rand(0,5);

        for($i=0;$i<$rand;$i++)
        {
            $subWod= new SubWod();
            $subWod->setWod($wod);
            $subWod->setTitle($faker->sentence(3));
            $this->em->persist($subWod);
        }

    }


}
