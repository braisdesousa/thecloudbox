<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 7/07/14
 * Time: 18:58
 */

namespace Unir\CloudBoxBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Helper\LoremProvider;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Doctrine\ORM\EntityManager;

/**
 * Class LoadWodComments
 * @package Unir\CloudBoxBundle\DataFixtures\ORM
 */
class LoadWodComments extends AbstractFixture implements
    FixtureInterface,
    OrderedFixtureInterface,
    ContainerAwareInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS AND CONSTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Class order for load
     */
    const ORDER = 8;

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @var EntityManager
     */
    private $em;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * DEPENDENCY INJECTION
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Container injection
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return self::ORDER;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $this->em = $em;

        if ($this->isTestEnvironment()) {
            $this->createWodCommentsTestEnvironment();
        } elseif ($this->isDevEnvironment()) {
            $this->createWodCommentsDevEnvironment();
        }

//        if($this->isDevEnvironment()){
//
//            $comments = [];
//
//            foreach($this->em->getRepository('UnirCloudBoxBundle:Wod')->findAll() as $wod){
//                $comments = array_merge($comments, $this->getFakeComents($wod));
//            }
//
//            $this->createWodComments($comments);
//
//        } elseif($this->isTestEnvironment()) {
//
//            if(!TestData::getIsInitialized()){
//                TestData::initialize();
//            }
//
//
//            $this->createWodComments(TestData::getWodComments());
//        }
//
//        $this->em->flush();
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Creates wodComments for test environment
     */
    private function createWodCommentsTestEnvironment()
    {
        TestData::initialize();

        foreach (TestData::getWodsComments() as $wodComment) {

            $wodComment->setUser($this->getUsersRepository()->find($wodComment->getUser()->getId()));
            $wodComment->setWod($this->getWodsRepository()->find($wodComment->getWod()->getId()));

            $this->changeGeneratorStrategy($wodComment);
            $this->em->persist($wodComment);
        }

        $this->em->flush();
    }

    /**
     * Create wodcomments on dev environment
     */
    private function createWodCommentsDevEnvironment()
    {
        $factory = new Factory();
        $faker = $factory->create('es');
        $faker->addProvider(new LoremProvider($faker));
        $numMaxCommentsPerWod = 10;

        //Extract all enterprises...
        $enterprises = $this->getEnterprisesRepository()->findAll();

        foreach ($enterprises as $enterprise) {

            $users = $enterprise->getUsers()->toArray();

            //No users, no comments
            if (count($users) == 0) {
                continue;
            }


            foreach ($enterprise->getWorkGroups() as $workGroup) {
                foreach ($workGroup->getWodGroups() as $wodGroup) {
                    foreach ($wodGroup->getWods() as $wod) {
                        $this->createRandomWodsComments(
                            $numMaxCommentsPerWod,
                            $faker,
                            $users,
                            $wod
                        );
                    }
                }
            }
        }
        $this->em->flush();
    }

    /**
     * creates a random wodsComments...
     * @param $numMax
     * @param Generator $faker
     * @param array $users
     * @param Wod $wod
     */
    private function createRandomWodsComments($numMax, Generator $faker, array $users, Wod $wod)
    {
        for ($i = 0; $i < rand(0, $numMax); $i++) {

            $randomUser = $users[array_rand($users)];

            $wodComment = new WodComments();


            $commentText = '<p>' . implode('</p><p>', $faker->paragraphs(100)) . '</p>';

            $wodComment
                ->setWod($wod)
                ->setUser($randomUser)
                ->setComment($commentText);

            $this->em->persist($wodComment);
        }
    }

//    /**
//     * Creates wods comments
//     * @param array $comments
//     */
//    private function createWodComments(array $comments)
//    {
//        foreach($comments as $comment){
//
//            if($comment instanceof WodComments){
//                $tc = new WodComments();
//
//                $dbUser = $this->em->getRepository("UnirCloudBoxBundle:User")->find($comment->getUser()->getId());
//                $dbWod = $this->em->getRepository("UnirCloudBoxBundle:Wod")->find($comment->getWod()->getId());
//
//                $tc->setId($comment->getId());
//                $tc->setComment($comment->getComment());
//                $tc->setUser($dbUser);
//                $tc->setWod($dbWod);
//
//            } else {
//                $tc = new WodComments();
//
//                $tc->setComment($comment["comment"])
//                    ->setUser($comment["user"])
//                    ->setWod($comment["wod"]);
//            }
//
//            $errors = $this->validator->validate($tc);
//            if (count($errors) > 0) {
//                $errorsString = (string) $errors;
//                $logger = $this->container->get('logger');
//                $logger->error($errorsString);
//            }
//            else{
//                $this->em->persist($tc);
//
//                if($this->isTestEnvironment()){
//                    $this->changeGeneratorStrategy($tc);
//                }
//            }
//
//        }
//
//    }
//
//    /**
//     * Generates 0 to $num comments for the wod
//     * @param $wod
//     * @param int $num
//     * @return array
//     */
//    private function getFakeComents($wod , $num = 6){
//
//        $factory = new Factory();
//        $faker = $factory->create("es");
//
//        $fakeComments = [];
//
//        for($i = 0; $i < rand(0,$num); $i++){
//            $fakeComments[] = [
//                'comment' => $faker->sentence("8"),
//                'wod' => $wod,
//                'user' => $this->getRandomUser($wod)
//            ];
//        }
//
//        return $fakeComments;
//    }
//
//    /**
//     * Gets a random user from the list of assigned users in a wod
//     * @param Wod $wod
//     * @return mixed
//     */
//    private function getRandomUser(Wod $wod)
//    {
//        $users = $wod->getUsersAssigned()->toArray();
//        return $users[array_rand($users)];
//    }

    /**
     * Validates a WodComments...
     * @param WodComments $wod
     * @return bool
     */
    private function validate(WodComments $wod)
    {
        $errors = $this->container->get('validator')->validate($wod);

        if (count($errors) == 0) {
            return true;
        } else {
            $logger = $this->container->get('logger');

            if (!is_array($errors)) {
                $errors = [$errors];
            }

            $logger->error(implode(', ', $errors));
            return false;
        }
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getWodsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Wod');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getWorkGroupsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:WorkGroup');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getWodGroupsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:WodGroup');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getUsersRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:User');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getGroupsRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Group');
    }

    /**
     * Retrieves Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getEnterprisesRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Enterprise');
    }


    /**
     * Changes the generator strategy for store correctly the id
     * @param $entity
     */
    private function changeGeneratorStrategy($entity)
    {
        $metadata = $this->em->getClassMetadata(get_class($entity));
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
    }

    /**
     * Checks if we are on test environment
     * @return bool
     */
    private function isTestEnvironment()
    {
        return 'test' == $this->getEnvironment();
    }

    /**
     * Check if we are on prod environment
     * @return bool
     */
    private function isProdEnvironment()
    {
        return 'prod' == $this->getEnvironment();
    }

    /**
     * Check if we are on dev environment
     * @return bool
     */
    private function isDevEnvironment()
    {
        return 'dev' == $this->getEnvironment();
    }

    /**
     * Return in what environment we are
     * @return mixed
     */
    private function getEnvironment()
    {
        return $this->container->get('kernel')->getEnvironment();
    }
}
