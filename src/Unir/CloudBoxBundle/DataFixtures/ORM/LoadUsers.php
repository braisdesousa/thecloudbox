<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 7/07/14
 * Time: 18:58
 */

namespace Unir\CloudBoxBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\es_ES\Address;
use Faker\Provider\es_ES\Internet;
use Faker\Provider\es_ES\Payment;
use Faker\Provider\es_ES\Person;
use Faker\Provider\es_ES\PhoneNumber;
use FOS\UserBundle\Model\UserInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Doctrine\ORM\EntityManager;

class LoadUsers extends AbstractFixture implements
    FixtureInterface,
    OrderedFixtureInterface,
    ContainerAwareInterface
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS AND CONSTANTS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Class order for load
     */
    const ORDER = 4;

    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @var EntityManager
     */
    private $em;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * DEPENDENCY INJECTION
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Container injection
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * IMPLEMENTATIONS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return self::ORDER;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $this->em = $em;

        if ($this->isTestEnvironment()) {
            $this->createUsersTestEnvironment();
        } else {

            $roles = $this->getRoleRepository()->findAll();

            $users = [[
                'userName'   => 'admin',
                'password'   => 'admin',
                'email'      => 'admin@unir.es',
                'enabled'    => 'true',
                'roles'      => ['ROLE_OWNER'],
            ]];

            //If is test environment
            if ($this->isDevEnvironment()) {

                $fakeUsers = 50;


                    $users = array_merge(
                        $users,
                        $this->getFakeUsers(
                            $fakeUsers, $roles
                        )
                    );

            }

            //Create users...
            $this->createUsersNonTestEnvironment($users);
        }
        $this->associateUsersWithEnterprises();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    private function associateUsersWithEnterprises()
    {
        $users=$this->em->getRepository("UnirCloudBoxBundle:User")->findAll();
        $enterprises=$this->em->getRepository("UnirCloudBoxBundle:Enterprise")->findAll();
        foreach ($enterprises as $enterprise) {
            $rand= rand(1,(count($users)-1));
            $rands=array_rand($users,$rand);
            if (!is_array($rands)){
                $rands=[$rands];
            }
            foreach ($rands as $user_key){
                $is_admin=(2==rand(0,5));
                if ($is_admin) {
                    $enterprise->addAdminUser($users[$user_key]);
                }
                $enterprise->addUser($users[$user_key]);
            }
            $this->em->persist($enterprise);
        }
        $this->em->flush();
    }
    /**
     * Creates the users that are needed in testEnvironment
     */
    private function createUsersTestEnvironment()
    {
        TestData::initialize();

        foreach (TestData::getUsers() as $newUser) {

            $user = new User();

            $user
                ->setId($newUser->getId())
                ->setUsername($newUser->getUsername())
                ->setPlainPassword($newUser->getPlainPassword())
                ->setEmail($newUser->getEmail())
                ->setEnabled($newUser->isEnabled());



            $this->changeGeneratorStrategy($user);

            //If is not valid, continue...
            if (!$this->validate($user)) {
                continue;
            }

            $this->em->persist($user);
            $this->em->flush();

            foreach ($newUser->getRoles() as $role) {
                $user->addRole($this->getRoleRepository()->findOneBy(['role' => $role]));
            }

            $this->em->flush();
        }
    }

    /**
     * crates users in non test environment
     * @param array $users
     */
    private function createUsersNonTestEnvironment(array $users)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        foreach ($users as $newUser) {

            $newUser = $this->transform($newUser);

            $user = $userManager->createUser();

            $user
                ->setUsername($newUser->getUsername())
                ->setPlainPassword($newUser->getPlainPassword())
                ->setEmail($newUser->getEmail())
                ->setEnabled(true);

            foreach ($newUser->getRoles() as $role) {
                $user->addRole($this->getRoleRepository()->findOneBy(['role' => $role]));
            }

            if ($this->validate($user)) {
                $userManager->updateUser($user);
            }
        }
    }

    /**
     * Retrieves some fake users
     * @param $number
     * @param array $allRoles
     * @return array
     */
    private function getFakeUsers($number, array $allRoles)
    {
        $fakeUsers = [];
        $factory= new Factory();

        $faker = $factory->create("es");
        $faker->addProvider(new Person($faker));
        $faker->addProvider(new Internet($faker));
        $faker->addProvider(new Address($faker));
        $faker->addProvider(new Payment($faker));
        $faker->addProvider(new PhoneNumber($faker));
        for ($i = 0; $i < $number; $i++) {

            $rolesIds = array_rand($allRoles, rand(1, count($allRoles)));

            $roles = [];


            if (is_array($rolesIds)) {

                if (rand(1, 2) == 1) {
                    array_pop($rolesIds);
                }

                foreach ($rolesIds as $roleId) {
                    $roles[] = $allRoles[$roleId]->getRole();
                }
            } elseif (is_numeric($rolesIds)) {
                $roles[] = $allRoles[$rolesIds]->getRole();
            }

            $fakeUsers[] = array(
                'userName'   => $faker->name,
                'password'   => '1234',
                'email'      => $faker->word."_".$faker->email,
                'enabled'    => 'true',
                'roles'      => [],
            );
        }

        return $fakeUsers;
    }

    /**
     * Transform userArray into user Object
     * @param array $userArr
     * @return User
     */
    private function transform(array $userArr)
    {
        $user = new User();
        $user
            ->setUsername($userArr['userName'])
            ->setPlainPassword($userArr['password'])
            ->setEmail($userArr['email']);

        foreach ($userArr['roles'] as $role) {
            $user->addRole($this->getRoleRepository()->findOneBy(['role' => $role]));
        }

        return $user;
    }

    /**
     * Validates an user...
     * @param User $user
     * @return bool
     */
    private function validate(User $user)
    {
        $errors = $this->container->get('validator')->validate($user);

        if (count($errors) == 0) {
            return true;
        } else {
            $logger = $this->container->get('logger');
            $logger->error(implode(', ', $errors));
            return false;
        }
    }

    /**
     * Retrieves Roles Repository
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getRoleRepository()
    {
        return $this->em->getRepository('UnirCloudBoxBundle:Role');
    }


    /**
     * Changes the generator strategy for store correctly the id
     * @param $entity
     */
    private function changeGeneratorStrategy($entity)
    {
        $metadata = $this->em->getClassMetadata(get_class($entity));
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
    }

    /**
     * Checks if we are on test environment
     * @return bool
     */
    private function isTestEnvironment()
    {
        return "test" == $this->getEnvironment();
    }

    /**
     * Check if we are on prod environment
     * @return bool
     */
    private function isProdEnvironment()
    {
        return "prod" == $this->getEnvironment();
    }

    /**
     * Check if we are on dev environment
     * @return bool
     */
    private function isDevEnvironment()
    {
        return "dev" == $this->getEnvironment();
    }

    /**
     * Return in what environment we are
     * @return mixed
     */
    private function getEnvironment()
    {
        return $this->container->get("kernel")->getEnvironment();
    }
}
