<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 5/08/14
 * Time: 10:50
 */

namespace Unir\CloudBoxBundle\Tests\Listener;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Listener\UpdateEntityListener;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\ContainerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Entity\User;


/**
 * Tests a listener
 * Class UpdateEntityListenerTest
 * @package Unir\CloudBoxBundle\Tests\Listener
 */
class UpdateEntityListenerTest extends BaseTestService
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /**
     * tests that if nothing changed therer nothing happens
     * @test
     */
    public function testOnFlushWithNoChanges()
    {
        //Arrage

        $containerMock = new ContainerMock();
        $emMock = new EntityManagerMock();

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertEmpty($emMock->getPersistedEntities());
        $this->assertEmpty($emMock->getRemovedEntities());
    }

    /**
     * tests that nothing happens if an entity distinct than a wod has changed
     * @test
     */
    public function testOnFlushWithEntityDistinctAsAWod()
    {
        //Arrage

        $containerMock = new ContainerMock();
        $emMock = new EntityManagerMock();

        $emMock->getUnitOfWork()->markForUpdate(new User(),[
            'username' => [ 0 => 'Pepe', 1 => 'Juan' ]
        ]);

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertEmpty($emMock->getPersistedEntities());
        $this->assertEmpty($emMock->getRemovedEntities());
    }

    /**
     * Tests on flush method when a wod title has changed
     */
    public function testOnFlushWhenWodTitleChanged()
    {
        //Arrage
        $formatPhrase = 'changed the title: %s -> %s';
        $userUserName = 'pepito';
        $oldTitle = 'oldTitle';
        $newTitle = 'newTitle';

        $expectedDescription = sprintf($formatPhrase, $oldTitle, $newTitle);

        $user = new User();
        $user->setUsername($userUserName);

        $wod = new Wod();

        $emMock = new EntityManagerMock();

        $emMock->getUnitOfWork()->markForUpdate($wod,[
            'title' =>[0 => $oldTitle, 1 => $newTitle]
        ]);

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($user)
        ]);

        $containerMock->enterScope("request");

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());

        $wodActivity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $wodActivity->getDescription());
        $this->assertEquals($wod, $wodActivity->getWod());
        $this->assertEquals($user, $wodActivity->getUser());
    }

    /**
     * Tests on flush method when a wod title has changed
     */
    public function testOnFlushWhenWodTitleChangedAndThereIsNoUserLogged()
    {
        //Arrage
        $formatPhrase = 'changed the title: %s -> %s';
        $userUserName = 'pepito';
        $oldTitle = 'oldTitle';
        $newTitle = 'newTitle';

        $expectedDescription = sprintf($formatPhrase, $oldTitle, $newTitle);

        $user = new User();
        $user->setUsername($userUserName);

        $wod = new Wod();
        $wod->setCreationUser($user);

        $emMock = new EntityManagerMock();

        $emMock->getUnitOfWork()->markForUpdate($wod,[
            'title' =>[0 => $oldTitle, 1 => $newTitle]
        ]);

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock(null, false)
        ]);

        $containerMock->enterScope("request");

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());

        $wodActivity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $wodActivity->getDescription());
        $this->assertEquals($wod, $wodActivity->getWod());
        $this->assertEquals($user, $wodActivity->getUser());
    }

    /**
     * Tests on flush method when a wod description has changed
     */
    public function testOnFlushWhenWodDescriptionChanged()
    {
        //Arrage

        $userUserName = 'pepito';
        $oldOne = 'old description';
        $newOne = 'new description';

        $expectedDescription = "The description had changed";

        $user = new User();
        $user->setUsername($userUserName);

        $wod = new Wod();

        $emMock = new EntityManagerMock();

        $emMock->getUnitOfWork()->markForUpdate($wod, [
            'description' => [0 => $oldOne, 1 => $newOne]
        ]);

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($user)
        ]);

        $containerMock->enterScope("request");

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());

        $wodActivity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $wodActivity->getDescription());
        $this->assertEquals($wod, $wodActivity->getWod());
        $this->assertEquals($user, $wodActivity->getUser());
    }

    /**
     * Tests on flush method when a wod status has changed
     */
    public function testOnFlushWhenWodStatusChanged()
    {
        //Arrage
        $formatPhrase = 'changed the status: %s -> %s';
        $userUserName = 'pepito';
        $oldOne = Wod::STATUS_OPEN;
        $newOne = Wod::STATUS_INPROGRESS;

        $expectedDescription = sprintf($formatPhrase, $oldOne, $newOne);

        $user = new User();
        $user->setUsername($userUserName);

        $wod = new Wod();

        $emMock = new EntityManagerMock();

        $emMock->getUnitOfWork()->markForUpdate($wod,[
            'status' =>[0 => $oldOne, 1 => $newOne]
        ]);

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($user)
        ]);

        $containerMock->enterScope("request");

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());

        $wodActivity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $wodActivity->getDescription());
        $this->assertEquals($wod, $wodActivity->getWod());
        $this->assertEquals($user, $wodActivity->getUser());
    }

    /**
     * Tests on flush method when a wod difficulty has changed
     */
    public function testOnFlushWhenWodDifficultyChanged()
    {
        //Arrage
        $formatPhrase = 'changed the difficulty: %s -> %s';
        $userUserName = 'pepito';
        $oldOne = Wod::DIFFICULTY_CRITICAL;
        $newOne = Wod::DIFFICULTY_LOW;

        $expectedDescription = sprintf($formatPhrase, $oldOne, $newOne);

        $user = new User();
        $user->setUsername($userUserName);

        $wod = new Wod();

        $emMock = new EntityManagerMock();

        $emMock->getUnitOfWork()->markForUpdate($wod,[
            'difficulty' =>[0 => $oldOne, 1 => $newOne]
        ]);

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($user)
        ]);

        $containerMock->enterScope("request");

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());

        $wodActivity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $wodActivity->getDescription());
        $this->assertEquals($wod, $wodActivity->getWod());
        $this->assertEquals($user, $wodActivity->getUser());
    }

    /**
     * tests than multiple changes on an entity must make multiple activities
     * @test
     */
    public function testOnFlushMultipleChangesMultipleActivities()
    {
        //Arrage
        $user = new User();
        $user->setUsername('pepit');

        $wod = new Wod();

        $emMock = new EntityManagerMock();

        $entityUpdates = [
            'title' =>          [0 => 'oldTitle', 1 => 'new Title'],
            'description' =>    [0 => 'old Description', 1 => 'new Description'],
            'difficulty' =>       [0 => Wod::DIFFICULTY_LOW, 1 => Wod::DIFFICULTY_CRITICAL],
        ];

        $emMock->getUnitOfWork()->markForUpdate($wod,$entityUpdates);

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($user)
        ]);

        $containerMock->enterScope("request");

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());
        $this->assertEquals(count($entityUpdates), count($emMock->getPersistedEntities()));
    }

    /**
     * tests than a flush when the collection is not from a wod makes nothing...
     * @test
     */
    public function testOnFlushCollectionUpdateThatIsNotAWod()
    {
        //Arrage

        $prj1 = new WorkGroup();
        $prj2 = new WorkGroup();
        $prj3 = new WorkGroup();
        $prj4 = new WorkGroup();
        $prj5 = new WorkGroup();

        $prj1->setId(1);
        $prj2->setId(2);
        $prj3->setId(3);
        $prj4->setId(4);
        $prj5->setId(5);


        $insertedWorkGroups = [$prj1, $prj2, $prj3];
        $deletedWorkGroups = [$prj4, $prj5];

        $emMock = new EntityManagerMock();
        $emMock->getUnitOfWork()->markCollectionUpdate(new User(),"workGroups", $insertedWorkGroups, $deletedWorkGroups);

        $containerMock = new ContainerMock();

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertEmpty($emMock->getPersistedEntities());
    }

    /**
     * test onFlush with userAssigedInserteds
     * @test
     */
    public function testOnFlushCollectionUpdateWhenUsersAssignedAreAdded()
    {
        //Arrage
        $usr1 = new User();
        $usr2 = new User();
        $usr3 = new User();

        $usr1->setUsername('pepito1');
        $usr2->setUsername('pepito2');
        $usr3->setUsername('pepito3');



        $userOwner = new User();
        $userOwner->setUsername('pepit');

        $expectedDescription = 'Added 3 Users to AssignedUsers';

        $wod = new Wod();

        $emMock = new EntityManagerMock();
        $emMock->getUnitOfWork()->markCollectionUpdate($wod,'usersAssigned', [$usr1, $usr2, $usr3] );


        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($userOwner)
        ]);

        $containerMock->enterScope("request");


        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());

        $activity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $activity->getDescription());
    }


    /**
     * test onFlush with userAssiged deleteds
     * @test
     */
    public function testOnFlushCollectionUpdateWhenUsersAssignedAreRemoved()
    {
        //Arrage
        $usr1 = new User();
        $usr2 = new User();
        $usr3 = new User();

        $usr1->setUsername('pepito1');
        $usr2->setUsername('pepito2');
        $usr3->setUsername('pepito3');



        $userOwner = new User();
        $userOwner->setUsername('pepit');

        $expectedDescription = 'Removed 3 Users from AssignedUsers';

        $wod = new Wod();

        $emMock = new EntityManagerMock();
        $emMock->getUnitOfWork()->markCollectionUpdate($wod,'usersAssigned',array(),  [$usr1, $usr2, $usr3] );


        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($userOwner)
        ]);

        $containerMock->enterScope("request");


        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());

        $activity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $activity->getDescription());
    }

    /**
     * test onFlush with userFollowers added
     * @test
     */
    public function testOnFlushCollectionUpdateWhenUsersFollowersAreAdded()
    {
        //Arrage
        $usr1 = new User();
        $usr2 = new User();
        $usr3 = new User();

        $usr1->setUsername('pepito1');
        $usr2->setUsername('pepito2');
        $usr3->setUsername('pepito3');



        $userOwner = new User();
        $userOwner->setUsername('pepit');

        $expectedDescription = "Added 3 Users to FollowerUsers";

        $wod = new Wod();

        $emMock = new EntityManagerMock();
        $emMock->getUnitOfWork()->markCollectionUpdate($wod,'usersFollower', [$usr1, $usr2, $usr3] );


        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($userOwner)
        ]);

        $containerMock->enterScope("request");


        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());

        $activity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $activity->getDescription());
    }


    /**
     * test onFlush with usersFollowers are  deleteds
     * @test
     */
    public function testOnFlushCollectionUpdateWhenUsersFollowersAreRemoved()
    {
        //Arrage
        $usr1 = new User();
        $usr2 = new User();
        $usr3 = new User();

        $usr1->setUsername('pepito1');
        $usr2->setUsername('pepito2');
        $usr3->setUsername('pepito3');



        $userOwner = new User();
        $userOwner->setUsername('pepit');

        $expectedDescription = 'Removed 3 Users from FollowerUsers';

        $wod = new Wod();

        $emMock = new EntityManagerMock();
        $emMock->getUnitOfWork()->markCollectionUpdate($wod,'usersFollower',array(),  [$usr1, $usr2, $usr3] );


        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($userOwner)
        ]);

        $containerMock->enterScope("request");


        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());

        $activity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $activity->getDescription());
    }


    /**
     * test onFlush with usersFollowers are  deleteds
     * @test
     */
    public function testOnFlushCollectionUpdateWhenBothCollectionsAreChanged()
    {
        //Arrage
        $usr1 = new User();
        $usr2 = new User();
        $usr3 = new User();
        $usr4 = new User();
        $usr5 = new User();
        $usr6 = new User();

        $usr1->setUsername('pepito1');
        $usr2->setUsername('pepito2');
        $usr3->setUsername('pepito3');

        $usr4->setUsername('pepito4');
        $usr5->setUsername('pepito5');
        $usr6->setUsername('pepito6');



        $userOwner = new User();
        $userOwner->setUsername('pepit');

        $wod = new Wod();

        $emMock = new EntityManagerMock();
        $emMock->getUnitOfWork()->markCollectionUpdate($wod,'usersAssigned', [$usr1, $usr2, $usr3], [$usr4, $usr5, $usr6] );
        $emMock->getUnitOfWork()->markCollectionUpdate($wod,'usersFollower', [$usr1, $usr2, $usr3], [$usr4, $usr5, $usr6] );


        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($userOwner)
        ]);

        $containerMock->enterScope("request");


        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $this->assertNotEmpty($emMock->getPersistedEntities());
        $this->assertEquals(4, count($emMock->getPersistedEntities()));
    }

    /**
     * tets onFlush when a collection is deleted (usersAssigned)
     * @test
     */
    public function onFlushWhenCollectionDeletedWithUserAssigneds()
    {
        //Arrage
        $expectedDescription = 'Removed All Assigned Users';


        $userOwner = new User();
        $userOwner->setUsername('pepit');

        $wod = new Wod();

        $emMock = new EntityManagerMock();
        $emMock->getUnitOfWork()->markCollectionDelete($wod, 'usersAssigned');

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($userOwner)
        ]);

        $containerMock->enterScope("request");

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $activity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $activity->getDescription());
    }

    /**
     * tets onFlush when a collection is deleted (usersFollower)
     * @test
     */
    public function onFlushWhenCollectionDeletedWithUserFollower()
    {
        //Arrage
        $expectedDescription = 'Removed All Follower Users';


        $userOwner = new User();
        $userOwner->setUsername('pepit');

        $wod = new Wod();

        $emMock = new EntityManagerMock();
        $emMock->getUnitOfWork()->markCollectionDelete($wod, 'usersFollower');

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($userOwner)
        ]);

        $containerMock->enterScope("request");

        //Act
        $listener = new UpdateEntityListener($containerMock);
        $listener->onFlush(new OnFlushEventArgs($emMock));

        //Assert
        $activity = $emMock->getPersistedEntities()[0];
        $this->assertEquals($expectedDescription, $activity->getDescription());
    }


}
 