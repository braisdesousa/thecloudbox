<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 21/08/14
 * Time: 16:44
 */

namespace Unir\CloudBoxBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\LoginLog;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Group;

/**
 * Class GroupTest
 * @package Unir\CloudBoxBundle\Tests\Entity
 */
class GroupTest extends \PHPUnit_Framework_TestCase
{

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Test Constructor
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testConstructor1()
    {
        //Arrage

        //Act
        $item = new Group();

        //Assert
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $item->getUsers());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Getters and Setters
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testGetterAndSetters1()
    {
        //Arrage
        $id = 123;
        $name = 'asdasd';

        //Act
        $item = new Group();
        $item
            ->setId($id)
            ->setName($name);

        //Assert
        $this->assertEquals($id, $item->getId());
        $this->assertEquals($name, $item->getName());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  public function getUsers()
     *  public function addUser(User $user)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testAddUser1()
    {
        //Arrage
        $expected = new User();


        $item = new Group();
        //Act

        $item->addUser($expected);
        $result = $item->getUsers();

        //ASSERT
        $this->assertEquals(1, $result->count());
        $this->assertEquals($expected, $result->get(0));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function removeUser(User $user)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testRemoveUser1()
    {
        //Arrage
        $expected = new User();
        $toDelete = new User();

        $item = new Group();
        $item->addUser($expected)->addUser($toDelete);

        //Act
        $result = $item->removeUser($toDelete)->getUsers();

        //ASSERT
        $this->assertEquals(1, $result->count());
        $this->assertEquals($expected, $result->get(0));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function setUsers($users)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetUsers1()
    {
        //Arrage
        $expected = new User();
        $toDelete = new User();

        $item = new Group();
        $col = new ArrayCollection([$expected, $toDelete]);

        //Act
        $result = $item->setUsers($col)->getUsers();

        //ASSERT
        $this->assertEquals(2, $result->count());
        $this->assertEquals($expected, $result->get(0));
        $this->assertEquals($toDelete, $result->get(1));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function getEnterprise()
     * public function setEnterprise($enterprise)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetEnterprise1()
    {
        //Arrage
        $expected = new Enterprise();

        $item = new Group();

        //Act
        $result = $item->setEnterprise($expected)->getEnterprise();

        //ASSERT
        $this->assertEquals($expected, $result);
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function addRole($role)
     * public function hasRole($role)
     * public function getRoles()
     * public function removeRole($role)
     * public function setRoles(array $roles)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testRole1()
    {
        //Methods don't do nothing
        $group = new Group();
        $group->addRole(new Role());
        $group->hasRole(new Role());
        $group->getRoles();
        $group->removeRole(new Role());
        $group->setRoles([new Role(), new Role()]);
    }
}
