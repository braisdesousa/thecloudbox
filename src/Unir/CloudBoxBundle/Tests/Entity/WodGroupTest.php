<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 21/08/14
 * Time: 17:42
 */

namespace Unir\CloudBoxBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\LoginLog;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Group;

/**
 * Class WodGroupTest
 * @package Unir\CloudBoxBundle\Tests\Entity
 */
class WodGroupTest extends \PHPUnit_Framework_TestCase
{

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Test Constructor
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testConstructor1()
    {
        //Arrage

        //Act
        $item = new WodGroup();

        //Assert
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $item->getWods());
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $item->getGroupMembers());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Getters and Setters
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testGetterAndSetters1()
    {
        //Arrage
        $id = 123;
        $name = 'asdasd';
        $description = 'asdadqeqweqdasdasdqe';
        $workGroup = new WorkGroup();
        $wods = new ArrayCollection([new Wod(), new Wod()]);

        //Act
        $item = new WodGroup();
        $item
            ->setId($id)
            ->setName($name)
            ->setDescription($description)
            ->setWorkGroup($workGroup)
            ->setWods($wods);

        //Assert
        $this->assertEquals($id, $item->getId());
        $this->assertEquals($name, $item->getName());
        $this->assertEquals($description, $item->getDescription());
        $this->assertEquals($workGroup, $item->getWorkGroup());
        $this->assertEquals($wods, $item->getWods());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function getGroupMembers()
     * public function addGroupMember(Group $group)
     * public function removeGroupMember(Group $group)
     * public function setGroupMembers($groupMembers)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testGroupMembers1()
    {
        //Arrage
        $expected1 = new Group();
        $expected2 = new Group();

        $item = new WodGroup();
        //Act

        $result = $item
            ->setGroupMembers(new ArrayCollection([$expected1, $expected2]))
            ->removeGroupMember($expected2)
            ->addGroupMember($expected2)
            ->getGroupMembers();


        //Assert
        $this->assertEquals(2, $result->count());
        $this->assertEquals($expected1, $result->get(0));
        $this->assertEquals($expected2, $result->get(2));
    }

}
