<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 21/08/14
 * Time: 17:12
 */

namespace Unir\CloudBoxBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\LoginLog;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Group;

/**
 * Class BaseEntityTest
 * @package Unir\CloudBoxBundle\Tests\Entity
 */
class BaseEntityTest extends \PHPUnit_Framework_TestCase
{

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * TEST getters and setters
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testGettersAndSetters1()
    {
        //Arrage

        //Act
        $item = new Enterprise();
        $item
            ->setCreated()
            ->setUpdated()
            ->setDeleted();

        //Assert
        $this->assertInstanceOf('\DateTime', $item->getCreated());
        $this->assertInstanceOf('\DateTime', $item->getUpdated());
        $this->assertInstanceOf('\DateTime', $item->getDeleted());
    }
} 