<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 7/08/14
 * Time: 8:22
 */

namespace Unir\CloudBoxBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\LoginLog;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Group;

/**
 * Class intended for test {@link Unir\CloudBoxBundle\Entity\User} and
 * his base {@link Unir\CloudBoxBundle\Model\User}
 * Class UserTest
 * @package Unir\CloudBoxBundle\Tests\Entity
 */
class UserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * tests that constructor must set default params
     * @test
     */
    public function testConstructor()
    {
        //Arrage

        //Act
        $user = new User();


        //Assert
        $this->assertNotNull($user->getSalt());
        $this->assertFalse($user->isEnabled());
        $this->assertFalse($user->isLocked());
        $this->assertFalse($user->isExpired());
        $this->assertFalse($user->isCredentialsExpired());
        $this->assertTrue($user->isCredentialsNonExpired());
    }

    /**
     * tests user serialize method
     * @test
     */
    public function testSerialize()
    {
        //Arrage
        $user = new User();

        $user
            ->setPassword('myBigPassword')
            ->setUsernameCanonical('usernameCanonical')
            ->setUsername('username')
            ->setExpired(false)
            ->setLocked(true)
            ->setCredentialsExpired(false)
            ->setEnabled(true)
            ->setId(123);

        $expected = serialize([
            $user->getPassword(),
            $user->getSalt(),
            $user->getUsernameCanonical(),
            $user->getUsername(),
            $user->getExpired(),
            $user->isLocked(),
            $user->isCredentialsExpired(),
            $user->isEnabled(),
            $user->getId()
        ]);

        //Act

        //Assert

        $this->assertEquals($expected, $user->serialize());
    }

    /**
     * tests that unserialize works propertly
     * @test
     */
    public function testUnserialize()
    {
        //Arrage
        $user = new User();

        $user
            ->setPassword('myBigPassword')
            ->setUsernameCanonical('usernameCanonical')
            ->setUsername('username')
            ->setExpired(false)
            ->setLocked(true)
            ->setCredentialsExpired(false)
            ->setEnabled(true)
            ->setId(123);

        //Act
        $newUser = new User();
        $newUser->unserialize($user->serialize());

        //Assert
        $this->assertEquals($user, $newUser);
    }

    /**
     * test getters and setters
     * @test
     */
    public function testGettersAndSetters()
    {
        //Arrage
        $user = new User();

        $id = 10;
        $username = 'fuyou';
        $usernameCanonical = 'fuyouInCanonical';
        $email = 'fuyu@iamfuyed.com';
        $emailCanonical = 'fuyu@iamfuyedInCanonical.com';
        $password = 'asdfgh';
        $plainPassword = 'asdfgPlain';
        $lastLogin = new \DateTime('now');
        $confirmationToken = 'iamConfirmed';
        $enterprise = new Enterprise();
        $assignedWods = new ArrayCollection([new Wod(), new Wod()]);
        $followedWods = new ArrayCollection([new Wod(), new Wod()]);
        $createdWods = new ArrayCollection([new Wod(), new Wod()]);
        $wodActivities = new ArrayCollection([new WodActivity(), new WodActivity()]);
        $wodComments = new ArrayCollection([new WodComments(), new WodComments()]);
        $logins = new ArrayCollection([new LoginLog(), new LoginLog()]);

        $user
            ->setId($id)
            ->setUsername($username)
            ->setUsernameCanonical($usernameCanonical)
            ->setEmail($email)
            ->setEmailCanonical($emailCanonical)
            ->setPassword($password)
            ->setPlainPassword($plainPassword)
            ->setLastLogin($lastLogin)
            ->setConfirmationToken($confirmationToken)
            ->setAssignedWods($assignedWods)
            ->setFollowedWods($followedWods)
            ->setCreatedWods($createdWods)
            ->setWodActivities($wodActivities)
            ->setWodComments($wodComments)
            ->setLogins($logins);

        //Act

        //Assert
        $this->assertEquals($id, $user->getId());
        $this->assertEquals($username, $user->getUsername());
        $this->assertEquals($usernameCanonical, $user->getUsernameCanonical());
        $this->assertEquals($email, $user->getEmail());
        $this->assertEquals($emailCanonical, $user->getEmailCanonical());
        $this->assertEquals($password, $user->getPassword());
        $this->assertEquals($plainPassword, $user->getPlainPassword());
        $this->assertEquals($lastLogin, $user->getLastLogin());
        $this->assertEquals($confirmationToken, $user->getConfirmationToken());
        $this->assertEquals($assignedWods, $user->getAssignedWods());
        $this->assertEquals($followedWods, $user->getFollowedWods());
        $this->assertEquals($createdWods, $user->getCreatedWods());
        $this->assertEquals($wodActivities, $user->getWodActivities());
        $this->assertEquals($wodComments, $user->getWodComments());
        $this->assertEquals($logins, $user->getLogins());




    }

    /**
     * tests that erasecredentials clears plainPassword
     * @test
     */
    public function testEraseCredentials()
    {
        //Arrage

        $user = new User();
        $user->setPlainPassword('plainPassword');

        //Act
        $user->eraseCredentials();

        //Assert
        $this->assertNull($user->getPlainPassword());
    }

    /**
     * tests than the account is or not expired
     * @test
     */
    public function testAccountNonExpiredSuccess()
    {
        //Arrage
        $user = new User();
        $user->setExpired(false);

        $dt = new \DateTime();
        $dt->setTimestamp(time() + (60 * 60 * 24));
        $user->setExpiresAt($dt);

        //Act
        $result = $user->isAccountNonExpired();

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests than the account is or not expired
     * @test
     */
    public function testAccountNonExpiredUnsuccess1()
    {
        //Arrage
        $user = new User();
        $user->setExpired(true);

        $dt = new \DateTime();
        $dt->setTimestamp(time() + (60 * 60 * 24));
        $user->setExpiresAt($dt);

        //Act
        $result = $user->isAccountNonExpired();

        //Assert
        $this->assertFalse($result);
    }

    /**
     * tests than the account is or not expired
     * @test
     */
    public function testAccountNonExpiredUnsuccess2()
    {
        //Arrage
        $user = new User();
        $user->setExpired(false);

        $dt = new \DateTime();
        $dt->setTimestamp(time() - (60 * 60));
        $user->setExpiresAt($dt);

        //Act
        $result = $user->isAccountNonExpired();

        //Assert
        $this->assertFalse($result);
    }

    /**
     * tests if credentials have been expired
     * @test
     */
    public function testIsCredentialsNonExpiredSuccess()
    {
        //Arrage
        $user = new User();
        $user->setCredentialsExpired(false);

        $dt = new \DateTime();
        $dt->setTimestamp(time() + (60 * 60));
        $user->setCredentialsExpireAt($dt);

        //Act
        $result = $user->isCredentialsExpired();

        //Assert
        $this->assertFalse($result);
    }

    /**
     * tests if credentials have been expired
     * @test
     */
    public function testIsCredentialsNonExpiredUnsuccess1()
    {
        //Arrage
        $user = new User();
        $user->setCredentialsExpired(true);

        $dt = new \DateTime();
        $dt->setTimestamp(time() + (60 * 60));
        $user->setCredentialsExpireAt($dt);

        //Act
        $result = $user->isCredentialsExpired();

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests if credentials have been expired
     * @test
     */
    public function testIsCredentialsNonExpiredUnsuccess2()
    {
        //Arrage
        $user = new User();
        $user->setCredentialsExpired(false);

        $dt = new \DateTime();
        $dt->setTimestamp(time() - (60 * 60));
        $user->setCredentialsExpireAt($dt);

        //Act
        $result = $user->isCredentialsExpired();

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests than isPasswordRequestNonExpired time have non expired work correctly
     * @test
     */
    public function testIsPasswordRequestNonExpiredSuccess()
    {
        //Arrage
        $user = new User();
        $user->setPasswordRequestedAt(new \DateTime('now'));

        $millis = 60 * 60 *24;

        //Act
        $result = $user->isPasswordRequestNonExpired($millis);

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests than isPasswordRequestNonExpired time have non expired work correctly
     * @test
     */
    public function testIsPasswordRequestNonExpiredUnsuccess()
    {
        //Arrage
        $user = new User();

        $millis = 60 * 60 *24;
        $minus = 60 * 60;

        $t = time() - $millis - $minus;

        $dt = new \DateTime();
        $dt->setTimestamp($t);

        $user->setPasswordRequestedAt($dt);

        //Act
        $result = $user->isPasswordRequestNonExpired($millis);

        //Assert
        $this->assertFalse($result);
    }



    /**
     * tests toString method
     * @test
     */
    public function testToString()
    {
        //Arrage
        $user = new User();
        $expectedResult = 'lookAtMe';
        $user->setUsername($expectedResult);

        //Act
        $result = $user->__toString();

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * tests addgroup method
     * @test
     */
    public function testAddGroup1()
    {
        //Arrage
        $grp1 = (new Group())->setName('group1');
        $grp2 = (new Group())->setName('group2');

        $usr = new User();

        //Act
        $usr->addGroup($grp1)->addGroup($grp2);

        //Assert
        $this->assertNotEmpty($usr->getGroups()->toArray());
        $this->assertEquals([$grp1->getName(), $grp2->getName()], $usr->getGroupNames());
        $this->assertTrue($usr->hasGroup($grp1->getName()));
        $this->assertTrue($usr->hasGroup($grp2->getName()));
    }

    /**
     * tests addgroup method
     * @test
     */
    public function testAddGroup2()
    {
        //Arrage
        $grp1 = (new Group())->setName('group1');
        $grp2 = (new Group())->setName('group2');


        $usr = new User();

        //Act
        $usr->addGroup($grp1)
            ->addGroup($grp2)
            ->addGroup($grp1);

        //Assert
        $this->assertNotEmpty($usr->getGroups()->toArray());
        $this->assertEquals([$grp1->getName(), $grp2->getName()], $usr->getGroupNames());
        $this->assertTrue($usr->hasGroup($grp1->getName()));
        $this->assertTrue($usr->hasGroup($grp2->getName()));
    }

    /**
     * test remove group
     */
    public function testRemoveGroup()
    {
        //Arrage
        $grp1 = (new Group())->setName('group1');
        $grp2 = (new Group())->setName('group2');


        $usr = new User();

        //Act
        $usr->addGroup($grp1)->addGroup($grp2)->removeGroup($grp1);

        //Assert
        $this->assertNotEmpty($usr->getGroups()->toArray());
        $this->assertEquals([ $grp2->getName()], $usr->getGroupNames());
        $this->assertFalse($usr->hasGroup($grp1->getName()));
        $this->assertTrue($usr->hasGroup($grp2->getName()));
    }

    /**
     * tests method isUser
     * @test
     */
    public function testIsUserSuccess()
    {
        //Arrage
        $usr1 = new User();
        $usr2 = new User();

        $usr1->setId(10);
        $usr2->setId(10);

        //Act
        $result = $usr1->isUser($usr2);

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests method isUser
     * @test
     */
    public function testIsUserUnsuccess1()
    {
        //Arrage
        $usr1 = new User();
        $usr2 = null;

        $usr1->setId(10);

        //Act
        $result = $usr1->isUser($usr2);

        //Assert
        $this->assertFalse($result);
    }

    /**
     * tests method isUser
     * @test
     */
    public function testIsUserUnsuccess2()
    {
        //Arrage
        $usr1 = new User();
        $usr2 = new User();

        $usr1->setId(10);
        $usr2->setId(12);

        //Act
        $result = $usr1->isUser($usr2);

        //Assert
        $this->assertFalse($result);
    }

    /**
     * tests hasActivity method
     */
    public function testHasActivityUnSucess()
    {
        //Arrage
        $user = new User();

        //Act
        $result = $user->hasActivity();

        //Assert
        $this->assertFalse($result);

    }

    /**
     * tests hasActivity method
     */
    public function testHasActivitySuccess1()
    {
        //Arrage
        $user = new User();

        $user->getWodActivities()->add(new WodActivity());

        //Act
        $result = $user->hasActivity();

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests hasActivity method
     */
    public function testHasActivitySuccess2()
    {
        //Arrage
        $user = new User();

        $user->getCreatedWods()->add(new Wod());

        //Act
        $result = $user->hasActivity();

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests hasActivity method
     */
    public function testHasActivitySuccess3()
    {
        //Arrage
        $user = new User();

        $user->getWodComments()->add(new WodComments());

        //Act
        $result = $user->hasActivity();

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests hasActivity method
     */
    public function testHasActivitySuccess4()
    {
        //Arrage
        $user = new User();

        $user->getFollowedWods()->add(new Wod());

        //Act
        $result = $user->hasActivity();

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests hasActivity method
     */
    public function testHasActivitySuccess5()
    {
        //Arrage
        $user = new User();

        $user->getAssignedWods()->add(new Wod());

        //Act
        $result = $user->hasActivity();

        //Assert
        $this->assertTrue($result);
    }


    /**
     * tests getRoles when there is not any role
     * @test
     */
    public function testGetRolesIfIsEmpty()
    {
        //Arrage

        $user = new User();

        //Act
        $result = $user->getRoles();

        //Assert
        $this->assertEquals([User::ROLE_DEFAULT], $result);
    }

    /**
     * tests add role that is default
     */
    public function testAddRole1()
    {
        //Arrage
        $user = new User();
        $user->addRole(new Role(User::ROLE_DEFAULT));

        //Act
        $result = $user->getRoles();

        //Assert
        $this->assertEquals([User::ROLE_DEFAULT], $result);
    }

    /**
     * tests add role that is default
     */
    public function testAddRole2()
    {
        //Arrage
        $user = new User();
        $user->addRole(new Role(User::ROLE_ADMIN));

        //Act
        $result = $user->getRoles();

        //Assert
        $this->assertEquals([User::ROLE_ADMIN], $result);
    }

    /**
     * tests add role that is default
     */
    public function testAddRole3()
    {
        //Arrage
        $user = new User();
        $user->addRole(new Role(User::ROLE_ADMIN));
        $user->addRole(new Role(User::ROLE_OWNER));

        //Act
        $result = $user->getRoles();

        //Assert
        $this->assertEquals([User::ROLE_ADMIN, User::ROLE_OWNER], $result);
    }

    /**
     * tests add role that is default
     */
    public function testAddRole4()
    {
        //Arrage
        $user = new User();
        $user->addRole(new Role(User::ROLE_ADMIN));
        $user->addRole(new Role(User::ROLE_ADMIN));

        //Act
        $result = $user->getRoles();

        //Assert
        $this->assertEquals([User::ROLE_ADMIN], $result);
    }

    /**
     * tests add role that is default
     */
    public function testAddRole5()
    {
        //Arrage
        $user = new User();
        $user->addRole(new Role(User::ROLE_ADMIN));
        $user->addRole(new User());

        //Act
        $result = $user->getRoles();

        //Assert
        $this->assertEquals([User::ROLE_ADMIN], $result);
    }

    /**
     * tests isSuperAdmin
     */
    public function testIsSuperAdminSucecss()
    {
        //Arrage
        $user = new User();
        $user->addRole(new Role(User::ROLE_SUPER_ADMIN));

        //Act
        $result = $user->isSuperAdmin();

        //Assert
        $this->assertTrue($result);
    }

    /**
     * tests isSuperAdmin
     */
    public function testIsSuperAdminUnsucecss()
    {
        //Arrage
        $user = new User();

        //Act
        $result = $user->isSuperAdmin();

        //Assert
        $this->assertFalse($result);
    }

    /**
     * tests setSuperAdmin
     * @expectedException \Exception
     * @expectedExceptionMessage Method setSuperAdmin is no Callable
     */
    public function testSetSuperAdminThrowsException()
    {
        //Arrage

        $user = new User();

        //Act
        $user->setSuperAdmin(true);

        //Assert
    }

    /**
     * tests removeRole
     */
    public function testRemoveRole()
    {
        //Arrage
        $user = new User();
        $user->addRole(new Role(User::ROLE_ADMIN));

        //Act
        $user->removeRole(User::ROLE_ADMIN);
        $result = $user->getRoles();

        //Assert
        $this->assertEquals([User::ROLE_DEFAULT], $result);
    }


    /**
     * tests setRoles method
     */
    public function testSetRoles()
    {
        //Arrage
        $user = new User();


        //Act
        $user->setRoles([new Role(User::ROLE_ADMIN)]);
        $result = $user->getRoles();

        //Assert
        $this->assertEquals([User::ROLE_ADMIN], $result);
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function getGroups()
     * public function addToGroup(Group $group)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testAddToGroup1()
    {

        //Arrage
        $group = new Group();

        $user = new User();
        $user->addToGroup($group);

        //Act
        $result = $user->getGroups();

        //Assert
        $this->assertEquals(1, $result->count());
        $this->assertEquals($group, $result->get(0));
    }

    /**
     * @test
     */
    public function testAddToGroup2()
    {
        //Arrage
        $group1 = new Group();
        $group2 = new Group();

        $user = new User();


        //Act
        $user->addToGroup($group1)->addToGroup($group2);
        $result = $user->getGroups();

        //Assert
        $this->assertEquals(2, $result->count());
        $this->assertEquals($group1, $result->get(0));
        $this->assertEquals($group2, $result->get(1));
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function removeFromGroup(Group $group)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testRemoveFromGroup1()
    {
        //Arrage
        $group1 = new Group();
        $group2 = new Group();

        $user = new User();
        $user->addToGroup($group1)->addToGroup($group2);

        //Act
        $user->removeFromGroup($group1);
        $result = $user->getGroups();

        //Assert
        $this->assertEquals(1, $result->count());
        $this->assertEquals($group2, $result->get(1));
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function setGroups($groups)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetGroups()
    {
        //Arrage
        $group1 = new Group();
        $group2 = new Group();

        $arrayCollection = new ArrayCollection([$group1, $group2]);

        $user = new User();

        //Act
        $user->setGroups($arrayCollection);
        $result = $user->getGroups();

        //Assert
        $this->assertEquals(2, $result->count());
        $this->assertEquals($group1, $result->get(0));
        $this->assertEquals($group2, $result->get(1));
    }
}