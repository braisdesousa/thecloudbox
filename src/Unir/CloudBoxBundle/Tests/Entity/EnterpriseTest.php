<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 21/08/14
 * Time: 16:27
 */

namespace Unir\CloudBoxBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\LoginLog;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Group;

/**
 * Class EnterpriseTest
 * @package Unir\CloudBoxBundle\Tests\Entity
 */
class EnterpriseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Test Constructor
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testConstructor1()
    {
        //Arrage

        //Act
        $enterprise = new Enterprise();

        //Assert
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $enterprise->getUsers());
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $enterprise->getWorkGroups());
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $enterprise->getGroups());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Getters and Setters
     * -----------------------------------------------------------------------------------------------------------------
     */

    public function testGettersSetters1()
    {
        //Arrage
        $id = 123123;
        $name = 'holaaasdasd';

        //Act
        $enterprise = new Enterprise();
        $enterprise
            ->setId($id)
            ->setName($name);

        //ASSERT
        $this->assertEquals($id, $enterprise->getId());
        $this->assertEquals($name, $enterprise->getName());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function getWorkGroups()
     * public function addWorkGroup(WorkGroup $workGroup)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testAddWorkGroup1()
    {
        //Arrage
        $expected = new WorkGroup();


        $enterprise = new Enterprise();
        //Act

        $enterprise->addWorkGroup($expected);
        $result = $enterprise->getWorkGroups();

        //ASSERT
        $this->assertEquals(1, $result->count());
        $this->assertEquals($expected, $result->get(0));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function removeWorkGroup(WorkGroup $workGroup)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testRemoveWorkGroup1()
    {
        //Arrage
        $expected = new WorkGroup();
        $toRemove = new WorkGroup();


        $enterprise = new Enterprise();
        $enterprise->addWorkGroup($expected)->addWorkGroup($toRemove);
        //Act

        $enterprise->removeWorkGroup($toRemove);
        $result = $enterprise->getWorkGroups();

        //ASSERT
        $this->assertEquals(1, $result->count());
        $this->assertEquals($expected, $result->get(0));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function setWorkGroups($workGroups)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetWorkGroups1()
    {
        //Arrage
        $expected = new WorkGroup();
        $toRemove = new WorkGroup();


        $workGroups = new ArrayCollection([$expected, $toRemove]);

        $enterprise = new Enterprise();
        //Act

        $enterprise->setWorkGroups($workGroups);
        $result = $enterprise->getWorkGroups();

        //ASSERT
        $this->assertEquals(2, $result->count());
        $this->assertEquals($expected, $result->get(0));
        $this->assertEquals($toRemove, $result->get(1));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function getUsers()
     * public function addUser(User $user)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testAddUser1()
    {
        //Arrage
        $expected = new User();


        $enterprise = new Enterprise();

        //Act
        $enterprise->addUser($expected);
        $result = $enterprise->getUsers();

        //ASSERT
        $this->assertEquals(1, $result->count());
        $this->assertEquals($expected, $result->get(0));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function removeUser(User $user)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testRemoveUser1()
    {
        //Arrage
        $expected = new User();
        $toRemove = new User();


        $enterprise = new Enterprise();
        $enterprise->addUser($expected)->addUser($toRemove);

        //Act
        $enterprise->removeUser($toRemove);
        $result = $enterprise->getUsers();

        //ASSERT
        $this->assertEquals(1, $result->count());
        $this->assertEquals($expected, $result->get(0));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function setUsers($users)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetUsers1()
    {
        //Arrage
        $expected = new User();
        $toRemove = new User();


        $enterprise = new Enterprise();
        $users = new ArrayCollection([$expected, $toRemove]);

        //Act
        $enterprise->setUsers($users);
        $result = $enterprise->getUsers();

        //ASSERT
        $this->assertEquals(2, $result->count());
        $this->assertEquals($expected, $result->get(0));
        $this->assertEquals($toRemove, $result->get(1));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function getGroups()
     * public function addGroup(Group $group)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testAddGroup1()
    {
        //Arrage
        $expected = new Group();


        $enterprise = new Enterprise();

        //Act
        $enterprise->addGroup($expected);
        $result = $enterprise->getGroups();

        //ASSERT
        $this->assertEquals(1, $result->count());
        $this->assertEquals($expected, $result->get(0));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function removeGroup(Group $group)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testRemoveGroup1()
    {
        //Arrage
        $expected = new Group();
        $toRemove = new Group();


        $enterprise = new Enterprise();
        $enterprise->addGroup($expected)->addGroup($toRemove);

        //Act
        $enterprise->removeGroup($toRemove);
        $result = $enterprise->getGroups();

        //ASSERT
        $this->assertEquals(1, $result->count());
        $this->assertEquals($expected, $result->get(0));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function setGroups($groups)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetGroups1()
    {
        //Arrage
        $expected = new Group();
        $toRemove = new Group();


        $enterprise = new Enterprise();
        $groups = new ArrayCollection([$expected, $toRemove]);

        //Act
        $enterprise->setGroups($groups);
        $result = $enterprise->getGroups();

        //ASSERT
        $this->assertEquals(2, $result->count());
        $this->assertEquals($expected, $result->get(0));
        $this->assertEquals($toRemove, $result->get(1));
    }

} 