<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 21/08/14
 * Time: 17:20
 */

namespace Unir\CloudBoxBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\LoginLog;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Group;

/**
 * Class WorkGroupClass
 * @package Unir\CloudBoxBundle\Tests\Entity
 */
class WorkGroupTest extends \PHPUnit_Framework_TestCase
{

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Test Constructor
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testConstructor1()
    {
        //Arrage

        //Act
        $item = new WorkGroup();

        //Assert
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $item->getWodGroups());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Getters and Setters
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testGetterAndSetters1()
    {
        //Arrage
        $id = 123;
        $name = 'asdasd';
        $description = 'asdadqeqweqdasdasdqe';

        //Act
        $item = new WorkGroup();
        $item
            ->setId($id)
            ->setName($name)
            ->setDescription($description);

        //Assert
        $this->assertEquals($id, $item->getId());
        $this->assertEquals($name, $item->getName());
        $this->assertEquals($description, $item->getDescription());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * OVERRIDES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testToString1()
    {
        //Arrage
        $name = 'asdasd';

        //Act
        $item = new WorkGroup();
        $item->setName($name);

        //Assert
        $this->assertEquals($name, $item->__toString());
    }




    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function getEnterprise()
     * public function setEnterprise($enterprise)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetEnterprise1()
    {
        //Arrage
        $expected = new Enterprise();


        $item = new WorkGroup();
        //Act

        $result = $item->setEnterprise($expected)->getEnterprise();

        //ASSERT
        $this->assertEquals($expected, $result);
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function getWodGroups()
     * public function setWodGroups($wodGroups)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetWodGroups1()
    {
        //Arrage
        $expected = new ArrayCollection([new WodGroup(), new WodGroup()]);

        $item = new WorkGroup();
        //Act

        $result = $item->setWodGroups($expected)->getWodGroups();

        //ASSERT
        $this->assertEquals($expected, $result);
    }

} 