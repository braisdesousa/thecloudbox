<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 21/08/14
 * Time: 17:36
 */

namespace Unir\CloudBoxBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\LoginLog;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Group;

class RoleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Test Constructor
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testConstructor1()
    {
        //Arrage

        //Act
        $item = new Role();

        //Assert
        $this->assertNull($item->getRole());
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $item->getUsers());
    }

    /**
     * @test
     */
    public function testConstructor2()
    {
        //Arrage

        //Act
        $item = new Role(Role::ROLE_ADMIN);

        //Assert
        $this->assertEquals(Role::ROLE_ADMIN, $item->getRole());
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $item->getUsers());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Getters and Setters
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testGetterAndSetters1()
    {
        //Arrage
        $id = 12313;
        $role = Role::ROLE_ADMIN;
        $users = [new User(), new User()];

        //Act
        $item = new Role();
        $item
            ->setId($id)
            ->setRole($role)
            ->setUsers(new ArrayCollection($users));

        //Assert
        $this->assertEquals($id, $item->getId());
        $this->assertEquals($role, $item->getRole());
        $this->assertEquals($users, $item->getUsers()->toArray());
    }
}
