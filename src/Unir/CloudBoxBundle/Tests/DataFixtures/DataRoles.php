<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 12/08/14
 * Time: 16:01
 */

namespace Unir\CloudBoxBundle\Tests\DataFixtures;

use Unir\CloudBoxBundle\Entity\Role;

/**
 * Class that specifies the Roles Data for test purpose
 * Class DataRoles
 * @package Unir\CloudBoxBundle\Tests\DataFixtures
 */
class DataRoles
{
    /**
     * Initial data
     * @var array
     */
    private $data = [
        [
            'id'=> 1,
            'role'=> 'ROLE_ADMIN'
        ],
        [
            'id'=> 2,
            'role'=> 'ROLE_OWNER'
        ]
    ];

    /**
     * Retrieves data stored
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Transform a data item into the correct Object
     * @param array $item
     * @return Role
     */
    public function transform(array $item)
    {
        $role = new Role($item['role']);
        $role->setId($item['id']);

        return $role;
    }
}
