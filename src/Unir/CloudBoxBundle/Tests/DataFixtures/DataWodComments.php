<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 13/08/14
 * Time: 16:20
 */

namespace Unir\CloudBoxBundle\Tests\DataFixtures;

use Unir\CloudBoxBundle\Entity\WodComments;

/**
 * Class that specifies the WodComments that will be populated to Database in test environment
 * Class DataWodComment
 * @package Unir\CloudBoxBundle\Tests\DataFixtures
 */
class DataWodComments
{
    /**
     * @var array Initial data
     */
    private $data = [
        [
            "id" => 1,
            "comment" => "great",
            "wod_id" => 1,
            "user_id" => 3,
            'deleted' => false
        ]
    ];

    /**
     * retrieves data
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * Transform an array item into a WodComment
     * @param array $item
     * @return WodComments
     */
    public function transform(array $item)
    {
        $wodComment = new WodComments();
        $wodComment
            ->setId($item['id'])
            ->setComment($item['comment'])
            ->setUser(TestData::getUsers()[$item['user_id']])
            ->setWod(TestData::getWods()[$item['wod_id']]);

        if ($item['deleted']) {
            $wodComment->setDeleted();
        }

        return $wodComment;
    }
}
