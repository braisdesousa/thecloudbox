<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 13/08/14
 * Time: 9:45
 */

namespace Unir\CloudBoxBundle\Tests\DataFixtures;

use Unir\CloudBoxBundle\Entity\WorkGroup;

/**
 * Class that specifies the workGroups that will be populated to Database in test environment
 * Class DataWorkGroups
 * @package Unir\CloudBoxBundle\Tests\DataFixtures
 */
class DataWorkGroups
{
    /**
     * Initial data
     * @var array
     */
    private $data = [
        [
            "id" => 1,
            "name" => "ERP",
            "description" => "Programa de gestión de empresas",
            "adminUser_id" => 2,
            "enterprise_id" => 1,
            'userMembers' => [2,3],
            'groupMembers' => [1],
            'deleted' => false
        ],
        [
            "id" => 2,
            "name" => "CloudBoxManager",
            "description" => "Programa de gestion de tareas",
            "adminUser_id" => 2,
            "enterprise_id" => 1,
            'userMembers' => [2,3,4],
            'groupMembers' => [1,2],
            'deleted' => true
        ],
        [
            "id" => 3,
            "name" => "Deleted workGroup",
            "description" => "This one is deleted, i am sorry",
            "adminUser_id" => 2,
            "enterprise_id" => 1,
            'userMembers' => [2],
            'groupMembers' => [4],
            'deleted' => false
        ],
    ];

    /**
     * retrieves data
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * trasnforms an array item into a WorkGroup entity
     * @param array $item
     * @return WorkGroup
     */
    public function transform(array $item)
    {
        $workGroup = new WorkGroup();

        $workGroup
            ->setId($item['id'])
            ->setName($item['name'])
            ->setDescription($item['description'])
            ->setEnterprise(TestData::getEnterprises()[$item['enterprise_id']]);

        if ($item['deleted']) {
            $workGroup->setDeleted();
        }
        return $workGroup;
    }
}
