<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 12/08/14
 * Time: 17:03
 */

namespace Unir\CloudBoxBundle\Tests\DataFixtures;

use Unir\CloudBoxBundle\Entity\Group;

/**
 * Class that specifies the User Groups for test purpose
 * Class DataUserGroups
 * @package Unir\CloudBoxBundle\Tests\DataFixtures
 */
class DataUserGroups
{
    /**
     * Initial user groups
     * @var array
     */
    private $data = [
        [
            'id' => 1,
            'name' => 'Administrators',
            'enterprise_id' => 1,
            'deleted' => false
        ],
        [
            'id' => 2,
            'name' => 'Developers',
            'enterprise_id' => 1,
            'deleted' => false
        ],
        [
            'id' => 3,
            'name' => 'Designers',
            'enterprise_id' => 1,
            'deleted' => false
        ],
        [
            'id' => 4,
            'name' => 'SalesMan',
            'enterprise_id' => 1,
            'deleted' => false
        ],
        [
            'id' => 5,
            'name' => 'SalesMan',
            'enterprise_id' => 2,
            'deleted' => false
        ],
        [
            'id' => 6,
            'name' => 'Administrators',
            'enterprise_id' => 2,
            'deleted' => false
        ],
        [
            'id' => 7,
            'name' => 'Random people',
            'enterprise_id' => 3,
            'deleted' => false
        ],
        [
            'id' => 8,
            'name' => 'Random people',
            'enterprise_id' => 1,
            'deleted' => true
        ],
    ];

    /**
     * Retrieves de data stored
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Trasnform an array item into a group
     * @param array $item
     * @return Group
     */
    public function transform(array $item)
    {
        $group = new Group();

        $group
            ->setId($item['id'])
            ->setName($item['name'])
            ->setEnterprise(TestData::getEnterprises()[$item['enterprise_id']]);

        if ($item['deleted']) {
            $group->setDeleted();
        }

        return $group;
    }
}
