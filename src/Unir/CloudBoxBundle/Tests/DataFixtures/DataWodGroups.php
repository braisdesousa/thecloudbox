<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 13/08/14
 * Time: 11:33
 */

namespace Unir\CloudBoxBundle\Tests\DataFixtures;

use Unir\CloudBoxBundle\Entity\WodGroup;

/**
 * Class that specifies the wodGroups that will be populated to Database in test environment
 * Class TestWodGroups
 * @package Unir\CloudBoxBundle\Tests\DataFixtures
 */
class DataWodGroups
{
    /**
     * Initial data
     * @var array
     */
    private $data = [
        [
            "id" => 1,
            "name" => "Administración",
            "description" => "Tareas relacionadas con gestión administrativa",
            "workGroup_id" => 1,
            'followers' => [2,3],
            'userMembers' => [4,5],
            'groupMembers' => [1],
            'deleted' => false
        ],
        [
            "id" => 2,
            "name" => "Televenta",
            "description" => "Tareas relacionadas con la gestión comercial",
            "workGroup_id" => 1,
            'followers' => [4,5],
            'userMembers' => [4,5],
            'groupMembers' => [1],
            'deleted' => false
        ],
        [
            "id" => 3,
            "name" => "Postventa",
            "description" => "Tareas relacionadas con la gestión de servicios",
            "workGroup_id" => 1,
            'followers' => [3,6],
            'userMembers' => [4,5],
            'groupMembers' => [1],
            'deleted' => false
        ],
        [
            "id" => 4,
            "name" => "FrontEnd",
            "description" => "Tareas relacionadas con el Frontend de la aplicación",
            "workGroup_id" => 2,
            'followers' => [6],
            'userMembers' => [4,5],
            'groupMembers' => [1],
            'deleted' => false
        ],
        [
            "id" => 5,
            "name" => "BackEnd",
            "description" => "Tareas relacionadas con el BackEnd de la aplicación",
            "workGroup_id" => 2,
            'followers' => [4],
            'userMembers' => [4,5],
            'groupMembers' => [1],
            'deleted' => false
        ],
        [
            "id" => 6,
            "name" => "Configuración",
            "description" => "Tareas relacionadas con la configuración de la aplicación",
            "workGroup_id" => 2,
            'followers' => [1,5],
            'userMembers' => [4,5],
            'groupMembers' => [1],
            'deleted' => true
        ],
        [
            "id" => 7,
            "name" => "Deleted group",
            "description" => "This one is deleted",
            "workGroup_id" => 3,
            'followers' => [4,6],
            'userMembers' => [4,5],
            'groupMembers' => [1],
            'deleted' => true
        ],
    ];

    /**
     * Retrieves data
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * Transforms an array item as WodGroup
     * @param array $item
     * @return WodGroup
     */
    public function transform(array $item)
    {
        $wodGroup = new WodGroup();
        $wodGroup
            ->setId($item['id'])
            ->setName($item['name'])
            ->setDescription($item['description'])
            ->setWorkGroup(TestData::getWorkGroups()[$item['workGroup_id']]);

        if ($item['deleted']) {
            $wodGroup->setDeleted();
        }

        foreach ($item['groupMembers'] as $groupMemberId) {
            $wodGroup->addGroupMember(TestData::getGroups()[$groupMemberId]);
        }

        return $wodGroup;
    }
}
