<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 13/08/14
 * Time: 12:53
 */

namespace Unir\CloudBoxBundle\Tests\DataFixtures;

use Unir\CloudBoxBundle\Entity\Wod;

/**
 * Class that specifies the Wods that will be populated to Database in test environment
 * Class DataWods
 * @package Unir\CloudBoxBundle\Tests\DataFixtures
 */
class DataWods
{
    /**
     * @var array Initial wods
     */
    private $data = [
        [
            "id" => 1,
            "description" => "Creación de empleados en el sistema",
            "title" => "Crear empleados",
            "status" => Wod::STATUS_OPEN,
            "difficulty" => Wod::DIFFICULTY_HIGH,
            "estimationTime" => 30,
            "wodGroup_id" => 1,
            "creationUser_id" => 2,
            "assignedUsers" => [3],
            "followerUsers" => [4],
            'viewBy' => Wod::VIEW_BY_ALL,
            'deleted' => false
        ],
        [
            "id" => 2,
            "description" => "Creación de la ficha de empleados",
            "title" => "Ficha Empleados",
            "status" => Wod::STATUS_INPROGRESS,
            "difficulty" => Wod::DIFFICULTY_CRITICAL,
            "estimationTime" => 20,
            "wodGroup_id" => 2,
            "creationUser_id" => 2,
            "assignedUsers" => [3],
            "followerUsers" => [],
            'viewBy' => Wod::VIEW_BY_ALL,
            'deleted' => false
        ],
        [
            "id" => 3,
            "description" => "wod number 3",
            "title" => "wod number 3",
            "status" => Wod::STATUS_INPROGRESS,
            "difficulty" => Wod::DIFFICULTY_NORMAL,
            "estimationTime" => 30,
            "wodGroup_id" => 1,
            "creationUser_id" => 2,
            "assignedUsers" => [3],
            "followerUsers" => [4,5],
            'viewBy' => Wod::VIEW_BY_ALL,
            'deleted' => false
        ],
        [
            "id" => 4,
            "description" => "wod number 4",
            "title" => "wod number 4",
            "status" => Wod::STATUS_INPROGRESS,
            "difficulty" => Wod::DIFFICULTY_CRITICAL,
            "estimationTime" => 30,
            "wodGroup_id" => 1,
            "creationUser_id" => 2,
            "assignedUsers" => [3],
            "followerUsers" => [],
            'viewBy' => Wod::VIEW_BY_ALL,
            'deleted' => false
        ],
        [
            "id" => 5,
            "description" => "wod number 5",
            "title" => "wod number 5",
            "status" => Wod::STATUS_INPROGRESS,
            "difficulty" => Wod::DIFFICULTY_HIGH,
            "estimationTime" => 30,
            "wodGroup_id" => 6,
            "creationUser_id" => 2,
            "assignedUsers" => [2],
            "followerUsers" => [],
            'viewBy' => Wod::VIEW_BY_ALL,
            'deleted' => false
        ],
        [
            "id" => 6,
            "description" => "wod number 6",
            "title" => "wod number 6",
            "status" => Wod::STATUS_INPROGRESS,
            "difficulty" => Wod::DIFFICULTY_HIGH,
            "estimationTime" => 30,
            "wodGroup_id" => 4,
            "creationUser_id" => 2,
            "assignedUsers" => [2],
            "followerUsers" => [],
            'viewBy' => Wod::VIEW_BY_ALL,
            'deleted' => false
        ],
    ];

    /**
     * retrieves data stored
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * Trasnforms an array item into a Wod
     * @param array $item
     * @return Wod
     */
    public function transform(array $item)
    {
        $wod = new Wod();
        $wod
            ->setId($item['id'])
            ->setDescription($item['description'])
            ->setTitle($item['title'])
            ->setStatus($item['status'])
            ->setDifficulty($item['difficulty'])
            ->setEstimationTime($item['estimationTime'])
            ->setWodGroup(TestData::getWodGroups()[$item['wodGroup_id']])
            ->setCreationUser(TestData::getUsers()[$item['creationUser_id']])
            ->setAvailableViewers($item['viewBy']);

        if ($item['deleted']) {
            $wod->setDeleted();
        }

        foreach ($item['assignedUsers'] as $userId) {
            $wod->addUsersAssigned(TestData::getUsers()[$userId]);
        }

        foreach ($item['followerUsers'] as $userId) {
            $wod->addUsersFollower(TestData::getUsers()[$userId]);
        }

        //Add wod to WodGroup Wods Collection
        $wod->getWodGroup()->getWods()->add($wod);

        return $wod;
    }
}
