<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 12/08/14
 * Time: 17:52
 */

namespace Unir\CloudBoxBundle\Tests\DataFixtures;

use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

/**
 * Class that specifies the users that will be populated to Database in test environment
 * Class DataUsers
 * @package Unir\CloudBoxBundle\Tests\DataFixtures
 */
class DataUsers
{
    /**
     * Initial data
     * @var array
     */
    private $data = [
        [
            'id' => 1,
            'userName' => 'admin',
            'password' => 'admin',
            'email' => 'seuperadmin@iamgod.es',
            'enabled' => true,
            'roles' => [2],
            'groups' => [],
            'deleted' => false
        ],
        [
            "id" => 2,
            "userName" => "admin_unir-solutions-s-l",
            "password" => "admin",
            "email" => "admin_unir-solutions-s-l@admin.es",
            "enabled" => true,
            "roles" => [1, 2],
            'groups' => [1,2],
            'deleted' => false
        ],
        [
            "id" => 3,
            "userName" => "Evert Stark",
            "password" => "1234",
            "email" => "bins.felipa@cartwright.com",
            "enabled" => true,
            "roles" => [1],
            'groups' => [3],
            'deleted' => false
        ],
        [
            "id" => 4,
            "userName" => "Wuzu",
            "password" => "asd123",
            "email" => "wuzu@iamnotwuzu.es",
            "enabled" => true,
            "roles" => [2],
            'groups' => [2],
            'deleted' => false
        ],
        [
            "id" => 5,
            "userName" => "IamGod",
            "password" => "alamierdanosequeponer",
            "email" => "iamnotgod@butidontExist.es",
            "enabled" => true,
            "roles" => [],
            'groups' => [2,3],
            'deleted' => false
        ],
        [
            "id" => 6,
            "userName" => "username6",
            "password" => "uffabestpassword",
            "email" => "email7@testingemail7.es",
            "enabled" => true,
            "roles" => [1],
            'groups' => [5,6],
            'deleted' => false
        ],
    ];

    /**
     * Retrieves data
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Trasnform an array item into an User
     * @param array $item
     * @return User
     */
    public function transform(array $item)
    {
        $user = new User();
        $user
            ->setId($item['id'])
            ->setUsername($item['userName'])
            ->setPlainPassword($item['password'])
            ->setEmail($item['email'])
            ->setEnabled($item['enabled']);

       foreach ($item['roles'] as $roleId) {
            $user->addRole(TestData::getRoles()[$roleId]);
        }

        foreach ($item['groups'] as $groupId) {
            $user->addToGroup(TestData::getGroups()[$groupId]);
        }

        return $user;
    }
}
