<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 24/07/14
 * Time: 9:25
 */

namespace Unir\CloudBoxBundle\Tests\DataFixtures;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;

/**
 * This class will hold the data for tests.
 *
 * It must populate database as needed and will help mocked repositories for hold "well known data"
 *
 * Class TestData
 * @package Unir\CloudBoxBundle\Tests\DataFixtures
 */
class TestData
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Holds roles
     * @var array
     */
    private static $roles = [];

    /**
     * Holds enterprises
     * @var array
     */
    private static $enterprises = [];

    /**
     * Stores Groups
     * @var array
     */
    private static $groups = [];

    /**
     * Stores Users
     * @var array
     */
    private static $users = [];

    /**
     * Stores workGroups
     * @var array
     */
    private static $workGroups = [];

    /**
     * Stores wodGroups
     * @var array
     */
    private static $wodGroups = [];

    /**
     * Stores wods
     * @var array
     */
    private static $wods = [];

    /**
     * Stores wodComments
     * @var array
     */
    private static $wodsComments = [];

    /**
     * Stores wod Activities
     * @var array
     */
    private static $wodActivities = [];

    /**
     * Indicates if this class is initialized or not
     * @var bool
     */
    private static $initialized = false;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * Properties
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @return array
     */
    public static function getRoles()
    {
        return self::$roles;
    }

    /**
     * @return array
     */
    public static function getEnterprises()
    {
        return self::$enterprises;
    }

    /**
     * @return array
     */
    public static function getGroups()
    {
        return self::$groups;
    }

    /**
     * @return array
     */
    public static function getUsers()
    {
        return self::$users;
    }

    /**
     * @return array
     */
    public static function getWorkGroups()
    {
        return self::$workGroups;
    }

    /**
     * @return array
     */
    public static function getWodGroups()
    {
        return self::$wodGroups;
    }

    /**
     * @return array
     */
    public static function getWods()
    {
        return self::$wods;
    }

    /**
     * @return array
     */
    public static function getWodsComments()
    {
        return self::$wodsComments;
    }

    /**
     * @return array
     */
    public static function &getWodActivities()
    {
        return self::$wodActivities;
    }

    /**
     * @return boolean
     */
    public static function isInitialized()
    {
        return self::$initialized;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * Public Methods
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Initialized data
     */
    public static function initialize()
    {
        //If is initialized, just return
        if (self::isInitialized()) {
            return;
        }

        //Fill Data
        self::fillRoles();
        self::fillEnterprises();
        self::fillGroups();
        self::fillUsers();
        self::fillWorkGroups();
        self::fillWodGroups();
        self::fillWods();
        self::fillWodComments();

        //Mark as initialized
        self::$initialized = true;
    }

    /**
     * fills Roles
     */
    private static function fillRoles()
    {
        $dataRoles = new DataRoles();
        foreach ($dataRoles->getData() as $data) {

            $role = $dataRoles->transform($data);

            self::$roles[$role->getId()] = $role;
        }
    }

    /**
     * Fills enterprises
     */
    private static function fillEnterprises()
    {
        $dataEnterprises = new DataEnterprises();
        foreach ($dataEnterprises->getData() as $data) {

            $enterprise = $dataEnterprises->transform($data);

            self::$enterprises[$enterprise->getId()] = $enterprise;
        }
    }

    /**
     * Fills groups
     */
    private static function fillGroups()
    {
        $dataGroups = new DataUserGroups();
        foreach ($dataGroups->getData() as $data) {
            $group = $dataGroups->transform($data);

            self::$groups[$group->getId()] = $group;
        }
    }

    /**
     * Fill users
     */
    private static function fillUsers()
    {
        $dataUsers = new DataUsers();
        foreach ($dataUsers->getData() as $data) {
            $user = $dataUsers->transform($data);

            self::$users[$user->getId()] = $user;
        }
    }

    /**
     * Fills workGroups
     */
    private static function fillWorkGroups()
    {
        $dataWorkGroups = new DataWorkGroups();
        foreach ($dataWorkGroups->getData() as $data) {
            $workGroup = $dataWorkGroups->transform($data);

            self::$workGroups[$workGroup->getId()] = $workGroup;
        }
    }

    /**
     * Fills wodGroups
     */
    private static function fillWodGroups()
    {
        $dataWodGroups = new DataWodGroups();
        foreach ($dataWodGroups->getData() as $data) {
            $wodGroup = $dataWodGroups->transform($data);

            self::$wodGroups[$wodGroup->getId()] = $wodGroup;
        }
    }

    /**
     * Fills Wods
     */
    private static function fillWods()
    {
        $dataWods = new DataWods();
        foreach ($dataWods->getData() as $data) {
            $wod = $dataWods->transform($data);

            self::$wods[$wod->getId()] = $wod;
        }
    }

    /**
     * Fills wod comments
     */
    private static function fillWodComments()
    {
        $dataWodComments = new DataWodComments();
        foreach ($dataWodComments->getData() as $data) {
            $wodComment = $dataWodComments->transform($data);

            self::$wodsComments[$wodComment->getId()] = $wodComment;
        }
    }
}
