<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 12/08/14
 * Time: 16:39
 */

namespace Unir\CloudBoxBundle\Tests\DataFixtures;

use Unir\CloudBoxBundle\Entity\Enterprise;

/**
 * Class that specifies the enterprises that will be populated to Database in test enviroment
 * Class DataEnterprises
 * @package Unir\CloudBoxBundle\Tests\DataFixtures
 */
class DataEnterprises
{
    /**
     * Initial data
     * @var array
     */
    private $data = [
        [
            'id' => 1,
            'name' => 'Unir Box S.L.',
            'deleted' => false
        ],
        [
            'id' => 2,
            'name' => 'Web Box',
            'deleted' => false
        ],
        [
            'id' => 3,
            'name' => 'Cagones Box',
            'deleted' => false
        ]
    ];

    /**
     * Retrieves data stored
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Transform an item array into an enterprise
     * @param array $item
     * @return Enterprise
     */
    public function transform(array $item)
    {
        $enterprise = new Enterprise();
        $enterprise
            ->setId($item['id'])
            ->setName($item['name']);

        if ($item['deleted']) {
            $enterprise->setDeleted();
        }

        return $enterprise;
    }
}
