<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 18/08/14
 * Time: 19:10
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Repository;

use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Repository\UserGroupRepository;

class UserGroupRepositoryMock extends UserGroupRepository
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * COSNTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {

    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * OVERRIDES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function findByCompany(Enterprise $enterprise, $count = null, $include_deleted = false)
    {
        //Wow, so haaaard
        $groups = $enterprise->getGroups()->toArray();

        if (!$include_deleted) {
            $groups = $this->excludeDeletedOnes($groups);
        }

        return $count
            ? count($groups)
            : $groups;
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Excludes from an array the deleted ones
     * @param array $elements
     * @return array
     */
    private function excludeDeletedOnes(array $elements)
    {
        $result = [];

        foreach ($elements as $wod) {
            if (!$wod->getDeleted()) {
                $result[] = $wod;
            }
        }

        return $result;
    }

    /**
     * Order by wods
     * @param array $elements
     * @param array $orderBys
     */
    private function orderByElements(array &$elements, array $orderBys)
    {
        foreach ($orderBys as $orderByKey => $orderByValue) {

            $callback = function (User $item1, User $item2) use ($orderByKey, $orderByValue) {

                switch($orderByKey)
                {
                    case 'id':
                        return $orderByValue == 'asc'
                            ? $item1->getId() - $item2->getId()
                            : $item2->getId() - $item1->getId();

                    case 'username':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getUsername(), $item2->getUsername())
                            : strcmp($item2->getUsername(), $item1->getUsername());

                    case 'email':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getEmail(), $item2->getEmail())
                            : strcmp($item2->getEmail(), $item1->getEmail());
                }
            };

            usort($elements, $callback);
        }
    }
}