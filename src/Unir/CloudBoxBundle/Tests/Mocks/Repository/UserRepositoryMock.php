<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 7/08/14
 * Time: 11:02
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Repository;

use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityRepositoryMock;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Repository\UserRepository;

/**
 * Class UserRepositoryMock
 * @package Unir\CloudBoxBundle\Tests\Controller
 */
class UserRepositoryMock extends UserRepository
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {
        TestData::initialize();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * OVERRIDES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function countAll()
    {
        return count(TestData::getUsers());
    }



    /**
     * {@inheritdoc}
     */
    public function countAllByEnterprise(Enterprise $enterprise)
    {
        return count($this->findByEnterprise($enterprise));
    }



    /**
     * {@inheritdoc}
     */
    public function findByUserGroup(Group $group)
    {
        //If group is deleted, return empty
        if ($group->getDeleted()) {
            return [];
        }
        //El return users
        return $group->getUsers()->toArray();
    }

    /**
     * {@inheritdoc}
     */
    public function findByGroup(WodGroup $group)
    {
        //Retrieve members
        $result = $group->getUsers()->toArray();

        foreach ($group->getGroupMembers() as $userGroup) {
            foreach ($userGroup->getUsers() as $userInGroup) {
                if (!in_array($userInGroup, $result)) {
                    $result[] = $userInGroup;
                }
            }
        }

        $this->orderByElements($result, ['id' => 'asc']);

        return $result;
    }

    /**
     * @param Enterprise $enterprise
     * @param null $orderBy
     * @return array
     */
    public function findByEnterprise(Enterprise $enterprise, $orderBy = null)
    {
        return $this->getByEnterprise($enterprise, $orderBy);
    }
    /**
     * {@inheritdoc}
     */
    public function findByGroupRelatedNotMembers(WodGroup $wodGroup)
    {
        $result = [];

        //Cycle wods...
        foreach (TestData::getWods() as $wod) {

            //Not wodGroup wod, continue...
            if ($wod->getWodGroup()->getId() != $wodGroup->getId()) {
                continue;
            }

            //Add creator if not added...
            if (!in_array($wod->getCreationUser(), $result)) {
                $result[] = $wod->getCreationUser();
            }

            //Add not added followers
            foreach ($wod->getUsersFollower() as $userFollower) {
                if (!in_array($userFollower, $result)) {
                    $result[] = $userFollower;
                }
            }

            //Add not added assigned
            foreach ($wod->getUsersAssigned() as $userAssigned) {
                if (!in_array($userAssigned, $result)) {
                    $result[] = $userAssigned;
                }
            }
        }

        $finalResult = [];

        $usersInWodGroup = $this->findByGroup($wodGroup);

        foreach ($result as $user) {

            $found = false;
            foreach ($usersInWodGroup as $userInWodGroup) {
                if ($user->getId() == $userInWodGroup->getId()) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $finalResult[] = $user;
            }
        }

        return $finalResult;
    }


    /**
     * {@inheritdoc}
     */
    public function findByAdminOrOwner(Enterprise $enterprise = null)
    {
        $result = [];

        foreach (TestData::getUsers() as $user) {

            //If user is owner, isnert it
            if ($user->hasRole(Role::ROLE_OWNER)) {
                $result[] = $user;
                continue;
            }

            //If we have enterprise..
            if ($enterprise) {

                //If is another enterprise then don't do anything
                if ($user->getEnterprise() &&  $user->getEnterprise()->getId() != $enterprise->getId()) {
                    continue;
                }

                //If user is admin, then, insert it
                if ($user->hasRole(Role::ROLE_ADMIN)) {
                    $result[] = $user;
                }
            }
        }

        return $result;
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Excludes from an array the deleted ones
     * @param array $elements
     * @return array
     */
    private function excludeDeletedOnes(array $elements)
    {
        $result = [];

        foreach ($elements as $user) {
            $isDeleted = !$user->isEnabled() || $user->isLocked() || $user->isExpired();

            if (!$isDeleted) {
                $result[] = $user;
            }
        }

        return $result;
    }

    /**
     * Order by wods
     * @param array $elements
     * @param array $orderBys
     */
    private function orderByElements(array &$elements, array $orderBys)
    {
        foreach ($orderBys as $orderByKey => $orderByValue) {

            $callback = function (User $item1, User $item2) use ($orderByKey, $orderByValue) {

                switch($orderByKey)
                {
                    case 'id':
                        return $orderByValue == 'asc'
                            ? $item1->getId() - $item2->getId()
                            : $item2->getId() - $item1->getId();

                    case 'username':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getUsername(), $item2->getUsername())
                            : strcmp($item2->getUsername(), $item1->getUsername());

                    case 'email':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getEmail(), $item2->getEmail())
                            : strcmp($item2->getEmail(), $item1->getEmail());
                }
            };

            usort($elements, $callback);
        }
    }

    /**
     * @param Enterprise $enterprise
     * @param null $orderBy
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function getByEnterprise(Enterprise $enterprise, $orderBy = null)
    {

        foreach (TestData::getEnterprises() as $test_enterprise){
            if($test_enterprise->getName()==$enterprise->getName()){
                return $test_enterprise->getUsers();
            }
        }
    }
}
