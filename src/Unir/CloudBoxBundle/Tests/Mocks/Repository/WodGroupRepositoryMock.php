<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 4/08/14
 * Time: 13:35
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Repository;

use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Repository\WodGroupRepository;

/**
 * Mock of wodGroupRepository
 * Class WodGroupRepositoryMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Repository
 */
class WodGroupRepositoryMock extends WodGroupRepository
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var WodRepositoryMock
     */
    protected $wodRepositoryMock;

    /**
     * @var WorkGroupRepositoryMock
     */
    protected $workGroupRepositoryMock;

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {
        TestData::initialize();
        $this->wodRepositoryMock = new WodRepositoryMock();
        $this->workGroupRepositoryMock = new WorkGroupRepositoryMock();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * OVERRIDES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function findAll($count = false, $include_deleted = false)
    {
        $result = TestData::getWodGroups();

        if (!$include_deleted) {
            $result = $this->excludeDeletedOnes($result);
        }

        return $count
            ? count($result)
            : $result;
    }

    /**
     * {@inheritdoc}
     */
    public function countAll($include_delete = false)
    {
        return $this->findAll(true, $include_delete);
    }

    /**
     * {@inheritdoc}
     */
    public function findByWorkGroup(WorkGroup $workGroup, $count = false, array $orderBy = null)
    {
        $wodGroups = $workGroup->getWodGroups();

        $result = [];

        //Filter deleted ones...
        foreach ($wodGroups as $wodGroup) {
            if (!$wodGroup->getDeleted()) {
                $result[] = $wodGroup;
            }
        }

        if ($orderBy) {
            $this->orderByElements($result, $orderBy);
        }

        return $count
            ? count($result)
            : $result;
    }

    /**
     * {@inheritdoc}
     */
    public function findByUser(
        User $user,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {

        $workGroups = $this->workGroupRepositoryMock->findByUserMember($user);

        $workGroupIds = [];
        foreach ($workGroups as $prj) {
            $workGroupIds[] = $prj->getId();
        }

        $wodGroups = [];

        foreach (TestData::getWodGroups() as $wodGroup) {

            if (!in_array($wodGroup->getWorkGroup()->getId(), $workGroupIds)) {
                continue;
            }

            $userIsMember = false;
            foreach ($wodGroup->getUserMembers() as $uM) {
                if ($uM->getId() == $user->getId() ) {

                    $userIsMember = true;
                    break;
                }
            }

            if (!$userIsMember) {
                foreach ($wodGroup->getGroupMembers() as $gM) {
                    foreach ($gM->getUsers() as $uM) {
                        if ($uM->getId() == $user->getId() ) {

                            $userIsMember = true;
                            break;
                        }
                    }
                }
            }


            if (!$userIsMember && $wodGroup->getAvailableViewers() != WodGroup::VIEW_BY_ASSIGNED) {
                foreach ($wodGroup->getUserFollowers() as $uF) {
                    if ($uF->getId() == $user->getId()) {

                        $userIsMember = true;
                        break;
                    }
                }
            }

            if ($userIsMember) {
                $wodGroups[] = $wodGroup;
            }
        }

        if (!$include_deleted) {
            $wodGroups = $this->excludeDeletedOnes($wodGroups);
        }

        if ($orderBy) {
            $this->orderByElements($wodGroups, $orderBy);
        }

        return $count
            ? count($wodGroups)
            : $wodGroups;
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserAndWorkGroup(
        User $user,
        WorkGroup $workGroup,
        $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {

        //First of all, extract wodGroups that are on workGroup
        $wodGroups = $workGroup->getWodGroups()->toArray();

        $userIsInWorkGroup = false;

        //Navigate to see if the user is in workGroup
        foreach ($workGroup->getUserMembers() as $userMember) {
            if ($userMember->getId() == $user->getId()) {
                $userIsInWorkGroup = true;
                break;
            }
        }

        //Now check if is member of a workGroup's user group
        if (!$userIsInWorkGroup) {
            foreach ($workGroup->getGroupMembers() as $group) {
                foreach ($group->getUsers() as $userMember) {
                    if ($userMember->getId() == $user->getId()) {
                        $userIsInWorkGroup = true;
                        break;
                    }
                }
            }
        }

        //Retrieve all wods for the user
        $userWods = $this->wodRepositoryMock->findByUser($user, null, null, null, null, null);

        $result = [];
        foreach ($wodGroups as $wodGroup) {

            //If wodGroup is deleted and we must exclude em...
            if (!$include_deleted && $wodGroup->getDeleted()) {
                continue;
            }

            //If user is in workGroup and wodGroup is visible by all, then add it
            if ($userIsInWorkGroup && $wodGroup->getAvailableViewers() == WodGroup::VIEW_BY_ALL) {
                $result[] = $wodGroup;
                continue;
            }

            $found = false;

            //If user is member of the group, add it to result
            foreach ($wodGroup->getUserMembers() as $userMember) {
                if ($userMember->getId() == $user->getId()) {
                    $result[] = $wodGroup;
                    $found = true;
                    break;
                }
            }

            if ($found) {
                continue;
            }

            //If user is follower of the group, add it to result
            foreach ($wodGroup->getUserFollowers() as $userFollower) {
                if ($userFollower->getId() == $user->getId()) {
                    $result[] = $wodGroup;
                    $found = true;
                    break;
                }
            }

            if ($found) {
                continue;
            }

            //If there is any wod in the group that the user has, then we must include the group
            foreach ($wodGroup->getWods() as $wod) {
                foreach ($userWods as $userWod) {
                    if ($wod->getId() == $userWod->getId()) {
                        $result[] = $wodGroup;
                        $found = true;
                        break 2;
                    }
                }
            }

            if ($found) {
                continue;
            }
        }

        if ($orderBy) {
            $this->orderByElements($result, $orderBy);
        }

        return $count
            ? count($result)
            : $result;

    }

    /**
     * {@inheritdoc}
     */
    public function findByEnterprise(Enterprise $enterprise, $count = false, array $orderBy = null)
    {
        $result = [];
        foreach (TestData::getWodGroups() as $wodGroup) {
            if ($wodGroup->getWorkGroup()->getEnterprise()->getId() == $enterprise->getId()) {
                $result[] = $wodGroup;
            }
        }

        $result = $this->excludeDeletedOnes($result);

        if ($orderBy) {
            $this->orderByElements($result, $orderBy);
        }

        return $count
            ? count($result)
            : $result;
    }

    /**
     * {@inheritdoc}
     */
    public function findGroupsWithOpenWodsByEnterprise(Enterprise $enterprise)
    {
        $result = [];

        foreach ($enterprise->getWorkGroups() as $workGroup) {
            foreach ($workGroup->getWodGroups() as $wodGroup) {
                foreach ($wodGroup->getWods() as $wod) {
                    if ($this->isWodOpenAndNotDeleted($wod)) {
                        $result[] = $wodGroup;
                        break;
                    }
                }
            }
        }
        return $result;
    }






    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Excludes from an array the deleted ones
     * @param array $elements
     * @return array
     */
    private function excludeDeletedOnes(array $elements)
    {
        $result = [];

        foreach ($elements as $wod) {
            if (!$wod->getDeleted()) {
                $result[] = $wod;
            }
        }

        return $result;
    }

    /**
     * Order by wods
     * @param array $elements
     * @param array $orderBys
     */
    private function orderByElements(array &$elements, array $orderBys)
    {
        foreach ($orderBys as $orderByKey => $orderByValue) {

            $callback = function (WodGroup $item1, WodGroup $item2) use ($orderByKey, $orderByValue) {

                switch($orderByKey)
                {
                    case 'id':
                        return $orderByValue == 'asc'
                            ? $item1->getId() - $item2->getId()
                            : $item2->getId() - $item1->getId();

                    case 'name':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getName(), $item2->getName())
                            : strcmp($item2->getName(), $item1->getName());
                }
            };

            usort($elements, $callback);
        }
    }

    /**
     * Checks if a wod is open...
     * @param Wod $wod
     * @return bool
     */
    private function isWodOpenAndNotDeleted(Wod $wod)
    {
        return !$wod->getDeleted() && (
                $wod->getStatus() == Wod::STATUS_OPEN ||
                $wod->getStatus() == Wod::STATUS_INPROGRESS
            );
    }
}
