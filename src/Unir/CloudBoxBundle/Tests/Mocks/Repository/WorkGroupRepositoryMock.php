<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 31/07/14
 * Time: 13:57
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Repository;

use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Repository\WorkGroupRepository;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

/**
 * Mocks from workGroup repository
 * Class WorkGroupRepositoryMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Repository
 */
class WorkGroupRepositoryMock extends WorkGroupRepository
{

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var WodRepositoryMock
     */
    protected $wodRepository;

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {
        TestData::initialize();
        $this->wodRepository = new WodRepositoryMock();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * OVERRIDES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function findAll($include_deleted = false)
    {
        $workGroups = TestData::getWorkGroups();

        if (!$include_deleted) {
            $workGroups = $this->excludeDeletedOnes($workGroups);
        }

        return $workGroups;
    }

    /**
     * {@inheritdoc}
     */
    public function findByUser(User $user)
    {
        //First of all, retrieve wods that user is assigned to
        $wods = $this->wodRepository->findByUser($user);

        $workGroups = [];
        foreach ($wods as $wod) {
            if (!in_array($wod->getWodGroup()->getWorkGroup(), $workGroups)) {
                $workGroups[] = $wod->getWodGroup()->getWorkGroup();
            }
        }

        return $workGroups;
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserMember(User $user, array $orderBy = null, $count = null, $include_deleted = false)
    {
        //$user = TestData::getUsers()[$user->getId()];

        $result = [];

        //User is directly member of the workGroup
        foreach ($user->getMemberWorkGroups() as $workGroup) {
            if (!array_key_exists($workGroup->getId(), $result)) {
                $result[$workGroup->getId()] = $workGroup;
            }
        }

        //User is member of a group that is member of the workGroup...
        foreach ($user->getGroups() as $group) {
            foreach ($group->getMemberWorkGroups() as $workGroup) {
                if (!array_key_exists($workGroup->getId(), $result)) {
                    $result[$workGroup->getId()] = $workGroup;
                }
            }
        }

        $result = array_values($result);

        if (!$include_deleted) {
            $result = $this->excludeDeletedOnes($result);
        }

        $orderBy = $orderBy
            ? $orderBy
            : ['id' => 'desc'];

        $this->orderByElements($result, $orderBy);

        return $count
            ? count($result)
            : $result;
    }

    /**
     * {@inheritdoc}
     */
    public function findOrdered($orderBy = null)
    {
        //Starts with non deleted workGroups
        $workGroups = $this->excludeDeletedOnes(TestData::getWorkGroups());

        //If we must order...
        if ($orderBy) {
            $this->orderByElements($workGroups, $orderBy);
        }

        return $workGroups;
    }

    /**
     * {@inheritdoc
     */
    public function countAll($include_deleted = false)
    {
        if ($include_deleted) {
            return count(TestData::getWorkGroups());
        } else {
            return count($this->excludeDeletedOnes(TestData::getWorkGroups()));
        }
    }

    /**
     * {@inheritdoc
     */
    public function findByEnterprise(Enterprise $enterprise, array $orderBy = null)
    {
        //Navigate till enterprise...
        $workGroups = [];
        foreach ($this->excludeDeletedOnes(TestData::getWorkGroups()) as $workGroup) {
            if ($workGroup->getEnterprise()->getId() == $enterprise->getId()) {
                $workGroups[] = $workGroup;
            }
        }

        //If we must order...
        if ($orderBy) {
            $this->orderByElements($workGroups, $orderBy);
        }


        return $workGroups;
    }

    /**
     * {@inheritdoc}
     */
    public function countAllByEnterprise(Enterprise $enterprise)
    {
        return count($this->findByEnterprise($enterprise, null));
    }


    /**
     * {@inheritdoc}
     */
    public function findByEnterpriseOrdered(Enterprise $enterprise)
    {
        // Vicente: i don't understand what this method it's supossed to do
        // and his logic differs from the other methods logic, that's why i throw
        // a not implemented Exception
        throw new \Exception('Not implemented');
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Excludes from an array the deleted ones
     * @param array $elements
     * @return array
     */
    private function excludeDeletedOnes(array $elements)
    {
        $result = [];

        foreach ($elements as $wod) {
            if (!$wod->getDeleted()) {
                $result[] = $wod;
            }
        }

        return $result;
    }

    /**
     * Order by wods
     * @param array $elements
     * @param array $orderBys
     */
    private function orderByElements(array &$elements, array $orderBys)
    {
        foreach ($orderBys as $orderByKey => $orderByValue) {

            $callback = function (WorkGroup $item1, WorkGroup $item2) use ($orderByKey, $orderByValue) {

                switch($orderByKey)
                {
                    case 'id':
                        return $orderByValue == 'asc'
                            ? $item1->getId() - $item2->getId()
                            : $item2->getId() - $item1->getId();

                    case 'name':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getName(), $item2->getName())
                            : strcmp($item2->getName(), $item1->getName());
                }
            };

            usort($elements, $callback);
        }
    }
}
