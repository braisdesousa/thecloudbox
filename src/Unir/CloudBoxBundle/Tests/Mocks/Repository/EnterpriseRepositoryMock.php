<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 18/08/14
 * Time: 18:56
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Repository;

use Unir\CloudBoxBundle\Repository\EnterpriseRepository;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

class EnterpriseRepositoryMock extends EnterpriseRepository
{

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {
        TestData::initialize();
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * OVERRIDES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function findAll($include_deleted = false, $count = null)
    {
        $result = TestData::getEnterprises();

        if (!$include_deleted) {
            $result = $this->excludeDeletedOnes($result);
        }

        return $count
            ? count($result)
            : $result;
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Excludes from an array the deleted ones
     * @param array $elements
     * @return array
     */
    private function excludeDeletedOnes(array $elements)
    {
        $result = [];

        foreach ($elements as $wod) {
            if (!$wod->getDeleted()) {
                $result[] = $wod;
            }
        }

        return $result;
    }

    /**
     * Order by wods
     * @param array $elements
     * @param array $orderBys
     */
    private function orderByElements(array &$elements, array $orderBys)
    {
        foreach ($orderBys as $orderByKey => $orderByValue) {

            $callback = function (WorkGroup $item1, WorkGroup $item2) use ($orderByKey, $orderByValue) {

                switch($orderByKey)
                {
                    case 'id':
                        return $orderByValue == 'asc'
                            ? $item1->getId() - $item2->getId()
                            : $item2->getId() - $item1->getId();

                    case 'name':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getName(), $item2->getName())
                            : strcmp($item2->getName(), $item1->getName());
                }
            };

            usort($elements, $callback);
        }
    }
}
