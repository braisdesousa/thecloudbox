<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 19/08/14
 * Time: 10:24
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Repository;

use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Repository\WodActivityRepository;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

class WodActivityRepositoryMock extends WodActivityRepository
{

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {

    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * OVERRIDES
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function findByUser(User $user, array $orderBy = null, $count = false)
    {
        $result = [];

        foreach (TestData::getWodActivities() as $wodActivity) {

            if ($wodActivity->getUser()->getId() == $user->getId()) {
                $result[] = $wodActivity;
                continue;
            }

            if ($wodActivity->getWod()->getCreationUser()->getId() == $user->getId()) {
                $result[] = $wodActivity;
                continue;
            }
        }

        if ($orderBy) {
            $this->orderByElements($result, $orderBy);
        }

        return $count
            ? count($result)
            : $result;
    }


    /**
     * {@inheritdoc}
     */
    public function findByUserRelated(User $user, array $orderBy = null, $count = null)
    {
        $result = [];

        foreach (TestData::getWodActivities() as $wodActivity) {

            $wod = $wodActivity->getWod();

            if ($wod->getDeleted()) {
                continue;
            }

            if ($wod->getCreationUser()->getId() == $user->getId()) {
                $result[] = $wodActivity;
                continue;
            }

            $found = false;

            foreach ($wod->getUsersAssigned() as $userAssigned) {
                if ($userAssigned->getId() == $user->getId()) {
                    $result[] = $wodActivity;
                    $found = true;
                    break;
                }
            }

            if ($found) {
                continue;
            }

            foreach ($wod->getUsersFollower() as $userFollower) {
                if ($userFollower->getId() == $user->getId()) {
                    $result[] = $wodActivity;
                    $found = true;
                    break;
                }
            }

            if ($found) {
                continue;
            }
        }


        if ($orderBy) {
            $this->orderByElements($result, $orderBy);
        }

        return $count
            ? count($result)
            : $result;
    }





    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Excludes from an array the deleted ones
     * @param array $elements
     * @return array
     */
    private function excludeDeletedOnes(array $elements)
    {
        $result = [];

        foreach ($elements as $wod) {
            if (!$wod->getDeleted()) {
                $result[] = $wod;
            }
        }

        return $result;
    }

    /**
     * Order by wods
     * @param array $elements
     * @param array $orderBys
     */
    private function orderByElements(array &$elements, array $orderBys)
    {
        foreach ($orderBys as $orderByKey => $orderByValue) {

            $callback = function (WodActivity $item1, WodActivity $item2) use ($orderByKey, $orderByValue) {

                switch($orderByKey)
                {
                    case 'id':
                        return $orderByValue == 'asc'
                            ? $item1->getId() - $item2->getId()
                            : $item2->getId() - $item1->getId();

                    case 'description':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getDescription(), $item2->getDescription())
                            : strcmp($item2->getDescription(), $item1->getDescription());
                }
            };

            usort($elements, $callback);
        }
    }
} 