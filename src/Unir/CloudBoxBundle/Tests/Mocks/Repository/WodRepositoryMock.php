<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 14/08/14
 * Time: 9:48
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PhpCollection\CollectionInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Repository\WodRepository;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Service\CommonService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\ContainerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;

/**
 * Class WodRepositoryMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Repository
 */
class WodRepositoryMock extends WodRepository
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     */
    public function __construct()
    {
        TestData::initialize();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * OVERRIDES
     * -----------------------------------------------------------------------------------------------------------------
     */

    // RETRIEVE ALL
    //------------------------------------------------------------------------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function findAll(
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        return $this->getResult(TestData::getWods(), $status, $difficulty, $orderBy, $count, $include_deleted);
    }


    // BY USER GROUPS
    //------------------------------------------------------------------------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function findByUserGroup(
        Group $group,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {

        //First we collect all WodGroups that have this group...
        $wodGroups = [];
        foreach (TestData::getWodGroups() as $wodGroup) {
            foreach ($wodGroup->getGroupMembers() as $groupMember) {
                if ($groupMember->getId() == $group->getId()) {
                    $wodGroups[] = $wodGroup;
                }
            }
        }

        //No we extract wods that are contained in wodGroups
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            foreach ($wodGroups as $group) {
                if ($group->getId() == $wod->getWodGroup()->getId()) {
                    $wods[] = $wod;
                }
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }

    /**
     * {@inheritdoc}
     */
    public function findByUserGroups(
        Collection $groups,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        //First we collect all WodGroups that have any of this groups
        $wodGroups = [];
        foreach (TestData::getWodGroups() as $wodGroup) {
            foreach ($wodGroup->getGroupMembers() as $groupMember) {
                foreach ($groups as $group) {
                    if ($groupMember->getId() == $group->getId()) {
                        $wodGroups[] = $wodGroup;
                    }
                }
            }
        }

        //No we extract wods that are contained in wodGroups
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            foreach ($wodGroups as $group) {
                if ($group->getId() == $wod->getWodGroup()->getId()) {
                    $wods[] = $wod;
                }
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }

    // BY TASK GROUPS
    //------------------------------------------------------------------------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function findByWodGroup(
        WodGroup $wodGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        //We extract wods that are in WodGroup
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            if ($wod->getWodGroup()->getId() == $wodGroup->getId()) {
                $wods[] = $wod;
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }

    /**
     * {@inheritdoc}
     */
    public function findByWodGroups(
        Collection $groups,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        //We extract wods that are in the wodsgroups and user can view em
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            foreach ($groups as $wodGroup) {
                if ($wod->getWodGroup()->getId() == $wodGroup->getId()) {
                    $wods[] = $wod;
                }
            }
        }
        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }

    /**
     * {@inheritdoc}
     */
    public function findByWodGroupAndUser(
        User $user,
        WodGroup $wodGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        $user = TestData::getUsers()[$user->getId()];

        //We extract wods that are in WodGroup and user can view em
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            if (
                $wod->getWodGroup()->getId() == $wodGroup->getId() &&
                $this->canUserShowWod($user, $wod)
            ) {
                $wods[] = $wod;
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }

    /**
     * {@inheritdoc}
     */
    public function findByWodGroupsAndUser(
        User $user,
        Collection $wodGroups,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {

        $user = TestData::getUsers()[$user->getId()];

        //We extract wods that are in the wodsgroups and user can view em
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            foreach ($wodGroups as $wodGroup) {
                if (
                    $wod->getWodGroup()->getId() == $wodGroup->getId() &&
                    $this->canUserShowWod($user, $wod)
                ) {
                    $wods[] = $wod;
                }
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }


    // BY ENTERPRISE
    //------------------------------------------------------------------------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function findByEnterprise(
        Enterprise $enterprise,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        //Just navigate till enterprise and see if its same and return
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            if ($wod->getWodGroup()->getWorkGroup()->getEnterprise()->getId() == $enterprise->getId()) {
                $wods[] = $wod;
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }

    /**
     * {@inheritdoc}
     */
    public function findByEnterpriseAndUser(
        User $user,
        Enterprise $enterprise,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {

        $user = TestData::getUsers()[$user->getId()];
        $enterprise = TestData::getEnterprises()[$enterprise->getId()];

        //Just navigate till enterprise and see if its same and is user visible
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            if (
                $wod->getWodGroup()->getWorkGroup()->getEnterprise()->getId() == $enterprise->getId() &&
                $this->canUserShowWod($user, $wod)
            ) {
                $wods[] = $wod;
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }

    // BY PROJECT
    //------------------------------------------------------------------------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function findByWorkGroup(
        WorkGroup $workGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        //Just navigate till workGroup and test if are same
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            if ($wod->getWodGroup()->getWorkGroup()->getId() == $workGroup->getId()) {
                $wods[] = $wod;
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }

    /**
     * {@inheritdoc}
     */
    public function findByWorkGroupAndUser(
        User $user,
        WorkGroup $workGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {

        $user = TestData::getUsers()[$user->getId()];
        $workGroup = TestData::getWorkGroups()[$workGroup->getId()];

        //Just navigate till workGroup and test if are same and user can view em
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            if (
                $wod->getWodGroup()->getWorkGroup()->getId() == $workGroup->getId() &&
                $this->canUserShowWod($user, $wod)
            ) {
                $wods[] = $wod;
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }


    // BY USER
    //------------------------------------------------------------------------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function findByUser(
        User $user,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = false,
        $include_deleted = false
    ) {
        $user = TestData::getUsers()[$user->getId()];

        //Just test if user can see the wod
        $wods = [];
        foreach (TestData::getWods() as $wod) {
            if ($this->canUserShowWod($user, $wod)) {
                $wods[] = $wod;
            }
        }

        return $this->getResult($wods, $status, $difficulty, $orderBy, $count, $include_deleted);
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * gets the result of elements just filtering...
     * @param array $elements
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|int
     */
    private function getResult(
        array $elements,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        if ($status && is_array($status)) {
            $elements = $this->filterByStatus($elements, $status);
        }

        if ($difficulty && is_array($difficulty)) {
            $elements = $this->filterByDifficulty($elements, $difficulty);
        }

        if (!$include_deleted) {
            $elements = $this->excludeDeletedOnes($elements);
        }

        if ($orderBy && is_array($orderBy)) {
            $this->orderByElements($elements, $orderBy);
        }


        return $count
            ? count($elements)
            : $elements;
    }

    /**
     * Filters an array of elements by an array of statuses
     * @param array $elements
     * @param array $statuses
     * @return array
     */
    private function filterByStatus(array $elements, array $statuses)
    {
        $result = [];

        foreach ($elements as $wod) {
            foreach ($statuses as $status) {
                if ($wod->getStatus() == $status) {
                    $result[] = $wod;
                }
            }
        }

        return $result;
    }

    /**
     * Filters array of elements by difficulty
     * @param array $elements
     * @param array $difficulties
     * @return array
     */
    private function filterByDifficulty(array $elements, array $difficulties)
    {
        $result = [];

        foreach ($elements as $wod) {
            foreach ($difficulties as $difficulty) {
                if ($wod->getDifficulty() == $difficulty) {
                    $result[] = $wod;
                }
            }
        }

        return $result;
    }

    /**
     * Excludes from an array the deleted ones
     * @param array $elements
     * @return array
     */
    private function excludeDeletedOnes(array $elements)
    {
        $result = [];

        foreach ($elements as $wod) {
            if (!$wod->getDeleted()) {
                $result[] = $wod;
            }
        }

        return $result;
    }

    /**
     * Order by wods
     * @param array $elements
     * @param array $orderBys
     */
    private function orderByElements(array &$elements, array $orderBys)
    {
        foreach ($orderBys as $orderByKey => $orderByValue) {

            $callback = function (Wod $item1, Wod $item2) use ($orderByKey, $orderByValue) {

                switch($orderByKey)
                {
                    case 'id':
                        return $orderByValue == 'asc'
                            ? $item1->getId() - $item2->getId()
                            : $item2->getId() - $item1->getId();

                    case 'title':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getTitle(), $item2->getTitle())
                            : strcmp($item2->getTitle(), $item1->getTitle());

                    case 'description':
                        return $orderByValue == 'asc'
                            ? strcmp($item1->getDescription(), $item2->getDescription())
                            : strcmp($item2->getDescription(), $item1->getDescription());
                }
            };

            usort($elements, $callback);
        }
    }

    /**
     * tests if an user can show some wod
     * @param User $user
     * @param Wod $wod
     * @return boolean
     */
    private function canUserShowWod(User $user, Wod $wod)
    {
        $em = new EntityManagerMock();
        $container = new ContainerMock([
            'security.context' => new SecurityContextMock($user, true, [])
        ]);

        $commonService = new CommonService($em, $container);

        if (!$user->getEnterprise()) {
            return false;
        }

        return $commonService->isAllowToView($user, $wod);

//        //Extract wod workGroup
//        $workGroup = $wod->getWodGroup()->getWorkGroup();
//
//        //Navigate till workGroup members to see if user is member
//        foreach ($workGroup->getUserMembers() as $userMember) {
//            //IF is member...
//            if ($userMember->getId() == $user->getId()) {
//
//                //If the wod can be viewed by all and the group can be viewd by all
//                if (
//                    $wod->getAvailableViewers() == Wod::VIEW_BY_ALL &&
//                    $wod->getWodGroup()->getAvailableViewers() == WodGroup::VIEW_BY_ALL
//                ) {
//                    return true;
//                }
//            }
//        }
//
//        //Now test if user is member of the workGroup by the workGroup's groups
//        foreach ($workGroup->getGroupMembers() as $group) {
//            foreach ($group->getUsers() as $userMember) {
//                //IF is member...
//                if ($userMember->getId() == $user->getId()) {
//
//                    //If the wod can be viewed by all and the group can be viewd by all
//                    if (
//                        $wod->getAvailableViewers() == Wod::VIEW_BY_ALL &&
//                        $wod->getWodGroup()->getAvailableViewers() == WodGroup::VIEW_BY_ALL
//                    ) {
//                        return true;
//                    }
//                }
//            }
//        }
//
//        //If the user is wodCreator, then return true
//        if ($wod->getCreationUser()->getId() == $user->getId()) {
//            return true;
//        }
//
//        //If user is assigned to the wod, then check visibility
//        foreach ($wod->getUsersAssigned() as $assignedUser) {
//            if ($assignedUser->getId() == $user->getId()) {
//                return true;
//            }
//        }
//
//
//        //If user is follower, then return true
//        foreach ($wod->getUsersFollower() as $followerUser) {
//            if ($followerUser->getId() == $user->getId()) {
//                return true;
//            }
//        }
//
//        //If user is member of the wodGroup then return true
//        foreach ($wod->getWodGroup()->getGroupMembers() as $groupMember) {
//            if ($groupMember->getId() == $user->getId()) {
//                return true;
//            }
//        }
//
//        //If user is follower of the group, return true
//        foreach ($wod->getWodGroup()->getUserFollowers() as $followerUser) {
//            if ($followerUser->getId() == $user->getId()) {
//                return true;
//            }
//        }
    }
}
