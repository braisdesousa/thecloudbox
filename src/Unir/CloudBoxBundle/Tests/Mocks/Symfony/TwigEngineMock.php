<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 1/08/14
 * Time: 13:49
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\Response;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\ResponseMock;

/**
 * Mocks TwigEngine
 * Class TwigEngineMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class TwigEngineMock extends TwigEngine
{

    protected $name;
    protected $parameters;
    protected $response;

    /**
     * Empty constructor
     */
    public function __construct($response = null)
    {
        if($response) $this->response = $response;
    }

    /**
     * {@inheritdoc}
     */
    public function render($name, array $parameters = array())
    {
        $this->name = $name;
        $this->parameters = $parameters;


        return $this->response;;
    }

    /**
     * {@inheritdoc}
     */
    public function renderResponse($view, array $parameters = array(), Response $response = null)
    {
        if($response !== null){
            $response = new ResponseMock($this, $response);
        } else {
            $response = new ResponseMock($this);
        }

        $response->setContent($this->render($view, $parameters));

        return $response;
    }


    public function getName()
    {
        return $this->name;
    }

    public function getParameters()
    {
        return $this->parameters;
    }
} 