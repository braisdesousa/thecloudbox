<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 1/08/14
 * Time: 9:22
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;


use Symfony\Component\Routing\Router;

/**
 * Mock for router
 * Class RouterMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class RouterMock extends Router
{

    protected $routes;


    /**
     * constructor
     * @param null $routes
     */
    public function __construct($routes = null)
    {
        $this->routes = [];

        if($routes != null) {
            $this->setRoutes($routes);
        }
    }

    /**
     * sets routes values
     * @param $routes
     */
    public function setRoutes($routes)
    {
        foreach($routes as $name => $value)
        {
            $this->set($name, $value);
        }
    }

    /**
     * sets a route value
     * @param $name
     * @param $value
     * @return $this
     */
    public function set($name, $value)
    {
        if($name === null) {
            return $this;
        }

        if($this->has($name) && $value == null)
        {
            unset($this->routes[$name]);
        } else {
            $this->routes[$name] = $value;
        }

        return $this;
    }

    /**
     * This router contains a route ?
     * @param $name
     * @return bool
     */
    public function has($name)
    {
        return array_key_exists($name, $this->routes);
    }

    /**
     * {@inheritdoc}
     */
    public function generate($name, $parameters = array(), $referenceType = false)
    {
        if($this->has($name)) {
            return $this->routes[$name];
        }

        return $name;
    }
} 