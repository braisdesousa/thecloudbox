<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 4/08/14
 * Time: 12:05
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormView;
use Symfony\Component\Locale\Exception\NotImplementedException;

use Unir\CloudBoxBundle\Tests\Mocks\Symfony\FormFactoryMock;

/**
 * Mocks FormInterface
 * Class FormMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class FormMock implements \IteratorAggregate, FormInterface
{
    /**
     * @var FormFactoryMock
     */
    protected $factoryMock;


    /**
     * constructor
     * @param FormFactoryMock $factoryMock
     */
    public function __construct(FormFactoryMock $factoryMock)
    {
        $this->factoryMock = $factoryMock;
    }

    /**
     * {@inheritdoc}
     */
    public function setParent(FormInterface $parent = null)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function add($child, $type = null, array $options = array())
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getErrors($deep = false, $flatten = true)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function setData($modelData)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getNormData()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getViewData()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getExtraData()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function isSubmitted()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getPropertyPath()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function addError(FormError $error)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function isValid()
    {
        if($this->factoryMock->getConfig()->has('isValid')){
            return $this->factoryMock->getConfig()->get('isValid');
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function isRequired()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function isDisabled()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function isSynchronized()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function initialize()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function handleRequest($request = null)
    {
        if($this->factoryMock->getConfig()->has('handleRequestFunc')){
            $func =  $this->factoryMock->getConfig()->get('handleRequestFunc');
            $func($this->factoryMock->getData(), $request);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function submit($submittedData, $clearMissing = true)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getRoot()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function isRoot()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function createView(FormView $parent = null)
    {
        return new FormView($parent);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->add($value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }
} 