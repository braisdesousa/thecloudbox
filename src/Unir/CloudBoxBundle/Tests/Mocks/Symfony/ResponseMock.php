<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 5/08/14
 * Time: 8:27
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Unir\CloudBoxBundle\Tests\Mocks\Symfony\TwigEngineMock;
use Symfony\Component\HttpFoundation\Response;

/**
 * Mock of a response
 * Class ResponseMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class ResponseMock extends Response
{
    /**
     * @var TwigEngineMock
     */
    protected $twigEngine;

    /**
     * constructor
     * @param TwigEngineMock $twigEngine
     * @param Response $parentResponse
     */
    public function __construct(TwigEngineMock $twigEngine = null,Response $parentResponse = null)
    {
        $this->twigEngineMock = $twigEngine;

        if($parentResponse !== null){
            $this->setParentResponse($parentResponse);
        }
    }

    /**
     * Sets the parent response
     * @param Response $parentResponse
     * @return Response this
     */
    public function setParentResponse(Response $parentResponse)
    {
        $this->headers = $parentResponse->headers;
        $this->content = $parentResponse->content;
        $this->version = $parentResponse->version;
        $this->statusCode = $parentResponse->statusCode;
        $this->charset = $parentResponse->charset;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTwigEngine()
    {
        return $this->twigEngineMock;
    }

    /**
     * @param TwigEngineMock $twigEngine
     * @return $this
     */
    public function setTwigEngine(TwigEngineMock $twigEngine)
    {
        $this->twigEngineMock = $twigEngine;
        return $this;
    }
} 