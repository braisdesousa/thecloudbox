<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 1/08/14
 * Time: 9:45
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;


/**
 * Mocks for registryMock the purpose is be capable to do getManager
 *
 * Class RegistryMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class RegistryMock
{

    protected $manager;

    /**
     * Constructor
     * @param null $manager
     */
    public function __construct($manager = null)
    {
        $this->manager = $manager;
    }

    /**
     * Sets the manager
     * @param $manager
     * @return $this
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * Gets the manager
     * @return null
     */
    public function getManager()
    {
        return $this->manager;
    }
} 