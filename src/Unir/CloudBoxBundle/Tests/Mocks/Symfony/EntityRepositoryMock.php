<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 31/07/14
 * Time: 13:01
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Doctrine\ORM\EntityRepository;

/**
 * Mocks basic repository
 * Class EntityRepositoryMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class EntityRepositoryMock extends EntityRepository
{

    protected $map;

    /**
     * Constructor
     * @param null $map
     */
    public function __construct($map = null){

        if(!$map) {
            $this->map = array();
        } else {
            $this->map = $map;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function find($id, $lockMode = 0, $lockVersion = null)
    {
        if(array_key_exists($id, $this->map)){
            return $this->map[$id];
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll()
    {
        return array_values($this->map);
    }

} 