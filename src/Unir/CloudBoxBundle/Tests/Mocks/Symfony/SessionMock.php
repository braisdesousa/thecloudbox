<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 31/07/14
 * Time: 12:50
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Mocks of session
 * Class SessionMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class SessionMock extends Session
{

    protected $map;

    /**
     * Constructor
     * @param null $map
     */
    public function __construct($map = null)
    {
        if ($map) {
            $this->map = $map;
        } else {
            $this->map = array();
        }

    }

    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
        return array_key_exists($name, $this->map);
    }

    /**
     * {@inheritdoc}
     */
    public function get($name, $default = null)
    {
        if (!$this->has($name)) {
            return $default;
        } else {
            return $this->map[$name];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function set($name, $value)
    {
        //ISI as hell
        $this->map[$name] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
        if (!$this->has($name)) {
            return;
        }

        unset($this->map[$name]);
    }


}
