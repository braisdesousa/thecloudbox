<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 31/07/14
 * Time: 13:29
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Unir\CloudBoxBundle\Entity\User;

/**
 * Class AbstractTokenMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class AbstractTokenMock extends AbstractToken
{
    protected $user;

    /**
     * Constructor
     * @param null $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {

    }
} 