<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 5/08/14
 * Time: 12:51
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ScopeInterface;

/**
 * Class ContainerMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class ContainerMock extends Container
{
    protected $services;
    protected $parameters;
    protected $isFrozen = false;
    protected $scopedServices;

    /**
     * Constructor
     * @param array $services
     * @param array $parameters
     */
    public function __construct(array $services = null, array $parameters = null)
    {
        $this->services = [];
        $this->parameters = [];
        $this->scopedServices = [];

        if($services) {
            $this->setServices($services);
        }

        if($parameters) {
            $this->setParameters($parameters);
        }

    }

    /**
     * sets this container services
     * @param $services
     * @return $this
     */
    public function setServices(array $services)
    {
        foreach($services as $id => $service) {
            $this->set($id, $service);
        }

        return $this;
    }

    /**
     * sets this container parameteres
     * @param $parameters
     * @return $this
     */
    public function setParameters(array $parameters)
    {
        foreach($parameters as $name => $value) {
            $this->setParameter($name, $value);
        }

        return $this;
    }

    /**
     * Sets the value for isFrozen method for mock purposes
     * @param $isFrozen
     * @return $this
     */
    public function setIsFrozen($isFrozen)
    {
        $this->isFrozen = $isFrozen;
        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function compile()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function isFrozen()
    {
        return $this->isFrozen;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameterBag()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getParameter($name)
    {
        if($this->hasParameter($name)) {
            return $this->parameters[$name];
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function hasParameter($name)
    {
        return array_key_exists($name, $this->parameters);
    }

    /**
     * {@inheritdoc}
     */
    public function setParameter($name, $value)
    {
        if($name === null) return;

        if($this->hasParameter($name) && $value === null) {
            unset($this->parameters[$name]);
        } else {
            $this->parameters[$name] = $value;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function set($id, $service, $scope = self::SCOPE_CONTAINER)
    {
        if($id === null) {
            return;
        }

        $id = strtolower($id);


        if($this->has($id) && $service === null) {
            unset($this->services[$id]);
        } else {
            $this->services[$id] = $service;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function has($id)
    {
        return array_key_exists(strtolower($id), $this->services);
    }

    /**
     * {@inheritdoc}
     */
    public function get($id, $invalidBehavior = self::EXCEPTION_ON_INVALID_REFERENCE)
    {
        $id = strtolower($id);

        if($this->has($id)){
            return $this->services[$id];
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function initialized($id)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getServiceIds()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function enterScope($name)
    {
        if(!$this->isScopeActive($name)){
            $this->scopedServices[] = $name;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function leaveScope($name)
    {
        if($this->isScopeActive($name)){
            $this->scopedServices = array_diff($this->scopedServices, [$name]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addScope(ScopeInterface $scope)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function hasScope($name)
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function isScopeActive($name)
    {
        return in_array($name, $this->scopedServices);
    }
} 