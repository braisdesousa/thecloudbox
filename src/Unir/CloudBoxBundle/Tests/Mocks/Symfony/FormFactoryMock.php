<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 4/08/14
 * Time: 11:42
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

use Unir\CloudBoxBundle\Tests\Mocks\Symfony\FormMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\FormBuilderMock;

/**
 * Class FormFactoryMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class FormFactoryMock implements FormFactoryInterface
{
    protected $type;
    protected $data;
    protected $options;

    protected $config;

    /**
     * Constructor
     * @param array $options
     */
    public function __construct(array $config = array())
    {
        $this->config = new ParameterBag($config);
    }

    /**
     * {@inheritdoc}
     */
    public function create($type = 'form', $data = null, array $options = array())
    {
        $this->type = $type;
        $this->data = $data;
        $this->options = $options;

        return new FormMock($this);
    }

    /**
     * {@inheritdoc}
     */
    public function createNamed($name, $type = 'form', $data = null, array $options = array())
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function createForProperty($class, $property, $data = null, array $options = array())
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function createBuilder($type = 'form', $data = null, array $options = array())
    {
        return new FormBuilderMock($this);
    }

    /**
     * {@inheritdoc}
     */
    public function createNamedBuilder($name, $type = 'form', $data = null, array $options = array())
    {
        throw new \Exception('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function createBuilderForProperty($class, $property, $data = null, array $options = array())
    {
        throw new \Exception('Not implemented');
    }

    /**
     * Retrieves data object
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Retrieves options
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Returns the configuration
     * @return ParameterBag
     */
    public function getConfig()
    {
        return $this->config;
    }
}