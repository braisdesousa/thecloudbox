<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 31/07/14
 * Time: 13:28
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\AbstractTokenMock;
use Unir\CloudBoxBundle\Entity\User;

/**
 * Class SecurityContextMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class SecurityContextMock implements SecurityContextInterface
{

    protected $token;
    protected $isAuthenticated;
    protected $roles;

    public function __construct($user = null, $isAuthenticated = true, $roles = null)
    {
        $this->token = new AbstractTokenMock($user);
        $this->isAuthenticated = $isAuthenticated;

        if(!$roles){
            $this->roles = array();
        } else {
            $this->roles = $roles;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public function setToken(TokenInterface $token = null){
        $this->token = $token;
    }

    /**
     * {@inheritdoc}
     */
    public function isGranted($attributes, $object = null){

        if(!is_array($attributes)){
            $attributes = array($attributes);
        }

        foreach($attributes as $attr) {

            //If attribute is IS_AUTHENTICATED
            if($attr === 'IS_AUTHENTICATED_REMEMBERED') {
                //If not is authenticated, return false
                if(!$this->isAuthenticated) {
                    return false;
                }
            } //If element is not in array, return false
            else if(!in_array($attr, $this->roles)) {
                return false;
            }

        }

        //At last, return true;
        return true;
    }
}