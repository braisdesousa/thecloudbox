<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 5/08/14
 * Time: 12:01
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Mocks of entityManager
 * Class EntityManagerMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class EntityManagerMock extends EntityManager
{

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    protected $map;
    protected $persistedEntities;
    protected $removedEntities;
    protected $unitOfWork;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * COSNTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Constructor
     * @param null $map
     */
    public function __construct($map = null)
    {
        if ($map) {
            $this->map = $map;
        } else {
            $this->map = array();
        }

        $this->persistedEntities = array();
        $this->removedEntities = array();
        $this->unitOfWork = new UnitOfWorkMock($this);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PUBLIC METHODS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Retrieves persistedEntities
     * @return array
     */
    public function getPersistedEntities()
    {
        return $this->persistedEntities;
    }

    /**
     * @return mixedRetrieves removedEntities
     */
    public function getRemovedEntities()
    {
        return $this->removedEntities;
    }

    /**
     * Sets a repository...
     * @param $entityName
     * @param $repository
     * @return $this
     */
    public function setRepository($entityName, $repository)
    {
        $this->map[$entityName] = $repository;

        return $this;
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * OVERRIDES
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * {@inheritdoc}
     */
    public function getConnection()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadataFactory()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressionBuilder()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function beginTransaction()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function transactional($func)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function commit()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function rollback()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getClassMetadata($className)
    {
        //TODO Doesn't return nothing
        return new ClassMetadata($className);
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($dql = '')
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function createNamedQuery($name)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function createNativeQuery($sql, ResultSetMapping $rsm)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function createNamedNativeQuery($name)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function createQueryBuilder()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function flush($entity = null)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function find($entityName, $id, $lockMode = LockMode::NONE, $lockVersion = null)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getReference($entityName, $id)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function clear($entityName = null)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function persist($entity)
    {
        $this->persistedEntities[] = $entity;
    }

    /**
     * {@inheritdoc}
     */
    public function remove($entity)
    {
        $this->removedEntities[] = $entity;
    }

    /**
     * {@inheritdoc}
     */
    public function refresh($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function detach($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function merge($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function copy($entity, $deep = false)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository($entityName)
    {
        if (array_key_exists($entityName, $this->map)) {
            return $this->map[$entityName];
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function contains($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getEventManager()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getConfiguration()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    private function errorIfClosed()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isOpen()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getUnitOfWork()
    {
        return $this->unitOfWork;
    }

    /**
     * {@inheritdoc}
     */
    public function getHydrator($hydrationMode)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function newHydrator($hydrationMode)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getProxyFactory()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function initializeObject($obj)
    {
        throw new \Exception("Not implemented");
    }
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isFiltersStateClean()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function hasFilters()
    {
        throw new \Exception("Not implemented");
    }
}
