<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 1/08/14
 * Time: 13:48
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;


/**
 * Mocks swift mailer for doesn't send messages, just store em
 * Class Swift_MailerMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class SwiftMailerMock extends \Swift_Mailer
{
    /**
     * @var \Swift_Mime_Message sendedMessage
     */
    protected $message;

    /**
     * Empty constructor
     */
    public function __construct()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function send(\Swift_Mime_Message $message, &$failedRecipients = null)
    {
        $this->message = $message;
    }


    /**
     * Returns message sended
     * @return \Swift_Mime_Message
     */
    public function getSendedMessage()
    {
        return $this->message;
    }
} 