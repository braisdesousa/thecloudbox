<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 5/08/14
 * Time: 11:07
 */

namespace Unir\CloudBoxBundle\Tests\Mocks\Symfony;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\UnitOfWork;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\PersistentCollection;

/**
 * Mocks unit of work
 * Class UnitOfWorkMock
 * @package Unir\CloudBoxBundle\Tests\Mocks\Symfony
 */
class UnitOfWorkMock extends UnitOfWork
{

    protected $manager;
    protected $entityInsertions;
    protected $entityUpdates;
    protected $entityDeletions;
    protected $entityChangeSets;
    protected $entityCollectionUpdates;
    protected $collectionDeletions;

    /**
     * Constructor
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager = null)
    {
        $this->manager = $manager;
        $this->entityInsertions = [];
        $this->entityUpdates = [];
        $this->entityDeletions = [];
        $this->entityChangeSets = [];
        $this->entityCollectionUpdates = [];
        $this->collectionDeletions = [];
    }

    /**
     * Mark an entity like is inserted
     * @param $entity
     * @return $this
     */
    public function markForInsertion($entity)
    {
        $oid = spl_object_hash($entity);
        $this->entityInsertions[$oid] = $entity;
        return $this;
    }

    /**
     * Mark an entity like is updated
     * @param $entity
     * @param $changes
     * @return $this
     */
    public function markForUpdate($entity, $changes)
    {
        $oid = spl_object_hash($entity);
        $this->entityUpdates[$oid] = $entity;
        $this->entityChangeSets[$oid] = $changes;

        return $this;
    }

    /**
     * Mark an entity like is deleted
     * @param $entity
     * @return $this
     */
    public function markForDelete($entity)
    {
        $oid = spl_object_hash($entity);
        $this->entityDeletions[$oid] = $entity;
        return $this;
    }


    /**
     * marks an entity collection as updated
     * @param $entity
     * @param $fieldName
     * @param array $insertedEntities
     * @param array $deletedEntities
     * @return $this
     */
    public function markCollectionUpdate($entity, $fieldName ,array $insertedEntities = array(), array $deletedEntities = array())
    {
        $col = new PersistentCollection($this->manager, $this->manager->getClassMetadata(get_class($entity)), new ArrayCollection());
        $col->setOwner($entity, array(
            'fieldName' => $fieldName,
            'inversedBy' => false,
            'mappedBy' => false,
            'isOwningSide' => false,
            'type' => ClassMetadata::ONE_TO_MANY,
            'orphanRemoval' => false
        ));

        foreach($deletedEntities as $deleted){
            $col->hydrateAdd($deleted);
        }

        $col->takeSnapshot();

        foreach($deletedEntities as $deleted){
            $col->removeElement($deleted);
        }

        foreach($insertedEntities as $inserted){
            $col->add($inserted);
        }

        $this->entityCollectionUpdates[] = $col;


        return $this;
    }

    /**
     * mark a collection as deleted
     * @param $entity
     * @param $fieldName
     * @return $this
     */
    public function markCollectionDelete($entity, $fieldName)
    {
        $col = new PersistentCollection($this->manager, $this->manager->getClassMetadata(get_class($entity)), new ArrayCollection());
        $col->setOwner($entity,array(
            'fieldName' => $fieldName,
            'inversedBy' => false,
            'mappedBy' => false,
            'isOwningSide' => false,
            'type' => ClassMetadata::ONE_TO_MANY,
            'orphanRemoval' => false
        ));

        $this->collectionDeletions[] = $col;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function commit($entity = null)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    private function computeScheduleInsertsChangeSets()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityChangeSet($entity)
    {
        $oid = spl_object_hash($entity);

        if (isset($this->entityChangeSets[$oid])) {
            return $this->entityChangeSets[$oid];
        }

        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function computeChangeSet(ClassMetadata $class, $entity)
    {
        //TODO doesn't do nothing
    }

    /**
     * {@inheritdoc}
     */
    public function computeChangeSets()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function recomputeSingleEntityChangeSet(ClassMetadata $class, $entity)
    {
        //TODO doesn't do nothing
    }

    /**
     * {@inheritdoc}
     */
    public function scheduleForInsert($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isScheduledForInsert($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function scheduleForUpdate($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function scheduleExtraUpdate($entity, array $changeset)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isScheduledForUpdate($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isScheduledForDirtyCheck($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function scheduleForDelete($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isScheduledForDelete($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isEntityScheduled($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function addToIdentityMap($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityState($entity, $assume = null)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function removeFromIdentityMap($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getByIdHash($idHash, $rootClassName)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function tryGetByIdHash($idHash, $rootClassName)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isInIdentityMap($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function containsIdHash($idHash, $rootClassName)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function persist($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function remove($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function merge($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function detach($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getCommitOrderCalculator()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function clear($entityName = null)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function scheduleOrphanRemoval($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function scheduleCollectionDeletion(PersistentCollection $coll)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isCollectionScheduledForDeletion(PersistentCollection $coll)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function createEntity($className, array $data, &$hints = array())
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function triggerEagerLoads()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function loadCollection(PersistentCollection $collection)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentityMap()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getOriginalEntityData($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * @ignore
     *
     * @param object $entity
     * @param array  $data
     *
     * @return void
     */
    public function setOriginalEntityData($entity, array $data)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function setOriginalEntityProperty($oid, $property, $value)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityIdentifier($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getSingleIdentifierValue($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function tryGetById($id, $rootClassName)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function scheduleForDirtyCheck($entity)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function hasPendingInsertions()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function size()
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityPersister($entityName)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getCollectionPersister(array $association)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function registerManaged($entity, array $id, array $data)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function clearEntityChangeSet($oid)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function propertyChanged($entity, $propertyName, $oldValue, $newValue)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function getScheduledEntityInsertions()
    {
        return $this->entityInsertions;
    }

    /**
     * {@inheritdoc}
     */
    public function getScheduledEntityUpdates()
    {
        return $this->entityUpdates;
    }

    /**
     * {@inheritdoc}
     */
    public function getScheduledEntityDeletions()
    {
        return $this->entityDeletions;
    }

    /**
     * {@inheritdoc}
     */
    public function getScheduledCollectionDeletions()
    {
        return $this->collectionDeletions;
    }

    /**
     * {@inheritdoc}
     */
    public function getScheduledCollectionUpdates()
    {
        return $this->entityCollectionUpdates;
    }

    /**
     * {@inheritdoc}
     */
    public function initializeObject($obj)
    {
        throw new \Exception("Not implemented");
    }


    /**
     * {@inheritdoc}
     */
    public function markReadOnly($object)
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function isReadOnly($object)
    {
        throw new \Exception("Not implemented");
    }
} 