<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 11/08/14
 * Time: 11:27
 */

namespace Unir\CloudBoxBundle\Tests\Services\Repository;

use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Service\Repository\WorkGroupRepositoryService;


/**
 * Class WorkGroupRepositoryServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services\Repository
 */
class WorkGroupRepositoryServiceTest extends BaseTestService
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  public function findAll()
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindAll1()
    {
        //Arrage

        $expected = [
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
        ];

        $workGroupRepository = $this->getWorkGroupRepositoryMock();
        $em = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => $workGroupRepository
        ]);
        $securityCtx = new SecurityContextMock(new User(), true,[]);
        $commonService = $this->getCommonServiceMock();


        $workGroupRepository->expects($this->any())
            ->method('findByUserMember')
            ->will($this->returnValue($expected));


        //Act
        $service = new WorkGroupRepositoryService($em, $securityCtx, $commonService);

        $result = $service->findAll();

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindAll2()
    {
        //Arrage

        $expected = [
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
        ];

        $usr = new User();
        $usr->setEnterprise(new Enterprise());

        $workGroupRepository = $this->getWorkGroupRepositoryMock();
        $em = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => $workGroupRepository
        ]);
        $securityCtx = new SecurityContextMock($usr, true,[User::ROLE_ADMIN]);
        $commonService = $this->getCommonServiceMock();


        $workGroupRepository->expects($this->any())
            ->method('findByEnterprise')
            ->will($this->returnValue($expected));


        //Act
        $service = new WorkGroupRepositoryService($em, $securityCtx, $commonService);

        $result = $service->findAll();

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindAll3()
    {
        //Arrage

        $expected = [
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
        ];

        $workGroupRepository = $this->getWorkGroupRepositoryMock();
        $em = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => $workGroupRepository
        ]);
        $securityCtx = new SecurityContextMock(new User(), true,[User::ROLE_OWNER]);
        $commonService = $this->getCommonServiceMock();


        $workGroupRepository->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue($expected));


        //Act
        $service = new WorkGroupRepositoryService($em, $securityCtx, $commonService);

        $result = $service->findAll();

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  public function findByUser(User $user)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUser1()
    {
        //Arrage

        $expected = [
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
        ];

        $usr = new User();
        $usr->setEnterprise(new Enterprise());

        $workGroupRepository = $this->getWorkGroupRepositoryMock();
        $em = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => $workGroupRepository
        ]);
        $securityCtx = new SecurityContextMock(new User(), true,[]);
        $commonService = $this->getCommonServiceMock();


        $workGroupRepository->expects($this->any())
            ->method('findByUser')
            ->will($this->returnValue($expected));


        //Act
        $service = new WorkGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByUser($usr);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByUser2()
    {
        //Arrage

        $expected = [
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
        ];

        $usr = new User();
        $usr->setEnterprise(new Enterprise());
        $usr->addRole(new Role(User::ROLE_ADMIN));

        $workGroupRepository = $this->getWorkGroupRepositoryMock();
        $em = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => $workGroupRepository
        ]);
        $securityCtx = new SecurityContextMock(new User(), true,[]);
        $commonService = $this->getCommonServiceMock();


        $workGroupRepository->expects($this->any())
            ->method('findByEnterprise')
            ->will($this->returnValue($expected));


        //Act
        $service = new WorkGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByUser($usr);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByUser3()
    {
        //Arrage

        $expected = [
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
            new WorkGroup(),
        ];

        $usr = new User();
        $usr->setEnterprise(new Enterprise());
        $usr->addRole(new Role(User::ROLE_OWNER));

        $workGroupRepository = $this->getWorkGroupRepositoryMock();
        $em = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => $workGroupRepository
        ]);
        $securityCtx = new SecurityContextMock(new User(), true, []);
        $commonService = $this->getCommonServiceMock();


        $workGroupRepository->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue($expected));


        //Act
        $service = new WorkGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByUser($usr);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  public function setUserMembers(WorkGroup $workGroup, array $userIds)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetUserMembers1()
    {
        //Arrage

        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $userRepo = $em->getRepository('UnirCloudBoxBundle:User');

        $workGroup = new WorkGroup();
        $workGroup
            ->addUserMember((new User())->setId(1))
            ->addUserMember((new User())->setId(2))
            ->addUserMember((new User())->setId(3));

        $userRepo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new User())->setId(4),
                (new User())->setId(5),
                (new User())->setId(6),
                (new User())->setId(7),
                (new User())->setId(8)
            ));

        //Act
        $service = new WorkGroupRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setUserMembers($workGroup, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($workGroup, $result);
        $this->assertEquals(5, $result->getUserMembers()->count());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  public function setGroupMembers(WorkGroup $workGroup, array $groupIds)
     * -----------------------------------------------------------------------------------------------------------------
     */

    public function testSetGroupMembers1()
    {
        //Arrage

        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $userRepo = $em->getRepository('UnirCloudBoxBundle:Group');

        $workGroup = new WorkGroup();
        $workGroup
            ->addGroupMember((new Group())->setId(1))
            ->addGroupMember((new Group())->setId(2))
            ->addGroupMember((new Group())->setId(3));

        $userRepo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new Group())->setId(4),
                (new Group())->setId(5),
                (new Group())->setId(6),
                (new Group())->setId(7),
                (new Group())->setId(8)
            ));

        //Act
        $service = new WorkGroupRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setGroupMembers($workGroup, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($workGroup, $result);
        $this->assertEquals(5, $result->getGroupMembers()->count());
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  public function createDefault(Enterprise $enterprise)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testCreateDefault1()
    {
        //Arrage
        $loggedUser = new User();
        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock($loggedUser);
        $commonService = $this->getCommonServiceMock();

        $enterprise = new Enterprise();

        //Act
        $service = new WorkGroupRepositoryService($em, $sctCtx, $commonService);
        $workGroup = $service->createDefault($enterprise);

        //Assert
        $this->assertEquals($enterprise, $workGroup->getEnterprise());
        $this->assertEquals('Default WorkGroup', $workGroup->getName());
        $this->assertEquals($loggedUser, $workGroup->getAdminUser());
        $this->assertEquals(1, count($em->getPersistedEntities()));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */
}