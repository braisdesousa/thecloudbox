<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 11/08/14
 * Time: 18:47
 */

namespace Unir\CloudBoxBundle\Tests\Services\Repository;

use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Service\Repository\UserGroupRepositoryService;

/**
 * Class UsersGroupRepositoryServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services\Repository
 */
class UsersGroupRepositoryServiceTest extends BaseTestService
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWorkGroup(WorkGroup $workGroup)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWorkGroup()
    {
        //Arrage
        $expected = [
            new Group(),
            new Group(),
            new Group(),
            new Group(),
            new Group(),
        ];

        $searchWorkGroup = new WorkGroup();

        $loggedUser = new User();

        $em = $this->getEntityManagerMock();
        $securityCtx = new SecurityContextMock($loggedUser, true, []);
        $commonService = $this->getCommonServiceMock();

        $em->getRepository('UnirCloudBoxBundle:Group')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($searchWorkGroup)
            ->will($this->returnValue($expected));

        //Act
        $service = new UserGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByWorkGroup($searchWorkGroup);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function setWorkGroups(Group $group, array $workGroups)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetWorkGroups1()
    {
        //Arrage

        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $workGroupMock = $em->getRepository('UnirCloudBoxBundle:WorkGroup');

        $group = new Group();
        $group
            ->addMemberWorkGroup((new WorkGroup())->setId(1))
            ->addMemberWorkGroup((new WorkGroup())->setId(2))
            ->addMemberWorkGroup((new WorkGroup())->setId(3));

        $workGroupMock
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new WorkGroup())->setId(4),
                (new WorkGroup())->setId(5),
                (new WorkGroup())->setId(6),
                (new WorkGroup())->setId(7),
                (new WorkGroup())->setId(8)
            ));

        //Act
        $service = new UserGroupRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setWorkGroups($group, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($group, $result);
        $this->assertEquals(5, $result->getMemberWorkGroups()->count());
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function setUsers(Group $group, array $users)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetUsers1()
    {
        //Arrage

        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $userRepo = $em->getRepository('UnirCloudBoxBundle:User');

        $group = new Group();
        $group
            ->addUser((new User())->setId(1))
            ->addUser((new User())->setId(2))
            ->addUser((new User())->setId(3));

        $userRepo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new User())->setId(4),
                (new User())->setId(5),
                (new User())->setId(6),
                (new User())->setId(7),
                (new User())->setId(8)
            ));

        //Act
        $service = new UserGroupRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setUsers($group, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($group, $result);
        $this->assertEquals(5, $result->getUsers()->count());
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * private
     * -----------------------------------------------------------------------------------------------------------------
     */
}
