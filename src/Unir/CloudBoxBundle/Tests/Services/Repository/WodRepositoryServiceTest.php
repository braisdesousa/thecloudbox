<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 11/08/14
 * Time: 17:24
 */

namespace Unir\CloudBoxBundle\Tests\Services\Repository;

use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Service\Repository\WodRepositoryService;


/**
 * Class WodRepositoryServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services\Repository
 */
class WodRepositoryServiceTest extends BaseTestService
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUser(User $user,array $status= null, array $difficulty= null, array $orderBy= null, $count= null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUser1()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $searchUser = new User();

        $user = new User();

        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, []);
        $commonService = $this->getCommonServiceMock();

        $wodRepo->expects($this->any())
            ->method('findByUser')
            ->with($searchUser)
            ->will($this->returnValue($expected));

        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByUser($searchUser, null, null, null, null );

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByUser2()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $searchUser = new User();
        $searchUser->addRole(new Role(Role::ROLE_OWNER));

        $searchWorkGroup = new WorkGroup();

        $user = new User();

        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, []);
        $commonService = $this->getCommonServiceMock();

        $wodRepo->expects($this->any())
            ->method('findByWorkGroup')
            ->with($searchWorkGroup)
            ->will($this->returnValue($expected));

        $commonService->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($searchWorkGroup));

        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByUser($searchUser, null, null, null, null );

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByUser3()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $searchUser = new User();
        $searchUser->addRole(new Role(Role::ROLE_ADMIN));
        $searchWorkGroup = new WorkGroup();

        $user = new User();


        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, []);
        $commonService = $this->getCommonServiceMock();

        $wodRepo->expects($this->any())
            ->method('findByWorkGroup')
            ->with($searchWorkGroup)
            ->will($this->returnValue($expected));

        $commonService->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($searchWorkGroup));

        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByUser($searchUser, null, null, null, null );

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function findAll(array $status= null, array $difficulty= null, array $orderBy= null, $count= null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindAll1()
    {

        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $user = new User();

        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, []);
        $commonService = $this->getCommonServiceMock();

        $wodRepo->expects($this->any())
            ->method('findByUser')
            ->with($user)
            ->will($this->returnValue($expected));


        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findAll(null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindAll2()
    {

        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $user = new User();
        $user->setEnterprise(new Enterprise());

        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, [Role::ROLE_ADMIN]);
        $commonService = $this->getCommonServiceMock();

        $wodRepo->expects($this->any())
            ->method('findByEnterprise')
            ->with($user->getEnterprise())
            ->will($this->returnValue($expected));


        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findAll(null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindAll3()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $user = new User();
        $user->setEnterprise(new Enterprise());

        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, [Role::ROLE_OWNER]);
        $commonService = $this->getCommonServiceMock();

        $wodRepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue($expected));


        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findAll(null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByEnterprise(Enterprise $enterprise,array $status= null, array $difficulty= null, array $orderBy= null, $count= null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByEnterprise1()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $searchEnterprise = new Enterprise();

        $user = new User();


        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, []);
        $commonService = $this->getCommonServiceMock();

        $wodRepo->expects($this->any())
            ->method('findByEnterpriseAndUser')
            ->with($user, $searchEnterprise)
            ->will($this->returnValue($expected));


        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByEnterprise($searchEnterprise, null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByEnterprise2()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $searchEnterprise = new Enterprise();

        $user = new User();


        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, [Role::ROLE_ADMIN]);
        $commonService = $this->getCommonServiceMock();

        $wodRepo->expects($this->any())
            ->method('findByEnterprise')
            ->with($searchEnterprise)
            ->will($this->returnValue($expected));


        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByEnterprise($searchEnterprise, null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByGroup(WodGroup $wodGroup,array $status= null, array $difficulty= null, array $orderBy= null, $count= null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByGroup1()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $searchWodGroup = new WodGroup();

        $loggerUser = new User();


        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($loggerUser, true, []);
        $commonService = $this->getCommonServiceMock();


        $wodRepo->expects($this->any())
            ->method('findByWodGroupAndUser')
            ->with($loggerUser, $searchWodGroup)
            ->will($this->returnValue($expected));

        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByGroup($searchWodGroup, null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByGroup2()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $searchWodGroup = new WodGroup();

        $loggerUser = new User();


        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($loggerUser, true, [Role::ROLE_ADMIN]);
        $commonService = $this->getCommonServiceMock();


        $wodRepo->expects($this->any())
            ->method('findByWodGroup')
            ->with($searchWodGroup)
            ->will($this->returnValue($expected));

        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByGroup($searchWodGroup,null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function findByWorkGroup(WorkGroup $workGroup=null,array $status= null, array $difficulty= null, array $orderBy= null, $count= null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWorkGroup1()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $user = new User();
        $selectedWorkGroup = new WorkGroup();


        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, []);
        $commonService = $this->getCommonServiceMock();

        $commonService->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($selectedWorkGroup));


        $wodRepo->expects($this->any())
            ->method('findByWorkGroupAndUser')
            ->with($user, $selectedWorkGroup)
            ->will($this->returnValue($expected));


        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByWorkGroup(null, null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByWorkGroup2()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $user = new User();
        $selectedWorkGroup = new WorkGroup();


        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, [Role::ROLE_ADMIN]);
        $commonService = $this->getCommonServiceMock();

        $commonService->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($selectedWorkGroup));


        $wodRepo->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue($expected));


        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByWorkGroup(null, null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByWorkGroup3()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $searchWorkGroup = new WorkGroup();

        $user = new User();
        $selectedWorkGroup = new WorkGroup();


        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, []);
        $commonService = $this->getCommonServiceMock();

        $commonService->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($selectedWorkGroup));


        $wodRepo->expects($this->any())
            ->method('findByWorkGroupAndUser')
            ->with($user, $searchWorkGroup)
            ->will($this->returnValue($expected));


        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByWorkGroup($searchWorkGroup, null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByWorkGroup4()
    {
        //Arrage
        $expected = [
            new Wod(),
            new Wod(),
            new Wod(),
        ];

        $searchWorkGroup = new WorkGroup();

        $user = new User();
        $selectedWorkGroup = new WorkGroup();


        $wodRepo = $this->getWodRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:Wod' => $wodRepo]);
        $securityCtx = new SecurityContextMock($user, true, [Role::ROLE_ADMIN]);
        $commonService = $this->getCommonServiceMock();

        $commonService->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($selectedWorkGroup));


        $wodRepo->expects($this->any())
            ->method('findByWorkGroup')
            ->with($searchWorkGroup)
            ->will($this->returnValue($expected));


        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByWorkGroup($searchWorkGroup, null, null, null, null);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
    * -----------------------------------------------------------------------------------------------------------------
    * public function createDefault(WodGroup $wodGroup,User $user)
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
     * @test
     */
    public function testCreateDefault()
    {
        //Arrage
        $loggedUser = new User();
        $wodGroup = new WodGroup();
        $creationUser = new User();

        $em = new EntityManagerMock([]);
        $securityCtx = new SecurityContextMock($loggedUser, true, []);
        $commonService = $this->getCommonServiceMock();



        //Act
        $service = new WodRepositoryService($em, $securityCtx, $commonService);
        $result = $service->createDefault($wodGroup, $creationUser );

        //Assert
        $this->assertNotEmpty($em->getPersistedEntities());
        $persisted = $em->getPersistedEntities()[0];
        $this->assertEquals('Default Wod', $persisted->getTitle());
        $this->assertEquals($wodGroup, $persisted->getWodGroup());
        $this->assertEquals($creationUser, $persisted->getCreationUser());
        $this->assertTrue(strlen($persisted->getDescription()) > 0);
        $this->assertEquals(Wod::STATUS_INPROGRESS, $persisted->getStatus());
        $this->assertEquals(Wod::DIFFICULTY_NORMAL, $persisted->getDifficulty());
    }

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * private
    * -----------------------------------------------------------------------------------------------------------------
    */
}
 