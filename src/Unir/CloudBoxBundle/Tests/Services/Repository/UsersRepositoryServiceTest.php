<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 11/08/14
 * Time: 18:14
 */

namespace Unir\CloudBoxBundle\Tests\Services\Repository;


use Proxies\__CG__\Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Service\Repository\UserRepositoryService;

/**
 * Class UsersRepositoryServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services\Repository
 */
class UsersRepositoryServiceTest extends BaseTestService
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByGroup(WodGroup $wodGroup)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByGroup1()
    {
        //Arrage

        $expectedNormalUsers = [
            new User(),
            new User(),
            new User(),
            new User(),
        ];

        $expectedAdminsAndOwners = [
            new User(),
            new User()
        ];

        $searchWodGroup = new WodGroup();
        $searchWodGroup->setWorkGroup(new WorkGroup());
        $searchWodGroup->getWorkGroup()->setEnterprise(new Enterprise());

        $loggedUser = new User();

        $userRepo = $this->getUserRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:User' => $userRepo]);
        $securityCtx = new SecurityContextMock($loggedUser, true, []);
        $commonService = $this->getCommonServiceMock();

        $userRepo->expects($this->any())
            ->method('findByGroup')
            ->with($searchWodGroup)
            ->will($this->returnValue($expectedNormalUsers));

        $userRepo->expects($this->any())
            ->method('findByAdminOrOwner')
            ->with($searchWodGroup->getWorkGroup()->getEnterprise())
            ->will($this->returnValue($expectedAdminsAndOwners));

        //Act
        $service = new UserRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByGroup($searchWodGroup);


        //Assert
        $this->assertEquals(array_merge($expectedNormalUsers, $expectedAdminsAndOwners), $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWorkGroup(WorkGroup $workGroup,array $orderBy=null,$count=null,$include_deleted=false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWorkGroup1()
    {
        //Arrage

        $expectedNormalUsers = [
            new User(),
            new User(),
            new User(),
            new User(),
        ];

        $expectedAdminsAndOwners = [
            new User(),
            new User()
        ];

        $searchWorkGroup = new WorkGroup();
        $searchWorkGroup->setEnterprise(new Enterprise());

        $loggedUser = new User();

        $userRepo = $this->getUserRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:User' => $userRepo]);
        $securityCtx = new SecurityContextMock($loggedUser, true, []);
        $commonService = $this->getCommonServiceMock();

        $userRepo->expects($this->any())
            ->method('findByWorkGroup')
            ->with($searchWorkGroup)
            ->will($this->returnValue($expectedNormalUsers));

        $userRepo->expects($this->any())
            ->method('findByAdminOrOwner')
            ->with($searchWorkGroup->getEnterprise())
            ->will($this->returnValue($expectedAdminsAndOwners));

        //Act
        $service = new UserRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByWorkGroup($searchWorkGroup);


        //Assert
        $this->assertEquals(array_merge($expectedNormalUsers, $expectedAdminsAndOwners), $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByCompany(Enterprise $enterprise,array $orderBy=null,$count=null,$include_deleted=false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByCompany1()
    {
        //Arrage

        $expectedNormalUsers = [
            new User(),
            new User(),
            new User(),
            new User(),
        ];

        $expectedAdminsAndOwners = [
            new User(),
            new User()
        ];

        $searchEnterprise = new Enterprise();

        $loggedUser = new User();

        $userRepo = $this->getUserRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:User' => $userRepo]);
        $securityCtx = new SecurityContextMock($loggedUser, true, []);
        $commonService = $this->getCommonServiceMock();

        $userRepo->expects($this->any())
            ->method('findByEnterprise')
            ->with($searchEnterprise)
            ->will($this->returnValue($expectedNormalUsers));

        $userRepo->expects($this->any())
            ->method('findByAdminOrOwner')
            ->with()
            ->will($this->returnValue($expectedAdminsAndOwners));

        //Act
        $service = new UserRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByCompany($searchEnterprise);


        //Assert
        $this->assertEquals(array_merge($expectedNormalUsers, $expectedAdminsAndOwners), $result);
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function setWorkGroups(User $user, array $workGroupIds)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetWorkGroups1()
    {
        //Arrage
        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $repo = $em->getRepository('UnirCloudBoxBundle:WorkGroup');

        $user = new User();
        $user
            ->addMemberWorkGroup((new WorkGroup())->setId(1))
            ->addMemberWorkGroup((new WorkGroup())->setId(2))
            ->addMemberWorkGroup((new WorkGroup())->setId(3));

        $repo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new WorkGroup())->setId(4),
                (new WorkGroup())->setId(5),
                (new WorkGroup())->setId(6),
                (new WorkGroup())->setId(7),
                (new WorkGroup())->setId(8)
            ));

        //Act
        $service = new UserRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setWorkGroups($user, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($user, $result);
        $this->assertEquals(5, $result->getMemberWorkGroups()->count());
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function setWodGroups(User $user, array $wodGroupsIds)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetWodGroups1()
    {
        //Arrage
        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $repo = $em->getRepository('UnirCloudBoxBundle:WodGroup');

        $user = new User();
        $user
            ->addMemberWodGroup((new WodGroup())->setId(1))
            ->addMemberWodGroup((new WodGroup())->setId(2))
            ->addMemberWodGroup((new WodGroup())->setId(3));

        $repo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new WodGroup())->setId(4),
                (new WodGroup())->setId(5),
                (new WodGroup())->setId(6),
                (new WodGroup())->setId(7),
                (new WodGroup())->setId(8)
            ));

        //Act
        $service = new UserRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setWodGroups($user, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($user, $result);
        $this->assertEquals(5, $result->getMemberWodGroups()->count());
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     *   public function setFollowedWodGroups(User $user, array $wodGroupsIds)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetFollowedWodGroups()
    {
        //Arrage
        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $repo = $em->getRepository('UnirCloudBoxBundle:WodGroup');

        $user = new User();
        $user
            ->addFollowedWodGroup((new WodGroup())->setId(1))
            ->addFollowedWodGroup((new WodGroup())->setId(2))
            ->addFollowedWodGroup((new WodGroup())->setId(3));

        $repo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new WodGroup())->setId(4),
                (new WodGroup())->setId(5),
                (new WodGroup())->setId(6),
                (new WodGroup())->setId(7),
                (new WodGroup())->setId(8)
            ));

        //Act
        $service = new UserRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setFollowedWodGroups($user, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($user, $result);
        $this->assertEquals(5, $result->getFollowedWodGroups()->count());
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     *   public function setUserGroups(User $user, array $userGroupsIds)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetUserGroups1()
    {
        //Arrage
        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $repo = $em->getRepository('UnirCloudBoxBundle:Group');

        $user = new User();
        $user
            ->addToGroup((new Group())->setId(1))
            ->addToGroup((new Group())->setId(2))
            ->addToGroup((new Group())->setId(3));

        $repo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new Group())->setId(4),
                (new Group())->setId(5),
                (new Group())->setId(6),
                (new Group())->setId(7),
                (new Group())->setId(8)
            ));

        //Act
        $service = new UserRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setUserGroups($user, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($user, $result);
        $this->assertEquals(5, $result->getGroups()->count());
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * private
     * -----------------------------------------------------------------------------------------------------------------
     */

}
