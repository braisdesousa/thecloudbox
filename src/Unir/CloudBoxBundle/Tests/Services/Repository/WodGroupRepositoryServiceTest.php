<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 11/08/14
 * Time: 16:42
 */

namespace Unir\CloudBoxBundle\Tests\Services\Repository;

use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Service\Repository\WodGroupRepositoryService;


/**
 * Class WodGroupRepositoryServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services\Repository
 */
class WodGroupRepositoryServiceTest extends BaseTestService
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWorkGroup($status=null,$difficulty=null,$orderBy=null,$count=null,$include_deleted=false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWorkGroup1()
    {
        //Arrage

        $expected = [
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
        ];


        $usr = new User();

        $wodGroupRepository = $this->getWodGroupRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:WodGroup' => $wodGroupRepository]);
        $securityCtx = new SecurityContextMock($usr, true, []);
        $commonService = $this->getCommonServiceMock();


        $wodGroupRepository->expects($this->any())
            ->method('findByUserAndWorkGroup')
            ->will($this->returnValue($expected));

        $commonService->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue(new WorkGroup()));

        //Act
        $service = new WodGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByWorkGroup(null, null, null, null, false);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByWorkGroup2()
    {
        //Arrage

        $expected = [
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
        ];


        $usr = new User();

        $wodGroupRepository = $this->getWodGroupRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:WodGroup' => $wodGroupRepository]);
        $securityCtx = new SecurityContextMock($usr, true, [User::ROLE_ADMIN]);
        $commonService = $this->getCommonServiceMock();


        $wodGroupRepository->expects($this->any())
            ->method('findByWorkGroup')
            ->will($this->returnValue($expected));

        $commonService->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue(new WorkGroup()));

        //Act
        $service = new WodGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByWorkGroup(null, null, null, null, false);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function findAll()
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindAll1()
    {
        //Arrage

        $expected = [
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
        ];


        $usr = new User();

        $wodGroupRepository = $this->getWodGroupRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:WodGroup' => $wodGroupRepository]);
        $securityCtx = new SecurityContextMock($usr, true, []);
        $commonService = $this->getCommonServiceMock();

        $wodGroupRepository->expects($this->any())
            ->method('findByUser')
            ->with($usr)
            ->will($this->returnValue($expected));

        //Act
        $service = new WodGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findAll();

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindAll2()
    {
        //Arrage

        $expected = [
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
        ];


        $usr = new User();

        $wodGroupRepository = $this->getWodGroupRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:WodGroup' => $wodGroupRepository]);
        $securityCtx = new SecurityContextMock($usr, true, [User::ROLE_ADMIN]);
        $commonService = $this->getCommonServiceMock();

        $wodGroupRepository->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue($expected));

        //Act
        $service = new WodGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findAll();

        //Assert
        $this->assertEquals($expected, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function findByUser(User $user)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUser1()
    {
        //Arrage

        $expected = [
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
        ];


        $usr = new User();

        $wodGroupRepository = $this->getWodGroupRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:WodGroup' => $wodGroupRepository]);
        $securityCtx = new SecurityContextMock($usr, true, []);
        $commonService = $this->getCommonServiceMock();

        $wodGroupRepository->expects($this->any())
            ->method('findByUser')
            ->with($usr)
            ->will($this->returnValue($expected));

        //Act
        $service = new WodGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByUser($usr);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByUser2()
    {
        //Arrage

        $expected = [
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
        ];


        $usr = new User();
        $usr->addRole(new Role(User::ROLE_ADMIN));
        $usr->setEnterprise(new Enterprise());

        $wodGroupRepository = $this->getWodGroupRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:WodGroup' => $wodGroupRepository]);
        $securityCtx = new SecurityContextMock($usr, true, []);
        $commonService = $this->getCommonServiceMock();

        $wodGroupRepository->expects($this->any())
            ->method('findByEnterprise')
            ->with($usr->getEnterprise())
            ->will($this->returnValue($expected));

        //Act
        $service = new WodGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByUser($usr);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByUser3()
    {
        //Arrage

        $expected = [
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
            new WodGroup(),
        ];


        $usr = new User();
        $usr->addRole(new Role(User::ROLE_OWNER));

        $wodGroupRepository = $this->getWodGroupRepositoryMock();
        $em = new EntityManagerMock(['UnirCloudBoxBundle:WodGroup' => $wodGroupRepository]);
        $securityCtx = new SecurityContextMock($usr, true, []);
        $commonService = $this->getCommonServiceMock();

        $wodGroupRepository->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue($expected));

        //Act
        $service = new WodGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->findByUser($usr);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function setMembers(WodGroup $wodGroup, array $userIds)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetMembers1()
    {
        //Arrage
        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $repo = $em->getRepository('UnirCloudBoxBundle:User');

        $wodGroup = new WodGroup();
        $wodGroup
            ->addUserMember((new User())->setId(1))
            ->addUserMember((new User())->setId(2))
            ->addUserMember((new User())->setId(3));

        $repo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new User())->setId(4),
                (new User())->setId(5),
                (new User())->setId(6),
                (new User())->setId(7),
                (new User())->setId(8)
            ));

        //Act
        $service = new WodGroupRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setMembers($wodGroup, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($wodGroup, $result);
        $this->assertEquals(5, $result->getUserMembers()->count());
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function setGroupMembers(WodGroup $wodGroup, array $groupIds)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testSetGroupMembers1()
    {
        //Arrage
        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $repo = $em->getRepository('UnirCloudBoxBundle:Group');

        $wodGroup = new WodGroup();
        $wodGroup
            ->addGroupMember((new Group())->setId(1))
            ->addGroupMember((new Group())->setId(2))
            ->addGroupMember((new Group())->setId(3));

        $repo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new Group())->setId(4),
                (new Group())->setId(5),
                (new Group())->setId(6),
                (new Group())->setId(7),
                (new Group())->setId(8)
            ));

        //Act
        $service = new WodGroupRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setGroupMembers($wodGroup, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($wodGroup, $result);
        $this->assertEquals(5, $result->getGroupMembers()->count());
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function setFollowers(WodGroup $wodGroup, array $userIds)
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * @test
     */
    public function testSetFollowers1()
    {
        //Arrage
        $em = $this->getEntityManagerMock();
        $sctCtx = new SecurityContextMock();
        $commonService = $this->getCommonServiceMock();
        $repo = $em->getRepository('UnirCloudBoxBundle:User');

        $wodGroup = new WodGroup();
        $wodGroup
            ->addUserFollower((new User())->setId(1))
            ->addUserFollower((new User())->setId(2))
            ->addUserFollower((new User())->setId(3));

        $repo
            ->expects($this->any())
            ->method('find')
            ->will($this->onConsecutiveCalls(
                (new User())->setId(4),
                (new User())->setId(5),
                (new User())->setId(6),
                (new User())->setId(7),
                (new User())->setId(8)
            ));

        //Act
        $service = new WodGroupRepositoryService($em, $sctCtx, $commonService);
        $result = $service->setFollowers($wodGroup, [4,5,6,7,8]);

        //Assert
        $this->assertEquals($wodGroup, $result);
        $this->assertEquals(5, $result->getUserFollowers()->count());
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function createDefault(WorkGroup $workGroup)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testCreateDefault()
    {
        //Arrage

        $usr = new User();
        $workGroup = new WorkGroup();

        $em = new EntityManagerMock([]);
        $securityCtx = new SecurityContextMock($usr, true, []);
        $commonService = $this->getCommonServiceMock();

        //Act
        $service = new WodGroupRepositoryService($em, $securityCtx, $commonService);
        $result = $service->createDefault($workGroup);

        //Assert
        $this->assertNotEmpty($em->getPersistedEntities());
        $newGroup = $em->getPersistedEntities()[0];
        $this->assertEquals('Default', $newGroup->getName());
        $this->assertEquals($workGroup, $newGroup->getWorkGroup());
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  private
     * -----------------------------------------------------------------------------------------------------------------
     */
}