<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 11/08/14
 * Time: 10:58
 */

namespace Unir\CloudBoxBundle\Tests\Services\Repository;

use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Service\Repository\EnterpriseRepositoryService;

/**
 * Class intended for test Service/Repository/EnterpriseRepositoryService
 * Class EnterpriseRepositoryServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services\Repository
 */
class EnterpriseRepositoryServiceTest extends BaseTestService
{

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function checkOrCreateEnterprise()
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testCheckOrCreate1()
    {
        //Arrage

        $enterpriseRepMock = $this->getEnterpriseRepositoryMock();

        $em = new EntityManagerMock([
            'UnirCloudBoxBundle:Enterprise' => $enterpriseRepMock
        ]);

        $enterpriseRepMock->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(1));

        $securityCtx = new SecurityContextMock(new User());
        $commonServiceMock = $this->getCommonServiceMock();

        //Act

        $service = new EnterpriseRepositoryService($em, $securityCtx, $commonServiceMock);
        $service->checkOrCreate();

        //Assert
        $this->assertEmpty($em->getPersistedEntities());
    }

    /**
     * @test
     */
    public function testCheckOrCreate2()
    {
        //Arrage

        $enterpriseRepMock = $this->getEnterpriseRepositoryMock();

        $em = new EntityManagerMock([
            'UnirCloudBoxBundle:Enterprise' => $enterpriseRepMock
        ]);

        $enterpriseRepMock->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(0));

        $securityCtx = new SecurityContextMock(new User());
        $commonServiceMock = $this->getCommonServiceMock();

        //Act

        $service = new EnterpriseRepositoryService($em, $securityCtx, $commonServiceMock);
        $service->checkOrCreate();

        //Assert
        $this->assertNotEmpty($em->getPersistedEntities());
        $persisted = $em->getPersistedEntities()[0];
        $this->assertEquals('Default Enterprise', $persisted->getName());

    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * private
     * -----------------------------------------------------------------------------------------------------------------
     */
}