<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 1/08/14
 * Time: 13:56
 */

namespace Unir\CloudBoxBundle\Tests\Services;

use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Service\MailerService;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SwiftMailerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\TwigEngineMock;
use Unir\CloudBoxBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tests for mailServes
 * Class MailerServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services
 */
class MailerServiceTest extends BaseTestService
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function mailNewUser(User $user)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * test send email for new user
     * @test
     */
    public function testMailNewUser()
    {
        //Arrage
        $expectedEmail = 'expectedEmail1@email.es';
        $expectedUsername = 'expectedUsername';
        $expectedTemplate = 'UnirCloudBoxBundle:Mail:newUser.html.twig';
        $expectedSubject = 'Nuevo Usuario';

        $expectedUser = new User();
        $expectedUser
            ->setEmail($expectedEmail)
            ->setUsername($expectedUsername);



        $swiftMailerMock = new SwiftMailerMock();
        $twigEngineMock = new TwigEngineMock();

        //Act

        $mailerService = new MailerService($swiftMailerMock, $twigEngineMock);
        $mailerService->mailNewUser($expectedUser);

        //Assert
        $sendedMessage = $swiftMailerMock->getSendedMessage();

        //Have sended
        $this->assertNotNull($sendedMessage);

        $sendedTo = $sendedMessage->getTo();

        //To who have sended
        $this->assertArrayHasKey($expectedEmail, $sendedTo);
        $this->assertEquals($expectedUsername, $sendedTo[$expectedEmail]);

        //what has sended
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());

        $twigParameters = $twigEngineMock->getParameters();

        $this->assertArrayHasKey('user', $twigParameters);
        $this->assertEquals($expectedUser, $twigParameters['user']);

        $this->assertEquals($expectedSubject, $sendedMessage->getSubject());
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function mailWodComment(WodComments $wodComment)
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * tests the email that is sended when a comment is created
     * @test
     */
    public function testMailComment()
    {
        //Arrage
        $expectedTemplate = 'UnirCloudBoxBundle:Mail:newWodComment.html.twig';
        $expectedSubject = 'Nuevo Comentario';

        $expectedEmailsAndUserNames = [
            "expectedEmail1@isAnEmail1.es" => "user1",
            "expectedEmail1@isAnEmail2.es" => "user2",
            "expectedEmail1@isAnEmail3.es" => "user3",
            "expectedEmail1@isAnEmail4.es" => "user4",
            "expectedEmail1@isAnEmail5.es" => "user5",
            "expectedEmail1@isAnEmail6.es" => "user6",
        ];

        $numExpectedEmailsAndUserNames = count($expectedEmailsAndUserNames);


        $users = [];
        foreach($expectedEmailsAndUserNames as $email => $userName){
            $usr = new User();
            $users[] = $usr->setEmail($email)->setUsername($userName);
        }

        $usersAssigned = array_slice($users, 0, $numExpectedEmailsAndUserNames >= 2 ? 2 : $numExpectedEmailsAndUserNames);
        $usersFollower = $users;
        $userOwner = new User();

        $wod = new Wod();
        $wod->setUsersAssigned(new ArrayCollection($usersAssigned));
        $wod->setUsersFollower(new ArrayCollection($usersFollower));

        $wodComment = new WodComments();
        $wodComment->setUser($userOwner);
        $wodComment->setWod($wod);

        $swiftMailerMock = new SwiftMailerMock();
        $twigEngineMock = new TwigEngineMock();

        //Act

        $mailerService = new MailerService($swiftMailerMock, $twigEngineMock);
        $mailerService->mailWodComment($wodComment);

        //Assert
        $sendedMessage = $swiftMailerMock->getSendedMessage();

        //Have sended
        $this->assertNotNull($sendedMessage);
        $this->assertEquals($expectedSubject, $sendedMessage->getSubject());
        $this->assertEquals($numExpectedEmailsAndUserNames, count($sendedMessage->getTo()));

        //what has sended
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertArrayHasKey('wodcomment', $twigEngineMock->getParameters());
        $this->assertEquals($wodComment, $twigEngineMock->getParameters()['wodcomment']);
    }

    /**
     * tests than mailWodComment throws exception when wodComment haven't user
     * @test
     * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage Unable to find creation user
     */
    public function testMailWodCommentThrowsExceptionIfWodDoesntHaveUser()
    {

        //Arrage
        $wod = new Wod();
        $wodComment = new WodComments();
        $wodComment->setWod($wod);

        $swiftMailerMock = new SwiftMailerMock();
        $twigEngineMock = new TwigEngineMock();

        //Act
        $mailerService = new MailerService($swiftMailerMock, $twigEngineMock);
        $mailerService->mailWodComment($wodComment);

        //Assert
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function mailWod(Wod $wod)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * tests the email that is sended when a new wod is created
     * @test
     */
    public function testMailWod()
    {
        //Arrage
        $expectedTemplate = 'UnirCloudBoxBundle:Mail:addedtowod.html.twig';
        $expectedSubject = 'Nueva Tarea';

        $expectedEmailsAndUserNames = [
            "expectedEmail1@isAnEmail1.es" => "user1",
            "expectedEmail1@isAnEmail2.es" => "user2",
            "expectedEmail1@isAnEmail3.es" => "user3",
            "expectedEmail1@isAnEmail4.es" => "user4",
            "expectedEmail1@isAnEmail5.es" => "user5",
            "expectedEmail1@isAnEmail6.es" => "user6",
        ];

        $numExpectedEmailsAndUserNames = count($expectedEmailsAndUserNames);


        $users = [];
        foreach($expectedEmailsAndUserNames as $email => $userName){
            $usr = new User();
            $users[] = $usr->setEmail($email)->setUsername($userName);
        }

        $usersAssigned = array_slice($users, 0, $numExpectedEmailsAndUserNames >= 2 ? 2 : $numExpectedEmailsAndUserNames);
        $usersFollower = $users;
        $userOwner = new User();

        $wod = new Wod();
        $wod->setUsersAssigned(new ArrayCollection($usersAssigned));
        $wod->setUsersFollower(new ArrayCollection($usersFollower));
        $wod->setCreationUser($userOwner);


        $swiftMailerMock = new SwiftMailerMock();
        $twigEngineMock = new TwigEngineMock();

        //Act
        $mailerService = new MailerService($swiftMailerMock, $twigEngineMock);
        $mailerService->mailWod($wod);

        //Assert
        $sendedMessage = $swiftMailerMock->getSendedMessage();

        //Have sended
        $this->assertNotNull($sendedMessage);
        $this->assertEquals($expectedSubject, $sendedMessage->getSubject());
        $this->assertEquals($numExpectedEmailsAndUserNames, count($sendedMessage->getTo()));


        //what has sended
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertArrayHasKey('wod', $twigEngineMock->getParameters());
        $this->assertEquals($wod, $twigEngineMock->getParameters()['wod']);
    }

    /**
     * tests than mailWodComment throws exception when wodComment haven't user
     * @test
     * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage User not found
     */
    public function testMailWodThrowsExceptionIfWodHaventCreationUser()
    {
        //Arrage
        $wod = new Wod();

        $swiftMailerMock = new SwiftMailerMock();
        $twigEngineMock = new TwigEngineMock();

        //Act
        $mailerService = new MailerService($swiftMailerMock, $twigEngineMock);
        $mailerService->mailWod($wod);

        //Assert
    }


} 