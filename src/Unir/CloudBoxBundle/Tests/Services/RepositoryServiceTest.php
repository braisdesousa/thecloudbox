<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 6/08/14
 * Time: 9:57
 */

namespace Unir\CloudBoxBundle\Tests\Services;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\ContainerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Service\RepositoryService;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\User;

/**
 * Class for unit testing repositoryService
 * Class RepositoryServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services
 */
class RepositoryServiceTest extends BaseTestService
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * public function getWods(array $status= null, array $difficulty= null, array $orderBy= null, $count= null,$workGroup=null)
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
     * test getWods when there is not a workGroup and have choosen to count result
     * @test
     */
    public function testGetWodsWithNoWorkGroupAndCount()
    {
        //Arrage

        $commonServiceMock = $this->getCommonServiceMock();
        $wodRepositoryMock = $this->getWodRepositoryMock();

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:Wod'  => $wodRepositoryMock
        ]);

        $containerMock = new ContainerMock();


        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWods(null, null, null, true, null);

        //Assert
        $this->assertEquals(0, $result);
    }

    /**
     * test getWods when there is not a workGroup and have choosen normal output
     * @test
     */
    public function testGetWodsWithNoWorkGroupAndWithoutCount()
    {
        //Arrage

        $commonServiceMock = $this->getCommonServiceMock();
        $wodRepositoryMock = $this->getWodRepositoryMock();

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:Wod'  => $wodRepositoryMock
        ]);

        $containerMock = new ContainerMock();


        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWods(null,null,null,null,null);

        //Assert
        $this->assertEquals([], $result);
    }

    /**
     * test getWods passing a workGroup that doesn't have wodGroups
     * @test
     */
    public function testGetWodsPassingAWorkGroupThatHaveNotWodGroups()
    {
        //Arrage
        $workGroup = new WorkGroup();

        $commonServiceMock = $this->getCommonServiceMock();
        $wodRepositoryMock = $this->getWodRepositoryMock();

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:Wod'  => $wodRepositoryMock
        ]);

        $containerMock = new ContainerMock();


        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWods(null,null,null,null,$workGroup);

        //Assert
        $this->assertEquals([], $result);
    }


    /**
     * test getWods when is a workGroup selectedand Count
     * @test
     */
    public function testGetWodsWithASelectedWorkGroupAndCount()
    {
        //Arrage
        $workGroup = new WorkGroup();

        $commonServiceMock = $this->getCommonServiceMock();
        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($workGroup));

        $wodRepositoryMock = $this->getWodRepositoryMock();

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:Wod'  => $wodRepositoryMock
        ]);

        $containerMock = new ContainerMock();


        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWods(null,null,null,true,null);

        //Assert
        $this->assertEquals(0, $result);
    }

    /**
     * tests than getWods returns a collection when the workGroup have groups
     * @test
     */
    public function testGetWodsWithWorkGroupThatHaveWorkGroupGroups()
    {
        //Arrage
        $expectedResult = new ArrayCollection();
        $expectedResult->add(new Wod());
        $expectedResult->add(new Wod());
        $expectedResult->add(new Wod());

        $workGroup = new WorkGroup();
        $workGroup->getWodGroups()->add(new WodGroup());
        $workGroup->getWodGroups()->add(new WodGroup());
        $workGroup->getWodGroups()->add(new WodGroup());

        $commonServiceMock = $this->getCommonServiceMock();
        $wodRepositoryMock = $this->getWodRepositoryMock();

        $emMock = $this->getEntityManagerMock();
        $containerMock = new ContainerMock();


        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($workGroup));

        $emMock->getRepository('UnirCloudBoxBundle:Wod')
            ->expects($this->any())
            ->method('findByWodGroups')
            ->with($workGroup->getWodGroups())
            ->will($this->returnValue($expectedResult));


        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWods(null, null, null, null, null);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * tests than getWods returns a collection when the workGroup have groups
     * @test
     */
    public function testGetWodsWithWorkGroupThatHaveWorkGroupGroupsWithCount()
    {
        //Arrage
        $expectedResult = 123;

        $workGroup = new WorkGroup();
        $workGroup->getWodGroups()->add(new WodGroup());
        $workGroup->getWodGroups()->add(new WodGroup());
        $workGroup->getWodGroups()->add(new WodGroup());

        $commonServiceMock = $this->getCommonServiceMock();

        $wodRepositoryMock = $this->getWodRepositoryMock();

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:Wod'  => $wodRepositoryMock
        ]);

        $containerMock = new ContainerMock();

        $wodRepositoryMock->expects($this->any())
            ->method('findByWodGroups')
            ->will($this->returnValue($expectedResult));

        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($workGroup));

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWods(null, null, null, true, null);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function getWodGroups(array $orderBy= null, $count= null,$workGroup=null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * tests getWodGroups without a workGroup selected
     * @test
     */
    public function testGetWodGroupsWithoutSelectedWorkGroup()
    {
        //Arrage

        $commonServiceMock = $this->getCommonServiceMock();
        $getWodGroupRepositoryMock = $this->getWodGroupRepositoryMock();

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WodGroup'  => $getWodGroupRepositoryMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWodGroups();

        //Assert
        $this->assertEquals([], $result);
    }

    /**
     * tests getWodGroups without a workGroup selected and a count argument
     * @test
     */
    public function testGetWodGroupsWithoutSelectedWorkGroupAndCount()
    {
        //Arrage

        $commonServiceMock = $this->getCommonServiceMock();
        $getWodGroupRepositoryMock = $this->getWodGroupRepositoryMock();

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WodGroup'  => $getWodGroupRepositoryMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWodGroups(null, true, null);

        //Assert
        $this->assertEquals(0, $result);
    }

    /**
     * get wodgroups passing a workGroup as argument
     * @test
     */
    public function testGetWodGroupsPassingWorkGroup()
    {
        //Arrage
        $expectedResult = new ArrayCollection();
        $expectedResult->add(new WodGroup());
        $expectedResult->add(new WodGroup());

        $workGroup = new WorkGroup();

        $commonServiceMock = $this->getCommonServiceMock();
        $wodGroupRepositoryMock = $this->getWodGroupRepositoryMock();
        $wodGroupRepositoryMock->expects($this->any())
            ->method('findByWorkGroup')
            ->will($this->returnValue($expectedResult));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WodGroup'  => $wodGroupRepositoryMock
        ]);

        $containerMock = new ContainerMock();
        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWodGroups(null, null, $workGroup);

        //Assert
        $this->assertEquals($expectedResult, $result);

    }

    /**
     * tests getWodGroups when commonService have a workGroup and count arg is passed
     */
    public function testGetWodGroupsWithWorkGroupAndCount()
    {
        //Arrage
        $expectedResult = 123;

        $workGroup = new WorkGroup();

        $commonServiceMock = $this->getCommonServiceMock();
        $getWodGroupRepositoryMock = $this->getWodGroupRepositoryMock();

        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->will($this->returnValue($workGroup));

        $getWodGroupRepositoryMock->expects($this->any())
            ->method('findByWorkGroup')
            ->will($this->returnValue($expectedResult));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WodGroup'  => $getWodGroupRepositoryMock
        ]);

        $containerMock = new ContainerMock();
        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWodGroups(null, true, null);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function getWorkGroups(array $orderBy= null, $count= null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * tests getWorkGroups without enterpriseSelected
     * @test
     */
    public function testGetWorkGroupsWithoutEnterprise()
    {
        //Arrage
        $expected = new ArrayCollection();
        $expected->add(new WorkGroup());
        $expected->add(new WorkGroup());

        $commonServiceMock = $this->getCommonServiceMock();
        $workGroupRepoMock = $this->getWorkGroupRepositoryMock();
        $workGroupRepoMock->expects($this->any())
            ->method('findOrdered')
            ->will($this->returnValue($expected));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup'  => $workGroupRepoMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWorkGroups([],null);


        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * tests getWorkGroups without selected enterprise and count
     * @test
     */
    public function testGetWorkGroupsWithoutEnterpriseAndCount()
    {
        //Arrage
        $expected = 33;

        $commonServiceMock = $this->getCommonServiceMock();
        $workGroupRepoMock = $this->getWorkGroupRepositoryMock();
        $workGroupRepoMock->expects($this->any())
            ->method('countAll')
            ->will($this->returnValue($expected));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup'  => $workGroupRepoMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWorkGroups([], true);


        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * tests getWorkGroups when commonService have an enterprise
     * @test
     */
    public function testGetWorkGroupsWithEnterprise()
    {
        //Arrage
        $expected = new ArrayCollection();
        $expected->add(new WorkGroup());
        $expected->add(new WorkGroup());

        $commonServiceMock = $this->getCommonServiceMock();
        $commonServiceMock->expects($this->any())
            ->method('getCompany')
            ->will($this->returnValue(new Enterprise()));

        $workGroupRepoMock = $this->getWorkGroupRepositoryMock();
        $workGroupRepoMock->expects($this->any())
            ->method('findByEnterprise')
            ->will($this->returnValue($expected));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup'  => $workGroupRepoMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWorkGroups([], null);


        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * test getWorkGroups with enterprise and count
     */
    public function testGetWorkGroupsWithEnterpriseAndCount()
    {
        //Arrage
        $expected = 33;

        $commonServiceMock = $this->getCommonServiceMock();
        $commonServiceMock->expects($this->any())
            ->method('getCompany')
            ->will($this->returnValue(new Enterprise()));

        $workGroupRepoMock = $this->getWorkGroupRepositoryMock();
        $workGroupRepoMock->expects($this->any())
            ->method('countAllByEnterprise')
            ->will($this->returnValue($expected));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup'  => $workGroupRepoMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getWorkGroups([], true);


        //Assert
        $this->assertEquals($expected, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function getUsers(array $orderBy= null, $count = null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * tests getUsers without enterprise in commonService
     * @test
     */
    public function testGetUsersWithoutEnterprise()
    {
        //Arrage
        $expectedResult = new ArrayCollection();
        $expectedResult->add(new User());
        $expectedResult->add(new User());
        $expectedResult->add(new User());


        $commonServiceMock = $this->getCommonServiceMock();
        $userRepoMock = $this->getUserRepositoryMock();
        $userRepoMock->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue($expectedResult));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:User'  => $userRepoMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getUsers([], null);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * tests getUsers without enterprise in commonService and passing count
     */
    public function testGetUsersWithoutEnterpriseAndCount()
    {
        //Arrage
        $expectedResult = 1231;


        $commonServiceMock = $this->getCommonServiceMock();
        $userRepoMock = $this->getUserRepositoryMock();
        $userRepoMock->expects($this->any())
            ->method('countAll')
            ->will($this->returnValue($expectedResult));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:User'  => $userRepoMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getUsers([], true);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * tests getUsers with enterprise in commonService
     * @test
     */
    public function testGetUsersWithEnterprise()
    {
        //Arrage
        $expectedResult = new ArrayCollection();
        $expectedResult->add(new User());
        $expectedResult->add(new User());
        $expectedResult->add(new User());


        $commonServiceMock = $this->getCommonServiceMock();
        $userRepoMock = $this->getUserRepositoryMock();

        $commonServiceMock->expects($this->any())
            ->method('getCompany')
            ->will($this->returnValue(new Enterprise()));

        $userRepoMock->expects($this->any())
            ->method('findByEnterprise')
            ->will($this->returnValue($expectedResult));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:User'  => $userRepoMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getUsers([], null);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * tests getUsers with enterprise in commonService and passing count
     */
    public function testGetUsersWithEnterpriseAndCount()
    {
        //Arrage
        $expectedResult = 1231;


        $commonServiceMock = $this->getCommonServiceMock();
        $userRepoMock = $this->getUserRepositoryMock();

        $commonServiceMock->expects($this->any())
            ->method('getCompany')
            ->will($this->returnValue(new Enterprise()));

        $userRepoMock->expects($this->any())
            ->method('countAllByEnterprise')
            ->will($this->returnValue($expectedResult));

        $emMock = new EntityManagerMock([
            'UnirCloudBoxBundle:User'  => $userRepoMock
        ]);

        $containerMock = new ContainerMock();

        //Act
        $repoService = new RepositoryService($emMock, $containerMock, $commonServiceMock);
        $result = $repoService->getUsers([], true);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * private
     * -----------------------------------------------------------------------------------------------------------------
     */
}

 