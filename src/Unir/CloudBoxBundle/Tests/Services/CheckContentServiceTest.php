<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 6/08/14
 * Time: 12:50
 */

namespace Unir\CloudBoxBundle\Tests\Services;

use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\TwigEngineMock;
use Unir\CloudBoxBundle\Service\CheckContentService;

/**
 * class for unit testing CheckContentService
 *
 * Class CheckContentServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services
 */
class CheckContentServiceTest extends BaseTestService
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function hasMinimumContent($element)
     * -----------------------------------------------------------------------------------------------------------------
     */


    //
    // ENTERPRISE
    //

    /**
     * @test
     */
    public function testHasMinimumContentEnterprise1()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock(new User());
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);

        $emMock->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('findAll')
            ->with(false, true)
            ->will($this->returnValue(0));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('enterprise');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(0, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentEnterprise2()
    {
        //Arrage

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock(new User());
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock();

        $emMock->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('findAll')
            ->with(false, true)
            ->will($this->returnValue(1));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('enterprise');

        //Assert
        $this->assertNull($result);
    }

    //
    // PROJECT
    //

    /**
     * @test
     */
    public function testHasMinimumContentWorkGroup1()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock(new User());
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);

        $emMock->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('findAll')
            ->with(false, true)
            ->will($this->returnValue(0));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('workGroup');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(0, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWorkGroup2()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);


        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([new WorkGroup()]));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('workGroup');

        //Assert
        $this->assertNull($result);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWorkGroup3()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);

        $emMock->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('findAll')
            ->with(false, true)
            ->will($this->returnValue(1));


        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([]));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('workGroup');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(1, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWorkGroup4()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, [User::ROLE_ADMIN]);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);

        $emMock->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('findAll')
            ->with(false, true)
            ->will($this->returnValue(1));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('workGroup');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(1, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWorkGroup5()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, [User::ROLE_ADMIN]);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);

        $emMock->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('findAll')
            ->with(false, true)
            ->will($this->returnValue(1));



        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('workGroup');

        //Assert
        $this->assertNull($result);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWorkGroup6()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, [User::ROLE_OWNER]);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);

        $emMock->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('findAll')
            ->with(false, true)
            ->will($this->returnValue(1));

        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findAll')
            ->with(false)
            ->will($this->returnValue([]));


        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('workGroup');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(1, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWorkGroup7()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, [User::ROLE_OWNER]);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);

        $emMock->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('findAll')
            ->with(false, true)
            ->will($this->returnValue(1));



        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('workGroup');

        //Assert
        $this->assertNull($result);
    }

    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage NO COMPANY Exception
     */
    public function testHasMinimumContentWorkGroup8()
    {
        //Arrage

        $loggedUser = new User();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, [User::ROLE_OWNER]);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock('');

        $emMock->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('findAll')
            ->with(false, true)
            ->will($this->returnValue(1));


        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('workGroup');

        //Assert
    }

    //
    // TASK GROUP
    //

    /**
     * @test
     */
    public function testHasMinimumContentWodGroup1()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();


        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);

           $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([]));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('wodGroups');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(0, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWodGroup2()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);


        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([]));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('wodGroups');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(1, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWodGroup3()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();

        $selectedWorkGroup = new WorkGroup();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);


        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([new WorkGroup()]));

        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->with()
            ->will($this->returnValue($selectedWorkGroup));

        $emMock->getRepository('UnirCloudBoxBundle:WodGroup')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue([new WodGroup()]));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('wodGroups');

        //Assert
        $this->assertNull($result);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWodGroup4()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();


        $selectedWorkGroup = new WorkGroup();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);



        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([new WorkGroup()]));

        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->with()
            ->will($this->returnValue($selectedWorkGroup));

        $emMock->getRepository('UnirCloudBoxBundle:WodGroup')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue([]));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('wodGroups');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(2, $twigEngineMock->getParameters()['step']);
    }

    //
    // TASKS
    //

    /**
     * @test
     */
    public function testHasMinimumContentWods1()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();


        $selectedWorkGroup = new WorkGroup();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);


        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([new WorkGroup()]));

        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->with()
            ->will($this->returnValue($selectedWorkGroup));

        $emMock->getRepository('UnirCloudBoxBundle:WodGroup')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue([]));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('wods');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(0, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWods2()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();


        $selectedWorkGroup = new WorkGroup();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);



        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([]));

        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->with()
            ->will($this->returnValue($selectedWorkGroup));

        $emMock->getRepository('UnirCloudBoxBundle:WodGroup')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue([]));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('wods');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(1, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWods3()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();


        $selectedWorkGroup = new WorkGroup();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);

        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([new WorkGroup()]));

        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->with()
            ->will($this->returnValue($selectedWorkGroup));

        $emMock->getRepository('UnirCloudBoxBundle:WodGroup')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue([]));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('wods');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(2, $twigEngineMock->getParameters()['step']);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWods4()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();


        $selectedWorkGroup = new WorkGroup();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);



        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([new WorkGroup()]));

        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->with()
            ->will($this->returnValue($selectedWorkGroup));

        $emMock->getRepository('UnirCloudBoxBundle:WodGroup')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue([new WodGroup()]));

        $emMock->getRepository('UnirCloudBoxBundle:Wod')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue(2));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('wods');

        //Assert
        $this->assertNull($result);
    }

    /**
     * @test
     */
    public function testHasMinimumContentWods5()
    {
        //Arrage
        $expectedResult = '<h3>Hola Mundo</h3>';
        $expectedTemplate = 'UnirCloudBoxBundle:Default:empty_dashboard.html.twig';

        $loggedUser = new User();


        $selectedWorkGroup = new WorkGroup();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock($expectedResult);



        $emMock->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([new WorkGroup()]));

        $commonServiceMock->expects($this->any())
            ->method('getSelectedWorkGroup')
            ->with()
            ->will($this->returnValue($selectedWorkGroup));

        $emMock->getRepository('UnirCloudBoxBundle:WodGroup')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue([new WodGroup()]));

        $emMock->getRepository('UnirCloudBoxBundle:Wod')
            ->expects($this->any())
            ->method('findByWorkGroup')
            ->with($selectedWorkGroup)
            ->will($this->returnValue(0));

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('wods');

        //Assert
        $this->assertEquals($expectedResult, $result);
        $this->assertEquals($expectedTemplate, $twigEngineMock->getName());
        $this->assertEquals(3, $twigEngineMock->getParameters()['step']);

    }

    //
    // NONE
    //

    /**
     * @test
     * @expectedException Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function testHasMinimumContentNone1()
    {
        //Arrage
        $loggedUser = new User();


        $selectedWorkGroup = new WorkGroup();

        $emMock = $this->getEntityManagerMock();
        $secCtxMock = new SecurityContextMock($loggedUser, true, []);
        $commonServiceMock = $this->getCommonServiceMock();
        $twigEngineMock = new TwigEngineMock('');

        //Act
        $checkContentService = new CheckContentService($emMock, $secCtxMock, $commonServiceMock, $twigEngineMock);
        $result = $checkContentService->hasMinimumContent('noneNones');

        //Assert

    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  private
     * -----------------------------------------------------------------------------------------------------------------
     */


}
