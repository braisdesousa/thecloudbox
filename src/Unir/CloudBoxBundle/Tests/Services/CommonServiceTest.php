<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 31/07/14
 * Time: 10:28
 */

namespace Unir\CloudBoxBundle\Tests\Services;


use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Session\Session;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Service\CommonService;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestService;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\ContainerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SessionMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityRepositoryMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\WorkGroupRepositoryMock;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;


/**
 * Class for unit testing CommonService
 * Class CommonServiceTest
 * @package Unir\CloudBoxBundle\Tests\Services
 */
class CommonServiceTest extends BaseTestService
{

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function getCompany()
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * tests getCompany method on a valid securityContext
     * @test
     */
    public function testGetCompany1()
    {
        //Arrage
        $expectedEnterprise = new Enterprise();
        $expectedEnterprise->setId(1);

        $user = new User();


        $containerMock = new ContainerMock([
            "security.context" => new SecurityContextMock($user)
        ]);

        //Act
        $commonService = new CommonService(self::$em, $containerMock);

        $enterprise = $commonService->getCompany();

        //Assert
        $this->assertEquals($expectedEnterprise->getId(), $enterprise->getId());
    }

    /**
     * tests getCompany method on a valid securityContext
     * @test
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage Unable to find user logged
     */
    public function testGetCompany2()
    {
        //Arrage
        $expectedEnterprise = new Enterprise();
        $expectedEnterprise->setId(1);

        $user = new User();

        $containerMock = new ContainerMock([
            "security.context" => new SecurityContextMock($user, false)
        ]);

        //Act
        $commonService = new CommonService(self::$em, $containerMock);

        $commonService->getCompany();

        //Assert
    }

    /**
     * tests than is not enterprise and have selected_workGroup_id on session
     * @test
     */
    public function testGetCompany3()
    {
        //Arrage

        $expected = new Enterprise();

        $workGroupExpected = new WorkGroup();
        $workGroupExpected->setEnterprise($expected);

        $loggedUser = new User();

        $em = $this->getEntityManagerMock();

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($loggedUser),
            'session' => new SessionMock([
                    'selected_workGroup_id' => 10
                ])
        ]);

        $em->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('find')
            ->will($this->returnValue($workGroupExpected));


        //Act
        $commonService = new CommonService($em, $containerMock);
        $result = $commonService->getCompany();

        //Assert

        $this->assertEquals($expected, $result);

    }

    /**
     * tests than is not enterprise and session haven't seleceted workGroup
     * @test
     */
    public function testGetCompany4()
    {
        //Arrage

        $expected = new Enterprise();

        $user = new User();

        $em = new EntityManagerMock();

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($user),
            'session' => new SessionMock([])
        ]);

        //Act
        $commonService = new CommonService($em, $containerMock);
        $result = $commonService->getCompany();

        //Assert

        $this->assertEquals($expected, $result);
    }

    /**
     * Checks that loggedUser haven't enterprise and there is no workGroup selected then it must
     * check session for get enterprise_id And found it!
     * @test
     */
    public function testGetCompany5()
    {
        //Arrage

        $selectedWorkGroupId = null;
        $selectedEnterpriseId = 10;

        $expected = new Enterprise();



        $loggedUser = new User();

        $em = $this->getEntityManagerMock();

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($loggedUser),
            'session' => new SessionMock([
                    'selected_workGroup_id' => $selectedWorkGroupId,
                    'selected_enterprise_id' => $selectedEnterpriseId
                ])
        ]);

        $em->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('find')
            ->will($this->returnValue(null));

        $em->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([]));

        $em->getRepository('UnirCloudBoxBundle:Enterprise')
            ->expects($this->any())
            ->method('find')
            ->with($selectedEnterpriseId)
            ->will($this->returnValue($expected));


        //Act
        $commonService = new CommonService($em, $containerMock);
        $result = $commonService->getCompany();

        //Assert

        $this->assertEquals($expected, $result);
    }

    /**
     * Checks that loggedUser haven't enterprise and there is no workGroup selected then it must
     * check session for get enterprise_id And can't found it
     * @test
     * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage Unable to find an enterprise
     */
    public function testGetCompany6()
    {
        //Arrage

        $selectedWorkGroupId = null;
        $selectedEnterpriseId = null;

        $expected = null;



        $loggedUser = new User();

        $em = $this->getEntityManagerMock();

        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($loggedUser),
            'session' => new SessionMock([
                    'selected_workGroup_id' => $selectedWorkGroupId,
                    'selected_enterprise_id' => $selectedEnterpriseId
                ])
        ]);

        $em->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('find')
            ->will($this->returnValue(null));

        $em->getRepository('UnirCloudBoxBundle:WorkGroup')
            ->expects($this->any())
            ->method('findByUser')
            ->with($loggedUser)
            ->will($this->returnValue([]));


        //Act
        $commonService = new CommonService($em, $containerMock);
        $result = $commonService->getCompany();

        //Assert

        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function setCompany(Enterprise $enterprise)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Not a logged user, throws exception
     * @test
     * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage Unable to find user logged
     */
    public function testSetCompany1()
    {
        //Arrage

        $loggedUser = null;

        $em = $this->getEntityManagerMock();
        $session = new SessionMock();
        $container = new ContainerMock([
            'security.context' => new SecurityContextMock($loggedUser, false),
            'session' => $session
        ]);

        //Act
        $commonService = new CommonService($em, $container);
        $commonService->setCompany(new Enterprise());

        //Assert
    }

    /**
     * Logged user doesn't have enterprise, then session have a new value
     * @test
     */
    public function testSetCompany2()
    {
        //Arrage

        $loggedUser = new User();
        $newEnterprise = new Enterprise();
        $newEnterprise->setId(100);

        $em = $this->getEntityManagerMock();
        $session = new SessionMock();
        $container = new ContainerMock([
            'security.context' => new SecurityContextMock($loggedUser),
            'session' => $session
        ]);

        //Act
        $commonService = new CommonService($em, $container);
        $commonService->setCompany($newEnterprise);

        //Assert
        $this->assertEquals($newEnterprise->getId(), $session->get('selected_enterprise_id'));
    }


    /**
     * Logged user have an enterprise, then no changes are on session
     * @test
     */
    public function testSetCompany3()
    {
        //Arrage

        $loggedUser = new User();

        $em = $this->getEntityManagerMock();
        $session = new SessionMock();
        $container = new ContainerMock([
            'security.context' => new SecurityContextMock($loggedUser),
            'session' => $session
        ]);

        //Act
        $commonService = new CommonService($em, $container);
        $commonService->setCompany(new Enterprise());

        //Assert
        $this->assertFalse($session->has('selected_enterprise_id'));
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function getSelectedWorkGroup()
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Tests than an exception is thrown when getSelectedWorkGroup is called with not valid securityContext
     * @test
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage Unable to find user logged
     */
    public function testGetSelectedWorkGroupThrowsExceptionWithNotValidContext()
    {
        //Arrage
        $containerMock = new ContainerMock([
            "security.context" => new SecurityContextMock(new User(), false)
        ]);

        //Act
        $commonService = new CommonService(self::$em, $containerMock);

        $commonService->getSelectedWorkGroup();
        //Assert
    }

    /**
     * tests that getSelectedWorkGroup returns null when session has not selectedWorkGroup
     * @test
     */
    public function testGetSelectedWorkGroupReturnsNullWhenSessionHasNotSelectedWorkGroup()
    {
        //Arrage
        $expectedResult = new ArrayCollection();
        $expectedResult->add(new WorkGroup());
        $expectedResult->add(new WorkGroup());

        $containerMock = new ContainerMock([
            "session" => new SessionMock(),
            "security.context" => new SecurityContextMock(new User())
        ]);

        $workGroupRepoMock = $this->getMockBuilder("Unir\CloudBoxBundle\Repository\WorkGroupRepository")
            ->disableOriginalConstructor()
            ->getMock();

        $workGroupRepoMock->expects($this->any())
            ->method('findByUser')
            ->will($this->returnValue($expectedResult->toArray()));

        $em = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => $workGroupRepoMock
        ]);

        //Act
        $commonService = new CommonService($em, $containerMock);
        $result = $commonService->getSelectedWorkGroup();

        //Assert
        $this->assertEquals($expectedResult->toArray()[0], $result);
    }

    /**
     * tests that getSelectedWorkGroup returns correctWorkGroup when session has selected_workGroup_id
     * @test
     */
    public function testGetSelectedWorkGroupReturnsCorrectWorkGroup()
    {
        //Arrage
        $workGroup_id = 1;
        $expectedWorkGroup = TestData::getWorkGroups()[$workGroup_id];

        $containerMock = new ContainerMock([
            "session" => new SessionMock([
                    "selected_workGroup_id" => $workGroup_id
                ]),
            "security.context" => new SecurityContextMock(new User())
        ]);

        $entityManagerMock = new EntityManagerMock([
            "UnirCloudBoxBundle:WorkGroup" => new EntityRepositoryMock(TestData::getWorkGroups())
        ]);

        //Act
        $commonService = new CommonService($entityManagerMock, $containerMock);
        $result = $commonService->getSelectedWorkGroup();

        //Assert
        $this->assertNotNull($result);
        $this->assertEquals($expectedWorkGroup->getId(), $result->getId());
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     *  public function setSelectedWorkGroup(WorkGroup $workGroup)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * tets that when a workGroup is passed to this method, in session the value is correctly
     * added and the enterpriseId value is removed
     * @test
     */
    public function testSetSelectedWorkGroup1()
    {
        //Arrage

        $newWorkGroup = new WorkGroup();
        $newWorkGroup->setId(12312312);

        $session = new SessionMock([
            'selected_enterprise_id' => 123123
        ]);
        $em = $this->getEntityManagerMock();
        $container = new ContainerMock([
            'session' => $session
        ]);

        //Act
        $commonService = new CommonService($em, $container);
        $commonService->setSelectedWorkGroup($newWorkGroup);

        //Assert
        $this->assertEquals($newWorkGroup->getId(), $session->get('selected_workGroup_id'));
        $this->assertFalse($session->has('selected_enterprise_id'));
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function unsetSelectedWorkGroup()
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Tests the unsetSelectedWorkGroup method, this one must clear 2 session variables
     *
     * @test
     */
    public function testUnsetSelectedWorkGroup1()
    {
        //Arrage

        $session = new SessionMock([
            'selected_enterprise_id' => 123123,
            'selected_workGroup_id' => 123124123
        ]);
        $em = $this->getEntityManagerMock();
        $container = new ContainerMock([
            'session' => $session
        ]);

        //Act
        $commonService = new CommonService($em, $container);
        $commonService->unsetSelectedWorkGroup();

        //Assert
        $this->assertFalse($session->has('selected_workGroup_id'));
        $this->assertFalse($session->has('selected_enterprise_id'));
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function getUserWorkGroups()
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Tests that getUserWorkGroups throws exception if not user authenticated
     * @test
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage Unable to find user logged
     */
    public function testGetUserWorkGroupsThrowsExceptionIfNotUserAuthenticated()
    {
        //Arrage

        $containerMock = new ContainerMock([
           'security.context' => new SecurityContextMock(null, false)
        ]);

        //Act

        $commonService = new CommonService(self::$em, $containerMock);

        $commonService->getUserWorkGroups();

        //Assert
    }

    /**
     * Tests that getUserWorkGroups for ROLE_Owner should return all workGroups
     * @test
     */
    public function testGetUserWorkGroupsForOwnerReturnsAllWorkGroups()
    {
        //Arrage
        $expectedNumWorkGroups = count(TestData::getWorkGroups());

        $containerMock = new ContainerMock([
           'security.context' => new SecurityContextMock(new User(),true,["ROLE_OWNER"])
        ]);

        $entityManagerMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => new WorkGroupRepositoryMock()
        ]);

        //Act

        $commonService = new CommonService($entityManagerMock, $containerMock);
        $workGroups = $commonService->getUserWorkGroups();

        //Assert
        $this->assertEquals($expectedNumWorkGroups, count($workGroups));
    }

    /**
     * tests that an enterprise admin sholud get enterprise workGroups
     * @test
     */
    public function testGetUserWorkGroupsForAdminGetAllEnterpriseWorkGroups()
    {
        //Arrage
        $enterprise = new Enterprise();
        $enterprise->setId(1);

        $user = new User();

        $repo = new WorkGroupRepositoryMock();

        $expectedWorkGroups = $repo->findByEnterprise($enterprise);


        $containerMock = new ContainerMock([
           'security.context' => new SecurityContextMock($user, true, ["ROLE_ADMIN"])
        ]);

        $entityManagerMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => $repo
        ]);

        //Act

        $commonService = new CommonService($entityManagerMock, $containerMock);
        $resultWorkGroups = $commonService->getUserWorkGroups();

        //Assert
        $this->assertEquals(count($expectedWorkGroups), count($resultWorkGroups));
    }

    /**
     * tests than a user that has no role only can acces his workGroups
     * @test
     */
    public function testGetUserWorkGroupAnUserWithoutRoleOnlyCanGetAssignedWorkGroups()
    {
        //Arrage
        $user = new User();
        $user->setId(2);

        $repo = new WorkGroupRepositoryMock();

        $expectedWorkGroups = $repo->findByUser($user);


        $containerMock = new ContainerMock([
            'security.context' => new SecurityContextMock($user)
        ]);

        $entityManagerMock = new EntityManagerMock([
            'UnirCloudBoxBundle:WorkGroup' => $repo
        ]);

        //Act

        $commonService = new CommonService($entityManagerMock, $containerMock);
        $resultWorkGroups = $commonService->getUserWorkGroups();

        //Assert
        $this->assertEquals(count($expectedWorkGroups), count($resultWorkGroups));
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function isAllowToView(User $user, Wod $wod)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * ROLE_OWNER users always can view all
     * @test
     */
    public function testIsAllowToView1()
    {
        //Arrage

        $loggedUser = new User();
        $checkWod = new Wod();

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, ['ROLE_OWNER']);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * Not same enterprise, returns false
     * @test
     */
    public function testIsAllowToView2()
    {
        //Arrage

        $enterprise1 = new Enterprise();
        $enterprise1->setId(1);
        $enterprise2 = new Enterprise();
        $enterprise2->setId(2);

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWod->setWodGroup(new WodGroup());
        $checkWod->getWodGroup()->setWorkGroup(new WorkGroup());
        $checkWod->getWodGroup()->getWorkGroup()->setEnterprise($enterprise2);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertFalse($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * ROLE_ADMIN can view all from their enterprise
     * @test
     */
    public function testIsAllowToView3()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWod->setWodGroup(new WodGroup());
        $checkWod->getWodGroup()->setWorkGroup(new WorkGroup());
        $checkWod->getWodGroup()->getWorkGroup()->setEnterprise($enterprise1);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, ['ROLE_ADMIN']);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * User is not in workGroup, he cannot view nothing
     * @test
     */
    public function testIsAllowToView4()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWod->setWodGroup(new WodGroup());
        $checkWod->getWodGroup()->setWorkGroup(new WorkGroup());
        $checkWod->getWodGroup()->getWorkGroup()->setEnterprise($enterprise1);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertFalse($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * User is assigned, can view it
     * @test
     */
    public function testIsAllowToView5()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWod->setWodGroup(new WodGroup());
        $checkWod->getWodGroup()->setWorkGroup(new WorkGroup());
        $checkWod->getWodGroup()->getWorkGroup()->setEnterprise($enterprise1);
        $checkWod->getWodGroup()->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->addUsersAssigned($loggedUser);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));

    }

    /**
     * WodGroupVisible for all and Taks visible for all, can view it
     * @test
     */
    public function testIsAllowToView6()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * WodGroupVisible for all and Wod visible by followers user cannot se it without be follower
     * @test
     */
    public function testIsAllowToView7()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ASSIGNED_AND_FOLLOWERS);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertFalse($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * WodGroupVisible for all and Wod visible by followers user can see it being follower
     * @test
     */
    public function testIsAllowToView8()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ASSIGNED_AND_FOLLOWERS);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);


        $checkWod->addUsersFollower($loggedUser);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * WodGroupVisible opnly for followers user cannot see it
     * @test
     */
    public function testIsAllowToView9()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_FOLLOWERS_AND_ASSIGNED);


        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertFalse($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * WodGroupVisible opnly for followers, wod visible by all and user is on group and can see it
     * @test
     */
    public function testIsAllowToView10()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_FOLLOWERS_AND_ASSIGNED);

        $checkWodGroup->addUserMember($loggedUser);


        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * WodGroupVisible opnly for followers, wod visible by all and user is follower, can see it
     * @test
     */
    public function testIsAllowToView11()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_FOLLOWERS_AND_ASSIGNED);

        $checkWod->addUsersFollower($loggedUser);


        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * WodGroupVisible opnly for followers, wod visible only for assigned or followers,
     * user is follower of the wod, can view it
     * @test
     */
    public function testIsAllowToView12()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ASSIGNED_AND_FOLLOWERS);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_FOLLOWERS_AND_ASSIGNED);

        $checkWod->addUsersFollower($loggedUser);


        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * WodGroupVisible opnly for followers, wod visible only for assigned ,
     * user is follower of the wod, cant view it
     * @test
     */
    public function testIsAllowToView13()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ASSIGNED);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_FOLLOWERS_AND_ASSIGNED);

        $checkWod->addUsersFollower($loggedUser);


        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertFalse($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * User creator of the wod, always can see it
     * user is follower of the wod, cant view it
     * @test
     */
    public function testIsAllowToView14()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ASSIGNED);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_FOLLOWERS_AND_ASSIGNED);

        $checkWod->setCreationUser($loggedUser);


        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));
    }

    /**
     * WodGroup is visible by all, wod is visible by all and user is grom a workGroup's group
     * @test
     */
    public function testIsAllowToView15()
    {
        //Arrage

        $enterprise = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise);
        $group = new Group();
        $group->addUser($loggedUser);
        $checkWodGroup->getWorkGroup()->addGroupMember($group);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToView($loggedUser, $checkWod));
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function isAllowToEdit(User $user, Wod $wod)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * User owner always can edit
     * @test
     */
    public function testIsAlowToEdit1()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $checkWod->setCreationUser($loggedUser);


        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, ['ROLE_OWNER']);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToEdit($loggedUser, $checkWod));
    }

    /**
     * User not in same workGroup than the wod, then cannot edit it
     * @test
     */
    public function testIsAlowToEdit2()
    {
        //Arrage

        $enterprise1 = new Enterprise();
        $enterprise1->setId(1);
        $enterprise2 = new Enterprise();
        $enterprise2->setId(2);

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise2);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertFalse($commonService->isAllowToEdit($loggedUser, $checkWod));
    }

    /**
     * User is ROLE_ADMIN, can edit it
     * @test
     */
    public function testIsAlowToEdit3()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, ['ROLE_ADMIN']);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToEdit($loggedUser, $checkWod));
    }

    /**
     * User is creator, can edit it
     * @test
     */
    public function testIsAlowToEdit4()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $checkWod->setCreationUser($loggedUser);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToEdit($loggedUser, $checkWod));
    }

    /**
     * User is assigned, can edit it
     * @test
     */
    public function testIsAlowToEdit5()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $checkWod->addUsersAssigned($loggedUser);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToEdit($loggedUser, $checkWod));
    }

    /**
     * User is on group and wod can be viewed by all
     * @test
     */
    public function testIsAlowToEdit6()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ALL);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $checkWodGroup->addUserMember($loggedUser);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertTrue($commonService->isAllowToEdit($loggedUser, $checkWod));
    }

    /**
     * User is on group and wod can be viewed by assigned and followers, cannot edit it
     * @test
     */
    public function testIsAlowToEdit7()
    {
        //Arrage

        $enterprise1 = new Enterprise();

        $loggedUser = new User();
        $checkWod = new Wod();
        $checkWodGroup = new WodGroup();

        $checkWod->setWodGroup($checkWodGroup);
        $checkWodGroup->setWorkGroup(new WorkGroup());
        $checkWodGroup->getWorkGroup()->setEnterprise($enterprise1);
        $checkWodGroup->getWorkGroup()->addUserMember($loggedUser);

        $checkWod->setAvailableViewers(Wod::VIEW_BY_ASSIGNED_AND_FOLLOWERS);
        $checkWodGroup->setAvailableViewers(WodGroup::VIEW_BY_ALL);

        $checkWodGroup->addUserMember($loggedUser);

        $em = $this->getEntityManagerMock();
        $sCtx = new SecurityContextMock($loggedUser, true, []);
        $container = new ContainerMock([
            'security.context' => $sCtx
        ]);

        //ACT
        $commonService = new CommonService($em, $container);

        //ASSERT
        $this->assertFalse($commonService->isAllowToEdit($loggedUser, $checkWod));
    }
}
