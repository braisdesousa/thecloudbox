<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 31/07/14
 * Time: 10:24
 */

namespace Unir\CloudBoxBundle\Tests\BaseClass;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;

/**
 * Class for use as base for test Services
 * Class BaseTestService
 * @package Unir\CloudBoxBundle\Tests\BaseClass
 */
class BaseTestService extends KernelTestCase
{

    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * @var EntityManagerInterface
     */
    protected static $em;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        TestData::initialize();

        self::bootKernel();
        self::$container = static::$kernel->getContainer();
        self::$em = self::$container->get("doctrine")->getManager();
    }

    /**
     * Retrieves a mock from the enterpriseRepository
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getEnterpriseRepositoryMock()
    {
        return $this->getMockBuilder('Unir\CloudBoxBundle\Repository\EnterpriseRepository')
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * Retrieves a mock from the workGroupRepository
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getWorkGroupRepositoryMock()
    {
        return $this->getMockBuilder('Unir\CloudBoxBundle\Repository\WorkGroupRepository')
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * Retrieves a mock from the wodActivityRepository
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getWodActivityRepositoryMock()
    {
        return $this->getMockBuilder('Unir\CloudBoxBundle\Repository\WodActivityRepository')
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * Retrieves a mock from the WodGroupRepository
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getWodGroupRepositoryMock()
    {
        return $this->getMockBuilder('Unir\CloudBoxBundle\Repository\WodGroupRepository')
            ->disableOriginalConstructor()
            ->getMock();
    }


    /**
     * Retrieves a mock from the WodRepository
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getWodRepositoryMock()
    {
        return $this->getMockBuilder('Unir\CloudBoxBundle\Repository\WodRepository')
            ->disableOriginalConstructor()
            ->getMock();
    }


    /**
     * Retrieves a mock from the UserGroupRepository
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getUserGroupRepositoryMock()
    {
        return $this->getMockBuilder('Unir\CloudBoxBundle\Repository\UserGroupRepository')
            ->disableOriginalConstructor()
            ->getMock();
    }


    /**
     * Retrieves a mock from the UserRepository
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getUserRepositoryMock()
    {
        return $this->getMockBuilder('Unir\CloudBoxBundle\Repository\UserRepository')
            ->disableOriginalConstructor()
            ->getMock();
    }


    /**
     * Retrieves a mock from entityManager
     */
    protected function getEntityManagerMock()
    {
        return new EntityManagerMock([
            'UnirCloudBoxBundle:Enterprise' => $this->getEnterpriseRepositoryMock(),
            'UnirCloudBoxBundle:WorkGroup' => $this->getWorkGroupRepositoryMock(),
            'UnirCloudBoxBundle:WodActivity' => $this->getWodActivityRepositoryMock(),
            'UnirCloudBoxBundle:WodGroup' => $this->getWodGroupRepositoryMock(),
            'UnirCloudBoxBundle:Wod' => $this->getWodRepositoryMock(),
            'UnirCloudBoxBundle:UserGroup' => $this->getUserGroupRepositoryMock(),
            'UnirCloudBoxBundle:Group' => $this->getUserGroupRepositoryMock(),
            'UnirCloudBoxBundle:User' => $this->getUserRepositoryMock(),
        ]);
    }


    /**
     * gets a mock for commonService
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getCommonServiceMock()
    {
        return $this->getMockBuilder('Unir\CloudBoxBundle\Service\CommonService')
            ->disableOriginalConstructor()
            ->getMock();
    }
}

