<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 23/07/14
 * Time: 14:40
 */

namespace Unir\CloudBoxBundle\Tests\BaseClass;

use Doctrine\Bundle\FixturesBundle\Command\LoadDataFixturesDoctrineCommand;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Unir\CloudBoxBundle\DataFixtures\ORM\LoadCloudBoxManagerEnterprises;
use Unir\CloudBoxBundle\DataFixtures\ORM\LoadCloudBoxManagerWorkGroups;
use Unir\CloudBoxBundle\DataFixtures\ORM\LoadCloudBoxManagerWodGroups;
use Unir\CloudBoxBundle\DataFixtures\ORM\LoadCloudBoxManagerWods;
use Unir\CloudBoxBundle\DataFixtures\ORM\LoadCloudBoxManagerUsers;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

/**
 * Class BaseTestRepository
 * @package Unir\CloudBoxBundle\Tests\BaseClass
 */
class BaseTestRepository extends KernelTestCase
{
    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * @var EntityManagerInterface
     */
    protected static $em;

    /**
     * determines if that is loaded
     * @var bool
     */
    protected static $isDataLoaded = false;

    /**
     * Indicates if must drop schema and create before tests begins
     */
    const DROP_SCHEMA = false;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        TestData::initialize();

        //Kernel options, this one is usefull for specify another environment
        $kernelOptions = ['environment' => 'test', 'debug' => true];

        self::bootKernel($kernelOptions);
        self::$container = static::$kernel->getContainer();
        self::$em = self::$container->get("doctrine")->getManager();

        self::clearAndRebuildSchema();

    }

    /**
     * Clears the schema and rebuilds all data text fixtures on it
     */
    protected static function clearAndRebuildSchema()
    {
        //This will ensure that fixtures are only loaded one time, not
        // any time that a repository is tested
        if (self::$isDataLoaded) {
            return;
        }

        //This lines down will drop schema
        //For test purpose i will put a constant that will be checked for drop or not this one


        if (self::DROP_SCHEMA) {
            $schemaTool = new SchemaTool(self::$em);
            $metadata = self::$em->getMetadataFactory()->getAllMetadata();

            echo "Dropping schema... \n";
            foreach ($schemaTool->getDropSchemaSQL($metadata) as $str) {
                echo $str . "\n";
            }
            $schemaTool->dropSchema($metadata);

            echo "Creating schema... \n";

            foreach ($schemaTool->getCreateSchemaSql($metadata) as $str) {
                echo $str . "\n";
            }
            $schemaTool->createSchema($metadata);
        }




        $loader = new ContainerAwareLoader(self::$container);
        $loader->loadFromDirectory(__DIR__."/../../DataFixtures/ORM/");

        $purger = new ORMPurger(self::$em);
        $executor = new ORMExecutor(self::$em, $purger);


        $executor->execute($loader->getFixtures());

        self::$isDataLoaded = true;
    }

    /**
     * Ensures that 2 arrays have the same elements
     * @param array $array1
     * @param array $array2
     */
    protected function assertArrayHasSameElements(array $array1, array $array2)
    {
        $message = 'arrays haven\'t same elements';

        if (count($array1) != count($array2)) {
            $this->fail($message);
        }

        foreach ($array1 as $item) {
            if (!in_array($item, $array2)) {
                $this->fail($message);
            }
        }

        foreach ($array2 as $item) {
            if (!in_array($item, $array1)) {
                $this->fail($message);
            }
        }
    }
    /**
     * Trasnforms a collection into an idArray
     * @param array $collection
     * @return array
     */
    protected function toIdArray(array $collection)
    {
        $result = [];

        foreach ($collection as $item) {
            $result[] = $item->getId();
        }

        return $result;
    }

}
