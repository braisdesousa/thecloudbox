<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 1/08/14
 * Time: 9:05
 */

namespace Unir\CloudBoxBundle\Tests\BaseClass;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

/**
 * Base class for all controllers unit tests
 * Class BaseTestController
 * @package Unir\CloudBoxBundle\Tests\BaseClass
 */
class BaseTestController extends WebTestCase
{

    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * @var EntityManagerInterface
     */
    protected static $em;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        TestData::initialize();

        self::bootKernel();
        self::$container = static::$kernel->getContainer();
        self::$em = self::$container->get("doctrine")->getManager();
    }
} 