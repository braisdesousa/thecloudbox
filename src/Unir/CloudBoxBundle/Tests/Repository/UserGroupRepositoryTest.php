<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 18/08/14
 * Time: 19:13
 */

namespace Unir\CloudBoxBundle\Tests\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestRepository;
use Unir\CloudBoxBundle\Repository\WodRepository;
use Unir\CloudBoxBundle\Repository\EnterpriseRepository;
use Unir\CloudBoxBundle\Repository\WorkGroupRepository;
use Unir\CloudBoxBundle\Repository\WodGroupRepository;
use Unir\CloudBoxBundle\Repository\UserRepository;
use Unir\CloudBoxBundle\Repository\UserGroupRepository;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\UserGroupRepositoryMock;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

/**
 *
 * tests for UserGroupRepository
 *
 * Class UserGroupRepositoryTest
 * @package Unir\CloudBoxBundle\Tests\Repository
 */
class UserGroupRepositoryTest extends BaseTestRepository
{
    /**
     * @var EnterpriseRepository
     */
    protected static $enterpriseRepository;

    /**
     * @var UserGroupRepository
     */
    protected static $userGroupRepository;

    /**
     * @var WorkGroupRepository
     */
    protected static $workGroupRepository;

    /**
     * @var UserGroupRepositoryMock
     */
    protected static $userGroupRepositoryMock;


    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$enterpriseRepository = self::$em->getRepository('UnirCloudBoxBundle:Enterprise');
        self::$userGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:Group');
        self::$workGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:WorkGroup');

        self::$userGroupRepositoryMock = new UserGroupRepositoryMock();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByCompany(Enterprise $enterprise, $count = null, $include_deleted = false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByCompany1()
    {
        //Arrage
        $enterpriseId = 1;
        $count = null;
        $include_deleted = false;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userGroupRepositoryMock->findByCompany($enterprise, $count, $include_deleted);

        //Act

        $result = self::$userGroupRepository->findByCompany($enterprise, $count, $include_deleted);

        //Assert

        $this->assertArrayHasSameElements($this->toIdArray($result), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByCompany2()
    {
        //Arrage
        $enterpriseId = 2;
        $count = null;
        $include_deleted = false;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userGroupRepositoryMock->findByCompany($enterprise, $count, $include_deleted);

        //Act

        $result = self::$userGroupRepository->findByCompany($enterprise, $count, $include_deleted);

        //Assert

        $this->assertArrayHasSameElements($this->toIdArray($result), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByCompany3()
    {
        //Arrage
        $enterpriseId = 3;
        $count = null;
        $include_deleted = false;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userGroupRepositoryMock->findByCompany($enterprise, $count, $include_deleted);

        //Act

        $result = self::$userGroupRepository->findByCompany($enterprise, $count, $include_deleted);

        //Assert

        $this->assertArrayHasSameElements($this->toIdArray($result), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByCompany4()
    {
        //Arrage
        $enterpriseId = 1;
        $count = null;
        $include_deleted = true;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userGroupRepositoryMock->findByCompany($enterprise, $count, $include_deleted);

        //Act

        $result = self::$userGroupRepository->findByCompany($enterprise, $count, $include_deleted);

        //Assert

        $this->assertArrayHasSameElements($this->toIdArray($result), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByCompany5()
    {
        //Arrage
        $enterpriseId = 1;
        $count = true;
        $include_deleted = false;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userGroupRepositoryMock->findByCompany($enterprise, $count, $include_deleted);

        //Act

        $result = self::$userGroupRepository->findByCompany($enterprise, $count, $include_deleted);

        //Assert

        $this->assertEquals($expected, $result);
    }

}

