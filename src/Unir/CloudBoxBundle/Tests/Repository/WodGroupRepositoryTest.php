<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 28/07/14
 * Time: 12:18
 */

namespace Unir\CloudBoxBundle\Tests\Repository;

use Unir\CloudBoxBundle\Repository\WodGroupRepository;
use Unir\CloudBoxBundle\Repository\EnterpriseRepository;
use Unir\CloudBoxBundle\Repository\WorkGroupRepository;
use Unir\CloudBoxBundle\Repository\UserRepository;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestRepository;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\WodGroupRepositoryMock;
use Unir\CloudBoxBundle\Entity\Wod;

/**
 * Class WodGroupRepositoryTest
 * @package Unir\CloudBoxBundle\Tests\Repository
 */
class WodGroupRepositoryTest extends BaseTestRepository
{

    /**
     * @var WodGroupRepository
     */
    protected static $wodGroupRepository;

    /**
     * @var EnterpriseRepository
     */
    protected static $enterpriseRepository;

    /**
     * @var WorkGroupRepository
     */
    protected static $workGroupRepository;


    /**
     * @var UserRepository
     */
    protected static $userRepository;

    /**
     * @var WodGroupRepositoryMock
     */
    protected static $wodGroupRepositoryMock;


    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$wodGroupRepository = self::$em->getRepository("UnirCloudBoxBundle:WodGroup");
        self::$enterpriseRepository = self::$em->getRepository('UnirCloudBoxBundle:Enterprise');
        self::$workGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:WorkGroup');
        self::$userRepository = self::$em->getRepository('UnirCloudBoxBundle:User');

        self::$wodGroupRepositoryMock = new WodGroupRepositoryMock();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findAll($count=false,$include_deleted=false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindAll1()
    {
        //Arrage

        $count = null;
        $include_deleted = null;

        $expected = self::$wodGroupRepositoryMock->findAll($count, $include_deleted);

        //Act
        $result = self::$wodGroupRepository->findAll($count, $include_deleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindAll2()
    {
        //Arrage

        $count = true;
        $include_deleted = null;

        $expected = self::$wodGroupRepositoryMock->findAll($count, $include_deleted);

        //Act
        $result = self::$wodGroupRepository->findAll($count, $include_deleted);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindAll3()
    {
        //Arrage

        $count = null;
        $include_deleted = true;

        $expected = self::$wodGroupRepositoryMock->findAll($count, $include_deleted);

        //Act
        $result = self::$wodGroupRepository->findAll($count, $include_deleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function countAll()
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testCountAll1()
    {
        //Arrage
        $include_deleted = null;

        $expected = self::$wodGroupRepositoryMock->countAll($include_deleted);

        //Act
        $result = self::$wodGroupRepository->countAll($include_deleted);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testCountAll2()
    {
        //Arrage
        $include_deleted = null;

        $expected = self::$wodGroupRepositoryMock->countAll($include_deleted);

        //Act
        $result = self::$wodGroupRepository->countAll($include_deleted);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWorkGroup(WorkGroup $workGroup,$count=false,array $orderBy=null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWorkGroup1()
    {
        //Arrage
        $workGroupId = 1;
        $count = false;
        $orderBy = null;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByWorkGroup($workGroup, $count, $orderBy);

        //ACT
        $result = self::$wodGroupRepository->findByWorkGroup($workGroup, $count, $orderBy);

        //ASSERT
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWorkGroup2()
    {
        //Arrage
        $workGroupId = 2;
        $count = false;
        $orderBy = null;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByWorkGroup($workGroup, $count, $orderBy);

        //ACT
        $result = self::$wodGroupRepository->findByWorkGroup($workGroup, $count, $orderBy);

        //ASSERT
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWorkGroup3()
    {
        //Arrage
        $workGroupId = 1;
        $count = false;
        $orderBy = ['name' => 'desc'];

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByWorkGroup($workGroup, $count, $orderBy);

        //ACT
        $result = self::$wodGroupRepository->findByWorkGroup($workGroup, $count, $orderBy);

        //ASSERT
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWorkGroup4()
    {
        //Arrage
        $workGroupId = 1;
        $count = true;
        $orderBy = null;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByWorkGroup($workGroup, $count, $orderBy);

        //ACT
        $result = self::$wodGroupRepository->findByWorkGroup($workGroup, $count, $orderBy);

        //ASSERT
        $this->assertEquals($expected, $result);
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUser(User $user,$status= null, array $difficulty=null, array $orderBy=null,$count=null,$include_deleted=false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUser1()
    {
        //Arrage
        $userId = 2;
        $orderBy = null;
        $count = null;
        $include_deleted = null;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodGroupRepositoryMock->findByUser($user, $orderBy, $count, $include_deleted);

        //ACT
        $result = self::$wodGroupRepository->findByUser($user, $orderBy, $count, $include_deleted);

        //ASSERT
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser2()
    {
        //Arrage
        $userId = 3;
        $orderBy = null;
        $count = null;
        $include_deleted = null;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodGroupRepositoryMock->findByUser($user, $orderBy, $count, $include_deleted);

        //ACT
        $result = self::$wodGroupRepository->findByUser($user, $orderBy, $count, $include_deleted);

        //ASSERT
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser3()
    {
        //Arrage
        $userId = 2;
        $orderBy = ['name' => 'desc'];
        $count = null;
        $include_deleted = null;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodGroupRepositoryMock->findByUser($user, $orderBy, $count, $include_deleted);

        //ACT
        $result = self::$wodGroupRepository->findByUser($user, $orderBy, $count, $include_deleted);

        //ASSERT
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser4()
    {
        //Arrage
        $userId = 2;
        $orderBy = null;
        $count = true;
        $include_deleted = null;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodGroupRepositoryMock->findByUser($user, $orderBy, $count, $include_deleted);

        //ACT
        $result = self::$wodGroupRepository->findByUser($user, $orderBy, $count, $include_deleted);

        //ASSERT
        $this->assertEquals($expected, $result);
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUserAndWorkGroup(User $user,WorkGroup $workGroup,$status= null, array $difficulty=null, array $orderBy=null,$count=null,$include_deleted=false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUserAndWorkGroup1()
    {
        //ARRAGE
        $userId = 2;
        $workGroupId = 3;
        $orderBy = null;
        $count = null;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);
        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ACT

        $result = self::$wodGroupRepository->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ASSERT
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserAndWorkGroup2()
    {
        //ARRAGE
        $userId = 2;
        $workGroupId = 1;
        $orderBy = null;
        $count = null;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);
        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ACT

        $result = self::$wodGroupRepository->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ASSERT
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserAndWorkGroup3()
    {
        //ARRAGE
        $userId = 4;
        $workGroupId = 3;
        $orderBy = null;
        $count = null;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);
        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ACT

        $result = self::$wodGroupRepository->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ASSERT
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserAndWorkGroup4()
    {
        //ARRAGE
        $userId = 2;
        $workGroupId = 3;
        $orderBy = null;
        $count = null;
        $include_deleted = true;

        $user = self::$userRepository->find($userId);
        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ACT

        $result = self::$wodGroupRepository->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ASSERT
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserAndWorkGroup5()
    {
        //ARRAGE
        $userId = 2;
        $workGroupId = 1;
        $orderBy = ['name' => 'desc'];
        $count = null;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);
        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ACT

        $result = self::$wodGroupRepository->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ASSERT
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserAndWorkGroup6()
    {
        //ARRAGE
        $userId = 2;
        $workGroupId = 1;
        $orderBy = null;
        $count = true;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);
        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodGroupRepositoryMock->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ACT

        $result = self::$wodGroupRepository->findByUserAndWorkGroup(
            $user,
            $workGroup,
            null,
            null,
            $orderBy,
            $count,
            $include_deleted
        );

        //ASSERT
        $this->assertEquals($expected, $result);
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  public function findByEnterprise(Enterprise $enterprise, $count=false, array $orderBy=null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByEnterprise1()
    {
        //Arrage
        $enterpriseId = 1;
        $count = false;
        $orderBy = null;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodGroupRepositoryMock->findByEnterprise($enterprise, $count, $orderBy);

        //Act
        $result = self::$wodGroupRepository->findByEnterprise($enterprise, $count, $orderBy);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindByEnterprise2()
    {
        //Arrage
        $enterpriseId = 2;
        $count = false;
        $orderBy = null;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodGroupRepositoryMock->findByEnterprise($enterprise, $count, $orderBy);

        //Act
        $result = self::$wodGroupRepository->findByEnterprise($enterprise, $count, $orderBy);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindByEnterprise3()
    {
        //Arrage
        $enterpriseId = 3;
        $count = false;
        $orderBy = null;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodGroupRepositoryMock->findByEnterprise($enterprise, $count, $orderBy);

        //Act
        $result = self::$wodGroupRepository->findByEnterprise($enterprise, $count, $orderBy);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindByEnterprise4()
    {
        //Arrage
        $enterpriseId = 1;
        $count = false;
        $orderBy = ['name' => 'desc'];

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodGroupRepositoryMock->findByEnterprise($enterprise, $count, $orderBy);

        //Act
        $result = self::$wodGroupRepository->findByEnterprise($enterprise, $count, $orderBy);

        //Assert
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindByEnterprise5()
    {
        //Arrage
        $enterpriseId = 1;
        $count = true;
        $orderBy = null;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodGroupRepositoryMock->findByEnterprise($enterprise, $count, $orderBy);

        //Act
        $result = self::$wodGroupRepository->findByEnterprise($enterprise, $count, $orderBy);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findGroupsWithOpenWodsByEnterprise(Enterprise $enterprise)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindGroupsWithOpenWodsByEnterprise1()
    {
        //Arrage
        $enterpriseId = 1;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodGroupRepositoryMock->findGroupsWithOpenWodsByEnterprise($enterprise);

        //Act

        $result = self::$wodGroupRepository->findGroupsWithOpenWodsByEnterprise($enterprise);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindGroupsWithOpenWodsByEnterprise2()
    {
        //Arrage
        $enterpriseId = 2;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodGroupRepositoryMock->findGroupsWithOpenWodsByEnterprise($enterprise);

        //Act

        $result = self::$wodGroupRepository->findGroupsWithOpenWodsByEnterprise($enterprise);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindGroupsWithOpenWodsByEnterprise3()
    {
        //Arrage
        $enterpriseId = 3;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodGroupRepositoryMock->findGroupsWithOpenWodsByEnterprise($enterprise);

        //Act

        $result = self::$wodGroupRepository->findGroupsWithOpenWodsByEnterprise($enterprise);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * private
     * -----------------------------------------------------------------------------------------------------------------
     */

}
 