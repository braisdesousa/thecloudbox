<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 8/08/14
 * Time: 10:49
 */

namespace Unir\CloudBoxBundle\Tests\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestRepository;
use Unir\CloudBoxBundle\Repository\WodRepository;
use Unir\CloudBoxBundle\Repository\EnterpriseRepository;
use Unir\CloudBoxBundle\Repository\WodGroupRepository;
use Unir\CloudBoxBundle\Repository\UserRepository;
use Unir\CloudBoxBundle\Repository\WorkGroupRepository;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\EnterpriseRepositoryMock;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;


/**
 * Intended for test EnterpriseRepository
 * Class EnterpriseRepositoryTest
 * @package Unir\CloudBoxBundle\Tests\Repository
 */
class EnterpriseRepositoryTest extends BaseTestRepository
{

    /**
     * @var EnterpriseRepository
     */
    protected static $enterpriseRepository;

    /**
     * @var EnterpriseRepositoryMock
     */
    protected  static $enterpriseRepositoryMock;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$enterpriseRepository = self::$em->getRepository('UnirCloudBoxBundle:Enterprise');
        self::$enterpriseRepositoryMock = new EnterpriseRepositoryMock();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findAll($exclude_deleted=false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindAll1()
    {
        $activities = TestData::getWodActivities();

        //Arrage
        $include_deleted = false;
        $count = false;

        $expected = self::$enterpriseRepositoryMock->findAll($include_deleted, $count);

        //Act
        $result = self::$enterpriseRepository->findAll($include_deleted, $count);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindAll2()
    {
        //Arrage
        $include_deleted = true;
        $count = false;

        $expected = self::$enterpriseRepositoryMock->findAll($include_deleted, $count);

        //Act
        $result = self::$enterpriseRepository->findAll($include_deleted, $count);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindAll3()
    {
        //Arrage
        $include_deleted = false;
        $count = true;

        $expected = self::$enterpriseRepositoryMock->findAll($include_deleted, $count);

        //Act
        $result = self::$enterpriseRepository->findAll($include_deleted, $count);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * private
     * -----------------------------------------------------------------------------------------------------------------
     */

}
 