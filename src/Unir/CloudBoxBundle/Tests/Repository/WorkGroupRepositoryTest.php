<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 23/07/14
 * Time: 14:07
 */

namespace Unir\CloudBoxBundle\Tests\Repository;

use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestRepository;
use Unir\CloudBoxBundle\Repository\WorkGroupRepository;
use Unir\CloudBoxBundle\Repository\EnterpriseRepository;
use Unir\CloudBoxBundle\Repository\UserRepository;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\WorkGroupRepositoryMock;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\WodRepositoryMock;

/**
 * Class WorkGroupRepositoryTest
 * @package Unir\CloudBoxBundle\Tests\Repository
 */
class WorkGroupRepositoryTest extends BaseTestRepository
{

    /**
     * @var WorkGroupRepository
     */
    protected static $workGroupRepository;

    /**
     * @var EnterpriseRepository
     */
    protected static $enterpriseRepository;

    /**
     * @var UserRepository
     */
    protected static $userRepository;


    /**
     * @var WorkGroupRepositoryMock
     */
    protected static $workGroupRepositoryMock;

    /**
     * Setting before class, this will trigger only one time
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$workGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:WorkGroup');
        self::$enterpriseRepository = self::$em->getRepository('UnirCloudBoxBundle:Enterprise');
        self::$userRepository = self::$em->getRepository('UnirCloudBoxBundle:User');

        self::$workGroupRepositoryMock = new WorkGroupRepositoryMock();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findAll($include_deleted = false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindAll1()
    {
        //Arrage

        $include_deleted = null;

        $expected = self::$workGroupRepositoryMock->findAll($include_deleted);

        //Act
        $result = self::$workGroupRepository->findAll($include_deleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindAll2()
    {
        //Arrage

        $include_deleted = true;

        $expected = self::$workGroupRepositoryMock->findAll($include_deleted);

        //Act
        $result = self::$workGroupRepository->findAll($include_deleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUser(User $user)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUser1()
    {
        //Arrage
        $userId = 1;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUser($user);

        //Act
        $result = self::$workGroupRepository->findByUser($user);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser2()
    {
        //Arrage
        $userId = 2;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUser($user);

        //Act
        $result = self::$workGroupRepository->findByUser($user);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser3()
    {
        //Arrage
        $userId = 3;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUser($user);

        //Act
        $result = self::$workGroupRepository->findByUser($user);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser4()
    {
        //Arrage
        $userId = 4;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUser($user);

        //Act
        $result = self::$workGroupRepository->findByUser($user);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }



    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUserMember(User $user, array $orderBy = null, $count = null, $include_deleted = false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUserMember1()
    {
        //Arrage
        $userId = 2;
        $orderBy = null;
        $count = null;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Act
        $result = self::$workGroupRepository->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserMember2()
    {
        //Arrage
        $userId = 3;
        $orderBy = null;
        $count = null;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Act
        $result = self::$workGroupRepository->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserMember3()
    {
        //Arrage
        $userId = 4;
        $orderBy = null;
        $count = null;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Act
        $result = self::$workGroupRepository->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserMember4()
    {
        //Arrage
        $userId = 2;
        $orderBy = null;
        $count = null;
        $include_deleted = true;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Act
        $result = self::$workGroupRepository->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserMember5()
    {
        //Arrage
        $userId = 2;
        $orderBy = ['name' => 'desc'];
        $count = null;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Act
        $result = self::$workGroupRepository->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Assert
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserMember6()
    {
        //Arrage
        $userId = 2;
        $orderBy = null;
        $count = true;
        $include_deleted = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$workGroupRepositoryMock->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Act
        $result = self::$workGroupRepository->findByUserMember($user, $orderBy, $count, $include_deleted);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findOrdered($orderBy = null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindOrdered1()
    {
        //Arrage

        $orderBy = ['name' => 'asc'];

        $expected = self::$workGroupRepositoryMock->findOrdered($orderBy);

        //Act
        $result = self::$workGroupRepository->findOrdered($orderBy);

        //Assert
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindOrdered2()
    {
        //Arrage

        $orderBy = ['name' => 'desc'];

        $expected = self::$workGroupRepositoryMock->findOrdered($orderBy);

        //Act
        $result = self::$workGroupRepository->findOrdered($orderBy);

        //Assert
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function countAll($include_deleted = false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testCountAll1()
    {
        //Arrage

        $include_deleted = null;

        $expected = self::$workGroupRepositoryMock->countAll($include_deleted);

        //Act
        $result = self::$workGroupRepository->countAll($include_deleted);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testCountAll2()
    {
        //Arrage

        $include_deleted = true;

        $expected = self::$workGroupRepositoryMock->countAll($include_deleted);

        //Act
        $result = self::$workGroupRepository->countAll($include_deleted);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByEnterprise(Enterprise $enterprise, array $orderBy = null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByEnterprise1()
    {
        //Arrage
        $enterpriseId = 1;
        $orderBy = null;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$workGroupRepositoryMock->findByEnterprise($enterprise, $orderBy);

        //ACT
        $result = self::$workGroupRepository->findByEnterprise($enterprise, $orderBy);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByEnterprise2()
    {
        //Arrage
        $enterpriseId = 2;
        $orderBy = null;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$workGroupRepositoryMock->findByEnterprise($enterprise, $orderBy);

        //ACT

        $result = self::$workGroupRepository->findByEnterprise($enterprise, $orderBy);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByEnterprise3()
    {
        //Arrage
        $enterpriseId = 3;
        $orderBy = null;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$workGroupRepositoryMock->findByEnterprise($enterprise, $orderBy);

        //ACT

        $result = self::$workGroupRepository->findByEnterprise($enterprise, $orderBy);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByEnterprise4()
    {
        //Arrage
        $enterpriseId = 1;
        $orderBy = ['name' => 'asc'];

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$workGroupRepositoryMock->findByEnterprise($enterprise, $orderBy);

        //ACT

        $result = self::$workGroupRepository->findByEnterprise($enterprise, $orderBy);

        //Assert
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function countAllByEnterprise(Enterprise $enterprise)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function countAllByEnterprise1()
    {
        //Arrage
        $enterpriseId = 1;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$workGroupRepositoryMock->countAllByEnterprise($enterprise);

        //ACT

        $result = self::$workGroupRepository->countAllByEnterprise($enterprise);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function countAllByEnterprise2()
    {
        //Arrage
        $enterpriseId = 2;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$workGroupRepositoryMock->countAllByEnterprise($enterprise);

        //ACT

        $result = self::$workGroupRepository->countAllByEnterprise($enterprise);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function countAllByEnterprise3()
    {
        //Arrage
        $enterpriseId = 3;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$workGroupRepositoryMock->countAllByEnterprise($enterprise);

        //ACT

        $result = self::$workGroupRepository->countAllByEnterprise($enterprise);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */
}
