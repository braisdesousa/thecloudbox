<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 8/08/14
 * Time: 11:19
 */

namespace Unir\CloudBoxBundle\Tests\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestRepository;
use Unir\CloudBoxBundle\Repository\WodRepository;
use Unir\CloudBoxBundle\Repository\EnterpriseRepository;
use Unir\CloudBoxBundle\Repository\WodGroupRepository;
use Unir\CloudBoxBundle\Repository\WodActivityRepository;
use Unir\CloudBoxBundle\Repository\UserRepository;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\WodActivityRepositoryMock;
use Unir\CloudBoxBundle\Repository\WorkGroupRepository;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

/**
 * Intended for test wodActivityRepository
 * Class WodActivityRepositoryTest
 * @package Unir\CloudBoxBundle\Tests\Repository
 */
class WodActivityRepositoryTest extends BaseTestRepository
{

    /**
     * @var WodActivityRepository
     */
    protected static $wodActivityRepository;

    /**
     * @var UserRepository
     */
    protected static $userRepository;

    /**
     * @var WodActivityRepositoryMock
     */
    protected static $wodActivityRepositoryMock;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$wodActivityRepository = self::$em->getRepository('UnirCloudBoxBundle:WodActivity');
        self::$userRepository = self::$em->getRepository('UnirCloudBoxBundle:User');

        self::$wodActivityRepositoryMock = new WodActivityRepositoryMock();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUser(User $user,array $orderBy=null ,$count=false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUser1()
    {
        //Arrage
        $userId = 2;
        $orderBy = null;
        $count = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodActivityRepositoryMock->findByUser($user, $orderBy, $count);

        //Act
        $result = self::$wodActivityRepository->findByUser($user, $orderBy, $count);

        //Assert

        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser2()
    {
        //Arrage
        $userId = 3;
        $orderBy = null;
        $count = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodActivityRepositoryMock->findByUser($user, $orderBy, $count);

        //Act
        $result = self::$wodActivityRepository->findByUser($user, $orderBy, $count);

        //Assert

        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser4()
    {
        //Arrage
        $userId = 2;
        $orderBy = ['description' => 'desc', 'id' => 'asc'];
        $count = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodActivityRepositoryMock->findByUser($user, $orderBy, $count);

        //Act
        $result = self::$wodActivityRepository->findByUser($user, $orderBy, $count);

        //Assert

        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser5()
    {
        //Arrage
        $userId = 2;
        $orderBy = null;
        $count = true;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodActivityRepositoryMock->findByUser($user, $orderBy, $count);

        //Act
        $result = self::$wodActivityRepository->findByUser($user, $orderBy, $count);

        //Assert

        $this->assertEquals($expected, $result);
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUserRelated(User $user,array $orderBy=null,$count=null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUserRelated1()
    {
        //Arrage
        $userId = 2;
        $orderBy = null;
        $count = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodActivityRepositoryMock->findByUserRelated($user, $orderBy, $count);

        //Act
        $result = self::$wodActivityRepository->findByUserRelated($user, $orderBy, $count);

        //Assert

        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserRelated2()
    {
        //Arrage
        $userId = 3;
        $orderBy = null;
        $count = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodActivityRepositoryMock->findByUserRelated($user, $orderBy, $count);

        //Act
        $result = self::$wodActivityRepository->findByUserRelated($user, $orderBy, $count);

        //Assert

        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserRelated3()
    {
        //Arrage
        $userId = 4;
        $orderBy = null;
        $count = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodActivityRepositoryMock->findByUserRelated($user, $orderBy, $count);

        //Act
        $result = self::$wodActivityRepository->findByUserRelated($user, $orderBy, $count);

        //Assert

        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserRelated5()
    {
        //Arrage
        $userId = 2;
        $orderBy = ['description' => 'desc', 'id' => 'asc'];
        $count = false;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodActivityRepositoryMock->findByUserRelated($user, $orderBy, $count);

        //Act
        $result = self::$wodActivityRepository->findByUserRelated($user, $orderBy, $count);

        //Assert

        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserRelated6()
    {
        //Arrage
        $userId = 2;
        $orderBy = null;
        $count = true;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodActivityRepositoryMock->findByUserRelated($user, $orderBy, $count);

        //Act
        $result = self::$wodActivityRepository->findByUserRelated($user, $orderBy, $count);

        //Assert

        $this->assertEquals($expected, $result);
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * private
     * -----------------------------------------------------------------------------------------------------------------
     */
}
