<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 28/07/14
 * Time: 13:47
 */

namespace Unir\CloudBoxBundle\Tests\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestRepository;
use Unir\CloudBoxBundle\Repository\WodRepository;
use Unir\CloudBoxBundle\Repository\EnterpriseRepository;
use Unir\CloudBoxBundle\Repository\WodGroupRepository;
use Unir\CloudBoxBundle\Repository\UserRepository;
use Unir\CloudBoxBundle\Repository\WorkGroupRepository;
use Unir\CloudBoxBundle\Repository\UserGroupRepository;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\WodRepositoryMock;

/**
 * Tests WodRepository
 * Class WodRepositoryTest
 * @package Unir\CloudBoxBundle\Tests\Repository
 */
class WodRepositoryTest extends BaseTestRepository
{

    /**
     * @var WodRepository
     */
    protected static $wodRepository;

    /**
     * @var WodRepositoryMock
     */
    protected static $wodRepositoryMock;

    /**
     * @var EnterpriseRepository
     */
    protected static $enterpriseRepository;

    /**
     * @var WodGroupRepository
     */
    protected static $wodGroupRepository;

    /**
     * @var UserRepository
     */
    protected static $userRepository;

    /**
     * @var WorkGroupRepository
     */
    protected static $workGroupRepository;

    /**
     * @var UserGroupRepository
     */
    protected static $userGroupRepository;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$wodRepository = self::$em->getRepository('UnirCloudBoxBundle:Wod');
        self::$enterpriseRepository = self::$em->getRepository('UnirCloudBoxBundle:Enterprise');
        self::$wodGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:WodGroup');
        self::$userRepository = self::$em->getRepository('UnirCloudBoxBundle:User');
        self::$workGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:WorkGroup');
        self::$userGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:Group');


        self::$wodRepositoryMock = new WodRepositoryMock();
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findAll(
     *      $status = null,
     *      $difficulty = null,
     *      $orderBy = null,
     *      $count = null,
     *      $include_deleted = false
     * )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindAll1()
    {
        //Arrage

        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;

        $expected = self::$wodRepositoryMock->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindAll2()
    {
        //Arrage

        $status = [Wod::STATUS_CLOSED];
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;

        $expected = self::$wodRepositoryMock->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindAll3()
    {
        //Arrage

        $status = [Wod::STATUS_OPEN, Wod::STATUS_RESOLVED];
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;

        $expected = self::$wodRepositoryMock->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindAll4()
    {
        //Arrage

        $status = null;
        $difficulty = [Wod::DIFFICULTY_CRITICAL];
        $orderBy = null;
        $count = null;
        $include_deleted = null;

        $expected = self::$wodRepositoryMock->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindAll5()
    {
        //Arrage

        $status =  null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;

        $expected = self::$wodRepositoryMock->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindAll6()
    {
        //Arrage

        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = true;

        $expected = self::$wodRepositoryMock->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindAll7()
    {
        //Arrage

        $status = null;
        $difficulty = null;
        $orderBy = ['title' => 'asc'];
        $count = null;
        $include_deleted = true;

        $expected = self::$wodRepositoryMock->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindAll8()
    {
        //Arrage

        $status = null;
        $difficulty = null;
        $orderBy = ['id' => 'desc', 'title' => 'asc'];
        $count = null;
        $include_deleted = true;

        $expected = self::$wodRepositoryMock->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findAll(
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUserGroup(
        Group $group,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUserGroup1()
    {
        //Arrage

        $userGroupId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $userGroup = self::$userGroupRepository->find($userGroupId);

        $expected = self::$wodRepositoryMock->findByUserGroup(
            $userGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUserGroup(
            $userGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserGroup2()
    {
        //Arrage

        $userGroupId = 3;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $userGroup = self::$userGroupRepository->find($userGroupId);

        $expected = self::$wodRepositoryMock->findByUserGroup(
            $userGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUserGroup(
            $userGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserGroup3()
    {
        //Arrage

        $userGroupId = 1;
        $status = [Wod::STATUS_RESOLVED, Wod::STATUS_CLOSED];
        $difficulty = [Wod::DIFFICULTY_HIGH, Wod::DIFFICULTY_CRITICAL];
        $orderBy = ['title' => 'asc'];
        $count = null;
        $include_deleted = null;


        $userGroup = self::$userGroupRepository->find($userGroupId);

        $expected = self::$wodRepositoryMock->findByUserGroup(
            $userGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUserGroup(
            $userGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserGroup4()
    {
        //Arrage

        $userGroupId = 1;
        $status = [Wod::STATUS_RESOLVED, Wod::STATUS_CLOSED];
        $difficulty = [Wod::DIFFICULTY_HIGH, Wod::DIFFICULTY_CRITICAL];
        $orderBy = ['title' => 'asc'];
        $count = true;
        $include_deleted = null;


        $userGroup = self::$userGroupRepository->find($userGroupId);

        $expected = self::$wodRepositoryMock->findByUserGroup(
            $userGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUserGroup(
            $userGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUserGroups(
        Collection $groups,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUserGroups1()
    {
        $userGroupId = [1,2,3,4];
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $userGroups = new ArrayCollection();
        foreach ($userGroupId as $id) {
            $userGroups->add(self::$userGroupRepository->find($id));
        }

        $expected = self::$wodRepositoryMock->findByUserGroups(
            $userGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUserGroups(
            $userGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserGroups2()
    {
        $userGroupId = [1,2,3,4];
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = true;


        $userGroups = new ArrayCollection();
        foreach ($userGroupId as $id) {
            $userGroups->add(self::$userGroupRepository->find($id));
        }

        $expected = self::$wodRepositoryMock->findByUserGroups(
            $userGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUserGroups(
            $userGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByUserGroups3()
    {
        //Arrage
        $userGroupId = [1,2,3,4];
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $userGroups = new ArrayCollection();
        foreach ($userGroupId as $id) {
            $userGroups->add(self::$userGroupRepository->find($id));
        }

        $expected = self::$wodRepositoryMock->findByUserGroups(
            $userGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUserGroups(
            $userGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage FindByUsersGroups recibed and element who is not a Group in the collection
     */
    public function testFindByUserGroups4()
    {
        //Arrage
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $userGroups = new ArrayCollection();
        for ($i = 0; $i < 100; $i++) {
            $userGroups->add(new Wod());
        }

        //Act

        $result = self::$wodRepository->findByUserGroups(
            $userGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWodGroupAndUser(
        User $user,
        WodGroup $wodGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
       )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWodGroupAndUser1()
    {
        $wodGroupId = 1;
        $userId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $wodGroup = self::$wodGroupRepository->find($wodGroupId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWodGroupAndUser(
            $user,
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroupAndUser(
            $user,
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        $arr1 = $this->toIdArray($expected);
        $arr2 = $this->toIdArray($result);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWodGroupAndUser2()
    {
        $wodGroupId = 1;
        $userId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;


        $wodGroup = self::$wodGroupRepository->find($wodGroupId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWodGroupAndUser(
            $user,
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroupAndUser(
            $user,
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByWodGroupAndUser3()
    {
        $wodGroupId = 1;
        $userId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;


        $wodGroup = self::$wodGroupRepository->find($wodGroupId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWodGroupAndUser(
            $user,
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroupAndUser(
            $user,
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWodGroupsAndUser(
            User $user,
            Collection $wodGroups,
            array $status = null,
            array $difficulty = null,
            array $orderBy = null,
            $count = null,
            $include_deleted = false
        )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWodGroupsAndUser1()
    {
        $wodGroupsId = [1,2,3];
        $userId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $wodGroups = new ArrayCollection();
        foreach ($wodGroupsId as $id) {
            $wodGroups->add(self::$wodGroupRepository->find($id));
        }
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWodGroupsAndUser(
            $user,
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroupsAndUser(
            $user,
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindByWodGroupsAndUser2()
    {
        $wodGroupsId = [1,2,3];
        $userId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;


        $wodGroups = new ArrayCollection();
        foreach ($wodGroupsId as $id) {
            $wodGroups->add(self::$wodGroupRepository->find($id));
        }
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWodGroupsAndUser(
            $user,
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroupsAndUser(
            $user,
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWodGroupsAndUser3()
    {
        $wodGroupsId = [1,2,3];
        $userId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;


        $wodGroups = new ArrayCollection();
        foreach ($wodGroupsId as $id) {
            $wodGroups->add(self::$wodGroupRepository->find($id));
        }
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWodGroupsAndUser(
            $user,
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroupsAndUser(
            $user,
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWodGroup(
            WodGroup $wodGroup,
            array $status = null,
            array $difficulty = null,
            array $orderBy = null,
            $count = null,
            $include_deleted = false
        )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWodGroup1()
    {
        $wodGroupId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $wodGroup = self::$wodGroupRepository->find($wodGroupId);

        $expected = self::$wodRepositoryMock->findByWodGroup(
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroup(
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWodGroup2()
    {
        $wodGroupId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;


        $wodGroup = self::$wodGroupRepository->find($wodGroupId);

        $expected = self::$wodRepositoryMock->findByWodGroup(
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroup(
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWodGroup3()
    {
        $wodGroupId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;


        $wodGroup = self::$wodGroupRepository->find($wodGroupId);

        $expected = self::$wodRepositoryMock->findByWodGroup(
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroup(
            $wodGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWodGroups(
            Collection $groups,
            array $status = null,
            array $difficulty = null,
            array $orderBy = null,
            $count = null,
            $include_deleted = false
        )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWodGroups1()
    {
        $wodGroupsId = [1,2,3];
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $wodGroups = new ArrayCollection();
        foreach ($wodGroupsId as $id) {
            $wodGroups->add(self::$wodGroupRepository->find($id));
        }

        $expected = self::$wodRepositoryMock->findByWodGroups(
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroups(
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWodGroups2()
    {
        $wodGroupsId = [1,2,3];
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;


        $wodGroups = new ArrayCollection();
        foreach ($wodGroupsId as $id) {
            $wodGroups->add(self::$wodGroupRepository->find($id));
        }

        $expected = self::$wodRepositoryMock->findByWodGroups(
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroups(
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWodGroups3()
    {
        $wodGroupsId = [1,2,3];
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;


        $wodGroups = new ArrayCollection();
        foreach ($wodGroupsId as $id) {
            $wodGroups->add(self::$wodGroupRepository->find($id));
        }

        $expected = self::$wodRepositoryMock->findByWodGroups(
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWodGroups(
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     * @expectedException \Exception
     * @expectedExceptionMessage Collection Passed to Wodgroup has a non WodGroup Object
     */
    public function testFindByWodGroups4()
    {
        //Arrage
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;


        $wodGroups = new ArrayCollection();
        for ($i = 0; $i < 1000; $i++) {
            $wodGroups->add('lalalalalala'); //This one will trigger exception
        }


        //Act

        $result = self::$wodRepository->findByWodGroups(
            $wodGroups,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByEnterprise(
        Enterprise $enterprise,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByEnterprise1()
    {
        //Arrage
        $enterpriseId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodRepositoryMock->findByEnterprise(
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByEnterprise(
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByEnterprise2()
    {
        //Arrage
        $enterpriseId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodRepositoryMock->findByEnterprise(
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByEnterprise(
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByEnterprise3()
    {
        //Arrage
        $enterpriseId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;


        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodRepositoryMock->findByEnterprise(
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByEnterprise(
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByEnterprise4()
    {
        //Arrage
        $enterpriseId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;


        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$wodRepositoryMock->findByEnterprise(
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByEnterprise(
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByEnterpriseAndUser(
        User $user,
        Enterprise $enterprise,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function testFindByEnterpriseAndUser1()
    {
        //Arrage
        $enterpriseId = 1;
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $enterprise = self::$enterpriseRepository->find($enterpriseId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * {@inheritdoc}
     */
    public function testFindByEnterpriseAndUser2()
    {
        //Arrage
        $enterpriseId = 1;
        $userId = 4;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $enterprise = self::$enterpriseRepository->find($enterpriseId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * {@inheritdoc}
     */
    public function testFindByEnterpriseAndUser3()
    {
        //Arrage
        $enterpriseId = 2;
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;


        $enterprise = self::$enterpriseRepository->find($enterpriseId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * {@inheritdoc}
     */
    public function testFindByEnterpriseAndUser4()
    {
        //Arrage
        $enterpriseId = 1;
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;


        $enterprise = self::$enterpriseRepository->find($enterpriseId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * {@inheritdoc}
     */
    public function testFindByEnterpriseAndUser5()
    {
        //Arrage
        $enterpriseId = 1;
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;


        $enterprise = self::$enterpriseRepository->find($enterpriseId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByEnterpriseAndUser(
            $user,
            $enterprise,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWorkGroup(
        WorkGroup $workGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * {@inheritdoc}
     */
    public function testFindByWorkGroup1()
    {
        //Arrage
        $workGroupId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodRepositoryMock->findByWorkGroup(
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWorkGroup(
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * {@inheritdoc}
     */
    public function testFindByWorkGroup2()
    {
        //Arrage
        $workGroupId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodRepositoryMock->findByWorkGroup(
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWorkGroup(
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * {@inheritdoc}
     */
    public function testFindByWorkGroup3()
    {
        //Arrage
        $workGroupId = 1;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = false;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$wodRepositoryMock->findByWorkGroup(
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWorkGroup(
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWorkGroupAndUser(
        User $user,
        WorkGroup $workGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWorkGroupAndUser1()
    {
        //Arrage
        $workGroupId = 1;
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;

        $workGroup = self::$workGroupRepository->find($workGroupId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWorkGroupAndUser(
            $user,
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWorkGroupAndUser(
            $user,
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWorkGroupAndUser2()
    {
        //Arrage
        $workGroupId = 1;
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;

        $workGroup = self::$workGroupRepository->find($workGroupId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWorkGroupAndUser(
            $user,
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWorkGroupAndUser(
            $user,
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWorkGroupAndUser3()
    {
        //Arrage
        $workGroupId = 1;
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;

        $workGroup = self::$workGroupRepository->find($workGroupId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWorkGroupAndUser(
            $user,
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWorkGroupAndUser(
            $user,
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testFindByWorkGroupAndUser4()
    {
        //Arrage
        $workGroupId = 1;
        $userId = 2;
        $status = [Wod::STATUS_OPEN, Wod::STATUS_INPROGRESS];
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;

        $workGroup = self::$workGroupRepository->find($workGroupId);
        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByWorkGroupAndUser(
            $user,
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByWorkGroupAndUser(
            $user,
            $workGroup,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUser(
        User $user,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = false,
        $include_deleted = false
    )
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUser1()
    {
        //Arrage
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = null;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByUser(
            $user,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUser(
            $user,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser2()
    {
        //Arrage
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = null;
        $include_deleted = true;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByUser(
            $user,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUser(
            $user,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUser3()
    {
        //Arrage
        $userId = 2;
        $status = null;
        $difficulty = null;
        $orderBy = null;
        $count = true;
        $include_deleted = null;

        $user = self::$userRepository->find($userId);

        $expected = self::$wodRepositoryMock->findByUser(
            $user,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Act

        $result = self::$wodRepository->findByUser(
            $user,
            $status,
            $difficulty,
            $orderBy,
            $count,
            $include_deleted
        );

        //Assert
        $this->assertEquals($expected, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function getUserWodsQB(
        User $user,
        array $status = null,
        array $difficulty = null,
        $include_deleted = false
    )
     * -----------------------------------------------------------------------------------------------------------------
     */


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */
}
