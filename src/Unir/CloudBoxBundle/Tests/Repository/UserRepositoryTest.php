<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 8/08/14
 * Time: 8:16
 */

namespace Unir\CloudBoxBundle\Tests\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestRepository;
use Unir\CloudBoxBundle\Repository\WodRepository;
use Unir\CloudBoxBundle\Repository\EnterpriseRepository;
use Unir\CloudBoxBundle\Repository\WodGroupRepository;
use Unir\CloudBoxBundle\Repository\UserRepository;
use Unir\CloudBoxBundle\Repository\WorkGroupRepository;
use Unir\CloudBoxBundle\Repository\UserGroupRepository;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\UserRepositoryMock;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;


/**
 * Intended for test userRepository
 * Class UserRepositoryTest
 * @package Unir\CloudBoxBundle\Tests\Repository
 */
class UserRepositoryTest extends BaseTestRepository
{

    /**
     * @var UserRepository
     */
    protected static $userRepository;

    /**
     * @var EnterpriseRepository
     */
    protected static $enterpriseRepository;

    /**
     * @var WorkGroupRepository
     */
    protected static $workGroupRepository;

    /**
     * @var WodGroupRepository
     */
    protected static $wodGroupRepository;

    /**
     * @var UserGroupRepository
     */
    protected static $userGroupRepository;

    /**
     * @var UserRepositoryMock
     */
    protected static $userRepositoryMock;



    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$userRepository = self::$em->getRepository('UnirCloudBoxBundle:User');
        self::$enterpriseRepository = self::$em->getRepository('UnirCloudBoxBundle:Enterprise');
        self::$workGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:WorkGroup');
        self::$wodGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:WodGroup');
        self::$userGroupRepository = self::$em->getRepository('UnirCloudBoxBundle:Group');
        self::$userRepositoryMock = new UserRepositoryMock();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function countAll()
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testCountAll1()
    {
        //Arrage

        $expected = self::$userRepositoryMock->countAll();

        //Act
        $result = self::$userRepository->countAll();

        //Assert
        $this->assertEquals(6, $result);
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  public function findByEnterprise(Enterprise $enterprise, $orderBy=null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByEnterprise1()
    {
        //Arrage
        $enterpriseId = 1;
        $orderBy = null;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userRepositoryMock->findByEnterprise($enterprise, $orderBy);

        //Act
        $result = self::$userRepository->findByEnterprise($enterprise, $orderBy);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByEnterprise2()
    {

        //Arrage
        $enterpriseId = 1;
        $orderBy = ['email' => 'desc'];

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userRepositoryMock->findByEnterprise($enterprise, $orderBy);

        //Act
        $result = self::$userRepository->findByEnterprise($enterprise, $orderBy);

        //Assert
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByEnterprise3()
    {
        //Arrage
        $enterpriseId = 2;
        $orderBy = null;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userRepositoryMock->findByEnterprise($enterprise, $orderBy);

        //Act
        $result = self::$userRepository->findByEnterprise($enterprise, $orderBy);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function countAllByEnterprise(Enterprise $enterprise)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testCountAllByEnterprise1()
    {
        //Arrage
        $enterpriseId = 1;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userRepositoryMock->countAllByEnterprise($enterprise);

        //Act
        $result = self::$userRepository->countAllByEnterprise($enterprise);

        //Assert
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function testCountAllByEnterprise2()
    {
        //Arrage
        $enterpriseId = 2;

        $enterprise = self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userRepositoryMock->countAllByEnterprise($enterprise);

        //Act
        $result = self::$userRepository->countAllByEnterprise($enterprise);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByWorkGroup(WorkGroup $workGroup,array $status=null,array $difficulty=null,array $orderBy=null,$count=null,$include_deleted=false)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByWorkGroup1()
    {
        //Arrage
        $workGroupId = 1;
        $orderBy = null;
        $count = null;
        $includeDeleted = false;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$userRepositoryMock->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Act
        $result = self::$userRepository->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWorkGroup2()
    {
        //Arrage
        $workGroupId = 1;
        $orderBy = null;
        $count = null;
        $includeDeleted = true;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$userRepositoryMock->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Act
        $result = self::$userRepository->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWorkGroup3()
    {
        //Arrage
        $workGroupId = 3;
        $orderBy = null;
        $count = null;
        $includeDeleted = false;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$userRepositoryMock->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Act
        $result = self::$userRepository->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWorkGroup4()
    {
        //Arrage
        $workGroupId = 1;
        $orderBy = ['email' => 'desc'];
        $count = null;
        $includeDeleted = false;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$userRepositoryMock->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Act
        $result = self::$userRepository->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Assert
        $this->assertEquals($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByWorkGroup5()
    {
        //Arrage
        $workGroupId = 1;
        $orderBy = null;
        $count = true;
        $includeDeleted = false;

        $workGroup = self::$workGroupRepository->find($workGroupId);

        $expected = self::$userRepositoryMock->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Act
        $result = self::$userRepository->findByWorkGroup($workGroup, $orderBy, $count, $includeDeleted);

        //Assert
        $this->assertEquals($expected, $result);
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     *  public function findByUserGroup(Group $group)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByUserGroup1()
    {
        //Arrage
        $userGroupId = 1;

        $userGroup = self::$userGroupRepository->find($userGroupId);

        $expected = self::$userRepositoryMock->findByUserGroup($userGroup);

        //Act
        $result = self::$userRepository->findByUserGroup($userGroup);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserGroup2()
    {
        //Arrage
        $userGroupId = 2;

        $userGroup = self::$userGroupRepository->find($userGroupId);

        $expected = self::$userRepositoryMock->findByUserGroup($userGroup);

        //Act
        $result = self::$userRepository->findByUserGroup($userGroup);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByUserGroup3()
    {
        //Arrage
        $userGroupId =3;

        $userGroup = self::$userGroupRepository->find($userGroupId);

        $expected = self::$userRepositoryMock->findByUserGroup($userGroup);

        //Act
        $result = self::$userRepository->findByUserGroup($userGroup);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }



    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByGroup(WodGroup $group)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByGroup1()
    {
        //Arrage
        $wodGroupId = 1;

        $wodGroup = self::$wodGroupRepository->find($wodGroupId);

        $expected = self::$userRepositoryMock->findByGroup($wodGroup);

        //Act
        $result = self::$userRepository->findByGroup($wodGroup);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByGroup2()
    {
        //Arrage
        $wodGroupId = 2;

        $wodGroup = self::$wodGroupRepository->find($wodGroupId);

        $expected = self::$userRepositoryMock->findByGroup($wodGroup);

        //Act
        $result = self::$userRepository->findByGroup($wodGroup);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByGroupRelatedNotMembers(WodGroup $wodGroup)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByGroupRelatedNotMembers1()
    {
        //Arrage
        $wodGroupId = 1;

        $wodGroup = self::$wodGroupRepository->find($wodGroupId);

        $expected = self::$userRepositoryMock->findByGroupRelatedNotMembers($wodGroup);

        //Act
        $result = self::$userRepository->findByGroupRelatedNotMembers($wodGroup);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByGroupRelatedNotMembers2()
    {
        //Arrage
        $wodGroupId = 2;

        $wodGroup = self::$wodGroupRepository->find($wodGroupId);

        $expected = self::$userRepositoryMock->findByGroupRelatedNotMembers($wodGroup);

        //Act
        $result = self::$userRepository->findByGroupRelatedNotMembers($wodGroup);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByAdminOrOwner(Enterprise $enterprise=null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @test
     */
    public function testFindByAdminOrOwner1()
    {
        //Arrage

        $enterpriseId = null;

        $enterprise = $enterpriseId == null
            ? null
            : self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userRepositoryMock->findByAdminOrOwner($enterprise);

        //Act
        $result = self::$userRepository->findByAdminOrOwner($enterprise);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }

    /**
     * @test
     */
    public function testFindByAdminOrOwner2()
    {
        //Arrage

        $enterpriseId = 1;

        $enterprise = $enterpriseId == null
            ? null
            : self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userRepositoryMock->findByAdminOrOwner($enterprise);

        //Act
        $result = self::$userRepository->findByAdminOrOwner($enterprise);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindByAdminOrOwner3()
    {
        //Arrage

        $enterpriseId = 2;

        $enterprise = $enterpriseId == null
            ? null
            : self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userRepositoryMock->findByAdminOrOwner($enterprise);

        //Act
        $result = self::$userRepository->findByAdminOrOwner($enterprise);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }


    /**
     * @test
     */
    public function testFindByAdminOrOwner4()
    {
        //Arrage

        $enterpriseId = 3;

        $enterprise = $enterpriseId == null
            ? null
            : self::$enterpriseRepository->find($enterpriseId);

        $expected = self::$userRepositoryMock->findByAdminOrOwner($enterprise);

        //Act
        $result = self::$userRepository->findByAdminOrOwner($enterprise);

        //Assert
        $this->assertArrayHasSameElements($this->toIdArray($expected), $this->toIdArray($result));
    }



    /**
     * -----------------------------------------------------------------------------------------------------------------
     * private
     * -----------------------------------------------------------------------------------------------------------------
     */

}
 