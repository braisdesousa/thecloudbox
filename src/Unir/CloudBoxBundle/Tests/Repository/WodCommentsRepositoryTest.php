<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 25/08/14
 * Time: 10:20
 */

namespace Unir\CloudBoxBundle\Tests\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestRepository;
use Unir\CloudBoxBundle\Repository\WodRepository;
use Unir\CloudBoxBundle\Repository\EnterpriseRepository;
use Unir\CloudBoxBundle\Repository\WodGroupRepository;
use Unir\CloudBoxBundle\Repository\WodCommentsRepository;
use Unir\CloudBoxBundle\Repository\UserRepository;
use Unir\CloudBoxBundle\Repository\WorkGroupRepository;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

class WodCommentsRepositoryTest extends BaseTestRepository
{

    /**
     * @var WodCommentsRepository
     */
    protected static $wodCommentsRepository;

    /**
     * @var UserRepository
     */
    protected static $userRepository;


    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$wodCommentsRepository = self::$em->getRepository('UnirCloudBoxBundle:WodComments');
        self::$userRepository = self::$em->getRepository('UnirCloudBoxBundle:User');
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * public function findByUserRelated(User $user, array $orderBy = null, $count = null)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * This will check speed on method, only works if you use dev environment...
     * @test
     */
    public function testFindByUserRelated1()
    {
        //Arrage

        $wodComment = self::$wodCommentsRepository->findOneBy([], ['id' => 'asc']);

        $user = $wodComment->getUser();

        //Act


        $start = microtime(true);
        $result = self::$wodCommentsRepository->findByUserRelated($user);
        $time = microtime(true) - $start;

        //Assert
        //Must be executed in less than 3 seconds...
        $this->assertLessThan(3, $time);
    }

} 