<?php

namespace Unir\CloudBoxBundle\Tests\Controller;


use Symfony\Component\HttpFoundation\Request;
use Unir\CloudBoxBundle\Controller\DefaultController;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestController;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\ContainerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SessionMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityRepositoryMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\SecurityContextMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\RouterMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\RegistryMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\TwigEngineMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\ResponseMock;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\WorkGroupRepositoryMock;
use Unir\CloudBoxBundle\Entity\User;

/**
 * Class DefaultControllerTest
 * @package Unir\CloudBoxBundle\Tests\Controller
 */
class DefaultControllerTest extends BaseTestController
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }


    /**
     *
     */
    public function testEmpty()
    {

    }
}

