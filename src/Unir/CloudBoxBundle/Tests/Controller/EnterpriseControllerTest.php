<?php

namespace Unir\CloudBoxBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\Request;
use Unir\CloudBoxBundle\Controller\EnterpriseController;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Tests\BaseClass\BaseTestController;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\WorkGroupRepositoryMock;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\WodGroupRepositoryMock;
use Unir\CloudBoxBundle\Tests\Mocks\Repository\UserRepositoryMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\ContainerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityRepositoryMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\RegistryMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\FormFactoryMock;
use Unir\CloudBoxBundle\Tests\Mocks\Symfony\RouterMock;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

/**
 * Class EnterpriseControllerTest
 * @package Unir\CloudBoxBundle\Tests\Controller
 */
class EnterpriseControllerTest extends BaseTestController
{
    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    /**
     *
     */
    public function testEmpty()
    {

    }
}
