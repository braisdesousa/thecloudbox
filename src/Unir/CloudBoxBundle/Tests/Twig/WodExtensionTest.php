<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 4/08/14
 * Time: 8:57
 */

namespace Unir\CloudBoxBundle\Tests\Twig;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;

use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;
use Unir\CloudBoxBundle\Twig\WodExtension;

use Unir\CloudBoxBundle\Tests\Mocks\Symfony\EntityManagerMock;

/**
 * Test class for test WodExtension (a twig extension)
 *
 * Class WodExtensionTest
 * @package Unir\CloudBoxBundle\Tests\Twig
 */
class WodExtensionTest extends KernelTestCase
{
    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * @var EntityManagerInterface
     */
    protected static $em;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        TestData::initialize();

        self::bootKernel();

        self::$container = static::$kernel->getContainer();
        self::$em = self::$container->get("doctrine")->getManager();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * function getRoleClass(User $user)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
      * tests getRoleClass with user with role_owner
      */
    public function testGetRoleClass_RoleOwner()
    {
        //Arrage
        $role = new Role();
        $role->setRole(Role::ROLE_OWNER);

        $user = new User();
        $user->addRole($role);

        $expectedResult = 'alert-danger';

        //Act
        $wodExtension = $this->wodExtensionNewInstance();

        $result = $wodExtension->getRoleClass($user);

        //Assert

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * tests getRoleClass with user with role_owner
     */
    public function testGetRoleClass_RoleAdmin()
    {
        //Arrage
        $role = new Role();
        $role->setRole(Role::ROLE_ADMIN);

        $user = new User();
        $user->addRole($role);

        $expectedResult = 'alert-warning';

        //Act
        $wodExtension = $this->wodExtensionNewInstance();

        $result = $wodExtension->getRoleClass($user);

        //Assert

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * tests getRoleClass with user with role_owner
     */
    public function testGetRoleClass_NoRole()
    {
        //Arrage
        $user = new User();

        $expectedResult = '';

        //Act
        $wodExtension = $this->wodExtensionNewInstance();

        $result = $wodExtension->getRoleClass($user);

        //Assert

        $this->assertEquals($expectedResult, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * function  getWodStatusIcon($status)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * test getWodStatusIcon with closed
     * @test
     */
    public function testGetWodStatusIcon_Closed()
    {
        //Arrage
        $status = Wod::STATUS_CLOSED;
        $expectedResult = 'fa fa-lock';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodStatusIcon($status);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * test getWodStatusIcon with Resolved
     * @test
     */
    public function testGetWodStatusIcon_Resolved()
    {
        //Arrage
        $status = Wod::STATUS_RESOLVED;
        $expectedResult = 'fa fa-check';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodStatusIcon($status);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * test getWodStatusIcon with in progress
     * @test
     */
    public function testGetWodStatusIcon_InProgress()
    {
        //Arrage
        $status = Wod::STATUS_INPROGRESS;
        $expectedResult = 'fa fa-ellipsis-h';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodStatusIcon($status);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * test getWodStatusIcon with open
     * @test
     */
    public function testGetWodStatusIcon_Open()
    {
        //Arrage
        $status = Wod::STATUS_OPEN;
        $expectedResult = 'fa fa-unlock';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodStatusIcon($status);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * test getWodStatusIcon with nothing
     * @test
     */
    public function testGetWodStatusIcon_Nothing()
    {
        //Arrage
        $status = '';
        $expectedResult = '';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodStatusIcon($status);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * function getWodDifficulty($difficulty)
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * test wodDifficultyClass with Low
     */
    public function testWodDifficultyClass_Low()
    {
        //Arrage
        $difficulty = Wod::DIFFICULTY_LOW;
        $expectedResult = 'success';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodDifficultyClass($difficulty);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * test wodDifficultyClass with Minor
     */
    public function testWodDifficultyClass_Minor()
    {
        //Arrage
        $difficulty = Wod::DIFFICULTY_MINOR;
        $expectedResult = 'info';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodDifficultyClass($difficulty);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * test wodDifficultyClass with High
     */
    public function testWodDifficultyClass_High()
    {
        //Arrage
        $difficulty = Wod::DIFFICULTY_HIGH;
        $expectedResult = 'warning';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodDifficultyClass($difficulty);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * test wodDifficultyClass with Critical
     */
    public function testWodDifficultyClass_Critical()
    {
        //Arrage
        $difficulty = Wod::DIFFICULTY_CRITICAL;
        $expectedResult = 'danger';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodDifficultyClass($difficulty);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * test wodDifficultyClass with Low
     */
    public function testWodDifficultyClass_Nothing()
    {
        //Arrage
        $difficulty = '';
        $expectedResult = 'default';

        //Act

        $wodExtension = $this->wodExtensionNewInstance();
        $result = $wodExtension->getWodDifficultyClass($difficulty);

        //Assert
        $this->assertEquals($expectedResult, $result);
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * private
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Creates an instance of wodExtension
     * @return WodExtension
     */
    private function wodExtensionNewInstance()
    {
        $repositoryServiceMock = $this->getMockBuilder('Unir\CloudBoxBundle\Service\RepositoryService')
            ->disableOriginalConstructor()
            ->getMock();

        $securityContextMock = $this->getMockBuilder('Symfony\Component\Security\Core\SecurityContext')
            ->disableOriginalConstructor()
            ->getMock();

        $commonServiceMock = $this->getMockBuilder('Unir\CloudBoxBundle\Service\CommonService')
            ->disableOriginalConstructor()
            ->getMock();

        $wodGroupRepositoryServiceMock = $this->getMockBuilder('Unir\CloudBoxBundle\Service\Repository\WodGroupRepositoryService')
            ->disableOriginalConstructor()
            ->getMock();

        $wodRepositoryServiceMock = $this->getMockBuilder('Unir\CloudBoxBundle\Service\Repository\WodRepositoryService')
            ->disableOriginalConstructor()
            ->getMock();

        $workGroupRepositoryServiceMock = $this->getMockBuilder('Unir\CloudBoxBundle\Service\Repository\WorkGroupRepositoryService')
            ->disableOriginalConstructor()
            ->getMock();

        return new WodExtension(
            $repositoryServiceMock,
            $securityContextMock,
            $commonServiceMock,
            new EntityManagerMock(),
            $wodGroupRepositoryServiceMock,
            $wodRepositoryServiceMock,
            $workGroupRepositoryServiceMock
        );
    }
}
 