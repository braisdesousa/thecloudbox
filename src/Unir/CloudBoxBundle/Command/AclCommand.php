<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 9/11/14
 * Time: 5:56 PM
 */

namespace Unir\CloudBoxBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AclCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('unir:acl:generate')
            ->setDescription('Generate ACL for all Wods');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln("Starting to Generate ACL");
        $this->getContainer()->get("unir.acl_wod")->generateAclForAllWods();
        $output->writeln("Done");
    }
}