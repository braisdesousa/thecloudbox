<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 9/11/14
 * Time: 5:56 PM
 */

namespace Unir\CloudBoxBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Project;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;

class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('unir:import')
            ->setDescription('Generate ACL for all Wods');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $output->writeln("Starting to Generate ACL");
        $this->getContainer()->get("doctrine.orm.entity_manager")->beginTransaction();
        try{
            $this->loadRoles($this->getContainer()->get("doctrine.orm.entity_manager"));
            $this->loadJson1($this->getContainer()->get("doctrine.orm.entity_manager"));
            $this->loadJson2($this->getContainer()->get("doctrine.orm.entity_manager"));
            $this->loadJson3($this->getContainer()->get("doctrine.orm.entity_manager"));
            $this->getContainer()->get("doctrine.orm.entity_manager")->commit();
        } catch (\Exception $e){
            $this->getContainer()->get("doctrine.orm.entity_manager")->rollback();
            throw $e;
        }

        $output->writeln("Done");
    }

    private function loadRoles(EntityManager $entityManager){
        foreach (["ROLE_ADMIN","ROLE_OWNER"] as $roleName) {
            $role = new Role($roleName);
            $entityManager->persist($role);

        }
        $entityManager->flush();
    }
    private function loadJson1(EntityManager $entityManager)
    {
        $file=$this->getContainer()->getParameter("kernel.root_dir")."/../dump1.json";
        
        $EnteprriseArray=json_decode(file_get_contents($file),true);

        foreach ($EnteprriseArray as $enterpriseElement){
            $enterprise= new Enterprise();
            $enterprise->setName($enterpriseElement["name"]);
            $enterprise->setCreated(array_key_exists("created",$enterpriseElement)?$enterpriseElement["created"]:null);
            $enterprise->setUpdated(array_key_exists("updated",$enterpriseElement)?$enterpriseElement["updated"]:null);
            if (array_key_exists("deleted",$enterpriseElement)){
                $enterprise->setDeleted(array_key_exists("deleted",$enterpriseElement)?$enterpriseElement["deleted"]:null);
            }

            $entityManager->persist($enterprise);
            foreach($enterpriseElement["projects"] as $projectElement){
                $project= new Project();
                $project->setEnterprise($enterprise);
                $project->setName($projectElement["name"]);
                $project->setDescription(array_key_exists("description",$projectElement)?$projectElement["description"]:null);
                $project->setCreated(array_key_exists("created",$projectElement)?$projectElement["created"]:null);
                $project->setUpdated(array_key_exists("updated",$projectElement)?$projectElement["updated"]:null);
                if(array_key_exists("deleted",$projectElement)){
                    $project->setDeleted(array_key_exists("deleted",$projectElement)?$projectElement["deleted"]:null);
                }
                $entityManager->persist($project);
                foreach ($projectElement["wod_groups"] as $wodGroupElement) {
                    $wodGroup= new WodGroup();
                    $wodGroup->setProject($project);
                    $wodGroup->setName($wodGroupElement["name"]);
                    $wodGroup->setDescription(array_key_exists("description",$wodGroupElement)?$wodGroupElement["description"]:null);
                    $wodGroup->setCreated(array_key_exists("created",$wodGroupElement)?$wodGroupElement["created"]:null);
                    $wodGroup->setUpdated(array_key_exists("updated",$wodGroupElement)?$wodGroupElement["updated"]:null);
                    if(array_key_exists("deleted",$wodGroupElement)){
                        $wodGroup->setDeleted(array_key_exists("deleted",$wodGroupElement)?$wodGroupElement["deleted"]:null);
                    }
                    $entityManager->persist($wodGroup);
                    foreach($wodGroupElement["wods"] as $wodElement){
                        $wod= new Wod();
                        $wod->setTitle($wodElement["title"]);
                        $wod->setWodGroup($wodGroup);
                        $wod->setDescription(array_key_exists("description",$wodElement)?$wodElement["description"]:null);
                        $wod->setAvailableViewers(strtolower($wodElement["view_by"]));
                        $wod->setEstimationTime(array_key_exists("estimation_time",$wodElement)?$wodElement["estimation_time"]:null);
                        $wod->setDifficulty(strtolower($wodElement["difficulty"]));
                        $wod->setStatus(strtolower($wodElement["status"]));
                        $wod->setCreated(array_key_exists("created",$wodElement)?$wodElement["created"]:null);
                        $wod->setUpdated(array_key_exists("updated",$wodElement)?$wodElement["updated"]:null);
                        if (array_key_exists("deleted",$wodElement)){
                            $wod->setDeleted(array_key_exists("deleted",$wodElement)?$wodElement["deleted"]:null);
                        }
                        $entityManager->persist($wod);
                    }
                }
            }
        }
        $entityManager->flush();
        
    }
    private function addUsersTotWodGroupAndProject(Wod $wod,User $user,EntityManager $entityManager){
        $wodGroup=$wod->getWodGroup();
        $project=$wodGroup->getProject();
        $wodGroup->addUser($user);
        $project->getEnterprise()->addAdminUser($user);
        $entityManager->persist($wodGroup);
        $entityManager->persist($project);
}
    private function addUsersTotWodGroupAndProjectFromWodGroup(WodGroup $wodGroup,User $user,EntityManager $entityManager){

        $project=$wodGroup->getProject();
        $wodGroup->addUser($user);
        $project->getEnterprise()->addAdminUser($user);
        $entityManager->persist($wodGroup);
        $entityManager->persist($project);
}
    private function loadJson2(EntityManager $entityManager)
    {
        $file=$this->getContainer()->getParameter("kernel.root_dir")."/../dump2.json";
        $usersArray=json_decode(file_get_contents($file),true);
        foreach ($usersArray as $userElement)
        {
            $user= new User();
            $user->setUsername($userElement["username"]);
            if($user->getUsername()=="admin"){
                $user->addRole($entityManager->getRepository("UnirCloudBoxBundle:Role")->findOneBy(["role"=>"ROLE_OWNER"]));
            }
            $user->setEmail($userElement["email"]);
            $user->setPlainPassword("1234");
            $user->setEnabled(true);
            $this->getContainer()->get("fos_user.user_manager")->updateUser($user);
            foreach ($userElement["created_wods"] as $createdWodArray){
                /**
                 * @var Wod
                 */
                $wod=$entityManager->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($createdWodArray["slug"]);
                $wod->setCreationUser($user);
                $this->addUsersTotWodGroupAndProject($wod,$user,$entityManager);
                $entityManager->persist($wod);

            }
            foreach($userElement["assigned_wods"] as $createdWodArray){
                $wod=$entityManager->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($createdWodArray["slug"]);
                $wod->addUsersAssigned($user);
                $this->addUsersTotWodGroupAndProject($wod,$user,$entityManager);
                $entityManager->persist($wod);
            }
            foreach($userElement["followed_wods"] as $createdWodArray){
                $wod=$entityManager->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($createdWodArray["slug"]);
                $wod->addUsersFollower($user);
                $this->addUsersTotWodGroupAndProject($wod,$user,$entityManager);
                $entityManager->persist($wod);
            }
            foreach($userElement["member_wod_groups"] as $createdWodArray){
                $wod=$entityManager->getRepository("UnirCloudBoxBundle:WodGroup")->findOneByName($createdWodArray["name"]);
                $this->addUsersTotWodGroupAndProjectFromWodGroup($wod,$user,$entityManager);
                $wod->addUser($user);
                $entityManager->persist($wod);
            }
            foreach($userElement["wod_comments"] as $wodCommentArray){
                $wod=$entityManager->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($wodCommentArray["wod"]["slug"]);
                $this->addUsersTotWodGroupAndProject($wod,$user,$entityManager);
                $wodComment=new WodComments();
                $wodComment->setWod($wod);
                $wodComment->setUser($user);
                $wodComment->setComment(array_key_exists("comment",$wodCommentArray)?$wodCommentArray["comment"]:null);
                $wodComment->setCreated(array_key_exists("created",$wodCommentArray)?$wodCommentArray["created"]:null);
                $wodComment->setUpdated(array_key_exists("updated",$wodCommentArray)?$wodCommentArray["updated"]:null);
                if (array_key_exists("deleted",$wodCommentArray)){
                    $wodComment->setDeleted(array_key_exists("deleted",$wodCommentArray)?$wodCommentArray["deleted"]:null);
                }
                $entityManager->persist($wodComment);
            }
            foreach($userElement["wod_activities"] as $wodActivitiesArray){
                $wod=$entityManager->getRepository("UnirCloudBoxBundle:Wod")->findOneBySlug($wodActivitiesArray["wod"]["slug"]);
                $this->addUsersTotWodGroupAndProject($wod,$user,$entityManager);
                $wodActivity=new WodActivity();
                $wodActivity->setDescription(array_key_exists("description",$wodActivitiesArray)?$wodActivitiesArray["description"]:null);
                $wodActivity->setCreated(array_key_exists("created",$wodActivitiesArray)?$wodActivitiesArray["created"]:null);
                $wodActivity->setUpdated(array_key_exists("updated",$wodActivitiesArray)?$wodActivitiesArray["updated"]:null);
                if (array_key_exists("deleted",$wodActivitiesArray)){
                $wodActivity->setDeleted(array_key_exists("deleted",$wodActivitiesArray)?$wodActivitiesArray["deleted"]:null);
            }
                $wodActivity->setAction(array_key_exists("action",$wodActivitiesArray)?$wodActivitiesArray["action"]:"Undefined");
                $wodActivity->setWod($wod);
                $wodActivity->setUser($user);
                $entityManager->persist($wodActivity);
            }

        }
        $entityManager->flush();

    }
    private function loadJson3(EntityManager $entityManager)
    {
        $file=$this->getContainer()->getParameter("kernel.root_dir")."/../dump3.json";
        $GroupsArray=json_decode(file_get_contents($file));
        //$entityManager->flush();

    }
}