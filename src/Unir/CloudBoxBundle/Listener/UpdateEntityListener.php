<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 9/07/14
 * Time: 17:00
 */

namespace Unir\CloudBoxBundle\Listener;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Tests\DataFixtures\TestData;

/**
 * Class  that is a Doctrine's listener.
 * This one will track entities on Doctrine and will generate new ones
 * Class UpdateEntityListener
 * @package Unir\CloudBoxBundle\Listener
 */
class UpdateEntityListener
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    protected $activities;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->activities = new ArrayCollection();

    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PUBLIC METHODS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * When there is a doctrine flush...
     * @param OnFlushEventArgs $event
     */
    public function onFlush(OnFlushEventArgs $event)
    {
        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();



        foreach ($uow->getScheduledEntityInsertions() as $entity) {
        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof  Wod) {
                $this->onWodUpdate($entity, $uow->getEntityChangeSet($entity));
            }
        }

        foreach ($uow->getScheduledCollectionDeletions() as $col) {
            if ($col instanceof PersistentCollection) {
                $this->onScheduledCollectionDelete($col);
            }
        }

        foreach ($uow->getScheduledCollectionUpdates() as $col) {
            if ($col instanceof PersistentCollection) {
                $this->onScheduledCollectionUpdate($col);
            }
        }

        //Foreach new activiy generated...
        foreach ($this->activities as $activity) {

            //Persist the new activity
            $em->persist($activity);

            //Computes the change to the entity
            $em->getUnitOfWork()->computeChangeSet(
                $em->getClassMetadata(get_class($activity)),
                $activity
            );

            //Same as anterior
            $em->getUnitOfWork()->recomputeSingleEntityChangeSet(
                $em->getClassMetadata(get_class($activity)),
                $activity
            );

            //If we are on test environment... populate TestData
            if ($this->isTestEnvironment()) {
                TestData::getWodActivities()[] = $activity;
            }
        }

        //Clear collection
        $this->activities = new ArrayCollection();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * On collection delete...
     * @param PersistentCollection $collection
     */
    private function onScheduledCollectionDelete(PersistentCollection $collection)
    {
        //Reducing nesting
        if (!$collection->getOwner() instanceof Wod) {
            return;
        }

        $usersAssignedRemoved = 'Removed All Assigned Users';
        $usersFollowerRemoved = 'Removed All Follower Users';

        $mapping = $collection->getMapping();

        switch ($mapping['fieldName']) {
            case 'usersAssigned':
                $this->activities->add($this->createWodActivity(
                    $collection->getOwner(),
                    $usersAssignedRemoved,
                    WodActivity::UPDATE
                ));
                break;

            case 'usersFollower':
                $this->activities->add($this->createWodActivity(
                    $collection->getOwner(),
                    $usersFollowerRemoved,
                    WodActivity::UPDATE
                ));
                break;
        }
    }

    /**
     * On collection update
     * @param PersistentCollection $collection
     */
    private function onScheduledCollectionUpdate(PersistentCollection $collection)
    {
        //Reducing nesting
        if (!$collection->getOwner() instanceof Wod) {
            return;
        }

        $mapping = $collection->getMapping();
        $usersAssignedInserted = 'Added %s to AssignedUsers';
        $usersAssignedDeleted = 'Removed %s from AssignedUsers';
        $usersFollowerInserted = 'Added %s to FollowerUsers';
        $usersFollowerDeleted = 'Removed %s from FollowerUsers';


        switch ($mapping['fieldName'])
        {
            case 'usersAssigned':

                if ($collection->getInsertDiff()) {
                    $this->activities->add($this->createWodActivity(
                        $collection->getOwner(),
                        sprintf($usersAssignedInserted, $this->getUsernamesList($collection->getInsertDiff())),
                        WodActivity::UPDATE
                    ));
                }

                if ($collection->getDeleteDiff()) {
                    $this->activities->add($this->createWodActivity(
                        $collection->getOwner(),
                        sprintf($usersAssignedDeleted, $this->getUsernamesList($collection->getDeleteDiff())),
                        WodActivity::UPDATE
                    ));
                }

                break;

            case 'usersFollower':

                if ($collection->getInsertDiff()) {
                    $this->activities->add($this->createWodActivity(
                        $collection->getOwner(),
                        sprintf($usersFollowerInserted, $this->getUsernamesList($collection->getInsertDiff())),
                        WodActivity::UPDATE
                    ));
                }

                if ($collection->getDeleteDiff()) {
                    $this->activities->add($this->createWodActivity(
                        $collection->getOwner(),
                        sprintf($usersFollowerDeleted, $this->getUsernamesList($collection->getDeleteDiff())),
                        WodActivity::UPDATE
                    ));
                }

                break;
        }
    }


    /**
     * ON wod Update
     * @param $entity
     * @param $changes
     */
    private function onWodUpdate($entity, $changes)
    {

        $titleChangedFormat = 'changed the title: %s -> %s';
        $descriptionChangedFormat = 'The description had changed';
        $statusChangedFormat = 'changed the status: %s -> %s';
        $difficultyChangedFormat = 'changed the difficulty: %s -> %s';

        if (array_key_exists('title', $changes)) {

            $oldItem = $changes['title']['0'];
            $newItem = $changes['title']['1'];

            $this->activities->add($this->createWodActivity(
                $entity,
                sprintf($titleChangedFormat, $oldItem, $newItem),
                    WodActivity::UPDATE
            ));
        }

        if (array_key_exists('description', $changes)) {
            $this->activities->add($this->createWodActivity(
                $entity,
                sprintf($descriptionChangedFormat),
                    WodActivity::UPDATE
            ));
        }

        if (array_key_exists('status', $changes)) {

            $oldItem = $changes['status']['0'];
            $newItem = $changes['status']['1'];

            $this->activities->add($this->createWodActivity(
                $entity,
                sprintf($statusChangedFormat, $oldItem, $newItem),$newItem
            ));
        }

        if (array_key_exists('difficulty', $changes)) {

            $oldItem = $changes['difficulty']['0'];
            $newItem = $changes['difficulty']['1'];

            $this->activities->add($this->createWodActivity(
                $entity,
                sprintf($difficultyChangedFormat, $oldItem, $newItem),$newItem
            ));
        }
    }



    /**
     * Creates a wod Comment from an entity
     */
    private function createWodActivity(Wod $entity, $description,$action)
    {
        $format = '%s';


        $wodActivity = new WodActivity();
        $wodActivity->setAction($action);
        $securityContext = $this->container->get('security.context');
        $token = $securityContext->getToken();

        //If token is not null and is authenticated, then retrieve logged user
        if ($token && ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))) {
            $loggedUser = $token->getUser();
        } else {
            $loggedUser = $entity->getCreationUser();
        }

        $wodActivity->setDescription(sprintf($format, $description));
        $wodActivity->setWod($entity);
        $wodActivity->setUser($loggedUser);


        return $wodActivity;
    }

    /**
     * Converts users array in a string of names
     * @param array $users
     * @return string
     */
    private function getUsernamesList(array $users)
    {

        if (count($users)==1) {
            return array_shift($users)->getUserName();
        } else {
            return count($users)." Users";
        }
    }

    /**
     * Tests if we are on test Environment
     * @return bool
     */
    private function isTestEnvironment()
    {
        return $this->container->get('kernel') &&  'test' == $this->container->get('kernel')->getEnvironment();
    }
}
