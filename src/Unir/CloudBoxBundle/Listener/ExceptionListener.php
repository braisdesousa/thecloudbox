<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 9/07/14
 * Time: 17:00
 */

namespace Unir\CloudBoxBundle\Listener;

use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Kernel;


class ExceptionListener{


    private $kernel;
    private $logger;


     function __construct(Logger $logger, Kernel $kernel){

        $this->logger= $logger;
        $this->kernel=$kernel;

    }


    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        
        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
//        if ($exception instanceof NotFoundHttpException) {
//            $event->setResponse(new RedirectResponse($this->kernel->getContainer()->get("router")->generate("e404")));
//            $event->stopPropagation();
//            $this->logger->error("404 Error Loading page");
//        } elseif ($exception->getCode()==403) {
//            $event->setResponse(new RedirectResponse($this->kernel->getContainer()->get("router")->generate("e403")));
//            $event->stopPropagation();
//            $this->logger->error("404 Error Loading page");
//        } else {
//
////            $event->setResponse(new RedirectResponse($this->kernel->getContainer()->get("router")->generate("e500")));
////            $event->stopPropagation();
////            $this->logger->error("500 Error Loading page");
//        }

        // Send the modified response object to the event
//        if ($this->kernel->getEnvironment()!="dev")
//            {$event->setResponse($response);}
    }


} 