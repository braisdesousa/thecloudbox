<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/28/14
 * Time: 10:50 AM
 */

namespace Unir\CloudBoxBundle\Listener;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CommonService;
use Unir\CloudBoxBundle\Service\Repository\WorkGroupRepositoryService;

class AuthenticationListener
{

    private $securityContext;
    private $em;
    private $workGroupRepositoryService;
    private $commonService;
    /**
     * onAuthenticationSuccess
     *
     * @author     Joe Sexton <joe@webtipblog.com>
     * @param     InteractiveLoginEvent $event
     */
    public function __construct(SecurityContext $security_context, EntityManager $em,
        WorkGroupRepositoryService $workGroupRepositoryService, CommonService $commonService)
    {
        $this->securityContext=$security_context;
        $this->em=$em;
        $this->workGroupRepositoryService=$workGroupRepositoryService;
        $this->commonService=$commonService;
    }
    public function onAuthenticationSuccess( InteractiveLoginEvent $event)
    {
        $session= $event->getRequest()->getSession();
        $this->commonService->unsetSelectedWorkGroup();
        if ($session && $session->isStarted() && (($user=$this->securityContext->getToken()->getUser()) instanceof User))
        {
           $workGroups=$this->workGroupRepositoryService->findAll();
            if ($workGroups instanceof Collection){
                $workGroups=$workGroups->toArray();
            }
            $workGroup=array_shift($workGroups);
           if ($workGroup){
               $this->commonService->setSelectedWorkGroup($workGroup);
           }
        }
    }
}