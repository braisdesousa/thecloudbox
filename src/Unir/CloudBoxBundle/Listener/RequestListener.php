<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 8/7/14
 * Time: 10:42 AM
 */

namespace Unir\CloudBoxBundle\Listener;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CommonService;
use Unir\CloudBoxBundle\Service\Repository\WorkGroupRepositoryService;

class RequestListener {

    private $securityContext;
    private $em;
    private $workGroupRepositoryService;
    private $commonService;

    function __construct(SecurityContext $security_context, EntityManager $em,WorkGroupRepositoryService $workGroupRepositoryService,CommonService $commonService){
        $this->securityContext=$security_context;
        $this->em=$em;
        $this->workGroupRepositoryService=$workGroupRepositoryService;
        $this->commonService=$commonService;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->isMasterRequest()) {
            $session=$event->getRequest()->getSession();
            if ($session && $session->isStarted() && (($user=$this->securityContext->getToken()->getUser()) instanceof User)){
                if (!$this->commonService->getSelectedWorkGroup()){
                    $workGroups=$this->workGroupRepositoryService->findAll();
                    if ($workGroups instanceof PersistentCollection){
                        $workGroups=$workGroups->toArray();
                    }
                    $workGroup=array_shift($workGroups);
                    if ($workGroup){
                        $this->commonService->setSelectedWorkGroup($workGroup);
                    }
                }
            }
        }
    }
} 