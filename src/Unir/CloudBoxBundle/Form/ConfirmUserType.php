<?php

namespace Unir\CloudBoxBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\RepositoryService;

class ConfirmUserType extends AbstractType
{


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('email','email',[
                'label'=>'email'
            ])
            ->add("username","text",[
                    'label'=>'user.username',
                    "data"=>"",
                    "attr"=>["autocomplete"=>"off"]
                ])
            ->add('plain_password','repeated',[
                    'label'=>"Password",
                    'type'=> 'password',
                    "attr"=>["autocomplete"=>"off"],
                     "data"=>"",
                    'invalid_message'=>"The Password fields must match.",
                    'options' => array('attr' => array('class' => 'password-field')),
                    'required' => true,
                    'first_options'  => array('label' => 'user.password'),
                    'second_options' => array('label' => 'user.passwordRepeat'),
                ]);


    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Unir\CloudBoxBundle\Entity\User',
            'validation_groups' => array('default'),
            'translation_domain'=>'formLabels',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanagerbundle_confirm_user';
    }
}
