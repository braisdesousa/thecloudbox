<?php

namespace Unir\CloudBoxBundle\Form\ngForm;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Service\Repository\WodGroupRepositoryService;
use Unir\CloudBoxBundle\Service\Repository\UserRepositoryService;
use Unir\CloudBoxBundle\Service\RepositoryService;

class UsersFollowerWodType extends AbstractType
{

    private $wodGroupRepositoryService;
    private $userRepositoryService;

    function __construct(WodGroupRepositoryService $wodGroupRepositoryService,UserRepositoryService $userRepositoryService){
        $this->wodGroupRepositoryService=$wodGroupRepositoryService;
        $this->userRepositoryService=$userRepositoryService;
    }
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usersFollower',"entity",[
                'label' => "wod.usersFollower",
                "class" => "UnirCloudBoxBundle:User",
                "choices" =>$this->userRepositoryService->findByCompany(),
                "property" => "username",
                "expanded"=>false,
                "multiple"=>"multiSelect",
                "required"=>false,

            ]);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Unir\CloudBoxBundle\Entity\Wod',
            'translation_domain'=>'formLabels',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanagerbundle_wod_follower';
    }
}
