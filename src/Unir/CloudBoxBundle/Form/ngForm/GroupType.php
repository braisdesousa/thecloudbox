<?php

namespace Unir\CloudBoxBundle\Form\ngForm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Unir\CloudBoxBundle\Service\CommonService;
use Unir\CloudBoxBundle\Service\Repository\UserRepositoryService;

class GroupType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    private $commonService;
    private $userRepositoryService;
    public function __construct(CommonService $commonService,UserRepositoryService $userRepositoryService)
    {
        $this->commonService=$commonService;
        $this->userRepositoryService=$userRepositoryService;

    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,['label'=>'name'])
            ->add('users',"entity",[
                    'label' => "userGroups.userMembers",
                    "class" => "UnirCloudBoxBundle:User",
                    "choices"=> $this->userRepositoryService->findByCompany($this->commonService->getCompany()),
                    "property" => "userName",
                    "required"=>false,
                    "expanded"=>false,
                    "multiple"=> "multiSelect",
                ]);
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Unir\CloudBoxBundle\Entity\Group',
            'translation_domain'=>'formLabels',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanagerbundle_group';
    }
}
