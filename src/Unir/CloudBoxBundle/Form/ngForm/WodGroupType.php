<?php

namespace Unir\CloudBoxBundle\Form\ngForm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Service\CommonService;
use Unir\CloudBoxBundle\Service\Repository\WorkGroupRepositoryService;
use Unir\CloudBoxBundle\Service\Repository\UserGroupRepositoryService;
use Unir\CloudBoxBundle\Service\Repository\UserRepositoryService;
use Unir\CloudBoxBundle\Service\RepositoryService;

class WodGroupType extends AbstractType
{


    private $workGroupRepositoryService;
    private $commonService;
    private $userRepositoryService;
    private $userGroupRepositoryService;
    function __construct(
            WorkGroupRepositoryService $workGroupRepositoryService,
            UserRepositoryService $userRepositoryService,
            UserGroupRepositoryService $userGroupRepositoryService,
            CommonService $commonService ,
            $workGroup=null

    ){

        $this->workGroupRepositoryService=$workGroupRepositoryService;
        $this->commonService=$commonService;
        $this->userRepositoryService=$userRepositoryService;
        $this->userGroupRepositoryService=$userGroupRepositoryService;
    }


        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,['label'=>'name'])
            ->add('description',"textarea",[
                "required"=>false,
                'label'=>'description'
            ])
            ->add('workGroup',"entity",[
                'label' => "wodGroup.workGroup",
                "class" => "UnirCloudBoxBundle:WorkGroup",
                "choices" =>$this->workGroupRepositoryService->findAllFromCompany(),
                "property" => "name",
                "data"=>$this->commonService->getSelectedWorkGroup(),
                "expanded"=>false,
                "multiple"=>false,
            ])
            ->add("groupMembers","entity",[
                'label' => "wodGroup.groupMembers",
                "class" => "UnirCloudBoxBundle:Group",
                "choices" =>$this->userGroupRepositoryService->findByEnterprise($this->commonService->getCompany()),
                "property" => "name",
                "required"=>false,
                "expanded"=>false,
                "multiple"=>"multiSelect",
            ])
            ->add("users","entity",[
                'label' => "wodGroup.userMembers",
                "class" => "UnirCloudBoxBundle:User",
                "choices" =>$this->userRepositoryService->findByCompany($this->commonService->getCompany()),
                "property" => "username",
                "required"=>false,
                "expanded"=>false,
                "multiple"=>"multiSelect",
            ])
        ;
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Unir\CloudBoxBundle\Entity\WodGroup',
            'translation_domain'=>'formLabels',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanagerbundle_wodgroup';
    }
}
