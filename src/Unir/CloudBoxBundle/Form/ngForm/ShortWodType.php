<?php

namespace Unir\CloudBoxBundle\Form\ngForm;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Service\Repository\WodGroupRepositoryService;
use Unir\CloudBoxBundle\Service\Repository\UserRepositoryService;
use Unir\CloudBoxBundle\Service\RepositoryService;

class ShortWodType extends AbstractType
{

    private $wodGroupRepositoryService;
    private $userRepositoryService;

    function __construct(WodGroupRepositoryService $wodGroupRepositoryService,UserRepositoryService $userRepositoryService){
        $this->wodGroupRepositoryService=$wodGroupRepositoryService;
        $this->userRepositoryService=$userRepositoryService;
    }
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',null,['label'=>'wod.title'])
            ->add('description',"textarea",[
                'label'=>'description',
                "required"=>false
            ])
            ->add('estimationTime','text',[
                'label'=>'wod.estimationTime',
                "required"=>false
            ])
            ->add('wodGroup',"entity",[
                'label' => 'wod.wodGroup',
                "class" => "UnirCloudBoxBundle:WodGroup",
                "choices" =>$this->wodGroupRepositoryService->findByWorkGroup(),
                "property" => "name",
                "expanded"=>false,
                "multiple"=>false,
            ])
            ->add('viewBy','choice',[
                'label' => "wod.viewBy",
                    'choices'=> [
                        Wod::VIEW_BY_ALL=>"All",
                        Wod::VIEW_BY_ASSIGNED=>"Assigned Users",
                        Wod::VIEW_BY_ASSIGNED_AND_FOLLOWERS=>"Assigned and Follower Users",
                    ],
                    "data"=> Wod::VIEW_BY_ALL

            ]);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Unir\CloudBoxBundle\Entity\Wod',
            'translation_domain'=>'formLabels',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanagerbundle_wod_short';
    }
}
