<?php

namespace Unir\CloudBoxBundle\Form\ngForm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UpdatePasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plain_password',"repeated",array(
                    'type' => 'password',
                    'invalid_message' => 'The password fields must match.',
                    'options' => array('attr' => array('class' => 'password-field')),
                    'required' => true,
                    'first_options'  => array('label' => 'user.newPassword'),
                    'second_options' => array('label' => 'user.newPasswordRepeat')))
            ->add("actual_password","password",
                [   "label"=>"user.actualPassword",
                    "required"=>true,
                    "mapped"=>false,
                ]);
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Unir\CloudBoxBundle\Entity\User',
                'translation_domain'=>'formLabels',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanaget_update_password';
    }
}
