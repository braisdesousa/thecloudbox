<?php

namespace Unir\CloudBoxBundle\Form\ngForm;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Service\Repository\WodGroupRepositoryService;
use Unir\CloudBoxBundle\Service\Repository\UserRepositoryService;
use Unir\CloudBoxBundle\Service\RepositoryService;

class WodType extends AbstractType
{

    private $wodGroupRepositoryService;
    private $userRepositoryService;

    function __construct(WodGroupRepositoryService $wodGroupRepositoryService,UserRepositoryService $userRepositoryService){
        $this->wodGroupRepositoryService=$wodGroupRepositoryService;
        $this->userRepositoryService=$userRepositoryService;
    }
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status','choice',[
                'label'=>'wod.status',
                'choices'=> ['open'=>'Open','closed'=>'Closed','in progress'=>'In progress','resolved'=>'Resolved']
            ])
            ->add('description',"textarea",[
                'label'=>'description',
                "required"=>false
            ])
            ->add('title',null,['label'=>'wod.title'])
            ->add('difficulty','choice',[
                'label'=>'wod.difficulty',
                'choices'=> [
                        Wod::DIFFICULTY_CRITICAL=>"Critical",
                        Wod::DIFFICULTY_HIGH=>"High",
                        Wod::DIFFICULTY_NORMAL=>"Normal",
                        Wod::DIFFICULTY_MINOR=>"Minor",
                        Wod::DIFFICULTY_LOW=>"Low",
                ],
                "data"=> Wod::DIFFICULTY_NORMAL
            ])
            ->add('estimationTime','text',[
                'label'=>'wod.estimationTime',
                "required"=>false
            ])
            ->add('wodGroup',"entity",[
                'label' => "wod.wodGroup",
                "class" => "UnirCloudBoxBundle:WodGroup",
                "choices" =>$this->wodGroupRepositoryService->findByWorkGroup(),
                "property" => "name",
                "expanded"=>false,
                "multiple"=>false,
            ])
            ->add('usersAssigned',"entity",[
                'label' => "wod.usersAssigned",
                "class" => "UnirCloudBoxBundle:User",
                "choices" =>$this->userRepositoryService->findByCompany(),
                "property" => "username",
                "expanded"=>false,
                "multiple"=>"multiSelect",
                "required"=>false,
            ])
            ->add('usersFollower',"entity",[
                'label' => "wod.usersFollower",
                "class" => "UnirCloudBoxBundle:User",
                "choices" =>$this->userRepositoryService->findByCompany(),
                "property" => "username",
                "expanded"=>false,
                "multiple"=>"multiSelect",
                "required"=>false,

            ]);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Unir\CloudBoxBundle\Entity\Wod',
            'translation_domain'=>'formLabels',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanagerbundle_wod';
    }
}
