<?php

namespace Unir\CloudBoxBundle\Form\ngForm;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CommonService;

class WorkGroupType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    private $em;
    private $commonService;

    function __construct(EntityManager $em,CommonService $commonService)
    {
        $this->em=$em;
        $this->commonService=$commonService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,[
                    'label'=>'name',
                ])
            ->add('description',"textarea",[
                    "required"=>false,
                    "label"=>'description',
                ]);
    }
    /**
     * @return array return enterprises
     */

    private function getCompanyAdminUsers()
    {
       return $this->commonService->getCompany()->getAdminUsers();
    }
    private function getUsers()
    {
        return array_merge($this->commonService->getCompany()->getAdminUsers()->toArray(),$this->commonService->getCompany()->getUsers()->toArray());
    }
    private function getUserGroups()
    {
       return $this->commonService->getCompany()->getGroups();
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Unir\CloudBoxBundle\Entity\WorkGroup',
            'translation_domain'=>'formLabels',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanagerbundle_workGroup';
    }
}
