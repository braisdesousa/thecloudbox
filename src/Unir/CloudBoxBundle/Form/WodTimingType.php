<?php

namespace Unir\CloudBoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WodTimingType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment',null,['label'=>'timing.comment'])
            ->add('date_start',"datetime",['label'=>'timing.dateStart'])
            ->add('date_end',"datetime",['label'=>'timing.dateEnd'])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Unir\CloudBoxBundle\Entity\WodTiming',
            'translation_domain'=>'formLabels',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanagerbundle_wodtiming';
    }
}
