<?php

namespace Unir\CloudBoxBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\RepositoryService;

class UserType extends AbstractType
{

    private $user;
    private $securityContext;
    private $em;
    function __construct(SecurityContext $securityContext,EntityManager $em){
        $this->em=$em;
        $this->user=$securityContext->getToken()->getUser();
        $this->securityContext=$securityContext;
    }
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('email','email',['label'=>'email'])
            ->add("roles","checkbox",[
                    'label'=>"user.isAdmin",
                    'mapped'=>false,
                    "required"=>false,
                    ]

            );
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Unir\CloudBoxBundle\Entity\User',
            'validation_groups' => array('default'),
            'translation_domain'=>'formLabels',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'unir_ticketmanagerbundle_user';
    }
}
