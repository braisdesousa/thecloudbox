<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 22/08/14
 * Time: 12:14
 */

namespace Unir\CloudBoxBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use PhpCollection\CollectionInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Helper\CollectionHelper;

/**
* Class WodCommentRepository
 * @package Unir\CloudBoxBundle\Repository
 */
class WodCommentsRepository extends EntityRepository
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * Public Methods
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * Find wodComments by workGroup
     * @param WorkGroup $workGroup
     * @param array $orderBy
     * @param null $count
     * @return array|int
     */
    public function findByWorkGroup(WorkGroup $workGroup, array $orderBy = null, $count = null)
    {
        $qb = $this->createQueryBuilder('ta')
            ->join('ta.wod', 'ta_t')
            ->join('ta_t.wodGroup', 'ta_t_tG')
            ->join('ta_t_tG.workGroup', 'ta_t_tG_p');


        //Retreive wodActivity from the workGroup
        $qb->where($qb->expr()->eq('ta_t_tG_p', $qb->expr()->literal($workGroup->getId())));

        //Exclude deleted one
        $qb->andWhere($qb->expr()->isNull('ta.deleted'));

        //Order
        if ($orderBy) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy('ta.'.$sort, $order);
            }
        }

        $results = $qb->getQuery()->getResult();

        return $count
            ? count($results)
            : $results;
    }

    /**
     * Find wodComments that are related to an user
     * @param User $user
     * @param array $orderBy
     * @param null $count
     * @return array|int
     */
    public function findByUserRelated(User $user, array $orderBy = null, $count = null)
    {
        //User comments Related are:
        //- Comments that the user have made OR
        //- Comments from wods that user is assigned OR
        //- Comments from wods that are user creator OR
        //- Comments that user belongs to workGroup AND (
        //      - Comments from wods that user is follower AND wod Visibility is not viewByAssigned OR
        //      - Comments from wodGroup's wods that user is follower AND wod
        //          Visibility is not viewByAssigned OR
        //      - Comments from wodGroup's wods that user is member of AND wod
        //          Visibility is not ViewByAssigned OR
        //      - Comments from wodGroup's wods that user is group member AND wod
        //          Visibility is not viewByAssigned OR
        // )

        $qb = $this->_em->createQueryBuilder();

        $userIdLiteral = $qb->expr()->literal($user->getId());
        $viewVyAssignedLiteral = $qb->expr()->literal(Wod::VIEW_BY_ASSIGNED);


        //Extract wod ids where user is assigned of
        $qb = $this->_em->createQueryBuilder();
        $wodsUserIsAssignedOn = CollectionHelper::reduceIdMatrix(
            $qb->select('t.id')
                ->from('Unir\CloudBoxBundle\Entity\Wod', 't')
                ->join('t.usersAssigned', 't_uA')
                ->where($qb->expr()->eq('t_uA', $userIdLiteral))
                ->getQuery()->getResult()
        );

        //Extract wods where user is creator of
        $qb = $this->_em->createQueryBuilder();
        $wodsUserIsCreator = CollectionHelper::reduceIdMatrix(
            $qb->select('t.id')
                ->from('Unir\CloudBoxBundle\Entity\Wod', 't')
                ->where($qb->expr()->eq('t.creationUser', $userIdLiteral))
                ->getQuery()->getResult()
        );

        //Extract wods that user is follower and wodvisibility is not viewByAssigned
        $qb = $this->_em->createQueryBuilder();
        $wodsThatUserIsFollower = CollectionHelper::reduceIdMatrix(
            $qb->select('t.id')
                ->from('Unir\CloudBoxBundle\Entity\Wod', 't')
                ->join('t.usersAssigned', 't_uA')
                ->where($qb->expr()->andX(
                    $qb->expr()->eq('t_uA', $userIdLiteral),
                    $qb->expr()->neq('t.viewBy', $viewVyAssignedLiteral)
                ))->getQuery()->getResult()
        );

        //Extract Wods where user is WodGorup's member and visibility is not viewByAssigned
        $qb = $this->_em->createQueryBuilder();
        $wodsWhereUserIsWodGroupMember = CollectionHelper::reduceIdMatrix(
            $qb->select('t.id')
                ->distinct()
                ->from('Unir\CloudBoxBundle\Entity\Wod', 't')
                ->join('t.wodGroup', 't_tG')
                ->leftJoin('t_tG.userMembers', 't_tG_uM')
                ->where($qb->expr()->andX(
                    $qb->expr()->eq('t_tG_uM', $userIdLiteral),
                    $qb->expr()->neq('t.viewBy', $viewVyAssignedLiteral)
                ))->getQuery()->getResult()
        );

        //Extract Wods where user is in a group that is member of WodGroup and wod is not viewByassigned
        $qb = $this->_em->createQueryBuilder();
        $wodsWhereUserIsWodGroupGroupMember = CollectionHelper::reduceIdMatrix(
            $qb->select('t.id')
                ->distinct()
                ->from('Unir\CloudBoxBundle\Entity\Wod', 't')
                ->join('t.wodGroup', 't_tG')
                ->leftJoin('t_tG.groupMembers', 't_tG_gM')
                ->leftJoin('t_tG_gM.users', 't_tG_gM_u')
                ->where($qb->expr()->andX(
                    $qb->expr()->eq('t_tG_gM_u', $userIdLiteral),
                    $qb->expr()->neq('t.viewBy', $viewVyAssignedLiteral)
                ))->getQuery()->getResult()
        );

        //Extract Wods where user is WodGorup's follower and visibility is not viewByAssigned
        $qb = $this->_em->createQueryBuilder();
        $wodsWhereUserIsGroupFollower = CollectionHelper::reduceIdMatrix(
            $qb->select('t.id')
                ->distinct()
                ->from('Unir\CloudBoxBundle\Entity\Wod', 't')
                ->join('t.wodGroup', 't_tG')
                ->leftJoin('t_tG.userFollowers', 't_tG_uM')
                ->where($qb->expr()->andX(
                    $qb->expr()->eq('t_tG_uM', $userIdLiteral),
                    $qb->expr()->neq('t.viewBy', $viewVyAssignedLiteral)
                ))->getQuery()->getResult()
        );

        //Extract workGroups that user is member of directly
        $qb = $this->_em->createQueryBuilder();
        $workGroupWhereUserIsMemberDirectly = CollectionHelper::reduceIdMatrix(
            $qb->select('p.id')
                ->distinct()
                ->from('Unir\CloudBoxBundle\Entity\WorkGroup', 'p')
                ->join('p.userMembers', 'p_u')
                ->where($qb->expr()->eq('p_u', $userIdLiteral))
                ->getQuery()->getResult()
        );

        //Extract workGroups that user is grou member of
        $qb = $this->_em->createQueryBuilder();
        $workGroupWhereUserIsInGroup = CollectionHelper::reduceIdMatrix(
            $qb->select('p.id')
                ->distinct()
                ->from('Unir\CloudBoxBundle\Entity\WorkGroup', 'p')
                ->join('p.groupMembers', 'p_gM')
                ->join('p_gM.users', 'p_gM_u')
                ->where($qb->expr()->eq('p_gM_u', $userIdLiteral))
                ->getQuery()->getResult()
        );


        $qb = $this->createQueryBuilder('c')
            ->distinct()
            ->join('c.wod', 'c_t') //Wod
            ->join('c.user', 'c_u') //User
            ->join('c_t.wodGroup', 'c_t_tG') //WodGroup
            ->join('c_t_tG.workGroup', 'c_t_tG_p'); //WorkGroup

        $firstLvlOr = $qb->expr()->orX();
        //Comments that the user have made
        $firstLvlOr->add($qb->expr()->eq('c_u', $userIdLiteral));
        //Comments from wods that user is assigned
        if (count($wodsUserIsAssignedOn)) {
            $firstLvlOr->add($qb->expr()->in('c_t', $wodsUserIsAssignedOn));
        }
        //Comments from wods that are user creator
        if (count($wodsUserIsCreator)) {
            $firstLvlOr->add($qb->expr()->in('c_t', $wodsUserIsCreator));
        }

        //User must belong to a workGroup, if not, the query is easier...
        if (count($workGroupWhereUserIsMemberDirectly) + count($workGroupWhereUserIsInGroup) > 0) {


            $secondLvlAnd = $qb->expr()->andX();

            //Comments that user belongs to workGroup
            $secondLvlFirstOr = $qb->expr()->orX();

            //HACK Changes on Daniel tracked bug

            //If user is member directly...
            if (count($workGroupWhereUserIsMemberDirectly)) {
                $secondLvlFirstOr->add($qb->expr()->in('c_t_tG_p', $workGroupWhereUserIsMemberDirectly));
            }

            //If user is on group...
            if (count($workGroupWhereUserIsInGroup)) {
                $secondLvlFirstOr->add($qb->expr()->in('c_t_tG_p', $workGroupWhereUserIsInGroup));
            }

            $secondLvlAnd->add($secondLvlFirstOr);

            //End HACK


            $thirdLvlOr = $qb->expr()->orX();
            //Comments from wods that user is follower AND wod Visibility is not viewByAssigned
            if (count($wodsThatUserIsFollower)) {
                $thirdLvlOr->add($qb->expr()->in('c_t', $wodsThatUserIsFollower));
            }
            //Comments from wodGroup's wods that user is follower AND wod
            //          Visibility is not viewByAssigned
            if (count($wodsWhereUserIsWodGroupMember)) {
                $thirdLvlOr->add($qb->expr()->in('c_t', $wodsWhereUserIsWodGroupMember));
            }
            //Comments from wodGroup's wods that user is member of AND wod
            //          Visibility is not ViewByAssigned
            if (count($wodsWhereUserIsGroupFollower)) {
                $thirdLvlOr->add($qb->expr()->in('c_t', $wodsWhereUserIsGroupFollower));
            }
            //Comments from wodGroup's wods that user is group member AND wod
            //          Visibility is not viewByAssigned
            if (count($wodsWhereUserIsWodGroupGroupMember))  {
                $thirdLvlOr->add($qb->expr()->in('c_t', $wodsWhereUserIsWodGroupGroupMember));
            }


            $firstLvlOr->add($secondLvlAnd->add($thirdLvlOr));
        }

        $qb->where($firstLvlOr);

        //If we order it, is slow as hell...
        $orderBy = $orderBy ? $orderBy : ['id' => 'desc'];

        foreach ($orderBy as $key => $sortOrder) {
            $qb->orderBy('c.'.$key, $sortOrder);
        }

        return $count
            ? count($qb->getQuery()->getResult())
            : $qb->getQuery()->getResult();
    }
}
