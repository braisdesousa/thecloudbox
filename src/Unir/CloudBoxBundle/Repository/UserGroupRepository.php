<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/11/14
 * Time: 1:39 PM
 */

namespace Unir\CloudBoxBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;

/**
 *
 * Class UserGroupRepository
 * @package Unir\CloudBoxBundle\Repository
 */
class UserGroupRepository extends EntityRepository
{

    /**
     *
     * @param Enterprise $enterprise
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findByCompany(Enterprise $enterprise, $count = null, $include_deleted = false)
    {
        $qb = $this->createQueryBuilder("ug");

        if ($count) {
            $qb->select('count(ug)');
        }

        $qb->where($qb->expr()->eq("ug.enterprise", $qb->expr()->literal($enterprise->getId())));

        if (!$include_deleted) {
            $qb->andwhere($qb->expr()->isNull("ug.deleted"));
        }

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }

    /**
     * Returns the userGroups assigned to a workGroup
     * @param WorkGroup $workGroup
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findByWorkGroup(WorkGroup $workGroup, $count = null, $include_deleted = false)
    {
        $qb = $this->createQueryBuilder('ug')
            ->join('ug.memberWorkGroups', 'ug_p');

        if ($count) {
            $qb->select('count(ug)');
        }

        $qb->where($qb->expr()->eq('ug_p', $qb->expr()->literal($workGroup->getId())));

        if (!$include_deleted) {
            $qb->andwhere($qb->expr()->isNull("ug.deleted"));
        }

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();

    }
}
