<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/11/14
 * Time: 1:39 PM
 */

namespace Unir\CloudBoxBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Helper\CollectionHelper;

/**
 * Class UserRepository
 * @package Unir\CloudBoxBundle\Repository
 */
class UserRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function countAll()
    {
        $qb = $this->getEntityManager()
            ->getRepository("UnirCloudBoxBundle:User")->createQueryBuilder("u");
        $qb->select("count(u.id)");

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Enterprise $enterprise
     * @param null $orderBy
     * @return array
     */
    public function findByEnterprise(Enterprise $enterprise, $orderBy = null)
    {
        $qb = $this->getByEnterprise($enterprise, $orderBy);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Enterprise $enterprise
     * @return mixed
     */
    public function countAllByEnterprise(Enterprise $enterprise)
    {
        $qb = $this->getByEnterprise($enterprise);
        $qb->select("count(u.id)");

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Retrieves users by a workGroup
     *
     * @param WorkGroup $workGroup
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findByWorkGroup(WorkGroup $workGroup, array $orderBy = null, $count = null, $include_deleted = false)
    {
        //Users from a workGroup are defined by..
        //- User is workGroup Creator OR
        //- User is member of the workGroup OR
        //- User is in a group that is member of the workGroup

        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.workGroups', 'u_p') //User workGroups that is creator of
            ->leftJoin('u.memberWorkGroups', 'u_mP') //User workGroups that is member of
            ->leftJoin('u.groups', 'u_g') //User groups that user is member of
            ->leftJoin('u_g.memberWorkGroups', 'u_g_mP'); //WorkGroups that userGroup is member of

        $workGroupIdExpr = $qb->expr()->literal($workGroup->getId());

        $firstLvlOr = $qb->expr()->orX();
        //User is workGroup Creator
        $firstLvlOr->add($qb->expr()->eq('u_p', $workGroupIdExpr));
        //User is member of the workGroup
        $firstLvlOr->add($qb->expr()->eq('u_mP', $workGroupIdExpr));
        //User is in a group that is member of the workGroup
        $firstLvlOr->add($qb->expr()->eq('u_g_mP', $workGroupIdExpr));

        $qb->where($firstLvlOr);

        //If we must not include deleted..
        if (!$include_deleted) {
            //Now exclude disabled users
            $qb->andWhere($qb->expr()->eq('u.enabled', $qb->expr()->literal(true)));
            //Exclude locked users
            $qb->andWhere($qb->expr()->eq('u.locked', $qb->expr()->literal(false)));
            //Exclude expired users
            $qb->andWhere($qb->expr()->eq('u.expired', $qb->expr()->literal(false)));
        }

        //Order by..
        if ($orderBy) {
            foreach ($orderBy as $key => $order) {
                $qb->orderBy('u.'.$key, $order);
            }
        } else {
            $qb->orderBy('u.id', 'asc');
        }

        $result =  $qb->getQuery()->getResult();

        //Return
        return $count
            ? count($result)
            : $result;
    }

    /**
     * @param Group $group
     * @return array
     */
    public function findByUserGroup(Group $group)
    {
        $qb = $this->createQueryBuilder("ug");
        $qb->join("ug.groups", "ug_g");
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("ug_g", $qb->expr()->literal($group->getId())),
            $qb->expr()->eq("ug.enabled", $qb->expr()->literal(true))
        ));
        return $qb->getQuery()->getResult();
    }

    /**
     * Find users that belongs to the group
     * @param WodGroup $group
     * @return array
     */
    public function findByGroup(WodGroup $group)
    {
        //An user is member of a group when:
        //- user is assigned to the wodGroup OR
        //- user is in a user group assigned to the wodGroup OR
        //- user is follower of the wodGroup


        //Query...
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.groups', 'u_g')
            ->leftJoin('u_g.memberWodGroups', 'u_g_mTG')
            ->leftJoin('u.memberWodGroups', 'u_mTG')
            ->leftJoin('u.followedWodGroups', 'u_fTG');

        $wodGroupIdExpr = $qb->expr()->literal($group->getId());


        $firstLvlOr = $qb->expr()->orX();
        //user is assigned to the wodGroup
        $firstLvlOr->add($qb->expr()->eq('u_mTG', $wodGroupIdExpr));
        //user is in a user group assigned to the wodGroup
        $firstLvlOr->add($qb->expr()->eq('u_g_mTG', $wodGroupIdExpr));
        //user is follower of the wodGroup
        $firstLvlOr->add($qb->expr()->eq('u_fTG', $wodGroupIdExpr));

        $qb->where($firstLvlOr);

        //Now exclude disabled users
        $qb->andWhere($qb->expr()->eq('u.enabled', $qb->expr()->literal(true)));
        //Exclude locked users
        $qb->andWhere($qb->expr()->eq('u.locked', $qb->expr()->literal(false)));
        //Exclude expired usersç
        $qb->andWhere($qb->expr()->eq('u.expired', $qb->expr()->literal(false)));


        //Order by userId
        $qb->orderBy('u.id', 'asc');

        //Return
        return $qb->getQuery()->getResult();
    }

    /**
     * Find users that are related to the wodGorup...
     * @param WodGroup $wodGroup
     * @return array
     */
    public function findByGroupRelated(WodGroup $wodGroup)
    {
        //User from group that are not related are:
        //- Users that are not from the group AND
        // (
        //      - Users that have an assigned a WodGroup's Wod OR
        //      - Users that are follower from a WodGroup's Wod OR
        //      - Users that are creators of a WodGroup's Wod
        // )

        $qb = $this->createQueryBuilder("u")
            ->leftJoin('u.followedWods', 'u_fT') //User's followedWods
            ->leftJoin('u_fT.wodGroup', 'u_fT_tG') //User followedWods WodGroups
            ->leftJoin('u.assignedWods', 'u_aT') //User AssignedWods
            ->leftJoin('u_aT.wodGroup', 'u_aT_tG') //User AssignedWods WodGroups
            ->leftJoin('u.createdWods', 'u_cT') //User CreatedWods
            ->leftJoin('u_cT.wodGroup', 'u_cT_tG'); //User CreatedWods WodGroups

        $wodGroupIdExpr = $qb->expr()->literal($wodGroup->getId());

        $firstLvlOr = $qb->expr()->orX();
        //Users that have an assigned a WodGroup's Wod
        $firstLvlOr->add($qb->expr()->eq('u_aT_tG', $wodGroupIdExpr));
        //Users that are follower from a WodGroup's Wod
        $firstLvlOr->add($qb->expr()->eq('u_fT_tG', $wodGroupIdExpr));
        //Users that are creators of a WodGroup's Wod
        $firstLvlOr->add($qb->expr()->eq('u_cT_tG', $wodGroupIdExpr));

        $qb->where($firstLvlOr);

        //Now exclude disabled users
        $qb->andWhere($qb->expr()->eq('u.enabled', $qb->expr()->literal(true)));
        //Exclude locked users
        $qb->andWhere($qb->expr()->eq('u.locked', $qb->expr()->literal(false)));
        //Exclude expired usersç
        $qb->andWhere($qb->expr()->eq('u.expired', $qb->expr()->literal(false)));

        //Order by userId
        $qb->orderBy('u.id', 'asc');

        //Needed to limit 1000 cuz database explodes...
        $qb->setMaxResults(1000);

        //Return
        return $qb->getQuery()->getResult();
    }
    /**
     * Find users that are not in group but have wods
     * @param WodGroup $wodGroup
     * @return array
     */
    public function findByGroupRelatedNotMembers(WodGroup $wodGroup)
    {
        //User from group that are not related are:
        //- Users that are not from the group AND
        // (
        //      - Users that have an assigned a WodGroup's Wod OR
        //      - Users that are follower from a WodGroup's Wod OR
        //      - Users that are creators of a WodGroup's Wod
        // )

        $qb = $this->_em->createQueryBuilder();
        $wodGroupIdExpr = $qb->expr()->literal($wodGroup->getId());

        //Retruive ids from users that are member directly form the group
        $usersThatAreMemberOf = CollectionHelper::reduceIdMatrix(
            $qb->select('u.id')
                ->from('Unir\CloudBoxBundle\Entity\User', 'u')
                ->join('u.memberWodGroups', 'u_mTG')
                ->where($qb->expr()->eq('u_mTG', $wodGroupIdExpr))
                ->getQuery()->getResult()
        );

        //Retrieve ids for users that are member of a group who is on the group
        $qb = $this->_em->createQueryBuilder();
        $usersThatAreMemberOfGroup = CollectionHelper::reduceIdMatrix(
            $qb->select('u.id')
                ->from('Unir\CloudBoxBundle\Entity\User', 'u')
                ->join('u.groups', 'u_g')
                ->join('u_g.memberWodGroups', 'u_g_mTG')
                ->where($qb->expr()->eq('u_g_mTG', $wodGroupIdExpr))
                ->getQuery()->getResult()
        );

        //Retrive ids from users that are followers of the group
        $qb = $this->_em->createQueryBuilder();
        $usersThatAreFollowersOf = CollectionHelper::reduceIdMatrix(
            $qb->select('u.id')
                ->from('Unir\CloudBoxBundle\Entity\User', 'u')
                ->join('u.followedWodGroups', 'u_fTG')
                ->where($qb->expr()->eq('u_fTG', $wodGroupIdExpr))
                ->getQuery()->getResult()
        );

        //Retrieve users that ahve assigned wods in that wodGroup
        $qb = $this->_em->createQueryBuilder();
        $usersThatHaveAssignedWodGroupsWod = CollectionHelper::reduceIdMatrix(
            $qb->select('u.id')
                ->from('Unir\CloudBoxBundle\Entity\User', 'u')
                ->join('u.assignedWods', 'u_aT')
                ->join('u_aT.wodGroup', 'u_aT_tG')
                ->where($qb->expr()->eq('u_aT_tG', $wodGroupIdExpr))
                ->getQuery()->getResult()
        );

        //Retrieve users that are follower from some wodgroup wod
        $qb = $this->_em->createQueryBuilder();
        $usersThatAreFollowerWodGroupsWod = CollectionHelper::reduceIdMatrix(
            $qb->select('u.id')
                ->from('Unir\CloudBoxBundle\Entity\User', 'u')
                ->join('u.followedWods', 'u_fT')
                ->join('u_fT.wodGroup', 'u_fT_tG')
                ->where($qb->expr()->eq('u_fT_tG', $wodGroupIdExpr))
                ->getQuery()->getResult()
        );

        //Retrieve users that are creators of some wodgroup wod
        $qb = $this->_em->createQueryBuilder();
        $usersThatHaveCreatedWodGroupsWod = CollectionHelper::reduceIdMatrix(
            $qb->select('u.id')
                ->from('Unir\CloudBoxBundle\Entity\User', 'u')
                ->join('u.createdWods', 'u_cT')
                ->join('u_cT.wodGroup', 'u_cT_tG')
                ->where($qb->expr()->eq('u_cT_tG', $wodGroupIdExpr))
                ->getQuery()->getResult()
        );

        $qb = $this->createQueryBuilder('u');

        $firstLvlAnd = $qb->expr()->andX();

        $secondLvlFirstOr = $qb->expr()->orX();

        if (count($usersThatHaveAssignedWodGroupsWod)) {
            $secondLvlFirstOr->add($qb->expr()->in('u', $usersThatHaveAssignedWodGroupsWod));
        }
        if (count($usersThatAreFollowerWodGroupsWod)) {
            $secondLvlFirstOr->add($qb->expr()->in('u', $usersThatAreFollowerWodGroupsWod));
        }

        if (count($usersThatHaveCreatedWodGroupsWod)) {
            $secondLvlFirstOr->add($qb->expr()->in('u', $usersThatHaveCreatedWodGroupsWod));
        }



        if (count($usersThatAreMemberOf)) {
            $firstLvlAnd->add($qb->expr()->notIn('u', $usersThatAreMemberOf));
        }
        if (count($usersThatAreMemberOfGroup)) {
            $firstLvlAnd->add($qb->expr()->notIn('u', $usersThatAreMemberOfGroup));
        }
        if (count($usersThatAreFollowersOf)) {
            $firstLvlAnd->add($qb->expr()->notIn('u', $usersThatAreFollowersOf));
        }

        $qb->where($firstLvlAnd->add($secondLvlFirstOr));

        /*
        $qb = $this->createQueryBuilder("u")
            ->leftJoin('u.followedWods', 'u_fT') //User's followedWods
            ->leftJoin('u_fT.wodGroup', 'u_fT_tG') //User followedWods WodGroups
            ->leftJoin('u.assignedWods', 'u_aT') //User AssignedWods
            ->leftJoin('u_aT.wodGroup', 'u_aT_tG') //User AssignedWods WodGroups
            ->leftJoin('u.createdWods', 'u_cT') //User CreatedWods
            ->leftJoin('u_cT.wodGroup', 'u_cT_tG'); //User CreatedWods WodGroups


        $firstLvlOr = $qb->expr()->orX();
        //Users that have an assigned a WodGroup's Wod
        $firstLvlOr->add($qb->expr()->eq('u_aT_tG', $wodGroupIdExpr));
        //Users that are follower from a WodGroup's Wod
        $firstLvlOr->add($qb->expr()->eq('u_fT_tG', $wodGroupIdExpr));
        //Users that are creators of a WodGroup's Wod
        $firstLvlOr->add($qb->expr()->eq('u_cT_tG', $wodGroupIdExpr));

        $qb->where($firstLvlOr); */

        //Now exclude disabled users
        $qb->andWhere($qb->expr()->eq('u.enabled', $qb->expr()->literal(true)));
        //Exclude locked users
        $qb->andWhere($qb->expr()->eq('u.locked', $qb->expr()->literal(false)));
        //Exclude expired usersç
        $qb->andWhere($qb->expr()->eq('u.expired', $qb->expr()->literal(false)));

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Enterprise $enterprise
     * @return array
     */
    public function findByAdminOrOwner(Enterprise $enterprise = null)
    {
        $qb=$this->createQueryBuilder("u");
        $qb->join("u.roles", "r");
        if ($enterprise) {
            $qb->where(
                $qb->expr()->andX(
                    $qb->expr()->orX(
                        $qb->expr()->andX(
                            $qb->expr()->eq("r.role", $qb->expr()->literal(Role::ROLE_ADMIN)),
                            $qb->expr()->eq("u.enterprise", $qb->expr()->literal($enterprise->getId()))
                        ),
                        $qb->expr()->eq("r.role", $qb->expr()->literal(Role::ROLE_OWNER))
                    ),
                    $qb->expr()->eq('u.enabled', $qb->expr()->literal(true))
                )
            );
        } else {
            $qb->where($qb->expr()->eq("r.role", $qb->expr()->literal(Role::ROLE_OWNER)));
        }
        return $qb->getQuery()->getResult();
    }

    /**
     * @param Enterprise $enterprise
     * @param null $orderBy
     * @return QueryBuilder
     */
    private function getByEnterprise(Enterprise $enterprise, $orderBy = null)
    {


        $qb = $this->getEntityManager()
            ->getRepository("UnirCloudBoxBundle:User")
            ->createQueryBuilder("u");

        $qb->where($qb->expr()->andX(
            $qb->expr()->eq('u.enterprise', $enterprise->getId()),
            $qb->expr()->eq('u.enabled', $qb->expr()->literal(true))
        ));

        //Disabled ones? we don't want em
        $qb->andWhere($qb->expr()->eq('u.enabled', $qb->expr()->literal(true)));

        if ($orderBy !== null) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy("u.".$sort, $order);
            }
        }



        return $qb;
    }
}
