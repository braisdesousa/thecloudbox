<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/11/14
 * Time: 1:39 PM
 */

namespace Unir\CloudBoxBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;

/**
 * Class EnterpriseRepository
 * @package Unir\CloudBoxBundle\Repository
 */
class EnterpriseRepository extends EntityRepository
{

    /**
     * @param bool $include_deleted
     * @param null $count
     * @return array|Enterprise
     */
    public function findAll($include_deleted = false, $count = null)
    {
        $qb = $this->createQueryBuilder("e");


        if ($count) {
            $qb->select($qb->expr()->count("e"));
        }
        if (!$include_deleted) {
            $qb->where($qb->expr()->isNull("e.deleted"));
        }

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }
}
