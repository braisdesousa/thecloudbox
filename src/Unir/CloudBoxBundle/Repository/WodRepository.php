<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/11/14
 * Time: 1:39 PM
 */

namespace Unir\CloudBoxBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use PhpCollection\CollectionInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;

/**
 * Class WodRepository
 * @package Unir\CloudBoxBundle\Repository
 */
class WodRepository extends EntityRepository
{
    /*
     * -----------------------------------------------------------------------------------------------------------------
     * Public Methods
     * -----------------------------------------------------------------------------------------------------------------
     */

    // RETRIEVE ALL
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Finds all wods
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findAll(
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        $qb = $this->createQueryBuilder("t");
        if ($count) {
            $qb->select("count(t)");
        }

        if (!$include_deleted) {
            $qb->where($qb->expr()->isNull("t.deleted"));
        }

        $qb = $this->setStatusDifficultyAndOrder($qb, "t", $status, $difficulty, $orderBy);

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }

    public function findByTitleText($text,WorkGroup $workGroup)
    {
        $text=str_replace(" ","%",trim($text));

        $qb= $this->createQueryBuilder("t");
        $qb->join("t.wodgroup","tg");
        $qb->join("tg.workGroup","tgp");
        $qb->where($qb->expr()->eq("tgp.id",$qb->expr()->literal($workGroup->getId())));
        $qb->andWhere($qb->expr()->like("t.title",$qb->expr()->literal($text)));
        $qb->orderBy("t.updated","DESC");

        return $qb->getQuery()->getResult();
    }
    public function findByDescriptionText($text,WorkGroup $workGroup)
    {
        $text=str_replace(" ","%",trim($text));

        $qb= $this->createQueryBuilder("t");
        $qb->join("t.wodgroup","tg");
        $qb->join("tg.workGroup","tgp");
        $qb->where($qb->expr()->eq("tgp.id",$qb->expr()->literal($workGroup->getId())));
        $qb->andWhere($qb->expr()->like("t.description",$qb->expr()->literal($text)));
        $qb->orderBy("t.updated","DESC");

        return $qb->getQuery()->getResult();
    }
    // BY USER GROUPS
    //------------------------------------------------------------------------------------------------------------------

    /**
     * @param Group $group
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findByUserGroup(
        Group $group,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        $qb = $this->createQueryBuilder("fug");

        if ($count) {
            $qb->select($qb->expr()->count("fug"));
        }

        $qb->join("fug.wodGroup", "fug_tg");
        $qb->join("fug_tg.groupMembers", "fug_tg_gm");

        $qb->where($qb->expr()->eq("fug_tg_gm", $qb->expr()->literal($group->getId())));

        if (!$include_deleted) {
            $qb->andWhere($qb->expr()->isNull("fug.deleted"));
        }

        $qb = $this->setStatusDifficultyAndOrder($qb, "fug", $status, $difficulty, $orderBy);

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }

    /**
     * @param Collection $groups
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     * @throws \Exception
     */
    public function findByUserGroups(
        Collection $groups,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {

        $qb = $this->createQueryBuilder("fug");

        if ($count) {
            $qb->select($qb->expr()->count("fug"));
        }

        $qb->join("fug.wodGroup", "fug_tg");
        $qb->join("fug_tg.groupMembers", "fug_tg_gm");

        $orX = $qb->expr()->orX();

        foreach ($groups as $group) {
            if (!($group instanceof Group)) {
                throw new \Exception("FindByUsersGroups recibed and element who is not a Group in the collection");
            }

            $orX->add($qb->expr()->eq("fug_tg_gm", $qb->expr()->literal($group->getId())));
        }

        $qb->where($orX);

        if (!$include_deleted) {
            $qb->andWhere($qb->expr()->isNull("fug.deleted"));
        }

        $qb = $this->setStatusDifficultyAndOrder($qb, "fug", $status, $difficulty, $orderBy);

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }


    // BY TASK GROUPS
    //------------------------------------------------------------------------------------------------------------------

    /**
     * @param WodGroup $wodGroup
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findByWodGroup(
        WodGroup $wodGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        $qb = $this->findByWodGroupsQB(new ArrayCollection([$wodGroup]), $status, $difficulty, $include_deleted);

        if ($count) {
            $qb->select($qb->expr()->count("tbtgs"));
        }
        $qb =$this->setStatusDifficultyAndOrder($qb, "tbtgs", null, null, $orderBy);

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }

    /**
     * @param Collection $groups
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findByWodGroups(
        Collection $groups,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        $qb = $this->findByWodGroupsQB($groups, $status, $difficulty, $include_deleted);

        if ($count) {
            $qb->select($qb->expr()->count("tbtgs"));
        }
        $qb = $this->setStatusDifficultyAndOrder($qb, "tbtgs", null, null, $orderBy);

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }

    // BY ENTERPRISE
    //------------------------------------------------------------------------------------------------------------------

    /**
     * @param Enterprise $enterprise
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findByEnterprise(
        Enterprise $enterprise,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {

        $qb = $this->findByEnterpriseQB($enterprise, $status, $difficulty, $include_deleted);

        if ($count) {
            $qb->select($qb->expr()->count("tbe"));
        }

        $qb = $this->setStatusDifficultyAndOrder($qb, "tbe", null, null, $orderBy);

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }

    // BY PROJECT
    //------------------------------------------------------------------------------------------------------------------

    /**
     * @param WorkGroup $workGroup
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findByWorkGroup(
        WorkGroup $workGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        $qb = $this->findByWorkGroupQB($workGroup, $status, $difficulty, $include_deleted);

        if ($count) {
            $qb->select($qb->expr()->count("tbp"));
        }
        $qb = $this->setStatusDifficultyAndOrder($qb, "tbp", null, null, $orderBy);

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param Enterprise $enterprise
     * @param array $status
     * @param array $difficulty
     * @param bool $include_deleted
     * @return QueryBuilder
     */
    private function findByEnterpriseQB(
        Enterprise $enterprise,
        array $status = null,
        array $difficulty = null,
        $include_deleted = false
    ) {
        $qb = $this->createQueryBuilder("tbe");
        $qb->join("tbe.wodGroup", "tbe_tg");
        $qb->join("tbe_tg.workGroup", "tbe_tg_p");
        $qb->where($qb->expr()->eq('tbe_tg_p.enterprise', $enterprise->getId()));
        if (!$include_deleted) {
            $qb->andWhere($qb->expr()->isNull("tbe.deleted"));
        }

        return $this->setStatusDifficultyAndOrder($qb, "tbe", $status, $difficulty);
    }

    /**
     * @param WorkGroup $workGroup
     * @param array $status
     * @param array $difficulty
     * @param bool $include_deleted
     * @return QueryBuilder
     */
    private function findByWorkGroupQB(
        WorkGroup $workGroup,
        array $status = null,
        array $difficulty = null,
        $include_deleted = false
    ) {
        $qb = $this->createQueryBuilder("tbp");
        $qb->join("tbp.wodGroup", "tbp_tg");
        $qb->join("tbp_tg.workGroup", "tbp_tg_p");

        $qb->where($qb->expr()->eq("tbp_tg_p", $workGroup->getId()));
        if (!$include_deleted) {
            $qb->andWhere($qb->expr()->isNull("tbp.deleted"));
        }
        return $this->setStatusDifficultyAndOrder($qb, "tbp", $status, $difficulty);
    }

    /**
     * @param QueryBuilder $qb
     * @param $alias
     * @param null $status
     * @param null $difficulty
     * @param null $orderBy
     * @return QueryBuilder
     */
    private function setStatusDifficultyAndOrder(
        QueryBuilder $qb,
        $alias,
        $status = null,
        $difficulty = null,
        $orderBy = null
    ) {
        if ($status !== null) {
            $orStatus = $qb->expr()->orX();
            foreach ($status as $newStatus) {
                $orStatus->add($qb->expr()->like("$alias.status", $qb->expr()->literal($newStatus)));
            }
            $qb->andWhere($orStatus);
        }

        if ($difficulty != null) {
            $orDifficulty = $qb->expr()->orX();
            foreach ($difficulty as $newDifficulty) {
                $orDifficulty->add($qb->expr()->like("$alias.difficulty", $qb->expr()->literal($newDifficulty)));
            }
            $qb->andWhere($orDifficulty);
        }

        if ($orderBy !== null) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy("$alias.".$sort, $order);
            }
        }
        return $qb;
    }

    /**
     * Returns a queryBuilder that will search wods by a wodgroups
     * @param Collection $collection
     * @param array $status
     * @param array $difficulty
     * @param bool $include_deleted
     * @return QueryBuilder
     * @throws \Exception
     */
    private function findByWodGroupsQB(
        Collection $collection,
        array $status = null,
        array $difficulty = null,
        $include_deleted = false
    ) {
        $qb = $this->createQueryBuilder("tbtgs");

        $orX = $qb->expr()->orX();
        foreach ($collection as $wodGroup) {
            //If is not a wodgRoup we throw an exception
            if (!($wodGroup instanceof WodGroup)) {
                throw new \Exception("Collection Passed to Wodgroup has a non WodGroup Object");
            }
            $orX->add($qb->expr()->eq("tbtgs.wodGroup", $qb->expr()->literal($wodGroup->getId())));
        }

        $qb->where($orX);

        if (!$include_deleted) {
            $qb->andWhere($qb->expr()->isNull("tbtgs.deleted"));
        }

        $qb = $this->setStatusDifficultyAndOrder($qb, "tbtgs", $status, $difficulty);

        return $qb;
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE (NEW QUERIES BY Vicente Aleixos)
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * Returns a queryBuilder that return wods that are from a wodGroup
     * @param array $wodGroups
     * @param string $prefix
     * @return QueryBuilder
     */
    private function queryWodsFromWodGroups(array $wodGroups, $prefix = '')
    {
        $qb = $this->createQueryBuilder($prefix.'t');
        $orX = $qb->expr()->orX();

        foreach ($wodGroups as $item) {
            $orX->add($qb->expr()->eq($prefix.'t.wodGroup', $qb->expr()->literal($item->getId())));
        }

        $qb->where($orX);

        return $qb;
    }
}
