<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/11/14
 * Time: 1:39 PM
 */

namespace Unir\CloudBoxBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;

/**
 * Class WodActivityRepository
 * @package Unir\CloudBoxBundle\Repository
 */
class WodTimingRepository extends EntityRepository
{

    public function findOneByOpenedWodsAndUser(User $user){
        $qb=$this->createQueryBuilder("tm");
        $qb->where($qb->expr()->andX(
                $qb->expr()->isNull("tm.date_end"),
                $qb->expr()->eq("tm.user",$qb->expr()->literal($user->getId()))
            ));

        return $qb->getQuery()->getOneOrNullResult();
    }
    public function findOpennedWodByUserAndWod(User $user, Wod $wod){
        $qb=$this->createQueryBuilder("tm");
        $qb->where($qb->expr()->andX(
                $qb->expr()->isNull("tm.date_end"),
                $qb->expr()->eq("tm.user",$qb->expr()->literal($user->getId())),
                $qb->expr()->eq("tm.wod",$qb->expr()->literal($wod->getId()))
            ));

        return $qb->getQuery()->getOneOrNullResult();
    }
}
