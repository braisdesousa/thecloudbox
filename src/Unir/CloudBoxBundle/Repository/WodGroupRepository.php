<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/11/14
 * Time: 1:39 PM
 */

namespace Unir\CloudBoxBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\Wod;

/**
 * Class WodGroupRepository
 * @package Unir\CloudBoxBundle\Repository
 */
class WodGroupRepository extends EntityRepository
{
    /**
     * @param bool $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findAll($count = false, $include_deleted = false)
    {
        $qb = $this->createQueryBuilder("tg");

        if ($count) {
            $qb->select("count(tg)");
        }

        if (!$include_deleted) {
            $qb->where($qb->expr()->isNull("tg.deleted"));
        }

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }

    public function findByTitleText($text,WorkGroup $workGroup)
    {
        $text=str_replace(" ","%",trim($text));

        $qb= $this->createQueryBuilder("tg");
        $qb->join("tg.workGroup","tgp");
        $qb->where($qb->expr()->eq("tgp.id",$qb->expr()->literal($workGroup->getId())));
        $qb->andWhere($qb->expr()->like("t.title",$qb->expr()->literal($text)));
        $qb->orderBy("t.updated","DESC");

        return $qb->getQuery()->getResult();
    }
    public function findByDescriptionText($text,WorkGroup $workGroup)
    {
        $text=str_replace(" ","%",trim($text));

        $qb= $this->createQueryBuilder("tg");
        $qb->join("tg.workGroup","tgp");
        $qb->where($qb->expr()->eq("tgp.id",$qb->expr()->literal($workGroup->getId())));
        $qb->andWhere($qb->expr()->like("t.description",$qb->expr()->literal($text)));
        $qb->orderBy("t.updated","DESC");

        return $qb->getQuery()->getResult();
    }
    /**
     * @deprecated
     * @param bool $include_delete
     * @return array|mixed
     */
    public function countAll($include_delete = false)
    {
        return $this->findAll(true, $include_delete);
    }

    /**
     * @param WorkGroup $workGroup
     * @param bool $count
     * @param array $orderBy
     * @return array|mixed
     */
    public function findByWorkGroup(WorkGroup $workGroup, $status=false, $difficulty=false, $orderBy=false, $count=false,$include_deleted=false,$limit=false)
    {
        $qb = $this->createQueryBuilder("t");
        $qb->where($qb->expr()->eq("t.workGroup", $qb->expr()->literal($workGroup->getId())));
        $qb->Andwhere($qb->expr()->isNull("t.deleted"));
        if ($count) {
            return $qb->select("count(t.id)")->getQuery()->getSingleScalarResult();
        } else {
            if (!empty($orderBy)) {
                foreach ($orderBy as $sort => $order) {
                    $qb->orderBy("t.".$sort, $order);
                }
            }
            if($limit){
                $qb->setMaxResults($limit);
            }

            return $qb->getQuery()->getResult();
        }
    }

    /**
     * @param User $user
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|mixed
     */
    public function findByUser(
        User $user,
        array $orderBy = null,
        $count = null,
        $include_deleted = false
    ) {
        //We can consider that a user is wodGroup member when:
        //- User is member of the workGroup AND
        //(     -User is member of the WodGroup (by self or Group) OR
        //      -User is follower of WodGroup and WodGroup visibility isn't VIEW BY ASSIGNED
        //)

        //We retrieve workGroups where the user is on
        $workGroupRepository = $this->_em->getRepository('UnirCloudBoxBundle:WorkGroup');
        $workGroups = $workGroupRepository->findByUserMember($user);

        $workGroupIds = [];
        foreach ($workGroups as $p) {
            $workGroupIds[] = $p->getId();
        }

        //Start with wodGroups where user is member...
        $wodGroupsUserIsMember = $user->getMemberWodGroups()->toArray();

        //Last of all, wodGroups where user is member from a group
        $qb = $this->createQueryBuilder('tg')
            ->join('tg.groupMembers', 'tg_gM')
            ->join('tg_gM.users', 'tg_gM_u');

        $qb->where($qb->expr()->eq('tg_gM_u', $qb->expr()->literal($user->getId())));
        $wodsGroupsWhereUserIsMemberGroup = $qb->getQuery()->getResult();


        //Now start iterating
        $resultWithIds = [];

        foreach (
            array_merge($wodGroupsUserIsMember, $wodsGroupsWhereUserIsMemberGroup) as $tg
        ) {

            //Id exists, continue
            if (array_key_exists($tg->getId(), $resultWithIds)) {
                continue;
            }

            //If user is not in workGroup, continue
            if(!in_array($tg->getWorkGroup()->getId(), $workGroupIds)) {
                continue;
            }

            if (!$include_deleted && $tg->getDeleted() != null) {
                continue;
            }

            $resultWithIds[$tg->getId()] = $tg;
        }

        $result = array_values($resultWithIds);

        //Array ordering
        if ($orderBy) {

            foreach ($orderBy as $orderByKey => $orderByValue) {

                $callback = function (WodGroup $item1, WodGroup $item2) use ($orderByKey, $orderByValue) {

                    switch($orderByKey)
                    {
                        case 'id':
                            return $orderByValue == 'asc'
                                ? $item1->getId() - $item2->getId()
                                : $item2->getId() - $item1->getId();

                        case 'name':
                            return $orderByValue == 'asc'
                                ? strcmp($item1->getName(), $item2->getName())
                                : strcmp($item2->getName(), $item1->getName());
                    }
                };

                usort($result, $callback);
            }
        }

        return $count
            ? count($result)
            : $result;
    }

    /**
     * @param Enterprise $enterprise
     * @param bool $count
     * @param array $orderBy
     * @return array|mixed
     */
    public function findByEnterprise(Enterprise $enterprise, $count = false, array $orderBy = null)
    {
        $qb = $this->getByEnterprise($enterprise, $orderBy);

        if ($count) {
            $qb->select("count(t)");
        }

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();
    }


    /**
     * Finds all TaksGroups from a given workGroup which have ViewByAll Visibility;
     */
    public function findByUserAndWorkGroup(User $user,WorkGroup $workGroup,$status=null,$difficulty=null,$orderBy=null,$count=false,$include_deleted=false,$limit=false)
    {
        $qb=$this->createQueryBuilder("tgvba");

        if ($count){
            $qb->select($qb->expr()->count("tgvba.id"));
        }

        $qb->distinct();
        $qb->join("tgvba.workGroup","tgvba_p");

        $orX=$qb->expr()->orX();


        $followedTg=[];
        if (!empty($memberTg)){
            $orX->add($qb->expr()->in("tgvba.id",$memberTg));
        }
        if (!empty($followedTg)){
            $orX->add($qb->expr()->in("tgvba.id",$followedTg));
        }


        $qb->where($qb->expr()->andX(
            $qb->expr()->eq("tgvba_p.id",$qb->expr()->literal($workGroup->getId())),
            $orX)
        );
        if (!$include_deleted){
            $qb->andWhere($qb->expr()->isNull("tgvba.deleted"));
        }

        $this->setStatusDifficultyAndOrder($qb,"tgvba",$orderBy);
        if ($limit) {
            $qb->setMaxResults($limit);
        }
     return $count?$qb->getQuery()->getSingleScalarResult():$qb->getQuery()->getResult();

    }
    /**
     * @param Enterprise $enterprise
     * @return array
     */
    public function findGroupsWithOpenWodsByEnterprise(Enterprise $enterprise)
    {
        //TODO this method have a bug, wodGroups deleted that have non deleted wods opened will be returned
        $qb = $this->getEntityManager()
            ->getRepository("UnirCloudBoxBundle:WodGroup")
            ->createQueryBuilder("tg");

        $qb->distinct(true);
        $qb->join("tg.wods", 't');
        $qb->join("tg.workGroup", 'p');
        $qb->where($qb->expr()->orX(
            $qb->expr()->like('t.status', $qb->expr()->literal("open")),
            $qb->expr()->like('t.status', $qb->expr()->literal("in progress"))
        ));
        $qb->andWhere($qb->expr()->eq('p.enterprise', $enterprise->getId()));
        $qb->andWhere($qb->expr()->isNull("t.deleted"));

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Enterprise $enterprise
     * @param array $orderBy
     * @return QueryBuilder
     */
    private function getByEnterprise(Enterprise $enterprise, array $orderBy = null)
    {
        $qb=$this->getEntityManager()
            ->getRepository("UnirCloudBoxBundle:WodGroup")
            ->createQueryBuilder("t");

        $qb->join("t.workGroup", "p")
            ->where($qb->expr()->eq('p.enterprise', $enterprise->getId()));

        $qb->andWhere($qb->expr()->isNull("t.deleted"));

        if ($orderBy !== null) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy("t.".$sort, $order);
            }
        }

        return $qb;

    }

    /**
     * @param QueryBuilder $qb
     * @param $alias
     * @param null $orderBy
     * @return QueryBuilder
     */
    private function setStatusDifficultyAndOrder(
        QueryBuilder $qb,
        $alias,
        $orderBy = null
    ) {
        if ($orderBy) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy("$alias.".$sort, $order);
            }
        } else {
            $qb->orderBy($alias.'.id', 'desc');
        }
        
        return $qb;
    }
}
