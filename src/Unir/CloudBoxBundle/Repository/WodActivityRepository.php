<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/11/14
 * Time: 1:39 PM
 */

namespace Unir\CloudBoxBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;

/**
 * Class WodActivityRepository
 * @package Unir\CloudBoxBundle\Repository
 */
class WodActivityRepository extends EntityRepository
{

    /**
     * Find activity by workGroup
     * @param WorkGroup $workGroup
     * @param array $orderBy
     * @param bool $count
     * @return array|mixed
     */
    public function findByWorkGroup(WorkGroup $workGroup, array $orderBy = null, $count = false)
    {

        $qb = $this->createQueryBuilder('ta')
            ->join('ta.wod', 'ta_t')
            ->join('ta_t.wodGroup', 'ta_t_tG')
            ->join('ta_t_tG.workGroup', 'ta_t_tG_p');


        //Retreive wodActivity from the workGroup
        $qb->where($qb->expr()->eq('ta_t_tG_p', $qb->expr()->literal($workGroup->getId())));

        //Exclude deleted one
        $qb->andWhere($qb->expr()->isNull('ta.deleted'));

        //Order
        if ($orderBy) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy('ta.'.$sort, $order);
            }
        }

        $results = $qb->getQuery()->getResult();

        return $count
            ? count($results)
            : $results;

    }


    /**
     * @param User $user
     * @param array $orderBy
     * @param bool $count
     * @return array|mixed
     */
    public function findByUser(User $user, array $orderBy = null, $count = false)
    {
        $qb=$this->createQueryBuilder("ta");
        if ($count) {
            $qb->select("count(t)");
        }
        $qb->join("ta.wod", "t");
        $qb->where($qb->expr()->orX(
            $qb->expr()->eq("ta.user", $user->getId()),
            $qb->expr()->isNull("t.created")
        ));


        if ($orderBy) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy('ta.'.$sort, $order);
            }
        }
        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();

    }

    /**
     * @param User $user
     * @param array $orderBy
     * @param null $count
     * @return array|mixed
     */
    public function findByUserRelated(User $user, array $orderBy = null, $count = null)
    {

        //Look for wods where user is assigned to get the activity
        $qb0 = $this->getEntityManager()->getRepository("UnirCloudBoxBundle:Wod")->createQueryBuilder("y");
        $qb0->select("y.id");
        $qb0->join("y.usersAssigned", "ua");
        $qb0->where($qb0->expr()->eq("ua", $qb0->expr()->literal($user->getId())));
        $qb0->andWhere($qb0->expr()->isNull("y.deleted"));

        //Look for wods where user is follow for get activity
        $qb1=$this->getEntityManager()->getRepository("UnirCloudBoxBundle:Wod")->createQueryBuilder("x");
        $qb1->select("x.id");
        $qb1->join("x.usersFollower", "uf");
        $qb1->where($qb1->expr()->eq("uf", $qb1->expr()->literal($user->getId())));
        $qb1->andWhere($qb1->expr()->isNull("x.deleted"));

        //Look for wods where user is creator for get activity
        $qb2=$this->getEntityManager()->getRepository("UnirCloudBoxBundle:Wod")->createQueryBuilder("z");
        $qb2->select("z.id");
        $qb2->where($qb2->expr()->eq("z.creationUser", $qb2->expr()->literal($user->getId())));
        $qb2->andWhere($qb2->expr()->isNull("z.deleted"));


        $transform = function($collection) {
            $result = [];
            foreach($collection as $v) {
                $result[] = $v['id'];
            }

            return $result;
        };



        $qb0_ids = $transform($qb0->getQuery()->getArrayResult());
        $qb1_ids = $transform($qb1->getQuery()->getArrayResult());
        $qb2_ids = $transform($qb2->getQuery()->getArrayResult());

        $qb=$this->createQueryBuilder("ta")->distinct();
        $orX= $qb->expr()->orX();
        $orX->add($qb->expr()->eq("ta.user", $user->getId()));

        if (!empty($qb0_ids)){
            $orX->add($qb->expr()->in("ta.wod", $qb0_ids));
        }
        if (!empty($qb1_ids)){
            $orX->add($qb->expr()->in("ta.wod", $qb1_ids));
        }
        if (!empty($qb2_ids)){
            $orX->add($qb->expr()->in("ta.wod", $qb2_ids));
        }


        if ($count) {
            $qb->select("count(ta)");
        }
        $qb->where($orX);
        if ($orderBy) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy('ta.'.$sort, $order);
            }
        }

        return $count
            ? $qb->getQuery()->getSingleScalarResult()
            : $qb->getQuery()->getResult();

    }
}
