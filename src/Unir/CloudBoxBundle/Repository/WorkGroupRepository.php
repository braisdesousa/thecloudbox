<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/11/14
 * Time: 1:39 PM
 */

namespace Unir\CloudBoxBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Wod;

/**
 * Class WorkGroupRepository
 * @package Unir\CloudBoxBundle\Repository
 */
class WorkGroupRepository extends EntityRepository
{
    /**
     * @param bool $include_deleted
     * @return array
     */
    public function findAll($include_deleted = false)
    {
        $qb = $this->createQueryBuilder("p");
        if (!$include_deleted) {
            $qb->where($qb->expr()->isNull("p.deleted"));
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Find workGroups where user is member of
     * @param User $user
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array|int
     */
    public function findByUserMember(User $user, array $orderBy = null, $count = null, $include_deleted = false)
    {
        //A workGroup user member is an user that:
        //- User is directly member of the workGroup OR
        //- User is member of a group that is member of the workGroup

        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.userMembers', 'p_u') //workGroup user members
            ->leftJoin('p.groupMembers', 'p_gM') //WorkGroup group members
            ->leftJoin('p_gM.users', 'p_gM_u'); //Group users

        $userIdExpr = $qb->expr()->literal($user->getId());

        $firstLvlOr = $qb->expr()->orX();
        //User is directly member of the workGroup
        $firstLvlOr->add($qb->expr()->eq('p_u', $userIdExpr));
        //User is member of a group that is member of the workGroup
        $firstLvlOr->add($qb->expr()->eq('p_gM_u', $userIdExpr));


        $qb->where($firstLvlOr);

        //Exclude deleted workGroups...
        if (!$include_deleted) {
            $qb->andWhere($qb->expr()->isNull('p.deleted'));
        }

        $orderBy = $orderBy
            ? $orderBy
            : ['id' => 'desc'];

        foreach ($orderBy as $key => $value) {
            $qb->orderBy('p.'.$key, $value);
        }

        $result = $qb->getQuery()->getResult();

        return $count
            ? count($result)
            : $result;
    }

    /**
     * @param null $orderBy
     * @return array
     */
    public function findOrdered($orderBy = null)
    {
        $qb = $this->createQueryBuilder("p");
        $qb->where($qb->expr()->isNull("p.deleted"));
        if ($orderBy !== null) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy("p.".$sort, $order);
            }
        }
        return $qb->getQuery()->getResult();
    }

    /**
     * @param bool $include_deleted
     * @return mixed
     */
    public function countAll($include_deleted = false)
    {
        $qb = $this->createQueryBuilder("p");
        $qb->select($qb->expr()->count("p"));
        if (!$include_deleted) {
            $qb->where($qb->expr()->isNull("p.deleted"));
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Enterprise $enterprise
     * @param array $orderBy
     * @return array
     */
    public function findByEnterprise(Enterprise $enterprise, array $orderBy = null)
    {
        $qb = $this->getByEnterprise($enterprise, $orderBy);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Enterprise $enterprise
     * @return mixed
     */
    public function countAllByEnterprise(Enterprise $enterprise)
    {
        $qb = $this->getByEnterprise($enterprise);
        $qb->select("count(p)");
        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Enterprise $enterprise
     * @param array $orderBy
     * @return \Doctrine\ORM\QueryBuilder
     */
    private function getByEnterprise(Enterprise $enterprise, array $orderBy = null)
    {
        $qb = $this->getEntityManager()
            ->getRepository("UnirCloudBoxBundle:WorkGroup")
            ->createQueryBuilder("p");


        $qb->where($qb->expr()->eq('p.enterprise', $enterprise->getId()));
        $qb->andWhere($qb->expr()->isNull("p.deleted"));
        if ($orderBy !== null) {
            foreach ($orderBy as $sort => $order) {
                $qb->orderBy("p.".$sort, $order);
            }
        }
        return $qb;
    }
}
