<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Intended for access database with an abstraction layer
 *
 * this one is deprecated use Service\Repository\* instead
 *
 * @deprecated
 * Class RepositoryService
 * @package Unir\CloudBoxBundle\Service
 */
class RepositoryService
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    /**
     * @var CommonService
     */
    private $commonService;

    /**
     *
     * this one is deprecated use Service\Repository\* instead
     *
     *
     * @deprecated
     * @param EntityManager $em
     * @param Container $container
     * @param CommonService $commonService
     */
    public function __construct(EntityManager $em, Container $container, CommonService $commonService)
    {
        $this->em = $em;
        $this->container = $container;
        $this->commonService = $commonService;
    }


    /**
     * @deprecated
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @param null $workGroup
     * @return array|int
     */
    public function getWods(
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null,
        $workGroup = null
    ) {

        $repository = $this->em->getRepository('UnirCloudBoxBundle:Wod');
        $workGroup = $workGroup
            ? $workGroup
            : $this->commonService->getSelectedWorkGroup();

        if ($workGroup === null) {

            if ($count) {
                return  0;
            } else {
                return [];
            }
        } else {

            $workGroupGroups = $workGroup->getWodGroups();

            if ($workGroupGroups->isEmpty()) {
                if ($count) {
                    return 0;
                } else {
                    return [];
                }
            }

            //TODO that fuctions non exists, tests fails
            if ($count) {
                return (integer) $repository->findByWodGroups($workGroupGroups, $status, $difficulty, null, true, false);
            } else {
                return $repository->findByWodGroups($workGroupGroups, $status, $difficulty, $orderBy, null, false);
            }
        }
    }

    /**
     * @deprecated
     * @param array $orderBy
     * @param null $count
     * @param null $workGroup
     * @return array|int
     */
    public function getWodGroups(array $orderBy = null, $count = null, $workGroup = null)
    {

        $repository = $this->em->getRepository('UnirCloudBoxBundle:WodGroup');
        $workGroup = $workGroup
            ? $workGroup
            : $this->commonService->getSelectedWorkGroup();

        if ($workGroup === null) {

            if ($count) {
                return  0;
            } else {
                return [];
            }
        } else {
            if ($count) {
                return $repository->findByWorkGroup($workGroup, true);
            } else {
                return $repository->findByWorkGroup($workGroup, false, $orderBy);
            }
        }
    }

    /**
     * @deprecated
     * @param array $orderBy
     * @param null $count
     * @return mixed
     */
    public function getWorkGroups(array $orderBy = null, $count = null)
    {

        $repository = $this->em->getRepository('UnirCloudBoxBundle:WorkGroup');
        $enterprise = $this->commonService->getCompany();

        if ($enterprise === null) {

            if ($count) {
                return  $repository->countAll(true);
            } else {
                return $repository->findOrdered($orderBy);
            }
        } else {
            if ($count) {
                return $repository->countAllByEnterprise($enterprise);
            } else {
                return $repository->findByEnterprise($enterprise, $orderBy);
            }
        }
    }

    /**
     * @deprecated
     * @param array $orderBy
     * @param null $count
     * @return array
     */
    public function getUsers(array $orderBy = null, $count = null)
    {

        $repository = $this->em->getRepository('UnirCloudBoxBundle:User');
        $enterprise = $this->commonService->getCompany();

        if ($enterprise === null) {

            if ($count) {
                return  $repository->countAll();
            } else {
                return $repository->findBy(array(), $orderBy);
            }
        } else {
            if ($count) {
                return $repository->countAllByEnterprise($enterprise);
            } else {
                return $repository->findByEnterprise($enterprise, $orderBy);
            }
        }
    }
}
