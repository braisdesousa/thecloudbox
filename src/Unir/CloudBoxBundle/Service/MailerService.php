<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/17/14
 * Time: 5:58 PM
 */

namespace Unir\CloudBoxBundle\Service;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\SubWod;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodComments;
use Unir\CloudBoxBundle\Entity\User;

/**
 * Class MailerService
 * @package Unir\CloudBoxBundle\Service
 */
class MailerService
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Symfony\Bundle\TwigBundle\TwigEngine
     */
    private $template;

    /**
     * @param \Swift_Mailer $mailer
     * @param TwigEngine $template
     */
    public function __construct(\Swift_Mailer $mailer, TwigEngine $template)
    {
        $this->mailer = $mailer;
        $this->template = $template;
    }

    /**
     * @param User $user
     */
    public function mailNewUser(User $user)
    {
        $template = $this->template->render("UnirCloudBoxBundle:Mail:newUser.html.twig", array("user"=>$user));
        $this->mail("Nuevo Usuario", [$user], $template);
    }

    /**
     * @param WodComments $wodComment
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function mailWodComment(WodComments $wodComment)
    {
        $user = $wodComment->getUser();
        $wod = $wodComment->getWod();
        $usersFollower = $wod->getUsersFollower();
        $usersAssigned = $wod->getUsersAssigned();

        if (!$user) {
            throw new NotFoundHttpException('Unable to find creation user');
        }

        $receivers = array_unique(array_merge($usersAssigned->toArray(), $usersFollower->toArray()));
        $title = "Nuevo Comentario";
        $template = $this->template->render(
            "UnirCloudBoxBundle:Mail:newWodComment.html.twig",
            array("wodcomment" => $wodComment)
        );
        $this->mail($title, $receivers, $template);
    }
    public function subWodComment(SubWod $subWod)
    {
        $wod = $subWod->getWod();
        $usersFollower = $wod->getUsersFollower();
        $usersAssigned = $wod->getUsersAssigned();

        $receivers = array_unique(array_merge($usersAssigned->toArray(), $usersFollower->toArray()));
        $title = "Nueva Subwod";
        $template = $this->template->render(
            "UnirCloudBoxBundle:Mail:newSubWod.html.twig",
            array("subWod" => $subWod)
        );
        $this->mail($title, $receivers, $template);
    }
    public function editWodStatus(Wod $wod)
    {

        $usersFollower = $wod->getUsersFollower();
        $usersAssigned = $wod->getUsersAssigned();

        $receivers = array_unique(array_merge($usersAssigned->toArray(), $usersFollower->toArray()));
        $title = "Tarea Modificada";
        $template = $this->template->render(
            "UnirCloudBoxBundle:Mail:WodStatusChange.html.twig",
            array("wod" => $wod)
        );
        $this->mail($title, $receivers, $template);
    }
    public function editWodDifficulty(Wod $wod)
    {
        $usersFollower = $wod->getUsersFollower();
        $usersAssigned = $wod->getUsersAssigned();

        $receivers = array_unique(array_merge($usersAssigned->toArray(), $usersFollower->toArray()));
        $title = "Tarea Modificada";
        $template = $this->template->render(
            "UnirCloudBoxBundle:Mail:WodDifficultyChange.html.twig",
            array("wod" => $wod)
        );
        $this->mail($title, $receivers, $template);
    }



    /**
     * @param Wod $wod
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function mailWod(Wod $wod)
    {
        $usersFollower = $wod->getUsersFollower();
        $usersAssigned = $wod->getUsersAssigned();
        $user = $wod->getCreationUser();
        if (!$user) {
            throw new NotFoundHttpException("User not found");
        }
        $receivers = array_unique(array_merge($usersFollower->toArray(), $usersAssigned->toArray()));
        $template = $this->template->render(
            'UnirCloudBoxBundle:Mail:addedtowod.html.twig',
            array('wod' => $wod)
        );
        $title = "Nueva Tarea";
        $this->mail($title, $receivers, $template);


    }
    public function mailExistingUser(User $user, Enterprise $enterprise)
    {
        $receivers = [$user];
        $template = $this->template->render(
            'UnirCloudBoxBundle:Mail:newUserInCompany.html.twig',
            array('user' => $user , "enterprise"=>$enterprise)
        );
        $title = "Has sido añadido a una nueva empresa";
        $this->mail($title, $receivers, $template);

    }

    /**
     * @param $subject
     * @param array $recivers
     * @param $body
     */
    private function mail($subject, array $recivers, $body)
    {

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom("tickets@unir.es", " Unir CloudBoxs")
            ->setContentType("text/html")
            ->setBody($body);

        $receiversArr = [];
        foreach ($recivers as $reciver) {
            $receiversArr[$reciver->getEmail()] = $reciver->getUserName();
        }

        $message->setTo($receiversArr);
        $this->mailer->send($message);
    }
}
