<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 2/10/14
 * Time: 10:56
 */

namespace Unir\CloudBoxBundle\Service;


use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Unir\CloudBoxBundle\Service\Repository\WodTimingRepositoryService;

class PostSerializeWodService implements EventSubscriberInterface {

    private $canViewOrCanEditService;
    private $commonService;


    function __construct(CanViewOrCanEditService $canViewOrCanEditService, CommonService $commonService){
        $this->canViewOrCanEditService=$canViewOrCanEditService;
        $this->commonService=$commonService;
    }


    public static function getSubscribedEvents()
    {
        return [
            [
                'event' => Events::POST_SERIALIZE,
                'format' => 'json',
                'class' => 'Unir\CloudBoxBundle\Entity\Wod',
                'method' => 'onPostSerializeWod',
            ],
            [
                'event' => Events::POST_SERIALIZE,
                'format' => 'json',
                'class' => 'Unir\CloudBoxBundle\Entity\Enterprise',
                'method' => 'onPostSerializeEnterprise',
            ],
            [
                'event' => Events::POST_SERIALIZE,
                'format' => 'json',
                'class' => 'Unir\CloudBoxBundle\Entity\WorkGroup',
                'method' => 'onPostSerializeWorkGroup',
            ],
            [
                'event' => Events::POST_SERIALIZE,
                'format' => 'json',
                'class' => 'Unir\CloudBoxBundle\Entity\User',
                'method' => 'onPostSerializeUser',
            ],

        ];
    }
    public function onPostSerializeWod(ObjectEvent $event){


            $event->getVisitor()->addData('canView',$this->canViewOrCanEditService->canViewWod($event->getObject()));
            $event->getVisitor()->addData('canEdit',$this->canViewOrCanEditService->canEditWod($event->getObject()));

        if($event->getContext()->attributes->containsKey("groups") && in_array("dashboard_list",$event->getContext()->attributes->get("groups")->get())){
            $workGroup=$event->getObject()->getWodGroup()->getWorkGroup();
            $company=$workGroup->getEnterprise();
            $event->getVisitor()->addData('workGroup',["id"=>$workGroup->getId(),"name"=>$workGroup->getName()]);
            $event->getVisitor()->addData('company',["id"=>$company->getId(),"name"=>$company->getName()]);
        }
    }
    public function onPostSerializeEnterprise(ObjectEvent $event){
        if ($event->getContext()->attributes->containsKey("groups") &&
            in_array("base_data",$event->getContext()->attributes->get("groups")->get()) &&
           ($this->commonService->getCompany() == $event->getObject())
            ){
            $event->getVisitor()->addData("is_selected_enterprise",true);
        }
        if($this->commonService->getUser() && $event->getObject()->isAdminUser($this->commonService->getUser())){
                $event->getVisitor()->addData("is_admin",true);
        }

    }
    public function onPostSerializeWorkGroup(ObjectEvent $event){
        if ($event->getContext()->attributes->containsKey("groups") &&
            in_array("base_data",$event->getContext()->attributes->get("groups")->get()) &&
           ($this->commonService->getSelectedWorkGroup() == $event->getObject())
            ){
            $event->getVisitor()->addData("is_selected_workGroup",true);
        }

    }
    public function onPostSerializeUser(ObjectEvent $event){

           if ($this->commonService->getSelectedWorkGroup()->getEnterprise()->isAdminUser($event->getObject()))
            {
                $event->getVisitor()->addData("is_admin",true);
            }

    }


} 