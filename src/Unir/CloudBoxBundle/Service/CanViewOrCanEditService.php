<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 11/09/14
 * Time: 18:32
 */

namespace Unir\CloudBoxBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Service\AclWodGeneratorService;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WorkGroup;

/**
 * The intend of this service is unify in one spot all the domain logic about if an user
 * can view an item or can edit it.
 *
 *
 * Class CanViewOrCanEditService
 * @package Unir\CloudBoxBundle\Service
 */
class CanViewOrCanEditService
{

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var AclWodGeneratorService
     */
    protected $acl;

    /**
     * @var SecurityContextInterface
     */
    protected $securityCtx;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param EntityManagerInterface $em
     * @param AclWodGeneratorService $aclWodGeneratorService
     * @param SecurityContextInterface $securityContextInterface
     */
    public function __construct(
        EntityManagerInterface $em,
        AclWodGeneratorService $aclWodGeneratorService,
        SecurityContextInterface $securityContextInterface
        )
    {
        $this->em = $em;
        $this->acl = $aclWodGeneratorService;
        $this->securityCtx = $securityContextInterface;
    }


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PUBLIC METHODS
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * Checks if an user can view or not a WodGorup
     * @param WodGroup $wodGroup
     * @eturn bool
     */
    public function canViewWodGroup(WodGroup $wodGroup)
    {
        $user = $this->getLoggedUser();

        if ($this->isOwner()) {
            return true;
        }
        //If here we don't have user, return false
        if (!$user) {
            return false;
        }

        //If is deleted you can't view it
        if ($wodGroup->getDeleted()) {
            return false;
        }



        //If user isn't in workGroup, return false
        if (!$this->userIsInCompany($user, $wodGroup->getWorkGroup()->getEnterprise())) {
            return false;
        }
        if ($this->isAdmin($wodGroup->getWorkGroup()->getEnterprise())) {
            return true;
        }
        //In this case visibility is ViewByAssignedAndFollowers and only can view it if is
        // in wodGroup as member or follower
        return $this->userIsInWodGroupAsMember($user, $wodGroup);

    }
    public function canEditWod(Wod $wod){
        $user = $this->getLoggedUser();


        if ($this->isOwner()) {
            return true;
        }
        //If here we don't have user, return false
        if (!$user) {
            return false;
        }
        //If user isn't in workGroup, return false
        if (!$this->userIsInCompany($user, $wod->getWodGroup()->getWorkGroup()->getEnterprise())) {
            return false;
        }
        //If is deleted you can't view it
        if ($wod->getDeleted()) {
            return false;
        }

        if ($this->isAdmin($wod->getWodGroup()->getWorkGroup()->getEnterprise())) {
            return true;
        }
        return $this->securityCtx->isGranted('EDIT', $wod);
    }
    /**
     * Checks if an user can view or not a Wod
     * @param Wod $wod
     * @return bool
     */
    public function canViewWod(Wod $wod)
    {
        $user = $this->getLoggedUser();



        if ($this->isOwner()) {
            return true;
        }
        //If here we don't have user, return false
        if (!$user) {
            return false;
        }

        //If is deleted you can't view it
        if ($wod->getDeleted()) {
            return false;
        }


        //If user isn't in workGroup, return false
        if (!$this->userIsInCompany($user, $wod->getWodGroup()->getWorkGroup()->getEnterprise())) {
            return false;
        }
        if ($this->isAdmin($wod->getWodGroup()->getWorkGroup()->getEnterprise())) {
            return true;
        }
        //If user cannot see wodGroup, then cannot view wod
        if (!$this->canViewWodGroup($wod->getWodGroup())) {
            return false;
        }

        //Return what ACL says about wod...
        return $this->securityCtx->isGranted('VIEW', $wod);
    }



    /**
     * Checks if an user is in a WorkGroup or not
     * @param User $user
     * @param WorkGroup $workGroup
     * @return bool
     */
    public function userIsInCompany(User $user, Enterprise $enterprise)
    {
        $isInWorkGroup = $enterprise->getUsers()->contains($user);

        if ($enterprise->getUsers()->contains($user) || $enterprise->getAdminUsers()->contains($user)) {
            return true;
        }

//        //If isn't in workGroup yet, can be through any of the workGroup groups
//        foreach ($enterprise->getGroups() as $groupMember) {
//            if ($groupMember->getUsers()->contains($user)) {
//                return true;
//
//            }
//        }

        return false;
    }


    /**
     * Checks if an user is in a wodgroup or not
     * @param User $user
     * @param WodGroup $wodGroup
     * @return bool
     */
    public function userIsInWodGroupAsMember(User $user, WodGroup $wodGroup)
    {
        $isInWodGroup = $wodGroup->getUsers()->contains($user);

        if ($isInWodGroup) {
            return true;
        }

        //If isn't in wodGroup yet, can be through any of the wodGroup userGroups
        foreach ($wodGroup->getGroupMembers() as $groupMember) {
            if($groupMember->getUsers()->contains($user)) {
                $isInWodGroup = true;
                break;
            }
        }

        return $isInWodGroup;
    }


    /**
     * Filter wodGroups that user can view from a collection
     * @param array $wodGroups
     * @return array
     */
    public function filterWodGroupsThatUserCanView($wodGroups)
    {
        $result = [];
        foreach ($wodGroups as $wodGroup) {
            if ($this->canViewWodGroup($wodGroup)) {
                $result[] = $wodGroup;
            }
        }

        return $result;
    }

    /**
     * Filter wods that user can view from a collection
     * @param array|collection $wods
     * @return array
     */
    public function filterWodsThatUserCanView($wods)
    {
        $result = [];
        foreach ($wods as $wod) {
            if ($this->canViewWod($wod)) {
                $result[] = $wod;
            }
        }

        return $result;
    }

    /**
     * Filter wodActivities that user can view from a collection
     * @param $wodActivities
     * @return array
     */
    public function filterWodActivityThatUserCanView($wodActivities)
    {
        $result = [];
        foreach ($wodActivities as $wodActivity) {
            if ($this->canViewWod($wodActivity->getWod())) {
                $result[] = $wodActivity;
            }
        }

        return $result;
    }

    /**
     * Filter wodComments that user can view from a collection
     * @param $wodComments
     * @return array
     */
    public function filterWodCommentsThatUserCanView($wodComments) {
        $result = [];
        foreach ($wodComments as $wodComment) {
            if ($this->canViewWod($wodComment->getWod())) {
                $result[] = $wodComment;
            }
        }

        return $result;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * HIDDEN METHODS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @return User|null logged user or null if we don't have user
     */
    private function getLoggedUser()
    {
        //Retrieve token
        $token = $this->securityCtx->getToken();

        //If we have token, retrieve user stored on it, else, return null
        return $token ? $token->getUser() : null;
    }

    /**
     * Checks if logged user have ROLE_ADMIN
     * @return bool
     */
    private function isAdmin(Enterprise $enterprise) {
        return $enterprise->isAdminUser($this->getLoggedUser());
    }
    private function isOwner()
    {
        return $this->securityCtx->isGranted(Role::ROLE_OWNER);
    }
}
