<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service\Repository;


use Doctrine\ORM\EntityManager;
use Faker\Factory;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CommonService;

/**
 * Class WodRepositoryService
 * @package Unir\CloudBoxBundle\Service\Repository
 */
class WodRepositoryService extends BaseRepositoryService
{
    /**
     * @param EntityManager $em
     * @param SecurityContextInterface $security
     * @param CommonService $commonService
     */
    public function __construct(EntityManager $em, SecurityContextInterface $security, CommonService $commonService)
    {
        parent::__construct($em, $security, $commonService);
    }

    /**
     * @param User $user
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @return mixed
     */
    public function findByUser(
        User $user,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null
    ) {
        if ($user->hasRole(ROLE::ROLE_ADMIN) || $user->hasRole(ROLE::ROLE_OWNER)) {

            if (!$workGroup=$this->commonService->getSelectedWorkGroup()){
                return $count?0:[];
            }
            return $this->em
                ->getRepository("UnirCloudBoxBundle:Wod")
                ->findByWorkGroup(
                    $workGroup,
                    $status,
                    $difficulty,
                    $orderBy,
                    $count
                );
        } else {
              $wods=[];
              foreach ($this->em->getRepository("UnirCloudBoxBundle:Wod")->findByEnterprise( $user->getEnterprise(),
                    $status,$difficulty, $orderBy) as $wod){
                  if ($this->security->isGranted("VIEW",$wod)){
                      $wods[]=$wod;
                  }
              }
            return $count?count($wods):$wods;
        }
    }

    /**
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @return array
     */
    public function findAll(
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null
    ) {
        $repository = $this->em->getRepository('UnirCloudBoxBundle:Wod');

        if ($this->isOwner()) {
            return $repository->findAll($status, $difficulty, $orderBy, $count);
        } elseif ($this->isAdmin($this->commonService->getCompany())) {
            return $repository->findByEnterprise(
                $this->getUser()->getEnterprise(),
                $status,
                $difficulty,
                $orderBy,
                $count
            );
        } else {
            $wods=[];
            foreach ($repository->findByWorkGroup(
                         $this->commonService->getSelectedWorkGroup(),
                         $status,
                         $difficulty,
                         $orderBy,
                         $count
                     ) as $wod)
            {
                if ($this->security->isGranted('VIEW',$wod)){
                    $wods[]=$wod;
                }
            }
            return $wods;
        }
    }

    /**
     * @param Enterprise $enterprise
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @return mixed
     */
    public function findByEnterprise(
        Enterprise $enterprise,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null
    ) {
        $repository = $this->em->getRepository('UnirCloudBoxBundle:Wod');
        if ($this->isAdmin($enterprise)) {
            return $repository->findByEnterprise($enterprise, $status, $difficulty, $orderBy, $count);
        } else {
            return $repository->findByEnterpriseAndUser(
                $this->getUser(),
                $enterprise,
                $status,
                $difficulty,
                $orderBy,
                $count
            );
        }
    }

    /**
     * @param WodGroup $wodGroup
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @return mixed
     */
    public function findByGroup(
        WodGroup $wodGroup,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null
    ) {
        $repository = $this->em->getRepository('UnirCloudBoxBundle:Wod');

        $filteredWods = $repository->findByWodGroup($wodGroup, $status, $difficulty, $orderBy, false);

        if ($this->isAdmin($wodGroup->getWorkGroup()->getEnterprise())||$this->isOwner()) {
            return $count ? count($filteredWods) : $filteredWods;
        } else {
            $wods = [];
            foreach ($filteredWods as $wod) {
                if ((!$wod->getDeleted()) && $this->security->isGranted("VIEW",$wod)) {
                    $wods[] = $wod;
                }
            }

            return $count ? count($wods) : $wods;
        }
    }


    /**
     * @param WorkGroup $workGroup
     * @param array $status
     * @param array $difficulty
     * @param array $orderBy
     * @param null $count
     * @return mixed
     */
    public function findByWorkGroup(
        WorkGroup $workGroup = null,
        array $status = null,
        array $difficulty = null,
        array $orderBy = null,
        $count = null
    ) {
        $workGroup = $workGroup
            ? $workGroup
            : $this->commonService->getSelectedWorkGroup();
        if (!$workGroup){
            return [];
        }
        $repository = $this->em->getRepository("UnirCloudBoxBundle:Wod");
        if ($this->isAdmin($workGroup->getEnterprise())) {
            return $repository->findByWorkGroup($workGroup, $status, $difficulty, $orderBy, $count);
        } else {
            $wods=[];
            foreach ($repository->findByWorkGroup($workGroup,$status,$difficulty,$orderBy) as $wod){
                    if ($this->security->isGranted('VIEW',$wod)){
                        $wods[]=$wod;
                    }
                }
                return $count?count($wods):$wods;

        }
    }

    /**
     * @param WodGroup $wodGroup
     * @param User $user
     * @return Wod
     */
    public function createDefault(WodGroup $wodGroup, User $user)
    {
        $wod = new Wod();
        $factory = new Factory();
        $faker=$factory->create("es");
        $wod->setTitle("Default Wod");
        $wod->setWodGroup($wodGroup);
        $wod->setDescription($faker->sentence(35));
        $wod->setCreationUser($user);
        $wod->setStatus(Wod::STATUS_INPROGRESS);
        $wod->setDifficulty(Wod::DIFFICULTY_NORMAL);
        $wod->setEstimationTime(10);
        $this->em->persist($wod);
        return $wod;
    }
}
