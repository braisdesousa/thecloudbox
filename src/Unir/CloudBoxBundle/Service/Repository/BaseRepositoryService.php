<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service\Repository;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CommonService;

/**
 * Class BaseRepositoryService
 * @package Unir\CloudBoxBundle\Service\Repository
 */
class BaseRepositoryService
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    protected $security;

    /**
     * @var \Unir\CloudBoxBundle\Service\CommonService
     */
    protected $commonService;

    /**
     * @param EntityManager $em
     * @param SecurityContextInterface $security
     * @param CommonService $commonService
     */
    public function __construct(EntityManager $em, SecurityContextInterface $security, CommonService $commonService)
    {
        $this->em = $em;
        $this->security = $security;
        $this->commonService = $commonService;
    }

    /**
     * returns if the user logged is owner
     * @return bool
     */
    protected function isOwner()
    {
        return $this->security->isGranted(Role::ROLE_OWNER);
    }

    /**
     * returns if the logger is admin
     * @return bool
     */
    protected function isAdmin(Enterprise $enterprise)
    {
        return $enterprise->isAdminUser($this->getUser());
    }

    /**
     * retrieves user Logged
     * @return User
     */
    protected function getUser()
    {
        return $this->security->getToken()->getUser();
    }
}
