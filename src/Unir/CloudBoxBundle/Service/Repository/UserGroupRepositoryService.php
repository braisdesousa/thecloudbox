<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service\Repository;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CommonService;

/**
 * Class UserGroupRepositoryService
 * @package Unir\CloudBoxBundle\Service\Repository
 */
class UserGroupRepositoryService extends BaseRepositoryService
{

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param EntityManager $em
     * @param SecurityContextInterface $security
     * @param CommonService $commonService
     */
    public function __construct(
        EntityManager $em,
        SecurityContextInterface $security,
        CommonService $commonService
    ) {
        parent::__construct($em, $security, $commonService);
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PUBLIC METHODS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param WorkGroup $workGroup
     * @return mixed
     */
    public function findByWorkGroup(WorkGroup $workGroup=null)
    {
        if(!$workGroup)
        {
            return [];
        }

        $groups = $this->em->getRepository("UnirCloudBoxBundle:Group")->findByWorkGroup($workGroup);

        return $groups;
    }
    /**
     * @param WorkGroup $workGroup
     * @return mixed
     */
    public function findByEnterprise(Enterprise $company)
    {
        $users = $this->em->getRepository("UnirCloudBoxBundle:Group")->findByCompany($company);

        return $users;
    }

    /**
     * @param Group $group
     * @param array $workGroups
     * @return Group
     */
    public function setWorkGroups(Group $group, array $workGroups)
    {
        $repository = $this->em->getRepository("UnirCloudBoxBundle:WorkGroup");
        $lastWorkGroups = $group->getMemberWorkGroups()->toArray();
        $newWorkGroups = [];

        foreach ($workGroups as $workGroup) {
            $object_workGroup = $repository->find($workGroup);
            if ($object_workGroup) {
                $newWorkGroups[]=$object_workGroup;
            }

        }
        foreach ($lastWorkGroups as $old_workGroup) {
            if (!in_array($old_workGroup->getId(), $workGroups)) {
                $group->removeMemberWorkGroup($old_workGroup);
                $old_workGroup->removeGroupMember($group);
            }
        }
        foreach ($newWorkGroups as $new_workGroup) {

            $group->addMemberWorkGroup($new_workGroup);
            $new_workGroup->addGroupMember($group);
        }
        return $group;
    }

    /**
     * @param Group $group
     * @param array $users
     * @return Group
     */
    public function setUsers(Group $group, array $users)
    {
        $repository = $this->em->getRepository("UnirCloudBoxBundle:User");
        $lastUsers = $group->getUsers()->toArray();
        $newUsers = [];

        foreach ($users as $user) {
            $object_user = $repository->find($user);
            if ($object_user) {
                $newUsers[]=$object_user;
            }

        }
        foreach ($lastUsers as $old_user) {
            if (!in_array($old_user->getId(), $users)) {
                $group->removeUser($old_user);
                $old_user->removeGroup($group);
            }
        }
        foreach ($newUsers as $new_user) {

            $group->addUser($new_user);
            $new_user->addGroup($group);
        }
        return $group;
    }
}
