<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CommonService;

/**
 * Class UserRepositoryService
 * @package Unir\CloudBoxBundle\Service\Repository
 */
class UserRepositoryService extends BaseRepositoryService
{
    /**
     * @param EntityManager $em
     * @param SecurityContextInterface $security
     * @param CommonService $commonService
     */
    public function __construct(EntityManager $em, SecurityContextInterface $security, CommonService $commonService)
    {
        parent::__construct($em, $security, $commonService);
    }

    /**
     * @param WodGroup $wodGroup
     * @return array
     */
    public function findByGroup(WodGroup $wodGroup)
    {
        $users = $this->em->getRepository("UnirCloudBoxBundle:User")->findByGroup($wodGroup);

        $owner_admin_users = $this->em
            ->getRepository("UnirCloudBoxBundle:User")
            ->findByAdminOrOwner($wodGroup->getWorkGroup()->getEnterprise());


        foreach ($owner_admin_users as $owner_admin) {
            if (!in_array($owner_admin, $users)) {
                $users[] = $owner_admin;
            }
        }

        return $users;
    }

    /**
     * @param WorkGroup $workGroup
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array
     */
    public function findByWorkGroup(WorkGroup $workGroup=null, array $orderBy = null, $count = null, $include_deleted = false)
    {
        $workGroup=$workGroup?$workGroup:$this->commonService->getSelectedWorkGroup();
        if (!$workGroup) {
            return [];
        }
        return $workGroup->getEnterprise()->getAllUsers();
    }

    /**
     * @param Enterprise $enterprise
     * @param array $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return array
     */
    public function findByCompany(
        Enterprise $enterprise=null,
        $include_groups=false
    ) {

        $enterprise=$enterprise?$enterprise:$this->commonService->getCompany();
        $users=new ArrayCollection($enterprise->getUsers()->toArray());
        $groups=$enterprise->getGroups();
        foreach ($groups as $group)
        {
            foreach ($group->getUsers() as $user){
                if(!$users->contains($user)){
                    $users->add($user);
                }
            }

        }

        foreach ($enterprise->getAdminUsers() as $user)
        {
            if(!$users->contains($user)){
                $users->add($user);
            }
        }
        if ($include_groups)
        {
            foreach($enterprise->getGroups() as $group) {
                foreach($group->getUsers() as $user){
                    if(!$users->contains($user)){
                        $users->add($user);
                    }
                }
            }
        }
        return $users->toArray();
    }

    /**
     * Sets user workGroups
     * @param User $user
     * @param array $workGroupIds
     * @return User
     */
    public function setWorkGroups(User $user, array $workGroupIds)
    {
        $rep = $this->em->getRepository('UnirCloudBoxBundle:WorkGroup');

        $actual = $user->getMemberWorkGroups()->toArray();

        //New ones...
        $news = [];
        foreach ($workGroupIds as $id) {
            $item = $rep->find($id);
            if ($item) {
                $news[] = $item;
            }
        }

        //Deleted
        foreach ($actual as $item) {
            if (!in_array($item->getId(), $workGroupIds)) {

                $user->removeMemberWorkGroup($item);
                $item->removeUserMember($user);
            }
        }

        //Inserted
        foreach ($news as $item) {
            $user->addMemberWorkGroup($item);
            $item->addUserMember($user);
        }

        return $user;
    }

    /**
     * Sets user wodGroups
     * @param User $user
     * @param array $wodGroupsIds
     * @return User
     */
    public function setWodGroups(User $user, array $wodGroupsIds)
    {
        $rep = $this->em->getRepository('UnirCloudBoxBundle:WodGroup');

        $actual = $user->getMemberWodGroups()->toArray();

        //New ones...
        $news = [];
        foreach ($wodGroupsIds as $id) {
            $item = $rep->find($id);
            if ($item) {
                $news[] = $item;
            }
        }

        //Deleted
        foreach ($actual as $item) {
            if (!in_array($item->getId(), $wodGroupsIds)) {

                $user->removeMemberWodGroup($item);
                $item->removeUserMember($user);
            }
        }

        //Inserted
        foreach ($news as $item) {
            $user->addMemberWodGroup($item);
            $item->addUserMember($user);
        }

        return $user;
    }

    /**
     * Sets user wodGroups where it's follower of
     * @param User $user
     * @param array $wodGroupsIds
     * @return User
     */
    public function setFollowedWodGroups(User $user, array $wodGroupsIds)
    {
        $rep = $this->em->getRepository('UnirCloudBoxBundle:WodGroup');

        $actual = $user->getFollowedWodGroups()->toArray();

        //New ones...
        $news = [];
        foreach ($wodGroupsIds as $id) {
            $item = $rep->find($id);
            if ($item) {
                $news[] = $item;
            }
        }

        //Deleted
        foreach ($actual as $item) {
            if (!in_array($item->getId(), $wodGroupsIds)) {

                $user->removeFollowedWodGroup($item);
                $item->removeUserFollower($user);
            }
        }

        //Inserted
        foreach ($news as $item) {
            $user->addFollowedWodGroup($item);
            $item->addUserFollower($user);
        }

        return $user;
    }

    /**
     * Sets users Groups
     * @param User $user
     * @param array $userGroupsIds
     * @return User
     */
    public function setUserGroups(User $user, array $userGroupsIds)
    {
        $rep = $this->em->getRepository('UnirCloudBoxBundle:Group');

        $actual = $user->getGroups()->toArray();

        //New ones...
        $news = [];
        foreach ($userGroupsIds as $id) {
            $item = $rep->find($id);
            if ($item) {
                $news[] = $item;
            }
        }

        //Deleted
        foreach ($actual as $item) {
            if (!in_array($item->getId(), $userGroupsIds)) {

                $user->removeFromGroup($item);
                $item->removeUser($user);
            }
        }

        //Inserted
        foreach ($news as $item) {
            $user->addToGroup($item);
            $item->addUser($user);
        }

        return $user;
    }
}
