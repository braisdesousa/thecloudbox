<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service\Repository;


use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CommonService;

/**
 * Class WodTiming
 * @package Unir\CloudBoxBundle\Service\Repository
 */
class WodTimingRepositoryService extends BaseRepositoryService
{
    /**
     * @param EntityManager $em
     * @param SecurityContextInterface $security
     * @param CommonService $commonService
     */
    public function __construct(EntityManager $em, SecurityContextInterface $security, CommonService $commonService)
    {
        parent::__construct($em, $security, $commonService);
    }

    public function hasWorkingWods(User $user)
    {
        return $this->em->getRepository("UnirCloudBoxBundle:WodTiming")->findOneByOpenedWodsAndUser($user);
    }
    public function getUserWods(User $user)
    {
        return $this->em->getRepository("UnirCloudBoxBundle:WodTiming")->findBy(["user"=>$user]);
    }
    public function getWorkingWod(User $user, Wod $wod)
    {
        return $this->em->getRepository("UnirCloudBoxBundle:WodTiming")->findOpennedWodByUserAndWod($user,$wod);
    }
}
