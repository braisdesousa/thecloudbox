<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service\Repository;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CanViewOrCanEditService;
use Unir\CloudBoxBundle\Service\CommonService;

/**
 * Class WodGroupRepositoryService
 * @package Unir\CloudBoxBundle\Service\Repository
 */
class WodGroupRepositoryService extends BaseRepositoryService
{
    /**
     * @param EntityManager $em
     * @param SecurityContextInterface $security
     * @param CommonService $commonService
     */
    private $canEditService;
    public function __construct(EntityManager $em, SecurityContextInterface $security, CommonService $commonService,CanViewOrCanEditService $canEditService)
    {
        $this->canEditService=$canEditService;
        parent::__construct($em, $security, $commonService);
    }

    public function getLastGroups($numberOfGroups){

        $groups=$this->findByWorkGroup(null,null,["updated"=>"DESC"]);
        $returnedGroups=[];
        foreach ($groups as $group) {
                if($this->canEditService->canViewWodGroup($group)){
                    $returnedGroups[]=$group;
                }
            }
        return array_slice($returnedGroups, 0, $numberOfGroups);
    }
    /**
     * @param null $status
     * @param null $difficulty
     * @param null $orderBy
     * @param null $count
     * @param bool $include_deleted
     * @return mixed
     */
    public function findByWorkGroup(
        $status = null,
        $difficulty = null,
        $orderBy = null,
        $count = null,
        $include_deleted = false,
        $limit = false
    ) {
        $workGroup = $this->commonService->getSelectedWorkGroup();
        if (!$workGroup){
            return [];
        }

        if ($this->isAdmin($workGroup->getEnterprise()) || $this->isOwner()) {
            return $this->em
                ->getRepository("UnirCloudBoxBundle:WodGroup")
                ->findByWorkGroup(
                        $workGroup,
                        $status,
                        $difficulty,
                        $orderBy,
                        $count,
                        $include_deleted,
                        $limit
                );
        } else {
            $wodGroups=$this->em
                ->getRepository("UnirCloudBoxBundle:WodGroup")
                ->findByWorkGroup(
                    $workGroup,
                    $status,
                    $difficulty,
                    $orderBy,
                    $count,
                    $include_deleted,
                    $limit
                );
            $returnedWodGroups=[];
            foreach ($wodGroups as $wodGroup){
                if($wodGroup->hasUser($this->getUser())){
                    $returnedWodGroups[]=$wodGroup;
                }
            }
            return $returnedWodGroups;
        }
    }

    /**
     * @return array
     */
    public function findAll()
    {
        if ($this->isAdmin($this->commonService->getCompany())) {
            return $this->em->getRepository("UnirCloudBoxBundle:WodGroup")->findAll();
        } else {
            return $this->em->getRepository("UnirCloudBoxBundle:WodGroup")->findByUser($this->getUser());
        }
    }

    /**
     * @param User $user
     * @return array
     */
    public function findByUser(User $user)
    {

        if ($user->hasRole(Role::ROLE_OWNER)) {
            return $this->em->getRepository("UnirCloudBoxBundle:WodGroup")->findAll();
        } elseif ($user->hasRole(ROLE::ROLE_ADMIN)) {
            return $this->em
                ->getRepository("UnirCloudBoxBundle:WodGroup")
                ->findByEnterprise($user->getEnterprise());
        } else {
            //TODO slow query
            return $this->em->getRepository("UnirCloudBoxBundle:WodGroup")->findByUser($user);
        }

    }

    /**
     * Set the wodGroup members
     * @param WodGroup $wodGroup
     * @param array $userIds
     * @return WodGroup
     */
    public function setMembers(WodGroup $wodGroup, array $userIds)
    {
        $repository = $this->em->getRepository('UnirCloudBoxBundle:User');

        //Actually wodGroup members...
        $lastUsers = $wodGroup->getUserMembers()->toArray();

        //New users to include...
        $newUsers = [];

        //Retrieve users from repository
        foreach ($userIds as $userId) {
            $user = $repository->find($userId);
            if ($user) {
                $newUsers[] = $user;
            }
        }

        //First check deleted users
        foreach ($lastUsers as $user) {
            //If user is marked for delete...
            if (!in_array($user->getId(), $userIds)) {
                $wodGroup->removeUserMember($user);
                $user->removeMemberWodGroup($wodGroup);
            }
        }

        //Now add users...
        foreach ($newUsers as $user) {
            $wodGroup->addUserMember($user);
            $user->addMemberWodGroup($wodGroup);
        }


        return $wodGroup;
    }

    /**
     * Sets groups members of a wodGroup
     * @param WodGroup $wodGroup
     * @param array $groupIds
     * @return WodGroup
     */
    public function setGroupMembers(WodGroup $wodGroup, array $groupIds)
    {
        $repository = $this->em->getRepository('UnirCloudBoxBundle:Group');

        //Actual group members...
        $lastGroups = $wodGroup->getGroupMembers()->toArray();

        //New groups, fill em
        $newGroups = [];
        foreach ($groupIds as $id) { //Filling
            $group = $repository->find($id);
            if ($group) {
                $newGroups[] = $group;
            }
        }

        //Remove lost...
        foreach ($lastGroups as $group) {
            if (!in_array($group->getId(), $groupIds)) {
                $wodGroup->removeGroupMember($group);
                $group->removeMemberWodGroup($wodGroup);
            }
        }

        //Add
        foreach ($newGroups as $group) {
            $wodGroup->addGroupMember($group);
            $group->addMemberWodGroup($wodGroup);
        }


        return $wodGroup;
    }

    /**
     * sets the wodGroup followers
     * @param WodGroup $wodGroup
     * @param array $userIds
     * @return WodGroup
     */
    public function setFollowers(WodGroup $wodGroup, array $userIds)
    {
        $repository = $this->em->getRepository('UnirCloudBoxBundle:User');

        //Actually wodGroup members...
        $lastUsers = $wodGroup->getUserFollowers()->toArray();

        //New users to include...
        $newUsers = [];

        //Retrieve users from repository
        foreach ($userIds as $userId) {
            $user = $repository->find($userId);
            if ($user) {
                $newUsers[] = $user;
            }
        }

        //First check deleted users
        foreach ($lastUsers as $user) {
            //If user is marked for delete...
            if (!in_array($user->getId(), $userIds)) {
                $wodGroup->removeUserFollower($user);
                $user->removeFollowedWodGroup($wodGroup);
            }
        }

        //Now add users...
        foreach ($newUsers as $user) {
            $wodGroup->addUserFollower($user);
            $user->addFollowedWodGroup($wodGroup);
        }


        return $wodGroup;
    }

    /**
     * @param WorkGroup $workGroup
     * @return WodGroup
     */
    public function createDefault(WorkGroup $workGroup)
    {
        $wodGroup = new WodGroup();
        $wodGroup->setName("Default");
        $wodGroup->setWorkGroup($workGroup);
        $wodGroup->setDescription(' ');
        $this->em->persist($wodGroup);
        return $wodGroup;
    }

}
