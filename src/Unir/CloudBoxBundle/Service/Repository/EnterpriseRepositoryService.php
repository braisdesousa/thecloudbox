<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Service\CommonService;

/**
 * Class EnterpriseRepositoryService
 * @package Unir\CloudBoxBundle\Service\Repository
 */
class EnterpriseRepositoryService extends BaseRepositoryService
{
    /**
     * @param EntityManager $em
     * @param SecurityContextInterface $security
     * @param CommonService $commonService
     */
    public function __construct(EntityManager $em, SecurityContextInterface $security, CommonService $commonService)
    {
        parent::__construct($em, $security, $commonService);
    }

    public function findAll()
    {
        $user=$this->getUser();
        $enterprises= new ArrayCollection();
        $all_enteprises=$this->em->getRepository("UnirCloudBoxBundle:Enterprise")->findAll();
        foreach ($all_enteprises as $enterprise)
        {
            if($enterprise->getAdminUsers()->contains($user)||$enterprise->getUsers()->contains($user)||$this->isOwner()){
                $enterprises->add($enterprise);
            }
        }
        return $enterprises->toArray();

    }
    /**
     * If there are not enterprises in database, just create new one
     */
    public function checkOrCreate()
    {
        if ($this->em->getRepository("UnirCloudBoxBundle:Enterprise")->findAll(null, true) == 0) {
            return $this->createDefaultEnterprise();
        } else {
            return $this->commonService->getCompany();
        }
    }

    /**
     * Just creates a new enterprise
     */
    private function createDefaultEnterprise()
    {
        $enterprise = new Enterprise();
        $enterprise->setName("Default Enterprise");
        $this->em->persist($enterprise);
        $this->em->flush();
        $this->commonService->setCompany($enterprise);
        return $enterprise;
    }
}
