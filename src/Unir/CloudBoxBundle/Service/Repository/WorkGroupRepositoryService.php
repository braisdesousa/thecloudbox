<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Role;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CanViewOrCanEditService;
use Unir\CloudBoxBundle\Service\CommonService;

/**
 * Class WorkGroupRepositoryService
 * @package Unir\CloudBoxBundle\Service\Repository
 */
class WorkGroupRepositoryService extends BaseRepositoryService
{
    /**
     * @param EntityManager $em
     * @param SecurityContextInterface $security
     * @param CommonService $commonService
     */
    private $canEditService;
    public function __construct(EntityManager $em, SecurityContextInterface $security, CommonService $commonService,CanViewOrCanEditService $canEditService)
    {
        $this->canEditService=$canEditService;
        parent::__construct($em, $security, $commonService);
    }

    public function findAllFromCompany(){
        return $this->em->getRepository("UnirCloudBoxBundle:WorkGroup")->findByEnterprise($this->commonService->getCompany());
    }
    /**
     * @return array
     */
    public function findAll()
    {
        /**
         * @var Enterprise
         */
        $company=$this->commonService->getCompany();

        if ($this->isOwner()) {
             return $this->em->getRepository("UnirCloudBoxBundle:WorkGroup")->findByEnterprise($company);
        } elseif ($company->isAdminUser($this->getUser())) {
             return $this->em
                 ->getRepository("UnirCloudBoxBundle:WorkGroup")
                 ->findByEnterprise($company);
        } else {
               return $this->getUserWorkGroups($this->getUser(),$company);
        }
    }

    private function getUserWorkGroups(User $user , Enterprise $enterprise=null){


        $EnterpriseWods=$this->em->getRepository("UnirCloudBoxBundle:Wod")->findByEnterprise($enterprise);
        $workGroups= new ArrayCollection();
        foreach ($EnterpriseWods as $wod){
            if($this->canEditService->canViewWod($wod)){
                if(!$workGroups->contains($wod->getWodGroup()->getWorkGroup())){
                    $workGroups->add($wod->getWodGroup()->getWorkGroup());
                }
            }
        }
        return $workGroups->toArray();
    }

    /**
     * @param User $user
     * @return array
     */
    public function findByUser(User $user)
    {
        if ($user->hasRole(Role::ROLE_OWNER)) {
            return $this->em->getRepository("UnirCloudBoxBundle:WorkGroup")->findByEnterprise($this->commonService->getCompany());
        } elseif ($user->hasRole(ROLE::ROLE_ADMIN)) {
            return $this->em
                ->getRepository("UnirCloudBoxBundle:WorkGroup")
                ->findByEnterprise($user->getEnterprise());
        } else {
            return $this->em->getRepository("UnirCloudBoxBundle:WorkGroup")->findByUser($user);
        }
    }

    /**
     * Establish workGroup user members
     * @param WorkGroup $workGroup
     * @param array $userIds
     * @return WorkGroup
     */
    public function setUserMembers(WorkGroup $workGroup, array $userIds)
    {
        $repository = $this->em->getRepository('UnirCloudBoxBundle:User');

        //Actually members
        $lastUsers = $workGroup->getUserMembers()->toArray();

        //New ones..
        $newUsers = [];
        foreach ($userIds as $userId) {
            $user = $repository->find($userId);
            if ($user) {
                $newUsers[] = $user;
            }
        }

        //Check deleted ones
        foreach ($lastUsers as $user) {
            if (!in_array($user->getId(), $userIds)) {
                $workGroup->removeUserMember($user);
                $user->removeMemberWorkGroup($workGroup);
            }
        }

        //Now new ones
        foreach ($newUsers as $user) {
            $workGroup->addUserMember($user);
            $user->addMemberWorkGroup($workGroup);
        }

        return $workGroup;
    }

    /**
     * Establish workGroup group members
     * @param WorkGroup $workGroup
     * @param array $groupIds
     * @return WorkGroup
     */
    public function setGroupMembers(WorkGroup $workGroup, array $groupIds)
    {
        $repository = $this->em->getRepository('UnirCloudBoxBundle:Group');

        //Actually members
        $lastGroups = $workGroup->getGroupMembers()->toArray();

        //New ones..
        $newGroups = [];
        foreach ($groupIds as $id) {
            $group = $repository->find($id);
            if ($group) {
                $newGroups[] = $group;
            }
        }

        //Check deleted ones
        foreach ($lastGroups as $group) {
            if (!in_array($group->getId(), $groupIds)) {
                $workGroup->removeGroupMember($group);
                $group->removeMemberWorkGroup($workGroup);
            }
        }

        //Now new ones
        foreach ($newGroups as $group) {
            $workGroup->addGroupMember($group);
            $group->addMemberWorkGroup($workGroup);
        }

        return $workGroup;
    }

    /**
     * @param Enterprise $enterprise
     * @return WorkGroup
     */
    public function createDefault(Enterprise $enterprise)
    {
        $WorkGroup= new WorkGroup();
        $WorkGroup->setEnterprise($enterprise);
        $WorkGroup->setName("Default WorkGroup");
        $WorkGroup->setDescription(' ');
        $this->em->persist($WorkGroup);
        $this->commonService->setSelectedWorkGroup($WorkGroup);
        return $WorkGroup;
    }
}
