<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 8/4/14
 * Time: 9:51 AM
 */

namespace Unir\CloudBoxBundle\Service;


use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\User;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * Class CheckContentService
 * @package Unir\CloudBoxBundle\Service
 */
class CheckContentService
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    private $security;

    /**
     * @var CommonService
     */
    private $commonService;

    /**
     * @var \Symfony\Bundle\TwigBundle\TwigEngine
     */
    private $twig;

    /**
     * @param EntityManager $em
     * @param SecurityContextInterface $securityContext
     * @param CommonService $commonService
     * @param TwigEngine $twig
     */
    public function __construct(
        EntityManager $em,
        SecurityContextInterface $securityContext,
        CommonService $commonService,
        TwigEngine $twig
    ) {
        $this->em = $em;
        $this->security = $securityContext;
        $this->commonService = $commonService;
        $this->twig = $twig;
    }

    /**
     * @return mixed|null
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function hasMinimumContent($element)
    {
        switch($element)
        {
            case "enterprise":
                if ($response = $this->checkEnterprise()) {
                        return $response;
                }
                break;
            case "workGroup":
                if ($response = $this->checkEnterprise()) {
                    return $response;
                }
                if ($response = $this->checkWorkGroup()) {
                    return $response;
                }
                break;
            case "wodGroups":
                if ($response = $this->checkEnterprise()) {
                    return $response;
                }
                if ($response = $this->checkWorkGroup()) {
                    return $response;
                }
                if ($response = $this->checkWodGroup()) {
                    return $response;
                }
                break;
            case "wods":
                if ($response = $this->checkEnterprise()) {
                    return $response;
                }
                if ($response = $this->checkWorkGroup()) {
                    return $response;
                }
                if ($response = $this->checkWodGroup()) {
                    return $response;
                }
                if ($response = $this->checkWods()) {
                    return $response;
                }
                break;
            default:
                throw new HttpException(500, sprintf("Minimum content '%s' does not exist", $element));
        }
        return null;
    }

    /**
     * @return mixed
     */
    private function checkEnterprise()
    {

        if ($this->em->getRepository("UnirCloudBoxBundle:Enterprise")->findAll(false, true) == 0) {
            return $this->twig
                ->render("UnirCloudBoxBundle:Default:empty_dashboard.html.twig", array("step" => 0));
        }

        return null;
    }

    /**
     * @return mixed
     */
    private function checkWorkGroup()
    {
        if ($this->security->isGranted("ROLE_OWNER")) {

            $company = $this->commonService->getCompany();

            if (!$company) {
                throw new \Exception("NO COMPANY Exception");
            }

            $workGroups = $this->em->getRepository("UnirCloudBoxBundle:WorkGroup")->findByEnterprise($company);

            if (count($workGroups) == 0) {
                return $this->twig
                    ->render("UnirCloudBoxBundle:Default:empty_dashboard.html.twig", array("step" => 1));
            }
        } elseif ($this->security->isGranted("ROLE_ADMIN")) {


        } else {

            $workGroups = $this->em
                ->getRepository('UnirCloudBoxBundle:WorkGroup')
                ->findByUser($this->security->getToken()->getUser());

            if (count($workGroups) == 0) {
                return $this->twig
                    ->render("UnirCloudBoxBundle:Default:empty_dashboard.html.twig", array("step" => 1));
            }
        }
        return null;
    }

    /**
     * @return mixed
     */
    private function checkWodGroup()
    {
        if ($workGroup = $this->commonService->getSelectedWorkGroup()) {

            $wodGroups = $this->em->getRepository("UnirCloudBoxBundle:WodGroup")->findByWorkGroup($workGroup);

            if (count($wodGroups) == 0) {
                return $this->twig
                    ->render("UnirCloudBoxBundle:Default:empty_dashboard.html.twig", array("step" => 2));
            }
        }

        return null;
    }

    /**
     * @return mixed
     */
    private function checkWods()
    {
        if ($workGroup = $this->commonService->getSelectedWorkGroup()) {

            $numWorkGroups = $this->em->getRepository("UnirCloudBoxBundle:Wod")
                ->findByWorkGroup($workGroup, null, null, null, true);

            if ($numWorkGroups == 0) {
                return $this->twig
                    ->render("UnirCloudBoxBundle:Default:empty_dashboard.html.twig", array("step" => 3));
            }
        }
        return null;
    }
}
