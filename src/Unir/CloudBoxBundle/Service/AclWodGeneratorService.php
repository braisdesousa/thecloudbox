<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 9/9/14
 * Time: 12:14 PM
 */

namespace Unir\CloudBoxBundle\Service;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;

class AclWodGeneratorService {

    private $aclProvider;
    private $entityManager;

    public function __construct(MutableAclProviderInterface $aclProvider, EntityManagerInterface $entityManager)
    {
        $this->aclProvider=$aclProvider;
        $this->entityManager=$entityManager;
    }


    public function generateAclForAllWods(){
        $wods=$this->entityManager->getRepository("UnirCloudBoxBundle:Wod")->findAll();
        foreach ($wods as $wod){
            $this->generateAclForWod($wod);
        }
    }
    public function generateAclForWorkGroupWods(WorkGroup $workGroup){
        $wods=$this->entityManager->getRepository("UnirCloudBoxBundle:Wod")->findByWorkGroup($workGroup);
        foreach ($wods as $wod)
        {
            $this->generateAclForWod($wod);
        }
    }
    public function generateAclForWodGroupWods(WodGroup $wodGroup){
        $wods=$this->entityManager->getRepository("UnirCloudBoxBundle:Wod")->findByWodGroup($wodGroup);
        foreach ($wods as $wod)
        {
            $this->generateAclForWod($wod);
        }
    }
    public function generateAclForWod(Wod $wod){

        $objectIdentity = ObjectIdentity::fromDomainObject($wod);

        try{
            $this->aclProvider->findAcl($objectIdentity);
            $this->aclProvider->deleteAcl($objectIdentity);
        }
        catch(AclNotFoundException $e){}

        $acl = $this->aclProvider->createAcl($objectIdentity);

        switch($wod->getAvailableViewers()){
            case Wod::VIEW_BY_ASSIGNED :
                $this->insertEdit($acl,$wod->getUsersAssigned());
                break;
            case Wod::VIEW_BY_ASSIGNED_AND_FOLLOWERS:
                $this->insertEdit($acl,$wod->getUsersAssigned());
                $this->insertView($acl,$wod->getUsersFollower());
                break;
            case Wod::VIEW_BY_ALL: $this->generateAclForViewByAllWod($acl,$wod);

        }

        $this->aclProvider->updateAcl($acl);
    }
    public function removeAclForWorkGroupWods(WorkGroup $workGroup)
    {
        $wods=$this->entityManager->getRepository("UnirCloudBoxBundle:Wod")->findByWorkGroup($workGroup);
        foreach ($wods as $wod){
            $this->removeAclForWod($wod);
        }
    }
    public function removeAclForWodGroupWods(WodGroup $wodGroup)
    {
        $wods=$this->entityManager->getRepository("UnirCloudBoxBundle:Wod")->findByWodGroup($wodGroup);
        foreach ($wods as $wod){
            $this->removeAclForWod($wod);
        }
    }
    public function removeAclForCompanyWods(Enterprise $enterprise)
    {
        $wods=$this->entityManager->getRepository("UnirCloudBoxBundle:Wod")->findByEnterprise($enterprise);
        foreach ($wods as $wod){
            $this->removeAclForWod($wod);
        }

    }
    public function removeAclForWod(Wod $wod){
        try{

            $objectIdentity = ObjectIdentity::fromDomainObject($wod);
            try{
                $this->aclProvider->findAcl($objectIdentity);
                $this->aclProvider->deleteAcl($objectIdentity);
            }
            catch(AclNotFoundException $e){}
        }  catch(InvalidDomainObjectException $e){}
    }

    private function generateAclForViewByAllWod($acl,Wod $wod)
    {
        $wodGroup=$wod->getWodGroup();
        $this->insertView($acl,$wodGroup->getUsers());
        $this->insertGroupsView($acl,$wodGroup->getGroupMembers());



    }
    private function insertGroupsView($acl,$userGroups)
    {
        foreach ($userGroups as $userGroup){
            $this->insertView($acl,$userGroup->getUsers());
        }
    }
    private function insertGroupsEdit($acl,$userGroups)
    {
        foreach ($userGroups as $userGroup){
            $this->insertEdit($acl,$userGroup);
        }
    }
    private function insertEdit($acl,$users)
    {
        foreach ($users as $user){
            $securityIdentity = UserSecurityIdentity::fromAccount($user);
            $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_EDIT);
        }
    }
    private function insertView($acl,$users)
    {
        if($users instanceof Collection){
            foreach ($users as $user){
                $securityIdentity = UserSecurityIdentity::fromAccount($user);
                $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_VIEW);
            }
        } else {
            echo get_class($users);
        }
    }
} 