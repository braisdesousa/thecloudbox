<?php
/**
 * Created by PhpStorm.
 * User: alba
 * Date: 16/07/14
 * Time: 16:18
 */

namespace Unir\CloudBoxBundle\Service;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use PhpCollection\CollectionInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Class CommonService
 * @package Unir\CloudBoxBundle\Service
 */
class CommonService
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    /**
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * Retrieves the logged user company.
     * If user is not logged in an exception must be thrown
     * 100% code coverage
     * @return Enterprise
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function getCompany()
    {
        $securityContext = $this->container->get('security.context');
        $token = $securityContext->getToken();
        $loggedUser = $token->getUser();

        //No logged user, exception
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            throw new AccessDeniedException();
        }

            //Try to get selected workGroup
            if (
                $this->container->get("session")&&
                $this->container->get("session")->has("selected_enterprise_id") &&
                $this->container->get("session")->get("selected_enterprise_id")
            ) {
                return $this->em
                    ->getRepository("UnirCloudBoxBundle:Enterprise")
                    ->find($this->container->get("session")->get("selected_enterprise_id"));
            } else {
                $enterprises=$this->em->getRepository("UnirCloudBoxBundle:Enterprise")->findAll();
                if (!empty($enterprises)){
                    return array_shift($enterprises);
                } else {
                    throw new \Exception("UnCaught Exception");
                }
            }
    }

    public function getUser(){
        $securityContext = $this->container->get('security.context');
        $token = $securityContext->getToken();
        return $token->getUser();
    }
    /**
     * Sets the enterprise on session for get it later
     * 100% code coverage
     * @param Enterprise $enterprise
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function setCompany(Enterprise $enterprise)
    {
        $securityContext = $this->container->get('security.context');
        $token = $securityContext->getToken();
        $loggedUser = $token->getUser();

        //If is not logged user, throw an exception
        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            throw new NotFoundHttpException('Unable to find user logged');
        }
            $this->container
                ->get("session")
                ->set("selected_enterprise_id", $enterprise->getId());
    }

    /**
     * 100% code coverage
     * @return mixed|null|object
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function getSelectedWorkGroup()
    {
        $securityContext = $this->container->get('security.context');
        $token = $securityContext->getToken();
        $loggedUser = $token->getUser();

        if (!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            throw new NotFoundHttpException('Unable to find user logged');
        }

        if (
            $this->container->get("session")->has("selected_workGroup_id") &&
            $this->container->get("session")->get("selected_workGroup_id")
        ) {
            return $this->em
               ->getRepository("UnirCloudBoxBundle:WorkGroup")
               ->find($this->container->get("session")->get("selected_workGroup_id"));
        } else {
            $userWorkGroups = $this->getUserWorkGroups();
            return array_shift($userWorkGroups);
        }
    }

    /**
     * sets on session the selected workGroup
     * 100% code coverage
     * @param WorkGroup $workGroup
     */
    public function setSelectedWorkGroup(WorkGroup $workGroup)
    {
        $this->container->get("session")->set("selected_workGroup_id", $workGroup->getId());
        $this->container->get("session")->set("selected_enterprise_id", $workGroup->getEnterprise()->getId());
    }
    public function setSelectedEnterprise(Enterprise $enterprise)
    {
        $this->container->get("session")->set("selected_enterprise_id", $enterprise->getId());
    }

    /**
     * clears on session the selected workGroup
     * 100% Code coverage
     */
    public function unsetSelectedWorkGroup()
    {
        $this->container->get("session")->remove("selected_workGroup_id");
        $this->container->get("session")->remove("selected_enterprise_id");
    }

    /**
     * 100% code coverage
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function getUserWorkGroups()
    {
        $securityContext = $this->container->get('security.context');
        $token = $securityContext->getToken();
        $loggedUser = $token->getUser();

        if (!$this->container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            throw new NotFoundHttpException('Unable to find user logged');
        }
        return $this->em->getRepository("UnirCloudBoxBundle:WorkGroup")->findByEnterprise($this->getCompany());

    }

    /**
     * Checks if an user is allowed to view some wod
     * 0% Code coverage
     * @param User $user
     * @param Wod $wod
     * @return bool
     */
    public function isAllowToView(User $user, Wod $wod)
    {
        //TODO there is a need to change groupRepository and WodRepository. Also is needed to translate
        //this logic to another side

        $securityContext = $this->container->get('security.context');

        //Owners can view all
        if ($securityContext->isGranted("ROLE_OWNER")) {
            return true;
        }
        //If is admin can view it without problems
        if (($wod->getWodGroup()->getWorkGroup()->getEnterprise()->getAdminUsers()->contains($user))) {
            return true;
        }

        //If the user is the creator of the wod, always always can view it
        if ($wod->getCreationUser() == $user) {
            return true;
        }

        //Helper conditions...
        $userIsWodFollower = $wod->getUsersFollower()->contains($user);
        $userIsWodAssigned = $wod->getUsersAssigned()->contains($user);


        $userIsGroupMember = $wod->getWodGroup()->getUserMembers()->contains($user);
        $userIsGroupFollower = $wod->getWodGroup()->getUserFollowers()->contains($user);

        $userIsOnGroup = $userIsGroupFollower || $userIsGroupMember;

        $wodIsVisibleForAll = $wod->getAvailableViewers() == Wod::VIEW_BY_ALL;
        $wodIsVisibleByAssignedAndFollowers = $wod->getAvailableViewers() == Wod::VIEW_BY_ASSIGNED_AND_FOLLOWERS;
        $wodIsVisibleByAssigned = $wod->getAvailableViewers() == Wod::VIEW_BY_ASSIGNED;

        $wodGroupIsVisibleForAll = $wod->getWodGroup()->getAvailableViewers() == WodGroup::VIEW_BY_ALL;
        $wodGroupIsVisibleForFollowers = $wod->getWodGroup()->getAvailableViewers() == WodGroup::VIEW_BY_FOLLOWERS_AND_ASSIGNED;

        //If user is assigned to wod, always can view it
        if ($userIsWodAssigned) {
            return true;
        }

        //If user isn't in workGroup, he cannot view it
        if (!$this->userInWorkGroup($user, $wod->getWodGroup()->getWorkGroup())) {
            return false;
        }

        //if wod is visible by all or by followers and user is follower
        if (($wodIsVisibleByAssignedAndFollowers || $wodIsVisibleForAll) && $userIsWodFollower) {
            return true;
        }

        //If wodGroup is visible for all and wod is visible for all
        if ($wodGroupIsVisibleForAll && $wodIsVisibleForAll) {
            return true;
        }

        //If the wod group is only visible for followers, and wod is visible for all and user is on group
        if ($wodGroupIsVisibleForFollowers && $userIsOnGroup && $wodIsVisibleForAll) {
            return true;
        }

        return false;
    }

    /**
     * Checks if an user can edit a wod
     * 0% Code Coverage
     * @param User $user
     * @param Wod $wod
     * @return bool
     */
    public function isAllowToEdit(User $user, Wod $wod)
    {
        $securityContext = $this->container->get('security.context');

        //ROLE_OWNER always can edit all
        if ($securityContext->isGranted("ROLE_OWNER")) {
            return true;
        }




        //If user is role admin can edit it
        if (($wod->getWodGroup()->getWorkGroup()->getEnterprise()->getAdminUsers()->contains($user))) {
            return true;
        }

        //If the user is the creator of the wod, always always can edit it
        if ($wod->getCreationUser() == $user) {
            return true;
        }
        if ($this->isUserInWodGroup($user,$wod->getWodGroup()))
        {
            return true;
        }

        //If user is Member of wodGroup, Or Users is in Group Member on wodgroup.




        //Helper conditions...
        $userIsWodFollower = $wod->getUsersFollower()->contains($user);
        $userIsWodAssigned = $wod->getUsersAssigned()->contains($user);


        $userIsGroupMember = $wod->getWodGroup()->getUserMembers()->contains($user);
        $userIsGroupFollower = $wod->getWodGroup()->getUserFollowers()->contains($user);

        $userIsOnGroup = $userIsGroupFollower || $userIsGroupMember;

        $wodIsVisibleForAll = $wod->getAvailableViewers() == Wod::VIEW_BY_ALL;
        $wodIsVisibleByAssignedAndFollowers = $wod->getAvailableViewers() == Wod::VIEW_BY_ASSIGNED_AND_FOLLOWERS;
        $wodIsVisibleByAssigned = $wod->getAvailableViewers() == Wod::VIEW_BY_ASSIGNED;

        $wodGroupIsVisibleForAll = $wod->getWodGroup()->getAvailableViewers() == WodGroup::VIEW_BY_ALL;
        $wodGroupIsVisibleForFollowers = $wod->getWodGroup()->getAvailableViewers() == WodGroup::VIEW_BY_FOLLOWERS_AND_ASSIGNED;


        //If user have wod assigned or wod is visible by all and user is on group, then can edit it
        if ($userIsWodAssigned || ($wodIsVisibleForAll && $userIsOnGroup)) {
            return true;
        }

        return false;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PRIVATE
     * -----------------------------------------------------------------------------------------------------------------
     */


    /**
     * @param User $user
     * @param WodGroup $wodGroup
     * @return bool
     */
    private function isUserInWodGroup(User $user, WodGroup $wodGroup)
    {
        if ($wodGroup->getUserMembers()->contains($user) || $this->isUserInCollection($user,$wodGroup->getGroupMembers())){
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @param CollectionInterface $collection
     * @return bool
     */
    private function isUserInCollection(User $user, Collection $collection)
    {
        return $collection->contains($user);
    }
    /**
     * 0% Code coverage
     * @param User $user
     * @param WorkGroup $workGroup
     * @return bool
     */
    private function userInWorkGroup(User $user, WorkGroup $workGroup)
    {
        //If user is in prject user Members then is in workGroup
        if ($workGroup->getUserMembers()->contains($user)) {
            return true;
        }

        //Foreach group
        foreach ($workGroup->getGroupMembers() as $member_group) {
            //If group contains user, then is on workGroup
            if ($member_group->getUsers()->contains($user)) {
                return true;
            }
        }

        //If arrives here user is not in workGroup
        return false;
    }
}
