<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 9/15/14
 * Time: 10:24 AM
 */

namespace Unir\CloudBoxBundle\Service;



use FOS\RestBundle\View\View;

class RestErrorViews
{

    public function formErrorView($httpCode,$form)
    {
        return View::create()->setStatusCode($httpCode)->setData($form)->setFormat("json");
    }
    public function errorView($httpCode,$errorCode,$message,$input=null)
    {
            return View::create()->setStatusCode($httpCode)->setData(["code"=>$errorCode,"message"=>$message,"input"=>$input]);
    }
    public function validationErrorView($errors)
    {
            return View::create()->setStatusCode(421)->setData($errors);
    }

} 