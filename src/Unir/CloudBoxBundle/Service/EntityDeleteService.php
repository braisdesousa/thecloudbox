<?php
/**
 * Created by PhpStorm.
 * User: vicente
 * Date: 11/09/14
 * Time: 10:14
 */

namespace Unir\CloudBoxBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\Group;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodActivity;
use Unir\CloudBoxBundle\Entity\WodComments;


/**
 * Service that must contain domain logic for delete each of the entities that we have on workGroup
 * Class EntityDeleteService
 * @package Unir\CloudBoxBundle\Service
 */
class EntityDeleteService {

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * FIELDS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var AclWodGeneratorService
     */
    protected $acl;


    /*
     * -----------------------------------------------------------------------------------------------------------------
     * CONSTRUCTOR
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Constructor
     *
     * @param EntityManagerInterface $em
     * @param AclWodGeneratorService $aclWodGeneratorService
     */
    public function __construct(EntityManagerInterface $em, AclWodGeneratorService $aclWodGeneratorService)
    {
        $this->em = $em;
        $this->acl = $aclWodGeneratorService;
    }

    /*
     * -----------------------------------------------------------------------------------------------------------------
     * PUBLIC METHODS
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * Removes an enterprise from the system
     * @param Enterprise $enterprise
     * @param bool $makeFlush indicates if must make flush or not
     */
    public function deleteEnterprise(Enterprise $enterprise, $makeFlush = true)
    {
        //Delete workGroups
        foreach ($enterprise->getWorkGroups() as $workGroup) {
            $this->deleteWorkGroup($workGroup, false);
        }

        //Delete user groups
        foreach ($enterprise->getGroups() as $group) {
            $this->deleteGroup($group, false);
        }

        //Delete users
        foreach ($enterprise->getUsers() as $user) {
            $this->deleteUser($user, false);
        }


        $isEmpty = $enterprise->getWorkGroups()->count() == 0 &&
            $enterprise->getGroups()->count() == 0 &&
            $enterprise->getUsers()->count() == 0;

        //Now enterprise is empty? remove it
        if ($isEmpty) {
            $this->em->remove($enterprise);
        } else { //If not is empty, mark as deleted
            $enterprise->setDeleted();
            $this->em->persist($enterprise);
        }


        if ($makeFlush) {
            $this->em->flush();

            $this->acl->removeAclForCompanyWods($enterprise);
        }
    }

    /**
     * Removes a workGroup from the system
     * @param WorkGroup $workGroup
     * @param bool $makeFlush indicates if must make flush or not
     */
    public function deleteWorkGroup(WorkGroup $workGroup, $makeFlush = true)
    {
        //Delete WodGroups
        foreach ($workGroup->getWodGroups() as $wodGroup) {
            $this->deleteWodGroup($wodGroup, false);
        }

        $isEmpty = $workGroup->getWodGroups()->count() == 0;

        //Now, workGroup is empty? remove it
        if ($isEmpty) {
            $this->em->remove($workGroup);
        } else {
            $workGroup->setDeleted();
            $this->em->persist($workGroup);
        }


        if ($makeFlush) {
            $this->em->flush();

            $this->acl->removeAclForWorkGroupWods($workGroup);
        }
    }

    /**
     * Removes a WodGroup from the system
     * @param WodGroup $wodGroup
     * @param bool $makeFlush indicates if must make flush or not
     */
    public function deleteWodGroup(WodGroup $wodGroup, $makeFlush = true)
    {
        //Deletes wods
        foreach ($wodGroup->getWods() as $wod) {
            $this->deleteWod($wod, false);
        }


        $isEmpty = $wodGroup->getWods()->count() == 0;

        //Now, WodGroup is empty? remove it
        if ($isEmpty) {
            $this->em->remove($wodGroup);
        } else {
            $wodGroup->setDeleted();
            $this->em->persist($wodGroup);
        }

        if ($makeFlush) {
            $this->em->flush();

            $this->acl->removeAclForWodGroupWods($wodGroup);
        }

    }

    /**
     * Removes a UserGroup from the system
     * @param Group $group
     * @param bool $makeFlush indicates if must make flush or not
     */
    public function deleteGroup(Group $group, $makeFlush = true)
    {

        //Delete users
        foreach ($group->getUsers() as $user) {
            $this->deleteUser($user, false);
        }

        $isEmpty = ($group->getUsers()->count() == 0) && (!$this->groupIsInCompanies($group) &&!($this->groupIsInWodGroups($group)));

        //Now if group is empty, remove it
        if ($isEmpty) {
            $this->em->remove($group);
        } else {
            $group->setDeleted();
            $this->em->persist($group);
        }


        if ($makeFlush) {
            $this->em->flush();
        }

    }

    /**
     * Removes an User from the system
     * @param User $user
     * @param bool $makeFlush indicates if must make flush or not
     */
    public function deleteUser(User $user, $makeFlush = true)
    {
        //RIGHT NOW ONLY ERASES USER FROM COMPANY
        $enterprises=$this->em->getRepository("UnirCloudBoxBundle:Enterprise")->findAll(true);
        foreach($enterprises as $enterprise){
            $enterprise->removeUser($user);
            $this->em->persist($enterprise);
        }
        $this->em->flush();
        return [];

        //Check if user is empty
        $isEmpty = $user->getWodComments()->count() == 0 &&
            $user->getCreatedWods()->count() == 0 &&
            $user->getWodActivities()->count() &&
            $user->getAssignedWods()->count()==0 &&
            $user->getFollowedWods()->count()==0 &&
            !$this->UserIsInGroups($user)&&
            !$this->UserIsInCompanies($user)&&
            !$this->UserIsInWodGroups($user)
            ;


        if ($isEmpty) {
            $this->em->remove($user);
        } else {
            $user->setEnabled(false);
            $this->em->persist($user);
            foreach ($user->getAssignedWods() as $wod) {
                $wod->removeUsersAssigned($user);
                $this->em->persist($wod);
            }
            $user->setAssignedWods(new ArrayCollection());

            foreach ($user->getFollowedWods() as $wod) {
                $wod->removeUsersFollower($user);
                $this->em->persist($wod);
            }
            $user->setFollowedWods(new ArrayCollection());
        }


        if ($makeFlush) {
            $this->em->flush();
        }
    }

    /**
     * Removes a Wod from the systemchmod
     * @param Wod $wod
     * @param bool $makeFlush indicates if must make flush or not
     */
    public function deleteWod(Wod $wod, $makeFlush = true)
    {
        //Delete wodComments
        foreach ($wod->getWodComments() as $wodComment) {
            $this->deleteWodComment($wodComment, false);
        }

        //Delete wodActivity
        foreach ($wod->getWodActivities() as $wodActivity) {
            $this->deleteWodActivity($wodActivity, false);
        }

        foreach($wod->getSubWods() as $subWod) {
            $subWod->setDeleted();
            $this->em->persist($subWod);
        }

        $Wodtimings=$this->em->getRepository("UnirCloudBoxBundle:WodTiming")->findByWod($wod);
        foreach ($Wodtimings as $wodTimming) {
            $wodTimming->forceClose();
            $this->em->persist($wodTimming);
        }

        $isEmpty = $wod->getWodComments()->count() == 0 &&
            $wod->getWodActivities()->count() == 0 &&
            (count($Wodtimings)==0) &&
            $wod->getSubWods()->count()==0;

        ;

        //Now if wod is empty, remove it
        if ($isEmpty) {
            $this->em->remove($wod);
        } else {
            $wod->setDeleted();
            $this->em->persist($wod);
        }


        if ($makeFlush) {
            $this->em->flush();
            $this->acl->removeAclForWod($wod);
        }

    }

    /**
     * Removes a WodComment from the system
     * @param WodComments $wodComment
     * @param bool $makeFlush indicates if must make flush or not
     */
    public function deleteWodComment(WodComments $wodComment, $makeFlush = true)
    {
        $wodComment->setDeleted();
        $this->em->persist($wodComment);

        if ($makeFlush) {
            $this->em->flush();
        }
    }

    /**
     * Removes a WodActivity from the system
     * @param WodActivity $wodActivity
     * @param bool $makeFlush indicates if must make flush or not
     */
    public function deleteWodActivity(WodActivity $wodActivity, $makeFlush = true)
    {
        $wodActivity->setDeleted();
        $this->em->persist($wodActivity);

        if ($makeFlush) {
            $this->em->flush();
        }
    }
    private function UserIsInGroups(User $user){

        foreach($this->em->getRepository("UnirCloudBoxBundle:Group")->findAll() as $group){
            if ($group->getUsers()->contains($user)){
                return true;
            }
        }
        return false;
    }
    private function UserIsInWodGroups(User $user){

        foreach($this->em->getRepository("UnirCloudBoxBundle:WodGroup")->findAll() as $wodGroup){
            if($wodGroup->getUsers()->contains($user)){
                return true;

            }
        }
        return false;
    }
    private function UserIsInCompanies(User $user){

        foreach($this->em->getRepository("UnirCloudBoxBundle:Enterprise")->findAll() as $enterprise){
            if($enterprise->getUsers()->contains($user)){
                return true;

            }
        }
        return false;
    }
    private function GroupIsInCompanies(Group $group){

        foreach($this->em->getRepository("UnirCloudBoxBundle:Enterprise")->findAll() as $enterprise){
            if($enterprise->getGroups()->contains($group)){
                return true;

            }
        }
        return false;

    }private function GroupIsInWodGroups(Group $group){

        foreach($this->em->getRepository("UnirCloudBoxBundle:WodGroup")->findAll() as $wodGroup){
            if($wodGroup->getGroupMembers()->contains($group)){
                return true;

            }
        }
        return false;
    }
} 