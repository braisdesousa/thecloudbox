<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/15/14
 * Time: 5:25 PM
 */

namespace Unir\CloudBoxBundle\Twig;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Session\Session;

class FormExtension extends \Twig_Extension
{


    private $formFactory;
    private $container;

    function __construct(FormFactory $formFactory , ContainerInterface $container)
    {
        $this->formFactory=$formFactory;
        $this->container=$container;
    }
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('wodTiming_form', array($this, 'getWodTimingForm')),
            new \Twig_SimpleFunction('wod_form', array($this, 'getWodForm')),
            new \Twig_SimpleFunction('wod_short_form', array($this, 'getShortWodForm')),
            new \Twig_SimpleFunction('wod_assigned_form', array($this, 'getAssignedWodForm')),
            new \Twig_SimpleFunction('wod_follower_form', array($this, 'getFollowerWodForm')),
            new \Twig_SimpleFunction('wodgroup_form', array($this, 'getWodGroupForm')),
            new \Twig_SimpleFunction('workGroup_form', array($this, 'getWorkGroupForm')),
            new \Twig_SimpleFunction('enterprise_form', array($this, 'getEnterpriseForm')),
            new \Twig_SimpleFunction('user_form', array($this, 'getUserForm')),
            new \Twig_SimpleFunction('user_edit_form', array($this, 'getUserEditForm')),
            new \Twig_SimpleFunction('user_update_password_form', array($this, 'getUserUpdatePasswordForm')),
            new \Twig_SimpleFunction('user_group_form', array($this, 'getUserGroupForm')),
            new \Twig_SimpleFunction('name_of_full_name', array($this, 'getFormNameOfFullName')),
            new \Twig_SimpleFunction('name_to_ng_model', array($this, 'getNameToNgModel')),
        );
    }

    public function getUserUpdatePasswordForm(){
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/user:modalChangePassword.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanaget_update_password')->createView()]
        );
    }
    public function getWodTimingForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/wod:modalTimingClose.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_wodtiming')->createView()]
        );
    }
    public function getWodForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/wod:modalCreateWod.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_wod')->createView()]
        );
    }
    public function getShortWodForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/wod:modalEditWod.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_wod_short')->createView()]
        );
    }
    public function getAssignedWodForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/wod:modalUsersAssigned.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_wod_assigned')->createView()]
        );
    }
    public function getFollowerWodForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/wod:modalUsersFollower.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_wod_follower')->createView()]
        );
    }
    public function getWodGroupForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/wodGroup:modalCreate.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_wodgroup')->createView()]
        );
    }
    public function getWorkGroupForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/workGroup:modalCreate.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_workGroup')->createView()]
        );
    }
    public function getEnterpriseForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/enterprise:modalCreate.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_enterprise')->createView()]
        );
    }
    public function getUserForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/user:modalCreate.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_user')->createView()]
        );
    }
    public function getUserGroupForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/user:modalCreateGroupUser.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_group')->createView()]
        );
    }
    public function getUserEditForm()
    {
        return $this->container->get("templating")->render(
            "UnirCloudBoxBundle:Ng/Template/user:modalEdit.html.twig",
            ["form"=>$this->formFactory->create('unir_ticketmanagerbundle_user_mail')->createView()]
        );
    }

    // form render
    public function getFormNameOfFullName($fullName)
    {
        return explode('[',$fullName)[0];
    }

    public function getNameToNgModel($fullName)
    {
        $fullName = str_replace( "[]", "", $fullName);

        $fullName = str_replace( "[", ".", $fullName);

        $fullName = str_replace( "]", "", $fullName);
        return $fullName;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return "ticketmanager_form_extension";
    }

} 