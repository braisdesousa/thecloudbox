<?php
/**
 * Created by PhpStorm.
 * User: bra
 * Date: 7/15/14
 * Time: 5:25 PM
 */

namespace Unir\CloudBoxBundle\Twig;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use Unir\CloudBoxBundle\Entity\Enterprise;
use Unir\CloudBoxBundle\Entity\WorkGroup;
use Unir\CloudBoxBundle\Entity\Wod;
use Unir\CloudBoxBundle\Entity\WodGroup;
use Unir\CloudBoxBundle\Entity\User;
use Unir\CloudBoxBundle\Service\CommonService;
use Unir\CloudBoxBundle\Service\Repository\EnterpriseRepositoryService;
use Unir\CloudBoxBundle\Service\Repository\WorkGroupRepositoryService;
use Unir\CloudBoxBundle\Service\Repository\WodGroupRepositoryService;
use Unir\CloudBoxBundle\Service\Repository\WodRepositoryService;
use Unir\CloudBoxBundle\Service\RepositoryService;


class WodExtension extends \Twig_Extension {

    private $repositoryService;
    private $security;
    private $commonService;
    private $em;
    private $wodGroupRepositoryService;
    private $wodRepositoryService;
    private $workGroupRepositoryService;
    private $enterpriseRepositoryService;

    function __construct(
        RepositoryService $repositoryService,
        SecurityContext $security,
        CommonService $commonService,
        EntityManager $em,
        WodGroupRepositoryService $wodGroupRepositoryService,
        WodRepositoryService $wodRepositoryService,
        WorkGroupRepositoryService $workGroupRepositoryService,
        EnterpriseRepositoryService $enterpriseRepositoryService
    )
    {
        $this->repositoryService=$repositoryService;
        $this->security=$security;
        $this->commonService=$commonService;
        $this->em=$em;
        $this->wodGroupRepositoryService=$wodRepositoryService;
        $this->wodRepositoryService=$wodRepositoryService;
        $this->workGroupRepositoryService=$workGroupRepositoryService;
        $this->enterpriseRepositoryService=$enterpriseRepositoryService;
    }
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('get_user_color',array($this,"getUserColor")),
            new \Twig_SimpleFunction('get_wod_status_icon',array($this,"getWodStatusIcon")),
            new \Twig_SimpleFunction('get_wod_difficulty_class',array($this,"getWodDifficultyClass")),
            new \Twig_SimpleFunction('get_role_class',array($this,"getRoleClass")),
            new \Twig_SimpleFunction('get_wod_statuses',array($this,"getWodStatuses")),
            new \Twig_SimpleFunction('get_wod_difficulties',array($this,"getWodDifficulties")),
            new \Twig_SimpleFunction('get_selected_workGroup',array($this,"getSelectedWorkGroup")),
            new \Twig_SimpleFunction('get_user_workGroups',array($this,"getUserWorkGroups")),
            new \Twig_SimpleFunction('get_company',array($this,"getCompany")),
            new \Twig_SimpleFunction('count_by_group_and_user',array($this,"countByGroupAndUser")),
            new \Twig_SimpleFunction('get_companies',array($this,"getEnterprises")),
            new \Twig_SimpleFunction('get_enterprise_workGroups',array($this,"getEnterpriseWorkGroup")),
        );
    }

//    public function getWodGroupCount(WorkGroup $workGroup=null)
//    {
//        return $this->wodGroupRepositoryService->findByWorkGroup($workGroup,null,null,null,true);
//    }

    public function getEnterpriseWorkGroup(Enterprise $enterprise)
    {
        return $this->em->getRepository("UnirCloudBoxBundle:WorkGroup")->findByEnterprise($enterprise);
    }
    public function getEnterprises()
    {
        return $this->enterpriseRepositoryService->findAll();
    }
    public function getCompany(){
        return $this->commonService->getCompany();
    }
    public function getUserWorkGroups(User $user){

        return $this->workGroupRepositoryService->findByUser($user);
    }
    public function getSelectedWorkGroup(){
        return $this->commonService->getSelectedWorkGroup();
    }
    public function getWodStatuses()
    {
        return Wod::getStatuses();
    }
    public function getWodDifficulties()
    {
        return Wod::getDifficulties();
    }
    public function getRoleClass(User $user)
    {
        if ($user->hasRole("ROLE_OWNER")){
            return "alert-danger";
        } elseif ($user->hasRole("ROLE_ADMIN")){
            return "alert-warning";
        }else{
            return "";
        }
    }
    public function getWodStatusIcon($status)
    {

         switch($status){
                case 'closed' :     $icon="fa fa-lock";break;
                case 'resolved' :   $icon="fa fa-check";break;
                case 'in progress': $icon="fa fa-ellipsis-h";break;
                case 'open' :       $icon="fa fa-unlock";break;
                default:            $icon="";
            }

        return $icon;
    }
    public function getWodDifficultyClass($difficulty){
        switch($difficulty){
            case 'low' : $class="success";break;
            case 'minor' : $class="info";break;
            case 'high' : $class="warning";break;
            case 'critical' : $class="danger";break;
            default: $class="default";
        }
        return $class;
    }
    public function getUserColor($userName)
    {
        $colors=["avatar-darkblue",  "avatar-yellowgreen",  "avatar-brown",     "avatar-darkred",   "avatar-green",
                "avatar-orangered", "avatar-dodgerblue",   "avatar-seagreen",   "avatar-chocolate", "avatar-indigo",
                "avatar-steelblue", "avatar-saddlebrown",  "avatar-darkkhaki",  "avatar-teal",      "avatar-olive",
                "avatar-cadetblue", "avatar-darkslategray","avatar-lightgreen", "avatar-plum",      "avatar-tan",
                "avatar-limegreen", "avatar-lightsteelblue"] ;
        $numeric_string=0;
        foreach (preg_split('//',$userName,-1,PREG_SPLIT_NO_EMPTY) as $letter)
        {
            $numeric_string+=ord($letter);
        }
        $pseudo_random=strlen(count_chars($userName,3));

        return $colors[($numeric_string*$pseudo_random)%count($colors)];

    }
    public function countByGroupAndUser(WodGroup $group)
    {
        return (integer)$this->wodRepositoryService->findByGroup($group,null,null,null,true);
    }
    public function getName()
    {
        return 'wod_extension';
    }
} 