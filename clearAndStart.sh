#!/bin/bash
sh -c 'php bin/console assets:install --symlink --relative'
sh -c 'php bin/console cache:clear --no-warmup --verbose'
sh -c 'php bin/console fos:js-routing:dump'
sh -c 'php bin/console bazinga:js-translation:dump'
sh -c 'php bin/console assetic:dump'
sh -c 'chmod -R 777 var/cache/'
sh -c 'php bin/console assetic:watch'